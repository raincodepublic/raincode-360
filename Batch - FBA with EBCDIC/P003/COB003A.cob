       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB003A.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        
       FILE-CONTROL.
       SELECT DD1 ASSIGN TO DD1
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE IS SEQUENTIAL.
       DATA DIVISION.
        
       FILE SECTION.
       FD DD1 RECORD CONTAINS 132 CHARACTERS.
       01 R-DD1.
           05 W-DD1 PIC X(132).
            
       WORKING-STORAGE SECTION.
       01 A PIC X(30). 
       COPY SQLCA.
       COPY COB003A.
       EXEC SQL
   	       DECLARE CUR01 CURSOR FOR
      	   SELECT 
               C.CUSTOMERID, 
               C.FIRSTNAME, 
               C.LASTNAME, 
               C.ZIPCODE, 
               Z.CITY,
               1
           FROM 
               DBO.CUSTOMER C,
               DBO.ZIPCODE Z
           WHERE Z.ZIPCODE = C.ZIPCODE
   	   END-EXEC.
         
       LINKAGE SECTION.
       
       PROCEDURE DIVISION.
       EXEC SQL
   	       OPEN CUR01
   	   END-EXEC
   	   PERFORM FETCH-CUR01
       OPEN OUTPUT DD1
       PERFORM UNTIL SQLCODE > 0
           MOVE W-REC(5:100) TO R-DD1
           WRITE R-DD1 AFTER 2
   	       PERFORM FETCH-CUR01
       END-PERFORM
       CLOSE DD1
       EXEC SQL
   	       CLOSE CUR01
   	   END-EXEC
       EXEC SQL
           SELECT '*** COB003A FINISHED ****' 
           FROM SYSIBM.SYSDUMMY1 INTO :A
       END-EXEC
       DISPLAY A
       STOP RUN
       .
        
       FETCH-CUR01.
   	   EXEC SQL
   	      FETCH CUR01 INTO
              :W-CUSTOMERID,
              :W-FIRSTNAME,
              :W-LASTNAME,
              :W-ZIPCODE,
              :W-CITY,
              :W-1
   	   END-EXEC
       .