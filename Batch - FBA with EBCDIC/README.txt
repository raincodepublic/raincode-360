*---------------------------------------------------------------------------------------*
*                                                                                       *
*   Batch Demo : FBA with EBCDIC                                                        *
*                                                                                       *
*---------------------------------------------------------------------------------------*

PURPOSE
  . Produce a dataset of type FBA that is encoded in EBCDIC

CONTEXT
  . FBA is a print format, normally 133 bytes in length with 132 printable characters where the first character is a carriage control,
    which determines the action of the print head after the line is printed.
    The valid carriage control characters are :
    "1": new page
    "0": two new lines
    "+": no new line
    " ": new line
    In this example the "0" character is produced with the WRITE AFTER 2 statement in COBOL.

HOW TO RUN
  . Start the Raincode Debugger

EXPECTED OUTPUT
  . Navigate to the folder C:\ProgramData\Raincode\Batch\DefaultVolume\FBA and double-click on OUTPUT.meta.
  . This will open the dataset FBA.OUTPUT in the record editor.
  . Click on the 'i' icon to check the code page, it should be "37" (EBCDIC).
  . The content of the file should be as follows :

      0Michal                                  Ward                                    1000BRUSSEL                                         
      0Pete                                    York                                    1000BRUSSEL                                         
      0Tracey                                  George                                  1000BRUSSEL       
      ...

ABOUT THE DEMO
  . The demo is complicated somewhat by the fact that both the data and the carriage control character need to be encoded in EBCDIC.
  . The following parameter in the COBOL build parameters of the project will ensure that the output of the program is encoded in EBCDIC:

      :StringRuntimeEncoding=IBM037
  
  . In this particular case however, we must also ensure that the "0" at the beginning of each line is also encoded in EBCDIC. 
  . To accomplish this, we must also set the defaultCodePage in the catalog to EBCDIC, which is not the case in the default configuration file that is shared by most of the demos.
  . For this reason we provide a private catalog configuration file in the solution folder with the following line:
  
      <catalogConfiguration dbConnectionDataProvider="RcDbConnections.csv" defaultCodePage="IBM037" 
      ...
  
  . Because our program uses a database connection, we must also provide a private copy of the connection file RcDbConnections.csv that can be pointed to by the catalog configuration.
  . The submit utility will look for these file in the bin folder, which is the current folder from where it is executed. To make sure that it finds these files, we will copy them from the solution folder to the bin folder with a post-buid event:

      copy /Y "$(SolutionDir)\Raincode.Catalog.xml" . && copy /Y "$(SolutionDir)\RcDbConnections.csv"  .

  . The "&&" part is needed so that both files can be copied with a single command.
  . Finally, we need to set the "Catalog Configuration" field of the common JCL properties of our project as follows so that it points to the private catalog configuration file in the bin folder:

      Raincode.Catalog.xml

ALTERNATIVE APPROACHES
  . We can also set the StringRuntimeEncoding property at runtime rather that at compile time. This would involve copying the rclrun.args file in the solution folder to the bin folder as part of the post-build event. The rclrun.args file contains the following argument:

      -StringRuntimeEncoding=IBM037

  . We could set the catalog configuration field to "..\..\..\..\Raincode.Catalog.xml" so that it points back to the solution folder. This would eliminate the need to copy this file as part of the post-build event. We would still need to copy the connection file, however.

 
