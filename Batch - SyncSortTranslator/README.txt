********************************************************************
*                                                                  *
*   DEMO : SYNCSORT TRANSLATOR                                     *
*                                                                  *
********************************************************************

ABSTRACT
  . The Raincode SyncSort Translator converts DFSORT and ICETOOL syntax into SyncSort syntax.

CONCEPTS AND TERMS
  . Syncsort is a third-party sort program that needs to be purchased and installed seperately.
  . DFSORT is the name of the sort program that is provided by IBM on z/OS.
  . ICETOOL is a reporting tool that relies on DFSORT to be executed. It has a syntax that is different from that of DFSORT.

ABOUT THE DEMO
  . Syncsort itself is not installed on this system.
  . Once installed, the Sort Translator allows the programs XSORT, SORTDTL, ICETOOL and ICEDTL to be executed from any JCL.
  . Syncsort is not installed on this system. All that is produced by the demo is the translated syntax in a set of intermediate files that would normally be cleaned up after successful execution. No actual sorting takes place.

RUNNING THE DEMO
  . Start the Raincode Debugger from the toolbar to run the JCL in JCL001.jcl.
  . Look at the job output in C:\ProgramData\Raincode\Batch\SYSOUT.
  . The SYSOUT member of each step (except the first one) contains a line simular to this one:

      Starting SyncsortTranslator : -FromJCL=true -WorkDir=workspace_directory -OutputDir=C:\Users\martin\AppData\Local\Temp\RC_SSXLate-0000000099-STEP001

  . The output directory is the place where the intermediate files with the translated syntax are kept.
  . Use the job number to locate the output directories and look at the converted syntax.

NOTES
  . The sort in step 1 not translated and produces actual output via Raincode Sort. The output is discarded when the step finishes.
  . Syncsort comes in a DTL (Data Transformation Language) and a non-DTL variety so that there are two ways to process the same input. This is illustrated by steps 2 and 3 that use the same input for XSORT and SORTDTL, and again for steps 5 and 6 that illustrate ICETOOL and ICEDTL.

