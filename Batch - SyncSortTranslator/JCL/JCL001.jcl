//*********************************************************************
//*   
//*   SyncSortTranslator Examples
//* 
//*********************************************************************
//JCL001 JOB COND=(4,LT)
//*--------------------------------------------------------------------
//*   DFSORT Example
//*--------------------------------------------------------------------
//STEP001  EXEC PGM=SORT                                  
//SYSOUT   DD SYSOUT=*
//SORTIN   DD *
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa0000000001
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa0000000001
cccccccccccccccccccccccccccccccccccccccc0000000001
bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb0000000001
/*
//SORTOUT  DD DSN=FOOUT
//SYSIN    DD *
  SORT FIELDS=(1,1,CH,A)
  SUM FIELDS=(41,10,ZD)
  OUTREC FIELDS=(1,10,X,11,10)
/*
//*--------------------------------------------------------------------
//*   XSORT Example
//*--------------------------------------------------------------------
//STEP002  EXEC PGM=XSORT
//SYSOUT   DD SYSOUT=*
//SYSIN    DD *
   SORT FIELDS=(1,10,CH,A)
   OUTFIL FNAMES=OUTFIL1,
     OUTREC(THIRTYTEN,TWENTYTEN)
/*
//SYMNAMES DD *
  TWENTYTEN,20,10,PD
  THIRTYTEN,*,=,CH
/*
//SORTIN   DD DSNAME=FOOIN,DCB=(RECFM=FB,LRECL=42)
//SORTOUT  DD DSNAME=FOOUT,DCB=(RECFM=FB,LRECL=42)
//OUTFIL1  DD DSNAME=FOFIL,DCB=(RECFM=FB,LRECL=20)
/*
//*--------------------------------------------------------------------
//*   SORTDTL Example 1
//*--------------------------------------------------------------------
//STEP003  EXEC PGM=SORTDTL
//SYSOUT   DD SYSOUT=*
//SYSIN    DD *
   SORT FIELDS=(1,10,CH,A)
   OUTFIL FNAMES=OUTFIL1,
     OUTREC(THIRTYTEN,TWENTYTEN)
/*
//SYMNAMES DD *
  TWENTYTEN,20,10,PD
  THIRTYTEN,*,=,CH
/*
//SORTIN   DD DSNAME=FOOIN,DCB=(RECFM=FB,LRECL=42)
//SORTOUT  DD DSNAME=FOOUT,DCB=(RECFM=FB,LRECL=42)
//OUTFIL1  DD DSNAME=FOFIL,DCB=(RECFM=FB,LRECL=20)
//*--------------------------------------------------------------------
//*   SORTDTL Example 2
//*--------------------------------------------------------------------
//STEP004  EXEC PGM=SORTDTL
//SYSOUT   DD SYSOUT=*
//SYSIN    DD *
  JOINKEYS F1=INPUT1,FIELDS=(1,100,A,150,100,A)
  JOINKEYS F2=INPUT2,FIELDS=(1,100,A,101,100,A)
  OPTION COPY
/*
//JNF1CNTL DD *
  OUTREC BUILD=(1,308,C'T1')
*/
//JNF2CNTL DD *
   OUTREC BUILD=(1,308,C'T2')
*/
//INPUT1   DD DSNAME=JOININ1,DCB=(RECFM=FB,LRECL=400)
//INPUT2   DD DSNAME=JOININ2,DCB=(RECFM=FB,LRECL=400)
//SORTOUT  DD DSNAME=FOOUT,DCB=(RECFM=FB,LRECL=420)
/*
//*--------------------------------------------------------------------
//*   ICETOOL Example
//*--------------------------------------------------------------------
//STEP005  EXEC PGM=ICETOOL
//SYSOUT   DD SYSOUT=*
//IN       DD *
DUMMY DATA
//COP      DD SYSOUT=*
//OUT      DD SYSOUT=*
//FMA1OUT  DD SYSOUT=*            
//TOOLMSG  DD SYSOUT=*            
//DFSMSG   DD SYSOUT=*            
//TOOLIN   DD *                   
	SORT FROM(IN) TO(COP) USING(FMA1)
        SPLICE FROM(IN) TO(OUT) ON(1,2,CH) WITH(3,4) WITHALL
/*
//FMA1CNTL DD *
    OPTION COPY
/*
//*--------------------------------------------------------------------
//*   ICEDTL Example
//*--------------------------------------------------------------------
//STEP006  EXEC PGM=ICEDTL
//SYSOUT   DD SYSOUT=*
//IN       DD *
DUMMY DATA
//COP      DD SYSOUT=*
//OUT      DD SYSOUT=*
//FMA1OUT  DD SYSOUT=*            
//TOOLMSG  DD SYSOUT=*            
//DFSMSG   DD SYSOUT=*            
//TOOLIN   DD *                   
	SORT FROM(IN) TO(COP) USING(FMA1)
        SPLICE FROM(IN) TO(OUT) ON(1,2,CH) WITH(3,4) WITHALL
/*
//FMA1CNTL DD *
    OPTION COPY
/*
//