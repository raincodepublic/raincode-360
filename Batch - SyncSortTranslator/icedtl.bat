@echo off

REM The below variable needs to be set to a sensible value
SET X_WorkDir=workspace_directory

SET X_OUTDIR=%TEMP%\RC_SSXLate-%RC_JOB_ID%-%RC_STEP_ID%
SET X_DMXJOB=%RC_JOB_NAME%-%RC_STEP_ID%.dxj

ECHO "Starting translator"
"C:\Program Files\Raincode\SortTranslator\IcetoolTranslator.exe" -WorkDir=%X_WorkDir% -OutputDir=%X_OUTDIR% -DTLOutput=True
IF %ERRORLEVEL% NEQ 0 GOTO TranslatorError
ECHO "Translation finished"

ECHO "Starting SyncSort DMX Job"
REM Replace @ECHO dmxjob ...  with the required command to actually run dmxjob
@ECHO dmxjob /run %X_OUTDIR%\%X_DMXJOB%
ECHO "SyncSort Finished"

REM ECHO "Removing generated files"
REM del %X_OUTDIR%\%X_DMXJOB%
REM del %X_OUTDIR%\*.dxt
REM rd %X_OUTDIR%

:EndOfStep
ECHO "Step Done"
EXIT
:TranslatorError
ECHO "Translator Error"
EXIT /b %errorlevel%

