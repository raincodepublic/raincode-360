@echo off

REM The below variable needs to be set to a sensible value
SET X_WorkDir=workspace_directory

SET X_OUTDIR=%TEMP%\RC_SSXLate-%RC_JOB_ID%-%RC_STEP_ID%
SET X_OUTFILES=%RC_JOB_NAME%-%RC_STEP_ID%.*.txt

ECHO "Starting translator"
"C:\Program Files\Raincode\SortTranslator\IcetoolTranslator.exe" -WorkDir=%X_WorkDir% -OutputDir=%X_OUTDIR%
IF %ERRORLEVEL% NEQ 0 GOTO TranslatorError
ECHO "Translation finished"

ECHO "Starting SyncSort"
REM Replace @ECHO dmexpress /run %%f with the required command to actually run SyncSort
for %%f in (%X_OUTDIR%\%X_OUTFILES%) do @ECHO dmexpress /run %%f
ECHO "SyncSort Finished"

REM ECHO "Removing generated files"
REM del %X_OUTDIR%\%X_OUTFILES%
REM rd %X_OUTDIR%

:EndOfStep
ECHO "Step Done"
EXIT
:TranslatorError
ECHO "Translator Error"
EXIT /b %errorlevel%

