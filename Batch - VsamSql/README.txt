VSAMSQL DEMO

VsamSql is a file driver that stores catalog and data in a SQL Server database
instead of storing them on a filesystem.

PURPOSE

This demo demonstrates five aspects of VsamSql:

* 'JCL001' creates 2 VsamSql indexed sequential files that can be
viewed in the Catalog Explorer, by executing a COBOL program accessing
respective database tables and writing to VSAM.

* 'JCL002' reads 2 VsamSql indexed files (one sequentially and one
using the record key) to create a report (a sequential file stored
locally).

* 'JCL003' migrates 2 files to VsamSql indexed sequential files using
the 'IDCAMS' utility.

* 'generate tables' generates the creation script for tables used to store the catalog and the data.

* 'generate view' generates the views and triggers creation script for the 2 files

Raincode 360 is configured to explicitly store 'VSAMSQL.DATA.CUSTOMER'
and 'VSAMSQL.DATA.ACCOUNT' through VsamSql. All other datasets are
stored locally.

HOW TO RUN

NOTE: The JCL batches start by deleting the output from the previous
run (if any).

* JCL001

. Right click on the 'JCL001' project and select 'Debug - Start a
new instance'.

. This will execute 'JCL001.jcl' that creates 2 indexed sequential
files ('VSAMSQL.DATA.CUSTOMER' and 'VSAMSQL.DATA.ACCOUNT').

. Launch the Catalog Explorer to look at the two output files.
  Because this project doesn't use the default catalog, you should change the configuration 
  (File - Change configuration) to 
  C:\Raincode360\Repositories\Demos\Batch - VsamSql\catalog\Raincode.Catalog.xml


* JCL002

. JCL001.jcl should have been executed before.

. Right click on the 'JCL002' project and select 'Debug - Start a
new instance'.

. This will execute 'JCL002.jcl' that reads the 2 indexed files to
create a report ('OUTPUT.DATA.REPORTVS').

. Launch the Catalog Explorer to look at the result ('OUTPUT.DATA.REPORTVS').
  Because this project doesn't use the default catalog, you should change the configuration 
  (File - Change configuration) to 
  C:\Raincode360\Repositories\Demos\Batch - VsamSql\catalog\Raincode.Catalog.xml

* JCL003

. JCL001.jcl should have been executed before.

. Right click on the 'JCL003' project and select 'Debug - Start a
new instance'.

. This will execute 'JCL003.jcl' that migrates the VsamSql indexed file VSAMSQL.DATA.ACCOUNT 
  to the flat file on disk OUTPUT.DATA.ACCOUNT and back to VSAMSQL.DATA3.ACCOUNT

. Launch the Catalog Explorer to look at the result or double click.
  Because this project doesn't use the default catalog, you should change the configuration 
  (File - Change configuration) to 
  C:\Raincode360\Repositories\Demos\Batch - VsamSql\catalog\Raincode.Catalog.xml

* 'generate tables'

. Execute 'generate_tables.bat'

. This script reads the catalog definition (Catalog\Raincode.Catalog.xml) and generate the tables 
  creation script (table_creation.sql). The tables are already created, you didn't need to execute
  the sql script.

* 'generate views'

. Execute 'generate_views.bat'

. For each copybook (COBOL\ACCOUNT.cpy, COBOL\CUSTOMER.cpy):
  . Use the COBOL compiler (cobrc) to convert the .cpy into an .xml file containing the declaration 
    of the variables
  . The CopybookViewGenerator use the .xml file and connects to the DB to generate the views creation
    script and the triggers.

. The views and triggers are already created, you didn't need to execute the SQL script.