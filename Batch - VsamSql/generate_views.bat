SET SCRIPT_DIR=%~dp0
echo "generate view for table REC_ACCOUNT"
"%RCBIN%\cobrc.exe" :DeclDescriptors="%SCRIPT_DIR%ACCOUNT.xml" :MaxMem=1G "%SCRIPT_DIR%COBOL\ACCOUNT.cpy"
"%RCBIN%\CopybookViewGenerator.exe" -xml="%SCRIPT_DIR%ACCOUNT.xml" -struct=ACCOUNT-RECORD -table=REC_ACCOUNT -output="%SCRIPT_DIR%ACCOUNT_view.sql" -conn="Server=%DB_SERVER%;User ID=%DB_USER%;Password=%DB_PASSWORD%;Persist Security Info=False;Encrypt=False;Connection Timeout=30;Initial Catalog=VsamSql;"

echo "generate view for table REC_CUSTOMER"
"%RCBIN%\cobrc.exe" :DeclDescriptors="%SCRIPT_DIR%CUSTOMER.xml" :MaxMem=1G "%SCRIPT_DIR%COBOL\CUSTOMER.cpy"
"%RCBIN%\CopybookViewGenerator.exe" -xml="%SCRIPT_DIR%CUSTOMER.xml" -struct=CUSTOMERS-RECORD -table=REC_CUSTOMER -output="%SCRIPT_DIR%CUSTOMER_view.sql" -conn="Server=%DB_SERVER%;User ID=%DB_USER%;Password=%DB_PASSWORD%;Persist Security Info=False;Encrypt=False;Connection Timeout=30;Initial Catalog=VsamSql;"
