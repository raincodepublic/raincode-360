//JCL003   JOB CLASS=A,MSGCLASS=C
//* delete target file
//STEP001  EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
 DELETE                   OUTPUT.DATA.ACCOUNT
 SET       MAXCC = 0
/*
//* copy VsamSql file (indexed file) to a disk file (flat file)
//STEP002  EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SORTIN   DD   DSN=VSAMSQL.DATA.ACCOUNT,DISP=SHR
//SORTOUT  DD   DSN=OUTPUT.DATA.ACCOUNT,
//              DISP=(NEW,CATLG,)
//SYSIN    DD  *
 REPRO IFILE(SORTIN) OFILE(SORTOUT)
/*
//* create empty indexed file
//STEP003  EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
 DELETE                   VSAMSQL.DATA3.ACCOUNT
 SET       MAXCC = 0
 DEFINE    CLUSTER  (NAME(VSAMSQL.DATA3.ACCOUNT) INDEXED -
                    RECORDSIZE (36 36)  -           
                    KEYS (19 0) ) -
           DATA     (NAME(VSAMSQL.DATA3.ACCOUNT.DAT)) -
           INDEX    (NAME(VSAMSQL.DATA3.ACCOUNT.IDX))
/*
//* copy a disk file (flat file) to VsamSql file (indexed file)
//STEP004  EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SORTIN   DD   DSN=OUTPUT.DATA.ACCOUNT,DISP=SHR
//SORTOUT  DD   DSN=VSAMSQL.DATA3.ACCOUNT,
//              DISP=SHR
//SYSIN    DD  *
 REPRO IFILE(SORTIN) OFILE(SORTOUT)
/*