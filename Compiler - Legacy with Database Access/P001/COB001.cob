﻿       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB001.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
      *Level 1 group item                             size: 61 bytes    
       01 ITEM.
      *    Fixed length string                        size: 50 bytes 
           10 ITEM-NM-ENG PIC X(50).    
      *    Signed binary integer                      size:  2 bytes 
           10 ITEM-QTY PIC S9(4) COMP.  
      *    Signed zoned numeric (characters '0'..'9') size:  9 bytes
           10 ITEM-PRICE PIC S9(7)V99.
   	   EXEC SQL
   	       DECLARE CUR01 CURSOR FOR
      	   SELECT 
               ITEM_NM_ENG,
               ITEM_QTY,
               ITEM_PRICE
           FROM 
               DBO.OFFICESUPPLIES
   	   END-EXEC.
       PROCEDURE DIVISION.
           EXEC SQL
   	           OPEN CUR01
   	       END-EXEC
   	       PERFORM FETCH-CUR01
           PERFORM UNTIL SQLCODE NOT = 0
               DISPLAY ITEM-NM-ENG ITEM-QTY 
   	           PERFORM FETCH-CUR01
           END-PERFORM
           EXEC SQL
   	           CLOSE CUR01
   	       END-EXEC
           GOBACK
           .
       FETCH-CUR01.
   	       EXEC SQL
   	           FETCH CUR01 INTO
                   :ITEM-NM-ENG,
                   :ITEM-QTY,
                   :ITEM-PRICE
   	       END-EXEC
           .
