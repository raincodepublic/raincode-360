********************************************************************
*                                                                  *
*   Compiler - Legacy with Database Access                         *
*                                                                  *
*                                                                  *
********************************************************************

PURPOSE
  . Demonstrate how to connect a COBOL program directly to a database.

HOW TO RUN
  . Put a breakpoint on the GOBACK statement
  . Start the Raincode Debugger

