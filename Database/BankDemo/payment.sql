--USE BankDemo;

--DROP TABLE dbo.Payment;

CREATE TABLE dbo.Payment (
    PaymentId INT IDENTITY(1,1) PRIMARY KEY,
    CreditAccountNumber CHAR(19),
    DebitAccountNumber CHAR(19),
    PaymentDate DATETIME2 NOT NULL DEFAULT (GETDATE()),
    Amount DECIMAL(10,2) NOT NULL,
    DescriptionText CHAR(60)
);
CREATE INDEX i1 ON dbo.Payment (CreditAccountNumber);
CREATE INDEX i2 ON dbo.Payment (DebitAccountNumber);

--TRUNCATE TABLE dbo.Payment;

INSERT INTO dbo.Payment (DebitAccountNumber, CreditAccountNumber, Amount, DescriptionText) 
VALUES ('BE00-1000-1131-5888', 'BE00-2000-1131-5888', 100, 'from 1 to 2' );