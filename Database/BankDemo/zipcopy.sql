CREATE TABLE dbo.ZipCopy (
    ZipCode CHAR(4) PRIMARY KEY,
    City VARCHAR(60) NOT NULL,
    Country CHAR(2) NOT NULL
);
