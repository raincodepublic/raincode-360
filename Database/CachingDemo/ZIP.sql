DROP TABLE IF EXISTS [dbo].[ZIP];  
GO 
CREATE TABLE [dbo].[ZIP](
	[ZIPCODE] [char](6) NOT NULL,
	[CITY_NAME] [varchar](80) NULL,
	[POPULATION] [decimal](7, 0) NULL
);
GO
