###############################################
##                                           ##
##    I M S   M I G R A T I O N   D E M O    ##
##                                           ##
###############################################

$InputDir     = "Input"                               # Input folder
$OutputDir    = "Output"                              # Output folder

$Dbd          = "$InputDir\DBD"                       # DBD file location
$Unload       = "$InputDir\Unload"                    # Unload file location
$Psb          = "$InputDir\PSB"                       # PSB file location
$Cpy          = "$InputDir\Source\Cpy"                # Copybook folder
$Cobol        = "$InputDir\Source\Cobol"              # Cobol program folder
$Jcl          = "$InputDir\Source\Jcl"                # JCL file location
$MfsDir       = "$InputDir\Mfs"                       # MFS maps
$ScriptDir    = "$InputDir\Script"                    # User-Written Scripts
$AppMappings  = "InsertDealerMappings.sql"            # Installation Script for Online Application 

$CpyXml       = "$OutputDir\CpyXml"                   # copybook XML destination folder
$CpySqlDir    = "$OutputDir\CpySql"                   # Copybook SQL destination folder
$DbGen        = "$OutputDir\DbGen"                    # DbGenerator output location
$ViewGen      = "$OutputDir\ViewGen"                  # CopybookViewGenerator output location
$Bin          = "$OutputDir\Bin"                      # Binary file location

$Db           = "IMSQL_01"                            # database name
$DbConfig     = $Db + "_Config"                       # configuration database name
$DbConfigDir  = "$OutputDir\DbConfig"                 # configuration database creation script location
$Conn         = $env:RC360_CONNSTRING  + "MultipleActiveResultSets=True"       
                                                      # Connection string to Sql Server instance
$ConnDbConfig = "$Conn;Initial Catalog=$DbConfig"     # Connection string to configuration database
$ConnDb       = "$Conn;Initial Catalog=$Db"           # Connection string to Sql Server database
$Wc3270       = "C:\Program Files\wc3270\wc3270.exe"  # 3270 Terminal Emulator Application

$ErrorActionPreference = "Stop"

###############################
##  PART 1: Create Database  ##
###############################

if (1 -gt 0) {
### Cleanup  
Invoke-Sqlcmd -Query "DROP DATABASE IF EXISTS $Db"       -ConnectionString $Conn
Invoke-Sqlcmd -Query "DROP DATABASE IF EXISTS $DbConfig" -ConnectionString $Conn
if (Test-Path $OutputDir) {
   Remove-Item $OutputDir -Force -Recurse
}

### Create database
& "$Env:RCBIN\IMSql.DbGenerator" :DbName=$Db :Files=$Dbd :OutputDir=$DbGen
Invoke-Sqlcmd -InputFile "$DbGen\$Db.sql" -ConnectionString $Conn

### Install prerequisite functions
Invoke-Sqlcmd -InputFile "$Env:RCBIN\sql\Functions.sql" -ConnectionString $ConnDb
Invoke-Sqlcmd -InputFile "$Env:RCBIN\sql\EbcdicFuncs.sql" -ConnectionString $ConnDb

### Load data
$ArgList = ":LogLevel=TRACE", ":DbdFile=`"$Dbd\MODELDB.dbd`"", ":UnloadFile=$Unload\UMODELD.UNLOAD", ":ConnectionString=`"$ConnDb`""
Start-Process -NoNewWindow -Wait -Verbose "$Env:RCBIN\IMSql.Load" -ArgumentList $ArgList
$ArgList = ":LogLevel=TRACE", ":DbdFile=`"$Dbd\DEALERDB.dbd`"", ":UnloadFile=$Unload\UDEALERD.UNLOAD", ":ConnectionString=`"$ConnDb`""
Start-Process -NoNewWindow -Wait -Verbose "$Env:RCBIN\IMSql.Load" -ArgumentList $ArgList

### Convert copybook to XML
New-Item -Path $CpyXml -ItemType Directory
& "$Env:RCBIN\cobrc" :DeclDescriptors=$CpyXml\Orders.xml $Cpy\Orders.cpy

### Create views and triggers 
New-Item -Name $ViewGen -ItemType Directory
& "$Env:RCBIN\CopybookViewGenerator" -xml="$CpyXml\Orders.xml" -table=DEALERDB_ORDERS -output="$ViewGen\orders_view.sql" -Conn="$ConnDb" -OnlyTech -Loglevel=TRACE
Invoke-Sqlcmd -InputFile "$ViewGen\orders_view.sql" -ConnectionString $ConnDb

#####################
##  PART 2: Batch  ##
#####################

## Build DBContext
dotnet build $DbGen\DEALERDB.csproj --output=$Bin
dotnet build $DbGen\DEALERDI.csproj --output=$Bin
dotnet build $DbGen\MODELDB.csproj --output=$Bin
dotnet build $DbGen\MODELDI.csproj --output=$Bin

## Compile batch program
& "$Env:RCBIN\cobrc" :OutputDir=$Bin $Cobol\Dlrstat.cbl

## Convert PSBs to XML
#& "$Env:RCBIN\IMSql.Psb" -OutputDirectory="$Bin" $Psb\DSEARCH.psb  -LogLevel=TRACE
& "$Env:RCBIN\IMSql.Psb" -OutputDirectory="$Bin" $Psb\DLRSTAT.psb  -LogLevel=TRACE

## Cleanup Bin folder
Remove-Item "$Bin\*.pdb","$Bin\*.deps.json"

## Run JCL
$Env:RC_DB_TYPE = "Sqlserver"
$Env:RC_DB_CONNECTION = $ConnDb
$Env:RCLRUNARGS = "-StringRuntimeEncoding=ibm037"
$WorkDir = Get-Location
Push-Location $Bin
& "$Env:RCBATCHDIR\submit" -File="$WorkDir\$Jcl\Jcl001.Jcl" -LogLevel=WARNING 
Pop-Location

######################
##  PART 3: Online  ##
######################

### Create configuration database
& "$Env:RCBIN\IMSql.DbGenerator" -Online=True -Data=False -OnlineDbName="$DbConfig" :OutputDir=$DbConfigDir
Invoke-Sqlcmd -InputFile "$DbConfigDir\${DbConfig}_ServiceBroker.sql" -ConnectionString $Conn
Invoke-Sqlcmd -InputFile "$DbConfigDir\${DbConfig}_Tables.sql"        -ConnectionString $ConnDbConfig

### Compile PSBs
Get-ChildItem $Psb -Filter "*.psb" 
| ForEach-Object {
   Write-Host $_.BaseName
   & "$Env:RCBIN\IMSql.Psb" -OutputDirectory="$Bin" -LogLevel=INFO -SqlDatabase="$ConnDbConfig" "$($_.FullName)"
}

### Compile MFS Maps
Get-ChildItem $MfsDir -Filter "*.mfs"
| ForEach-Object {
   & "$Env:RCBIN\IMSql.Mfs" -OutputDirectory="$Bin" -SqlDatabase="$ConnDbConfig" -LogLevel=INFO "$($_.FullName)"
}

### Load Copybooks
New-Item -Name $CpySqlDir -ItemType Directory
& "$ScriptDir\InsertCopybook.ps1" > $CpySqlDir\InsertCopybook.sql
Invoke-Sqlcmd -InputFile "$CpySqlDir\InsertCopybook.sql" -ConnectionString $ConnDbConfig

### Install Application Mappings
Invoke-Sqlcmd -InputFile "$ScriptDir\$AppMappings" -ConnectionString $ConnDbConfig

### Start Terminal Server and Processing Server
$ArgList = ":LogLevel=TRACE", ":ConnectionString=`"$ConnDbConfig`"", ":RegionId=DEALER"
Start-Process -PassThru -WorkingDirectory $Bin -FilePath "$Env:RCBIN\IMSql.ProcessingServer" -ArgumentList $ArgList
Start-Process -PassThru -WorkingDirectory $Bin -FilePath "$Env:RCBIN\IMSql.TerminalServer"   -ArgumentList $ArgList

#### Start 3270 Terminal Emulator
Start-Sleep 15
Start-Process -FilePath $wc3270 -ArgumentList "+S", "-model", "4", "-title", "`"IMSql DealerOnline`"", "127.0.0.1:32023"
} ##NORUN







