
       01 CPY-ORDERS.
          05 CPY-PRE                  PIC X(20).
          05 CPY-ORDNBR               PIC X(20).
          05 CPY-DETAILS.
             10 CPY-DESCR             PIC X(176).
             10 CPY-DATE-9            PIC 9(8).
             10 CPY-DATE-X REDEFINES CPY-DATE-9 
                                      PIC X(8).
             10 CPY-NUM.
                15 CPY-TOTAL          PIC 9(5)V9(2) COMP-3.
                15 CPY-VAT            PIC 9(7)V9(2) COMP.
             10 CPY-STR REDEFINES CPY-NUM 
                                      PIC X(8).
             10 FILLER                PIC X(8).