Function GetInsert($dbd, $segment, $copybook){
	$txt=Get-Content $copybook -raw
	"insert into Segment ([DbdName], [SegmentName], [Copybook]) values('$dbd', '$segment', N'$txt');"
	"GO"
}

GetInsert "DEALERDB" "DEALER" "Input\Source\cpy\dealer.cpy"
GetInsert "DEALERDB" "CATALOG" "Input\Source\cpy\catalog.cpy"
GetInsert "DEALERDB" "ORDERS" "Input\Source\cpy\orders.cpy"
GetInsert "DEALERDB" "SALES" "Input\Source\cpy\sales.cpy"

GetInsert "MODELDB" "MODEL" "Input\Source\cpy\model.cpy"
GetInsert "MODELDB" "STOCK" "Input\Source\cpy\stock.cpy"