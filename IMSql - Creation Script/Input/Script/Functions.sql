CREATE OR ALTER PROCEDURE RC_REMOVE_FUNCS as
  begin
  drop function if exists RC_ASCII_TO_EBCDIC, 
                          RC_EBCDIC_TO_ASCII,            
						  RC_UTF16BE_TO_NCHAR,
						  RC_NCHAR_TO_UTF16BE,
                          RC_NUM_TO_DISPLAY_SIGNED_TRAILING, 
						  RC_NUM_TO_DISPLAY_UNSIGNED,
						  RC_DISPLAY_UNSIGNED_TO_NUM,
						  RC_NUM_TO_COMP3,
						  RC_COMP3_TO_NUM,
						  RC_NUM_TO_COMP,
						  RC_COMP_TO_NUM,
						  RC_DISPLAY_SIGNED_TRAILING_TO_NUM
  drop function if exists RC_UNADJUST_SCALE
  drop function if exists RC_ADJUST_SCALE
  end 
go
exec RC_REMOVE_FUNCS
go
CREATE OR ALTER FUNCTION RC_ADJUST_SCALE(@v as bigint, 
                                         @scale as int)
  returns NUMERIC (28,10) with schemabinding
  as
  begin
    declare @ix int = @scale;
	declare @res numeric (28,10);
	set @res = @v;
	while (@ix > 0)
	  begin
	  set @ix = @ix - 1;
	  set @res = @res / 10.0;
	  end
	return @res;
  end;
go
CREATE OR ALTER FUNCTION RC_UNADJUST_SCALE(@v as NUMERIC (28,10), 
                                           @scale as int)
  returns bigint with schemabinding
  as
  begin
    declare @ix int = @scale;
	while (@ix > 0)
	  begin
	  set @ix = @ix - 1;
	  set @v = @v * 10;
	  end
	return cast (@v as bigint)
  end;
go
CREATE OR ALTER FUNCTION RC_REVERSE_VARBINARY(@v as VARBINARY(20))
  returns VARBINARY(20) with schemabinding
  as
  begin
    declare @ix int = datalength(@v);
	declare @res varbinary(20) = substring (@v, @ix, 1);
	while (@ix > 1)
	  begin
	  set @ix = @ix - 1;
	  set @res = @res + substring (@v, @ix, 1);
	  end
	return @res
  end;
go
------------------------------------------
CREATE OR ALTER FUNCTION RC_DISPLAY_UNSIGNED_TO_BIGINT(@str AS VARBINARY(20))
  RETURNS BIGINT with schemabinding
AS
BEGIN
  DECLARE @res BIGINT = 0;
  DECLARE @ix integer = 0;
  DECLARE @byte integer = 0;
  DECLARE @len INT = LEN(@str);
  WHILE @ix < @len
     BEGIN
     SET @ix = @ix + 1;
     SET @byte = CAST(SUBSTRING(@str, @ix, 1) as TINYINT);
     SET @res = 10 * @res + (@byte % 240) 
     END;
  RETURN @res;
END ;
go
CREATE OR ALTER FUNCTION RC_BIGINT_TO_DISPLAY_UNSIGNED(@v as bigint, @width AS INT)
  RETURNS VARBINARY(20) with schemabinding
AS
BEGIN
  DECLARE @res VARBINARY(20);
  DECLARE @ix integer = 0;
  DECLARE @byte binary(1);
  SET @v = abs(@v)
  WHILE @ix < @width
     BEGIN
     SET @ix = @ix + 1;
	 set @byte = cast((@v % 10) + 240 as binary(1));
	 IF @ix = 1 
	   set @res = @byte
      else
	   set @res =  @byte + @res;
     set @v = @v / 10
     END;
  RETURN @res;
END ;
go
CREATE OR ALTER FUNCTION RC_DISPLAY_SIGNED_TRAILING_TO_BIGINT(@str AS VARBINARY(16))
  RETURNS BIGINT with schemabinding
AS
BEGIN
  DECLARE @res BIGINT = 0;
  DECLARE @ix integer = 0;
  DECLARE @byte integer = 0;
  DECLARE @len INT = LEN(@str);
  WHILE @ix < @len
     BEGIN
     SET @ix = @ix + 1;
     SET @byte = CAST(SUBSTRING(@str, @ix, 1) as TINYINT);
     SET @res = 10 * @res + @byte % 16 
     END;
  if @byte / 16 = 13
    set @res = - @res
  return @res
  end
go
CREATE OR ALTER FUNCTION RC_DISPLAY_SIGNED_TRAILING_TO_NUM(@str AS VARBINARY(16), 
                                                           @scale int)
  RETURNS decimal (28,10) with schemabinding
AS
  BEGIN
  return dbo.RC_ADJUST_SCALE(dbo.RC_DISPLAY_SIGNED_TRAILING_TO_BIGINT (@str), @scale)
  END
go
CREATE OR ALTER FUNCTION RC_NUM_TO_DISPLAY_SIGNED_TRAILING(@val   as decimal(28,10), 
                                                           @width as int,
                                                           @scale as int)
  RETURNS varbinary(16) with schemabinding
AS
BEGIN
  DECLARE @res varbinary(16);
  DECLARE @ix integer = 0;
  DECLARE @byte binary(1) = 0;
  DECLARE @v bigint = dbo.RC_UNADJUST_SCALE (@val, @scale);
  declare @compl tinyint
  IF @v < 0 
    begin
    set @Compl = 13*16
	set @v = -@v
	end
   else
    set @compl = 12*16
  SET @ix = 0
  WHILE @ix < @width
     begin
     SET @ix = @ix + 1;
	 set @byte = cast (@compl + @v % 10 as binary(1))
	 set @v = @v / 10
	 set @compl = 15*16
	 if @ix = 1 
	   set @res = @byte
	  else
	   set @res = @byte + @res
	 end
   return @res;
  end
go
CREATE OR ALTER FUNCTION RC_DISPLAY_UNSIGNED_TO_NUM(@str AS VARBINARY(20),
                                                    @scale as integer)
  RETURNS numeric(28,10) with schemabinding
AS
BEGIN
  return dbo.rc_adjust_scale(dbo.RC_DISPLAY_UNSIGNED_TO_BIGINT(@str), @scale)
END ;
go
CREATE OR ALTER FUNCTION RC_NUM_TO_DISPLAY_UNSIGNED(@val as decimal(28,10),
                                                    @width as integer,
                                                    @scale as integer)
  RETURNS varbinary(20) with schemabinding
AS
BEGIN
  return dbo.rc_bigint_to_display_unsigned (dbo.rc_unadjust_scale(@val,@scale),@width);
END ;
go
CREATE OR ALTER FUNCTION RC_COMP_TO_BIGINT(@str AS VARBINARY(20))
  RETURNS BIGINT with schemabinding
AS
BEGIN
  DECLARE @len INT = DATALENGTH(@str);
  -- set @str = dbo.RC_REVERSE_VARBINARY(@str)
  if @len = 2
    return cast (@str as smallint);
  if @len = 4
    return cast (@str as int);
  if @len = 8
    return cast (@str as bigint);
  if @len = 1
    return cast (@str as tinyint);
  -- raiserror (N'Invalid integer size: %d', -1, -1, @len);
  return -1;
END ;
go
CREATE OR ALTER FUNCTION RC_BIGINT_TO_COMP(@v as bigint,
                                           @width as integer)
  RETURNS VARBINARY(8) with schemabinding
AS
BEGIN
  Declare @buff as varbinary(8) = cast (@v AS varbinary(8));
  if @width < 8
    return substring(@buff, 8-@width+1, @width);
  return @buff;
END ;
go
CREATE OR ALTER FUNCTION RC_NUM_TO_COMP(@v as decimal(28,10),
                                         @width as integer,
										 @scale as integer)
  RETURNS VARBINARY(16) with schemabinding
AS
BEGIN
return dbo.rc_bigint_to_comp (dbo.rc_unadjust_scale (@v,@scale),@width);
END ;
go
CREATE OR ALTER FUNCTION RC_COMP3_TO_BIGINT(@str AS VARBINARY(20))
  RETURNS BIGINT with schemabinding
AS
BEGIN
  DECLARE @len INT = DATALENGTH(@str);
  DECLARE @idx int = 1;
  declare @res bigint = 0;
  declare @v int;
  while @idx < @len  
    begin
	set @v = cast(substring(@str, @idx, 1) as tinyint)
	if @v = 0
	  set @res = 100 * @res;
	 else
	  begin
	  set @res = @res * 10 + @v / 16 
	  set @res = @res * 10 + @v % 16 
	  end
	set @idx = @idx + 1;
	end
	set @v = cast(substring(@str, @len, 1) as tinyint)
    set @res = @res * 10 + @v / 16 
	if @v % 16 = 13
	  set @res = - @res;
	return @res;
END ;
go
CREATE OR ALTER FUNCTION RC_COMP3_TO_NUM(@str AS VARBINARY(20),
                                         @scale as integer)
  RETURNS numeric(28,10) with schemabinding
AS
BEGIN
  return dbo.rc_adjust_scale(dbo.RC_COMP3_TO_BIGINT(@str), @scale)
END ;
go
CREATE OR ALTER FUNCTION RC_BIGINT_TO_COMP3(@v as bigint,
                                            @width as integer)
  RETURNS VARBINARY(16) with schemabinding
AS
BEGIN
declare @nibble integer;
declare @res varbinary(16);
if (@v < 0)
  begin
  set @v = -@v
  set @nibble = 13; -- Negative, D
  end
 else
  set @nibble = 15; -- Positive F
set @nibble = @nibble + (16 * (@v % 10))
set @res = cast (@nibble as binary(1));
set @v = @v / 10
while @v > 0
  begin
  set @nibble = @v % 10
  set @v = @v / 10
  set @nibble = @nibble + 16 * (@v % 10)
  set @v = @v / 10
  set @res = cast (@nibble as binary(1)) + @res;
  end
while datalength(@res) < @width
  set @res = cast (0 as binary(1)) + @res;
return @res;
END ;
go
CREATE OR ALTER FUNCTION RC_COMP_TO_NUM(@str AS VARBINARY(20),
                                         @scale as integer)
  RETURNS numeric(28,10) with schemabinding
AS
BEGIN
  return dbo.rc_adjust_scale(dbo.RC_COMP_TO_BIGINT(@str), @scale)
END ;
go
CREATE OR ALTER FUNCTION RC_NUM_TO_COMP3(@v as decimal(28,10),
                                         @width as integer,
										 @scale as integer)
  RETURNS VARBINARY(16) with schemabinding
AS
BEGIN
return dbo.rc_bigint_to_comp3 (dbo.rc_unadjust_scale (@v,@scale),@width);
END ;
go
---------------------------------------
CREATE OR ALTER FUNCTION RC_UTF16BE_TO_NCHAR(@str AS VARBINARY(256))
  RETURNS NVARCHAR(256) WITH SCHEMABINDING
AS
BEGIN
  DECLARE @res NVARCHAR(256) = '';
  DECLARE @ix INT = 1;
  DECLARE @len INT = LEN(@str);
  WHILE @ix+1 <= @len
     BEGIN
	 set @res = @res + NCHAR(CONVERT (INT,SUBSTRING(@STR,@IX,2)))
     SET @ix = @ix + 2;
     END;
  RETURN RTRIM(@res);
END ;
go
CREATE OR ALTER FUNCTION RC_NCHAR_TO_UTF16BE(@str AS NVARCHAR(256),
                                             @width as integer)
  RETURNS VARBINARY(256) WITH SCHEMABINDING
AS
BEGIN
  DECLARE @res VARBINARY(256);
  DECLARE @t BINARY(2);
  DECLARE @sp BINARY(2);
  DECLARE @ix INT = 0;
  DECLARE @len INT = len(@str);
  set @len = iif(@len>@width, @width, @len);
  WHILE @ix < @len
     BEGIN
	 set @ix = @ix + 1
	 set @t = convert(binary(2), unicode(substring(@str, @ix, 1)))
     if @ix = 1 
       SET @res = @t;
      else
       SET @res = @res + @t;
     END;
  if (@len*2 < @width)
    begin
    set @sp = convert(binary(2), unicode(N' '))
	set @ix = @ix * 2
    while @ix < @width
      begin
      set @ix = @ix + 2
      set @res = @res + @sp
      end
    end;
  RETURN @res;
END ;

-- select dbo.RC_BIGINT_TO_COMP3(-7,4)
-- select cast(16*7+13 as binary(1))
-- select dbo.RC_NUM_TO_COMP3(-18.07,6,2)
