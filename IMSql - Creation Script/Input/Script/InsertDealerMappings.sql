
-- CREATE TABLE [dbo].[rc_ims_queues](
-- 	[UID] [uniqueidentifier] NOT NULL,
-- 	[DestCode] [char](8) NOT NULL,
-- 	[SrcCode] [char](8) NOT NULL,
-- 	[Data] [varbinary](8000) NOT NULL,
-- 	[Created] [datetime2](7) NOT NULL,
-- 	[Updated] [datetime2](7) NULL,
-- 	[State] [int] NULL,
-- 	[ModName] [char](8) NULL,
-- 	[IsOutput] [bit] NOT NULL
-- ) ON [PRIMARY]
-- GO

-- ALTER TABLE [dbo].[rc_ims_queues] ADD  CONSTRAINT [DF__rc_ims_queue_default_uid]  DEFAULT (newid()) FOR [UID]
-- GO

-- ALTER TABLE [dbo].[rc_ims_queues] ADD  CONSTRAINT [DF__rc_ims_queue_default_createdate]  DEFAULT (getdate()) FOR [Created]
-- GO

-- ALTER TABLE [dbo].[rc_ims_queues] ADD  CONSTRAINT [DF__rc_ims__default_state]  DEFAULT ((0)) FOR [State]
-- GO

-- ALTER TABLE [dbo].[rc_ims_queues] ADD  CONSTRAINT [DF_rc_ims_queues_IsOutput]  DEFAULT ((0)) FOR [IsOutput]
-- GO

INSERT INTO [dbo].[Systems]
           ([Name])
     VALUES
           ('IMS')
INSERT INTO [dbo].[Regions]
           ([Name]
           ,[LocalEncoding]
           ,[TerminalEncoding]
           ,[SystemName]
		   ,[MaxThreads]
		   ,[Enabled])
     VALUES
           ('DEALER'
           ,'ibm037'
           ,'ibm037'
           ,'IMS'
           ,8
		   ,1)

INSERT INTO [dbo].[Programs]
           ([ProgramName]
           ,[IsBMP]
           ,[Language]
           ,[ShedulingType])
     VALUES
           ('DINSERT' ,0 ,'COBOL' ,2),
           ('DSEARCH' ,0 ,'PL1' ,2),
		   ('CATALOGS' ,0 ,'COBOL' ,2),
		   ('DDELETE' ,0 ,'PL1' ,2),
		   ('MDELETE' ,0 ,'PL1' ,2),
		   ('MINSERT' ,0 ,'COBOL' ,2),
		   ('MREPLACE' ,0 ,'PL1' ,2),
		   ('MSEARCH' ,0 ,'PL1' ,2),
		   ('RCTE02' ,0 ,'COBOL' ,2),
		   ('SALORD' ,0 ,'COBOL' ,2)

INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     VALUES
           ('DEALER'
           ,'dsearch'
           ,'DSEARCH'
           ,'DSEARCH')
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     VALUES
           ('DEALER'
           ,'dinsert'
           ,'DINSERT'
           ,'DINSERT')
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     VALUES
           ('DEALER'
           ,'minsert'
           ,'MINSERT'
           ,'MINSERT')

INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     VALUES ('DEALER', 'msearch', 'MSEARCH', 'MSEARCH')
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     values ('DEALER', 'CATALOGS', 'CATALOGS', 'CATALOGS');
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     values ('DEALER', 'SALORD', 'SALORD', 'SALORD');
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     values ('DEALER', 'ddelete', 'DDELETE', 'DDELETE');
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     values ('DEALER', 'mdelete', 'MDELETE', 'MDELETE');
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     values ('DEALER', 'mreplace', 'MREPLACE', 'MREPLACE');