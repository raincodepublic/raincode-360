﻿IMSQL DOCS
^^^^^^^^^^
https://www.raincode.com/docs/IMSql/UserGuide.html#_imsql_user_guide


DATAMODEL
^^^^^^^^^

MODELDB                      DEALERDB                    MODELDI
┌───────────┐                ┌───────────┐              ┌───────────┐
│ MODEL     ├───────┐        │ DEALER    │              │ INDEX     │
└─────┬─────┘       │        └─────┬─────┘              └───────────┘
      │             │              │
      │             │              │                    DEALERDI
┌─────┴─────┐       │        ┌─────┴─────┐              ┌───────────┐
│ STOCK     │       └────────┤ CATALOG   │              │ INDEX     │
└───────────┘                └─────┬─────┘              └───────────┘
                                   │
                          ┌────────┴────────┐
                          │                 │
                    ┌─────┴─────┐     ┌─────┴─────┐
                    │ ORDERS    │     │ SALES     │
                    └───────────┘     └───────────┘


