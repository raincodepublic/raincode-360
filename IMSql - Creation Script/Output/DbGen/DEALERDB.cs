/// DEALERDB HIDAM 
namespace RainCode.IMS.Generated {
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Logging;
using RainCode.Core.Logging;
using RainCode.IMSql.Common.DB;
using RainCode.IMSql.Common.Interfaces.CallHandlers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
#region DEALERDB
    /* HIDAM */
    public class DEALERDB : DbContext {
        private static bool LogToConsole = false;
        private static bool LogToDebug = false;
        private static readonly ILoggerFactory loggerFactory = LoggerFactory.Create(c => {
            if(LogToConsole || Logger.LogLevel == Level.DIAGNOSTIC) { c.AddConsole(); }
            if(LogToDebug)  { c.AddDebug(); }
        });
        private SqlConnection Connection;

        public DEALERDB(SqlConnection con)
        {
            Connection = con;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Connection,

            opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds));
            optionsBuilder.UseLoggerFactory(loggerFactory);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var factory = this.GetService<ISqlExpressionFactory>();
            modelBuilder
                .Entity<ResultRow>(
                    eb =>
                       {
                           eb.HasNoKey();

                       });
            // Functions for byte compares

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("LessThan"))
                .HasTranslation((a) => factory.LessThan(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("LessThanOrEqual"))
                .HasTranslation((a) => factory.LessThanOrEqual(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("NotEqual"))
                .HasTranslation((a) => factory.NotEqual(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Datalength", new Type[]{typeof(System.Byte[])}))
                .HasTranslation((a) => factory.Function("Datalength", a, typeof(byte[]) ));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("SubString", new Type[]{typeof(System.Byte[]), typeof(System.Int32), typeof(System.Int32)}))
                .HasTranslation((a) => {
                    var args = a.ToArray();
                    args[1] = factory.Add(args[1], factory.Constant(1));
                    return factory.Function("SUBSTRING", args, typeof(byte[]));
                });

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("GreaterThanOrEqual", new Type[] { typeof(string), typeof(string) }))
                .HasTranslation((a) => factory.GreaterThanOrEqual(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("GreaterThanOrEqual", new Type[] { typeof(byte[]), typeof(byte[]) }))
                .HasTranslation((a) => factory.GreaterThanOrEqual(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Equal", new Type[] { typeof(byte[]), typeof(byte[]) }))
                .HasTranslation((a) => factory.Equal(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Equal", new Type[] { typeof(int), typeof(int) }))
                .HasTranslation((a) => factory.Equal(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("GreaterThan", new Type[] { typeof(string), typeof(string) }))
                .HasTranslation((a) => factory.GreaterThan(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("GreaterThan", new Type[] { typeof(byte[]), typeof(byte[]) }))
                .HasTranslation((a) => factory.GreaterThan(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Concat", new[] {typeof(byte[])}))
                .HasTranslation((a) => a.FirstOrDefault());

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Concat", new[] { typeof(byte[]), typeof(byte[]) }))
                .HasTranslation((a) => {
                    var args = a.ToList();
                    var left = args[0];
                    for (int i = 1; i < args.Count; i++)
                    {
                        left = factory.Add(left, args[i]);
                    }
                    return left;
                });

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Concat", new[] { typeof(byte[]), typeof(byte) }))
                .HasTranslation((a) => {
                    var args = a.ToList();
                    var left = args[0];
                    for (int i = 1; i < args.Count; i++)
                    {
                        left = factory.Add(left, factory.Convert(factory.Convert(args[i], typeof(byte)), typeof(byte[])));
                    }
                    return left;
                }); 
            // End of functions

            base.OnModelCreating(modelBuilder);
        }
        public DbSet<DEALERDB.DEALERSegment> DEALER { get; set; }
        public DbSet<DEALERDB.CATALOGSegment> CATALOG { get; set; }
        public DbSet<DEALERDB.ORDERSSegment> ORDERS { get; set; }
        public DbSet<DEALERDB.SALESSegment> SALES { get; set; }
        public DbSet<MODELDB.MODELSegment> MODEL { get; set; }
        public DbSet<ResultRow> GnResult { get; set; }

        /// <summary>
        /// Calls the GN stored procedure
        /// </summary>
        /// <param name="sensitiveSegments">list (separated by ";") of sensitive segments</param>
        /// <param name="rid">The RID of the current segment. null if no current segment</param>
        /// <param name="segmName">The name of the current segment. null if no current segment</param>
        /// <param name="nbr">The number of segments to retreave</param>
        /// <returns>The 'nbr' next segments</returns>
        public System.Linq.IQueryable<ResultRow> GetGnResultRow(string sensitiveSegments, byte[] ckey, byte[] chid, string segmName, int nbr)
        {
            return GnResult.FromSqlRaw("select * from [DEALERDB_GN]({0}, {1}, {2}, {3}, {4}) order by [order]", sensitiveSegments, ckey, chid, segmName, nbr);
        }

        /// <summary>
        /// Returns the name of the stored procedure that implement "ISRT" for segment 'segmentName'
        /// </summary>
        /// <param name="segmentName">Name of the segment</param>
        /// <returns>Name of the ISRT stored procedure</returns>
        public string GetIsrtStoredProcedure(string segmentName)
        {
            switch (segmentName)
            {
                case "DEALER":
                    return "DEALERDB_ISRT_DEALER";
                case "CATALOG":
                    return "DEALERDB_ISRT_CATALOG";
                case "ORDERS":
                    return "DEALERDB_ISRT_ORDERS";
                case "SALES":
                    return "DEALERDB_ISRT_SALES";
                default:
                    return null;
            }
        }

        /// <summary>
        /// Returns the name of the stored procedure that implement "REPL" for segment 'segmentName'
        /// </summary>
        /// <param name="segmentName">Name of the segment</param>
        /// <returns>Name of the REPL stored procedure</returns>
        public string GetReplStoredProcedure(string segmentName)
        {
            switch (segmentName)
            {
                case "DEALER":
                    return "DEALERDB_REPL_DEALER";
                case "CATALOG":
                    return "DEALERDB_REPL_CATALOG";
                case "ORDERS":
                    return "DEALERDB_REPL_ORDERS";
                case "SALES":
                    return "DEALERDB_REPL_SALES";
                default:
                    return null;
            }
        }

        /// <summary>
        /// Returns the name of the stored procedure that implement "GU" for the SSA's 'listSSA'
        /// </summary>
        /// <param name="listSSA"></param>
        /// <returns>Name of the stored procedure</returns>
        public (string, byte[]) GetGuStoredProcedure(List<Ssa> listSSA)
        {
            if (listSSA.Count == 1
                && listSSA[0].SegmentName == "DEALER" && listSSA[0].Qualifications.Count == 1 && listSSA[0].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[0].Qualifications[0].FieldName == "DLRNO" && listSSA[0].CommandCodes == null
                  )
            {
                byte[] keys = new byte[10];
                System.Buffer.BlockCopy(listSSA[0].Qualifications[0].Value, 0, keys, 0, 10);
                return ("DEALERDB_GU_DEALER", keys);
            }
            if (listSSA.Count == 2
                && listSSA[0].SegmentName == "DEALER" && listSSA[0].Qualifications.Count == 1 && listSSA[0].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[0].Qualifications[0].FieldName == "DLRNO" && listSSA[0].CommandCodes == null
                && listSSA[1].SegmentName == "CATALOG" && listSSA[1].Qualifications.Count == 1 && listSSA[1].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[1].Qualifications[0].FieldName == "MODTYPE" && listSSA[1].CommandCodes == null
                  )
            {
                byte[] keys = new byte[30];
                System.Buffer.BlockCopy(listSSA[0].Qualifications[0].Value, 0, keys, 0, 10);
                System.Buffer.BlockCopy(listSSA[1].Qualifications[0].Value, 0, keys, 10, 20);
                return ("DEALERDB_GU_CATALOG", keys);
            }
            if (listSSA.Count == 3
                && listSSA[0].SegmentName == "DEALER" && listSSA[0].Qualifications.Count == 1 && listSSA[0].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[0].Qualifications[0].FieldName == "DLRNO" && listSSA[0].CommandCodes == null
                && listSSA[1].SegmentName == "CATALOG" && listSSA[1].Qualifications.Count == 1 && listSSA[1].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[1].Qualifications[0].FieldName == "MODTYPE" && listSSA[1].CommandCodes == null
                && listSSA[2].SegmentName == "ORDERS" && listSSA[2].Qualifications.Count == 1 && listSSA[2].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[2].Qualifications[0].FieldName == "ORDNBR" && listSSA[2].CommandCodes == null
                  )
            {
                byte[] keys = new byte[50];
                System.Buffer.BlockCopy(listSSA[0].Qualifications[0].Value, 0, keys, 0, 10);
                System.Buffer.BlockCopy(listSSA[1].Qualifications[0].Value, 0, keys, 10, 20);
                System.Buffer.BlockCopy(listSSA[2].Qualifications[0].Value, 0, keys, 30, 20);
                return ("DEALERDB_GU_ORDERS", keys);
            }
            if (listSSA.Count == 3
                && listSSA[0].SegmentName == "DEALER" && listSSA[0].Qualifications.Count == 1 && listSSA[0].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[0].Qualifications[0].FieldName == "DLRNO" && listSSA[0].CommandCodes == null
                && listSSA[1].SegmentName == "CATALOG" && listSSA[1].Qualifications.Count == 1 && listSSA[1].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[1].Qualifications[0].FieldName == "MODTYPE" && listSSA[1].CommandCodes == null
                && listSSA[2].SegmentName == "SALES" && listSSA[2].Qualifications.Count == 1 && listSSA[2].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[2].Qualifications[0].FieldName == "SALDATE" && listSSA[2].CommandCodes == null
                  )
            {
                byte[] keys = new byte[38];
                System.Buffer.BlockCopy(listSSA[0].Qualifications[0].Value, 0, keys, 0, 10);
                System.Buffer.BlockCopy(listSSA[1].Qualifications[0].Value, 0, keys, 10, 20);
                System.Buffer.BlockCopy(listSSA[2].Qualifications[0].Value, 0, keys, 30, 8);
                return ("DEALERDB_GU_SALES", keys);
            }
            return (null, null);
        }
        [Table("DEALERDB_DEALER")]
        public class DEALERSegment {

            [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("HID")]
            public byte[] HID { get; set; }

            [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int RID { get; set; }

            public static string SegmentName { get; } = "DEALER";
            // DBD size definition (min:0, max:220, sum: 220)
            public static int Size { get; } = 220;
            public static string[] Fields { get; } = new [] { "DLRNO", "DLRNAME", "PHONE", "STREET", "ZIPCODE", "CITY", "COUNTRY"  };
            public static int[] FieldsSize { get; } = new [] { 10, 60, 20, 40, 10, 40, 40  };
            public static int KeyStartIndex { get; } = 1;
            public static int KeySize { get; } = 10;
            public static string KeyField { get; } = "DLRNO";
                //1=Unique sequence, 2=non-unique sequence, 3=no sequence
            public static int KeyType { get; } = 1;
            public static string SortKey { get; } = "DLRNO";
            public static int SortKeySize { get; } = 10;
            public static string Rule { get; } = "LAST";
            public static bool HasChild { get; } = true;
            public static string ParentName { get; } = null;
            public static string LogicalParentDbd { get; } = null;
            public static string LogicalParentSegment { get; } = null;
            public static int LogicalParentCkeySize { get; } = 0;
            public static int SegmentLevel { get; } = 0;
            // true if the segment is a child of the destination parent of a concatenated segment

            public static string Ims2Csharp(string imsName)
            {
                return imsName;
            }

            [MaxLength(220)]
            public byte[] Data { get; set; }

            // DLRNO => Data[0..11] Length : 10 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("DLRNO", Order =0)]
            public byte[] DLRNO { get; set; }

            // DLRNAME => Data[10..71] Length : 60 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("DLRNAME", Order =1)]
            public byte[] DLRNAME { get; set; }

            // PHONE => Data[70..91] Length : 20 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("PHONE", Order =2)]
            public byte[] PHONE { get; set; }

            // STREET => Data[90..131] Length : 40 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("STREET", Order =3)]
            public byte[] STREET { get; set; }

            // ZIPCODE => Data[130..141] Length : 10 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("ZIPCODE", Order =4)]
            public byte[] ZIPCODE { get; set; }

            // CITY => Data[140..181] Length : 40 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("CITY", Order =5)]
            public byte[] CITY { get; set; }

            // COUNTRY => Data[180..221] Length : 40 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("COUNTRY", Order =6)]
            public byte[] COUNTRY { get; set; }


        }

        [Table("DEALERDB_CATALOG")]
        public class CATALOGSegment {

            [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("HID")]
            public byte[] HID { get; set; }

            [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int RID { get; set; }

            public static string SegmentName { get; } = "CATALOG";
            // DBD size definition (min:0, max:220, sum: 220)
            public static int Size { get; } = 220;
            public static string[] Fields { get; } = new [] { "MODTYPE", "COMMENT"  };
            public static int[] FieldsSize { get; } = new [] { 20, 200  };
            public static int KeyStartIndex { get; } = 1;
            public static int KeySize { get; } = 20;
            public static string KeyField { get; } = "MODTYPE";
                //1=Unique sequence, 2=non-unique sequence, 3=no sequence
            public static int KeyType { get; } = 1;
            public static string SortKey { get; } = "MODTYPE";
            public static int SortKeySize { get; } = 20;
            public static string Rule { get; } = "LAST";
            public static bool HasChild { get; } = true;
            public static string ParentName { get; } = "DEALER";
            public static string LogicalParentDbd { get; } = "MODELDB";
            public static string LogicalParentSegment { get; } = "MODEL";
            public static int LogicalParentCkeySize { get; } = 20;
            public static int SegmentLevel { get; } = 1;
            // true if the segment is a child of the destination parent of a concatenated segment

            public static string Ims2Csharp(string imsName)
            {
                return imsName;
            }

            [MaxLength(220)]
            public byte[] Data { get; set; }

            public DEALERDB.DEALERSegment Parent { get; set; }

            [ForeignKey("Parent")]
            [Required]
            public int PID { get; set; }

            public MODELDB.MODELSegment LogicalParent { get; set; }

            [ForeignKey("LogicalParent")]
            [Required]
            public int LPID { get; set; }

            // MODTYPE => Data[0..21] Length : 20 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("MODTYPE", Order =0)]
            public byte[] MODTYPE { get; set; }

            // COMMENT => Data[20..221] Length : 200 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("COMMENT", Order =1)]
            public byte[] COMMENT { get; set; }


        }

        [Table("DEALERDB_ORDERS")]
        public class ORDERSSegment {

            [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("HID")]
            public byte[] HID { get; set; }

            [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int RID { get; set; }

            public static string SegmentName { get; } = "ORDERS";
            // DBD size definition (min:0, max:240, sum: 220)
            public static int Size { get; } = 240;
            public static string[] Fields { get; } = new [] { "ORDNBR", "DETAILS"  };
            public static int[] FieldsSize { get; } = new [] { 20, 200  };
            public static int KeyStartIndex { get; } = 21;
            public static int KeySize { get; } = 20;
            public static string KeyField { get; } = "ORDNBR";
                //1=Unique sequence, 2=non-unique sequence, 3=no sequence
            public static int KeyType { get; } = 1;
            public static string SortKey { get; } = "ORDNBR";
            public static int SortKeySize { get; } = 20;
            public static string Rule { get; } = "LAST";
            public static bool HasChild { get; } = false;
            public static string ParentName { get; } = "CATALOG";
            public static string LogicalParentDbd { get; } = null;
            public static string LogicalParentSegment { get; } = null;
            public static int LogicalParentCkeySize { get; } = 0;
            public static int SegmentLevel { get; } = 2;
            // true if the segment is a child of the destination parent of a concatenated segment

            public static string Ims2Csharp(string imsName)
            {
                return imsName;
            }

            [MaxLength(240)]
            public byte[] Data { get; set; }

            public DEALERDB.CATALOGSegment Parent { get; set; }

            [ForeignKey("Parent")]
            [Required]
            public int PID { get; set; }

            // ORDNBR => Data[20..41] Length : 20 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("ORDNBR", Order =0)]
            public byte[] ORDNBR { get; set; }

            // DETAILS => Data[40..241] Length : 200 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("DETAILS", Order =1)]
            public byte[] DETAILS { get; set; }


        }

        [Table("DEALERDB_SALES")]
        public class SALESSegment {

            [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("HID")]
            public byte[] HID { get; set; }

            [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int RID { get; set; }

            public static string SegmentName { get; } = "SALES";
            // DBD size definition (min:0, max:208, sum: 208)
            public static int Size { get; } = 208;
            public static string[] Fields { get; } = new [] { "SALDATE", "DETAILS"  };
            public static int[] FieldsSize { get; } = new [] { 8, 200  };
            public static int KeyStartIndex { get; } = 1;
            public static int KeySize { get; } = 8;
            public static string KeyField { get; } = "SALDATE";
                //1=Unique sequence, 2=non-unique sequence, 3=no sequence
            public static int KeyType { get; } = 1;
            public static string SortKey { get; } = "SALDATE";
            public static int SortKeySize { get; } = 8;
            public static string Rule { get; } = "LAST";
            public static bool HasChild { get; } = false;
            public static string ParentName { get; } = "CATALOG";
            public static string LogicalParentDbd { get; } = null;
            public static string LogicalParentSegment { get; } = null;
            public static int LogicalParentCkeySize { get; } = 0;
            public static int SegmentLevel { get; } = 2;
            // true if the segment is a child of the destination parent of a concatenated segment

            public static string Ims2Csharp(string imsName)
            {
                return imsName;
            }

            [MaxLength(208)]
            public byte[] Data { get; set; }

            public DEALERDB.CATALOGSegment Parent { get; set; }

            [ForeignKey("Parent")]
            [Required]
            public int PID { get; set; }

            // SALDATE => Data[0..9] Length : 8 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("SALDATE", Order =0)]
            public byte[] SALDATE { get; set; }

            // DETAILS => Data[8..209] Length : 200 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("DETAILS", Order =1)]
            public byte[] DETAILS { get; set; }


        }

    }
#endregion // DEALERDB
}
