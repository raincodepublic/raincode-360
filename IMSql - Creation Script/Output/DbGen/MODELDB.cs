/// MODELDB HIDAM 
namespace RainCode.IMS.Generated {
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Logging;
using RainCode.Core.Logging;
using RainCode.IMSql.Common.DB;
using RainCode.IMSql.Common.Interfaces.CallHandlers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
#region MODELDB
    /* HIDAM */
    public class MODELDB : DbContext {
        private static bool LogToConsole = false;
        private static bool LogToDebug = false;
        private static readonly ILoggerFactory loggerFactory = LoggerFactory.Create(c => {
            if(LogToConsole || Logger.LogLevel == Level.DIAGNOSTIC) { c.AddConsole(); }
            if(LogToDebug)  { c.AddDebug(); }
        });
        private SqlConnection Connection;

        public MODELDB(SqlConnection con)
        {
            Connection = con;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Connection,

            opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds));
            optionsBuilder.UseLoggerFactory(loggerFactory);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var factory = this.GetService<ISqlExpressionFactory>();
            modelBuilder
                .Entity<ResultRow>(
                    eb =>
                       {
                           eb.HasNoKey();

                       });
            // Functions for byte compares

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("LessThan"))
                .HasTranslation((a) => factory.LessThan(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("LessThanOrEqual"))
                .HasTranslation((a) => factory.LessThanOrEqual(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("NotEqual"))
                .HasTranslation((a) => factory.NotEqual(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Datalength", new Type[]{typeof(System.Byte[])}))
                .HasTranslation((a) => factory.Function("Datalength", a, typeof(byte[]) ));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("SubString", new Type[]{typeof(System.Byte[]), typeof(System.Int32), typeof(System.Int32)}))
                .HasTranslation((a) => {
                    var args = a.ToArray();
                    args[1] = factory.Add(args[1], factory.Constant(1));
                    return factory.Function("SUBSTRING", args, typeof(byte[]));
                });

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("GreaterThanOrEqual", new Type[] { typeof(string), typeof(string) }))
                .HasTranslation((a) => factory.GreaterThanOrEqual(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("GreaterThanOrEqual", new Type[] { typeof(byte[]), typeof(byte[]) }))
                .HasTranslation((a) => factory.GreaterThanOrEqual(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Equal", new Type[] { typeof(byte[]), typeof(byte[]) }))
                .HasTranslation((a) => factory.Equal(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Equal", new Type[] { typeof(int), typeof(int) }))
                .HasTranslation((a) => factory.Equal(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("GreaterThan", new Type[] { typeof(string), typeof(string) }))
                .HasTranslation((a) => factory.GreaterThan(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("GreaterThan", new Type[] { typeof(byte[]), typeof(byte[]) }))
                .HasTranslation((a) => factory.GreaterThan(a.ElementAt(0), a.ElementAt(1)));

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Concat", new[] {typeof(byte[])}))
                .HasTranslation((a) => a.FirstOrDefault());

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Concat", new[] { typeof(byte[]), typeof(byte[]) }))
                .HasTranslation((a) => {
                    var args = a.ToList();
                    var left = args[0];
                    for (int i = 1; i < args.Count; i++)
                    {
                        left = factory.Add(left, args[i]);
                    }
                    return left;
                });

            modelBuilder
                .HasDbFunction(typeof(CustomSqlFunctions).GetMethod("Concat", new[] { typeof(byte[]), typeof(byte) }))
                .HasTranslation((a) => {
                    var args = a.ToList();
                    var left = args[0];
                    for (int i = 1; i < args.Count; i++)
                    {
                        left = factory.Add(left, factory.Convert(factory.Convert(args[i], typeof(byte)), typeof(byte[])));
                    }
                    return left;
                }); 
            // End of functions

            base.OnModelCreating(modelBuilder);
        }
        public DbSet<MODELDB.MODELSegment> MODEL { get; set; }
        public DbSet<MODELDB.STOCKSegment> STOCK { get; set; }
        public DbSet<ResultRow> GnResult { get; set; }

        /// <summary>
        /// Calls the GN stored procedure
        /// </summary>
        /// <param name="sensitiveSegments">list (separated by ";") of sensitive segments</param>
        /// <param name="rid">The RID of the current segment. null if no current segment</param>
        /// <param name="segmName">The name of the current segment. null if no current segment</param>
        /// <param name="nbr">The number of segments to retreave</param>
        /// <returns>The 'nbr' next segments</returns>
        public System.Linq.IQueryable<ResultRow> GetGnResultRow(string sensitiveSegments, byte[] ckey, byte[] chid, string segmName, int nbr)
        {
            return GnResult.FromSqlRaw("select * from [MODELDB_GN]({0}, {1}, {2}, {3}, {4}) order by [order]", sensitiveSegments, ckey, chid, segmName, nbr);
        }

        /// <summary>
        /// Returns the name of the stored procedure that implement "ISRT" for segment 'segmentName'
        /// </summary>
        /// <param name="segmentName">Name of the segment</param>
        /// <returns>Name of the ISRT stored procedure</returns>
        public string GetIsrtStoredProcedure(string segmentName)
        {
            switch (segmentName)
            {
                case "MODEL":
                    return "MODELDB_ISRT_MODEL";
                case "STOCK":
                    return "MODELDB_ISRT_STOCK";
                default:
                    return null;
            }
        }

        /// <summary>
        /// Returns the name of the stored procedure that implement "REPL" for segment 'segmentName'
        /// </summary>
        /// <param name="segmentName">Name of the segment</param>
        /// <returns>Name of the REPL stored procedure</returns>
        public string GetReplStoredProcedure(string segmentName)
        {
            switch (segmentName)
            {
                case "MODEL":
                    return "MODELDB_REPL_MODEL";
                case "STOCK":
                    return "MODELDB_REPL_STOCK";
                default:
                    return null;
            }
        }

        /// <summary>
        /// Returns the name of the stored procedure that implement "GU" for the SSA's 'listSSA'
        /// </summary>
        /// <param name="listSSA"></param>
        /// <returns>Name of the stored procedure</returns>
        public (string, byte[]) GetGuStoredProcedure(List<Ssa> listSSA)
        {
            if (listSSA.Count == 1
                && listSSA[0].SegmentName == "MODEL" && listSSA[0].Qualifications.Count == 1 && listSSA[0].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[0].Qualifications[0].FieldName == "MODTYPE" && listSSA[0].CommandCodes == null
                  )
            {
                byte[] keys = new byte[20];
                System.Buffer.BlockCopy(listSSA[0].Qualifications[0].Value, 0, keys, 0, 20);
                return ("MODELDB_GU_MODEL", keys);
            }
            if (listSSA.Count == 2
                && listSSA[0].SegmentName == "MODEL" && listSSA[0].Qualifications.Count == 1 && listSSA[0].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[0].Qualifications[0].FieldName == "MODTYPE" && listSSA[0].CommandCodes == null
                && listSSA[1].SegmentName == "STOCK" && listSSA[1].Qualifications.Count == 1 && listSSA[1].Qualifications[0].Operator == Ssa.Qualification.OperatorEnum.EQ && listSSA[1].Qualifications[0].FieldName == "STKVIN" && listSSA[1].CommandCodes == null
                  )
            {
                byte[] keys = new byte[40];
                System.Buffer.BlockCopy(listSSA[0].Qualifications[0].Value, 0, keys, 0, 20);
                System.Buffer.BlockCopy(listSSA[1].Qualifications[0].Value, 0, keys, 20, 20);
                return ("MODELDB_GU_STOCK", keys);
            }
            return (null, null);
        }
        [Table("MODELDB_MODEL")]
        public class MODELSegment {

            [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("HID")]
            public byte[] HID { get; set; }

            [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int RID { get; set; }

            public static string SegmentName { get; } = "MODEL";
            // DBD size definition (min:0, max:260, sum: 260)
            public static int Size { get; } = 260;
            public static string[] Fields { get; } = new [] { "MODTYPE", "BRAND", "DETAILS"  };
            public static int[] FieldsSize { get; } = new [] { 20, 40, 200  };
            public static int KeyStartIndex { get; } = 1;
            public static int KeySize { get; } = 20;
            public static string KeyField { get; } = "MODTYPE";
                //1=Unique sequence, 2=non-unique sequence, 3=no sequence
            public static int KeyType { get; } = 1;
            public static string SortKey { get; } = "MODTYPE";
            public static int SortKeySize { get; } = 20;
            public static string Rule { get; } = "LAST";
            public static bool HasChild { get; } = true;
            public static string ParentName { get; } = null;
            public static string LogicalParentDbd { get; } = null;
            public static string LogicalParentSegment { get; } = null;
            public static int LogicalParentCkeySize { get; } = 0;
            public static int SegmentLevel { get; } = 0;
            // true if the segment is a child of the destination parent of a concatenated segment

            public static string Ims2Csharp(string imsName)
            {
                return imsName;
            }

            [MaxLength(260)]
            public byte[] Data { get; set; }

            // MODTYPE => Data[0..21] Length : 20 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("MODTYPE", Order =0)]
            public byte[] MODTYPE { get; set; }

            // BRAND => Data[20..61] Length : 40 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("BRAND", Order =1)]
            public byte[] BRAND { get; set; }

            // DETAILS => Data[60..261] Length : 200 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("DETAILS", Order =2)]
            public byte[] DETAILS { get; set; }


        }

        [Table("MODELDB_STOCK")]
        public class STOCKSegment {

            [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("HID")]
            public byte[] HID { get; set; }

            [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int RID { get; set; }

            public static string SegmentName { get; } = "STOCK";
            // DBD size definition (min:0, max:220, sum: 220)
            public static int Size { get; } = 220;
            public static string[] Fields { get; } = new [] { "STKVIN", "DETAILS"  };
            public static int[] FieldsSize { get; } = new [] { 20, 200  };
            public static int KeyStartIndex { get; } = 1;
            public static int KeySize { get; } = 20;
            public static string KeyField { get; } = "STKVIN";
                //1=Unique sequence, 2=non-unique sequence, 3=no sequence
            public static int KeyType { get; } = 1;
            public static string SortKey { get; } = "STKVIN";
            public static int SortKeySize { get; } = 20;
            public static string Rule { get; } = "LAST";
            public static bool HasChild { get; } = false;
            public static string ParentName { get; } = "MODEL";
            public static string LogicalParentDbd { get; } = null;
            public static string LogicalParentSegment { get; } = null;
            public static int LogicalParentCkeySize { get; } = 0;
            public static int SegmentLevel { get; } = 1;
            // true if the segment is a child of the destination parent of a concatenated segment

            public static string Ims2Csharp(string imsName)
            {
                return imsName;
            }

            [MaxLength(220)]
            public byte[] Data { get; set; }

            public MODELDB.MODELSegment Parent { get; set; }

            [ForeignKey("Parent")]
            [Required]
            public int PID { get; set; }

            // STKVIN => Data[0..21] Length : 20 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("STKVIN", Order =0)]
            public byte[] STKVIN { get; set; }

            // DETAILS => Data[20..221] Length : 200 Type : C
            [ReadOnly(true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            [Column("DETAILS", Order =1)]
            public byte[] DETAILS { get; set; }


        }

    }
#endregion // MODELDB
}
