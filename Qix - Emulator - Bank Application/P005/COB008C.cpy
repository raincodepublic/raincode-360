      *****************************************************************
      *                                                               *
      *   INPUT AMOUNT VALIDATION                                     *
      *                                                               *
      *   Validate a monetary amount that is entered by a user and    *
      *   return a result in the form of a display numeric number.    *
      *   If the input string contains a comma, it must also contain  *
      *   one or two decimals following it.                           *
      *   Thousand separators and signs are not accepted.             *
      *****************************************************************
       01 COB008C.
           10 COB008C-IN.
    15         20 COB008C-INFIELD PIC X(15).
           10 COB008C-OUT.
     2         20 COB008C-ERRORCODE PIC S9(4) COMP.
    14         20 COB008C-OUTFIELD PIC 9(12)V9(2).
------
    31
