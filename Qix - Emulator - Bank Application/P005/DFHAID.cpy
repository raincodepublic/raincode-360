************************************************************************
******* Standard DFHAID.CPY copybook, defining symbolic constants to
******* represent keys returned by CICS. 
************************************************************************
       01  DFHAID.
           05 DFHNULL  PIC X(1) VALUE X'00'.                    
           05 DFHENTER PIC X(1) VALUE X'7D'.                    
           05 DFHCLEAR PIC X(1) VALUE X'6D'.                    
           05 DFHCLRP  PIC X(1) VALUE X'6A'.
           05 DFHPEN   PIC X(1) VALUE X'7E'.                    
           05 DFHOPID  PIC X(1) VALUE X'E6'.                    
           05 DFHMSRE  PIC X(1) VALUE X'E7'.                    
           05 DFHSTRF  PIC X(1) VALUE X'88'.                    
           05 DFHTRIG  PIC X(1) VALUE X'7F'.                    
           05 DFHPA1   PIC X(1) VALUE X'6C'.                    
           05 DFHPA2   PIC X(1) VALUE X'6E'.                    
           05 DFHPA3   PIC X(1) VALUE X'6B'.                    
           05 DFHPA4   PIC X(1) VALUE X'84'.                    
           05 DFHPA5   PIC X(1) VALUE X'85'.                    
           05 DFHPA6   PIC X(1) VALUE X'86'.                    
           05 DFHPA7   PIC X(1) VALUE X'87'.                    
           05 DFHPA8   PIC X(1) VALUE X'88'.                    
           05 DFHPA9   PIC X(1) VALUE X'89'.                    
           05 DFHPA10  PIC X(1) VALUE X'91'.                    
           05 DFHPF1   PIC X(1) VALUE X'F1'.                    
           05 DFHPF2   PIC X(1) VALUE X'F2'.      
           05 DFHPF3   PIC X(1) VALUE X'F3'.      
           05 DFHPF4   PIC X(1) VALUE X'F4'.      
           05 DFHPF5   PIC X(1) VALUE X'F5'.      
           05 DFHPF6   PIC X(1) VALUE X'F6'.      
           05 DFHPF7   PIC X(1) VALUE X'F7'.      
           05 DFHPF8   PIC X(1) VALUE X'F8'.      
           05 DFHPF9   PIC X(1) VALUE X'F9'.      
           05 DFHPF10  PIC X(1) VALUE X'7A'.      
           05 DFHPF11  PIC X(1) VALUE X'7B'.      
           05 DFHPF12  PIC X(1) VALUE X'7C'.      
           05 DFHPF13  PIC X(1) VALUE X'C1'.      
           05 DFHPF14  PIC X(1) VALUE X'C2'.      
           05 DFHPF15  PIC X(1) VALUE X'C3'.      
           05 DFHPF16  PIC X(1) VALUE X'C4'.      
           05 DFHPF17  PIC X(1) VALUE X'C5'.      
           05 DFHPF18  PIC X(1) VALUE X'C6'.      
           05 DFHPF19  PIC X(1) VALUE X'C7'.      
           05 DFHPF20  PIC X(1) VALUE X'C8'.      
           05 DFHPF21  PIC X(1) VALUE X'C9'.      
           05 DFHPF22  PIC X(1) VALUE X'4A'.  
           05 DFHPF23  PIC X(1) VALUE X'4B'.  
           05 DFHPF24  PIC X(1) VALUE X'4C'.
