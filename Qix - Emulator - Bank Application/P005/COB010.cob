      ***********************************************************
      *                                                         *
      *   ACCOUNT LIST - SCREEN HANDLING                        *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB010.                                               
       DATA DIVISION.                                                   
       WORKING-STORAGE SECTION.          
       COPY COB010.
       COPY DFHAID.
       COPY DFHBMSCA.
       COPY COMMAREA.
       COPY COB010A.
       01 W-LINE.
         10 ACCOUNTNUMBER PIC X(19).
         10 FILLER PIC X.
         10 LASTNAME PIC X(16).
         10 FILLER PIC X.
         10 FIRSTNAME PIC X(16).
         10 FILLER PIC X.
         10 ZIPCODE PIC XXXX.
         10 FILLER PIC X.
         10 CITY PIC X(15).
       01 W-LINECOUNT PIC S9(4).
       01 W-ATTR PIC X.
       01 W-ROW PIC 99.
       01 W-SEL PIC 99.
       01 W-COLUMN PIC 99.
       01 W-MSG PIC X(80).
       01 W-EIBCPOSN PIC S9(4) COMP VALUE 0.
       01 W-I PIC S9(4) COMP.
       01 W-FILTERCHANGE PIC 9 VALUE 0.
       LINKAGE SECTION.                                                 
       COPY DFHCOMM.                                 
       PROCEDURE DIVISION.
       EXEC CICS IGNORE CONDITION MAPFAIL END-EXEC
       IF EIBCALEN > 0
           MOVE DFHCOMMAREA TO W-COMMAREA
       END-IF
       EVALUATE TRUE
       WHEN EIBCALEN = 0
       WHEN EIBTRNID = 'C008'
           INITIALIZE C010
           PERFORM F8
       WHEN EIBAID = DFHPF8
           PERFORM GET-MAP
           IF W-FILTERCHANGE > 0
               MOVE SPACES TO C010-ACCOUNTNUMBER-LAST
           END-IF
           PERFORM F8
       WHEN EIBAID = DFHPF7
           PERFORM GET-MAP
           IF W-FILTERCHANGE > 0
               MOVE SPACES TO C010-ACCOUNTNUMBER-LAST
               PERFORM F8
           ELSE
               PERFORM F7
           END-IF
       WHEN EIBAID = DFHENTER
           PERFORM GET-MAP
           DIVIDE EIBCPOSN BY 80 GIVING W-ROW REMAINDER W-COLUMN
           ADD 1 TO W-ROW W-COLUMN
           MOVE W-ROW TO W-SEL
           SUBTRACT 4 FROM W-SEL 
           IF W-SEL > 0 AND 
             W-SEL < 19 AND 
             C010-ACCOUNTNUMBER(W-SEL) > SPACES
               MOVE C010-ACCOUNTNUMBER(W-SEL) 
                 TO C010-ACCOUNTNUMBER-SELECTED
               EXEC CICS XCTL
                   PROGRAM('COB008') COMMAREA(W-COMMAREA)
               END-EXEC 
           ELSE
               IF W-FILTERCHANGE > 0
                   MOVE SPACES TO C010-ACCOUNTNUMBER-LAST
                   PERFORM F8
               END-IF
           END-IF 
       WHEN EIBAID = DFHPF3
           EXEC CICS XCTL
               PROGRAM('COB008') COMMAREA(W-COMMAREA)
           END-EXEC 
       WHEN EIBAID = DFHCLEAR
           EXEC CICS SEND CONTROL ERASE FREEKB
           END-EXEC
           EXEC CICS RETURN
           END-EXEC
       END-EVALUATE
       EXEC CICS SEND CONTROL FREEKB
       END-EXEC
       EXEC CICS RETURN 
           TRANSID ('C010') COMMAREA (W-COMMAREA)
       END-EXEC
       GOBACK                                       
       .    
       PUT-FILTER.
           IF C010-ACCOUNTNUMBER-FILTER > SPACES
               MOVE C010-ACCOUNTNUMBER-FILTER TO FACCO
           END-IF
           IF C010-LASTNAME-FILTER > SPACES
               MOVE C010-LASTNAME-FILTER TO FLASTO
           END-IF
           IF C010-FIRSTNAME-FILTER > SPACES
           MOVE C010-FIRSTNAME-FILTER TO FFIRSTO
           END-IF
           IF C010-ZIPCODE-FILTER > SPACES
           MOVE C010-ZIPCODE-FILTER TO FZIPO
           END-IF
           IF C010-CITY-FILTER > SPACES
           MOVE C010-CITY-FILTER TO FCITYO
           END-IF
       .
       GET-MAP.
       EXEC CICS RECEIVE
           MAP('MAP001')
           MAPSET('COB010')
       END-EXEC
       IF FACCL > 0 OR FACCF = DFHBMEF OR FACCF = DFHBMEOF
           MOVE FACCI TO C010-ACCOUNTNUMBER-FILTER
           INSPECT C010-ACCOUNTNUMBER-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       IF FLASTL > 0 OR FLASTF = DFHBMEF OR FLASTF = DFHBMEOF
           MOVE FLASTI TO C010-LASTNAME-FILTER
           INSPECT C010-LASTNAME-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       IF FFIRSTL > 0  OR FFIRSTF = DFHBMEF OR FFIRSTF = DFHBMEOF
           MOVE FFIRSTI TO C010-FIRSTNAME-FILTER
           INSPECT C010-FIRSTNAME-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       IF FZIPL > 0  OR FZIPF = DFHBMEF OR FZIPF = DFHBMEOF
           MOVE FZIPI TO C010-ZIPCODE-FILTER
           INSPECT C010-ZIPCODE-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       IF FCITYL > 0 OR FCITYF = DFHBMEF OR FCITYF = DFHBMEOF
           MOVE FCITYI TO C010-CITY-FILTER
           INSPECT C010-CITY-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       .
       F8.
       SET COB010A-DOWN TO TRUE
       PERFORM GET-LINES
       .
       F7.
       SET COB010A-UP TO TRUE
       PERFORM GET-LINES
       IF COB010A-SQLCODE = 0
           AND COB010A-FETCHED > 0
           AND COB010A-FETCHED < COB010A-LINECOUNT
               MOVE 0 TO C010-ACCOUNTNUMBER-FIRST
               PERFORM F8
       END-IF
       .
       GET-LINES.
       MOVE LOW-VALUE TO MAP001O
       PERFORM PUT-FILTER
       MOVE 18 TO COB010A-LINECOUNT
       MOVE C010-ACCOUNTNUMBER-FIRST TO COB010A-ACCOUNTNUMBER-FIRST
       MOVE C010-ACCOUNTNUMBER-LAST TO COB010A-ACCOUNTNUMBER-LAST
       MOVE C010-ACCOUNTNUMBER-FILTER TO COB010A-ACCOUNTNUMBER-FILTER
       MOVE C010-LASTNAME-FILTER TO COB010A-LASTNAME-FILTER
       MOVE C010-FIRSTNAME-FILTER TO COB010A-FIRSTNAME-FILTER
       MOVE C010-ZIPCODE-FILTER TO COB010A-ZIPCODE-FILTER
       MOVE C010-CITY-FILTER TO COB010A-CITY-FILTER
       EXEC CICS
           LINK PROGRAM('COB010A') COMMAREA(COB010A)
       END-EXEC
       IF COB010A-SQLCODE = 0
           IF COB010A-FETCHED > 0
               MOVE COB010A-ACCOUNTNUMBER(1) TO C010-ACCOUNTNUMBER-FIRST
               MOVE COB010A-ACCOUNTNUMBER(COB010A-FETCHED) 
                 TO C010-ACCOUNTNUMBER-LAST
               PERFORM VARYING W-I FROM 1 BY 1 UNTIL W-I > 18
                   IF W-I <= COB010A-FETCHED
                       MOVE SPACE TO C010-SELA(W-I)
                       MOVE COB010A-ACCOUNTNUMBER(W-I) 
                         TO C010-ACCOUNTNUMBER(W-I)
                       MOVE COB010A-ACCOUNTNUMBER(W-I) TO ACCOUNTNUMBER
                       MOVE COB010A-LASTNAME(W-I) TO LASTNAME
                       MOVE COB010A-FIRSTNAME(W-I) TO FIRSTNAME
                       MOVE COB010A-ZIPCODE(W-I) TO ZIPCODE
                       MOVE COB010A-CITY(W-I) TO CITY
                   ELSE
                       MOVE DFHPROTN TO C010-SELA(W-I)
                       MOVE SPACE TO W-LINE
                   END-IF
                   PERFORM PUT-LINE
               END-PERFORM
               PERFORM PUT-SEL
               IF W-EIBCPOSN > 0
                   EXEC CICS SEND 
                       MAP('MAP001')
                       MAPSET('COB010') 
                       ERASE 
                       CURSOR(W-EIBCPOSN)
                   END-EXEC
               ELSE
                   EXEC CICS SEND 
                       MAP('MAP001')
                       MAPSET('COB010') 
                       ERASE 
                   END-EXEC
               END-IF
           ELSE
               IF EIBAID = DFHENTER
                   MOVE SPACES TO 
                       L01O L02O L03O L04O L05O L06O L07O L08O L09O 
                       L10O L11O L12O L13O L14O L15O L16O L17O L18O
                   PERFORM VARYING W-I FROM 1 BY 1 UNTIL W-I > 18
                       MOVE DFHPROTN TO C010-SELA(W-I)
                   END-PERFORM
                   PERFORM PUT-SEL
                   MOVE 'No matches' TO W-MSG
               ELSE
                   IF COB010A-DOWN
                       MOVE 'Bottom of list' TO W-MSG
                   ELSE
                       MOVE 'Top of list' TO W-MSG
                   END-IF
               END-IF
               PERFORM PUT-MSG
           END-IF
       ELSE
           MOVE 'COB010A-SQLCODE=' TO W-MSG
           MOVE COB010A-SQLCODE TO W-MSG(17:)
           PERFORM PUT-MSG
       END-IF
       .
       PUT-SEL.
       MOVE C010-SELA(01) TO S01A 
       MOVE C010-SELA(02) TO S02A 
       MOVE C010-SELA(03) TO S03A 
       MOVE C010-SELA(04) TO S04A 
       MOVE C010-SELA(05) TO S05A 
       MOVE C010-SELA(06) TO S06A 
       MOVE C010-SELA(07) TO S07A 
       MOVE C010-SELA(08) TO S08A 
       MOVE C010-SELA(09) TO S09A 
       MOVE C010-SELA(10) TO S10A 
       MOVE C010-SELA(11) TO S11A 
       MOVE C010-SELA(12) TO S12A 
       MOVE C010-SELA(13) TO S13A 
       MOVE C010-SELA(14) TO S14A 
       MOVE C010-SELA(15) TO S15A 
       MOVE C010-SELA(16) TO S16A 
       MOVE C010-SELA(17) TO S17A 
       MOVE C010-SELA(18) TO S18A 
       .
       PUT-LINE.
       EVALUATE W-I
       WHEN 1 MOVE W-LINE TO L01O
       WHEN 2 MOVE W-LINE TO L02O
       WHEN 3 MOVE W-LINE TO L03O
       WHEN 4 MOVE W-LINE TO L04O
       WHEN 5 MOVE W-LINE TO L05O
       WHEN 6 MOVE W-LINE TO L06O
       WHEN 7 MOVE W-LINE TO L07O
       WHEN 8 MOVE W-LINE TO L08O
       WHEN 9 MOVE W-LINE TO L09O
       WHEN 10 MOVE W-LINE TO L10O
       WHEN 11 MOVE W-LINE TO L11O
       WHEN 12 MOVE W-LINE TO L12O
       WHEN 13 MOVE W-LINE TO L13O
       WHEN 14 MOVE W-LINE TO L14O
       WHEN 15 MOVE W-LINE TO L15O
       WHEN 16 MOVE W-LINE TO L16O
       WHEN 17 MOVE W-LINE TO L17O
       WHEN 18 MOVE W-LINE TO L18O
       END-EVALUATE
       .
       PUT-MSG.
       MOVE W-MSG TO MSGO
       PERFORM PUT-SEL
       EXEC CICS SEND 
           MAP('MAP001')
           MAPSET('COB010') 
           DATAONLY 
       END-EXEC
       .

