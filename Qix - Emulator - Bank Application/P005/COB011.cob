      ***********************************************************
      *                                                         *
      *   GET ACCOUNT BALANCE                                   *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB011.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
       DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       COPY COMMAREA.
       01 W-AMOUNT-NP3 PIC S9(8)V9(2) COMP-3.
       01 W-MSG PIC X(70).
       LINKAGE SECTION.
       COPY DFHCOMM.
       PROCEDURE DIVISION.
       BEGIN.
           MOVE DFHCOMMAREA TO W-COMMAREA
           PERFORM GET-ACCOUNT
           DISPLAY C007-ACCOUNTNUMBER ' ' C007-AMOUNT
           MOVE W-COMMAREA TO DFHCOMMAREA(1:EIBCALEN)
           EXEC CICS RETURN END-EXEC
           GOBACK.
      
       GET-ACCOUNT.
           EXEC SQL
             SELECT Amount
             INTO :W-AMOUNT-NP3
             FROM dbo.Account
             WHERE Accountnumber = :C007-ACCOUNTNUMBER
           END-EXEC
           EVALUATE SQLCODE
           WHEN 0
             MOVE W-AMOUNT-NP3 TO C007-AMOUNT
           WHEN OTHER
             MOVE 'SELECT ACCOUNT: SQLCODE:' TO W-MSG
             MOVE SQLCODE TO W-MSG(24:)
             DISPLAY W-MSG
           END-EVALUATE.
