        01 MAP001I .
            02 FILLER PIC X(12).
            02 CUSTL PIC S9(4) COMP-5.
            02 CUSTF PIC X(1).
            02 FILLER REDEFINES CUSTF.
              04 CUSTA PIC X.
            02 CUSTI PIC X(6).
            02 LASTL PIC S9(4) COMP-5.
            02 LASTF PIC X(1).
            02 FILLER REDEFINES LASTF.
              04 LASTA PIC X.
            02 LASTI PIC X(40).
            02 FIRSTL PIC S9(4) COMP-5.
            02 FIRSTF PIC X(1).
            02 FILLER REDEFINES FIRSTF.
              04 FIRSTA PIC X.
            02 FIRSTI PIC X(40).
            02 ZIPSELL PIC S9(4) COMP-5.
            02 ZIPSELF PIC X(1).
            02 FILLER REDEFINES ZIPSELF.
              04 ZIPSELA PIC X.
            02 ZIPSELI PIC X(1).
            02 ZIPL PIC S9(4) COMP-5.
            02 ZIPF PIC X(1).
            02 FILLER REDEFINES ZIPF.
              04 ZIPA PIC X.
            02 ZIPI PIC X(10).
            02 CITYL PIC S9(4) COMP-5.
            02 CITYF PIC X(1).
            02 FILLER REDEFINES CITYF.
              04 CITYA PIC X.
            02 CITYI PIC X(20).
            02 BDATEL PIC S9(4) COMP-5.
            02 BDATEF PIC X(1).
            02 FILLER REDEFINES BDATEF.
              04 BDATEA PIC X.
            02 BDATEI PIC X(10).
            02 ACCSEL1L PIC S9(4) COMP-5.
            02 ACCSEL1F PIC X(1).
            02 FILLER REDEFINES ACCSEL1F.
              04 ACCSEL1A PIC X.
            02 ACCSEL1I PIC X(1).
            02 ACCNM1L PIC S9(4) COMP-5.
            02 ACCNM1F PIC X(1).
            02 FILLER REDEFINES ACCNM1F.
              04 ACCNM1A PIC X.
            02 ACCNM1I PIC X(16).
            02 ACC1L PIC S9(4) COMP-5.
            02 ACC1F PIC X(1).
            02 FILLER REDEFINES ACC1F.
              04 ACC1A PIC X.
            02 ACC1I PIC X(40).
            02 ACCSEL2L PIC S9(4) COMP-5.
            02 ACCSEL2F PIC X(1).
            02 FILLER REDEFINES ACCSEL2F.
              04 ACCSEL2A PIC X.
            02 ACCSEL2I PIC X(1).
            02 ACCNM2L PIC S9(4) COMP-5.
            02 ACCNM2F PIC X(1).
            02 FILLER REDEFINES ACCNM2F.
              04 ACCNM2A PIC X.
            02 ACCNM2I PIC X(16).
            02 ACC2L PIC S9(4) COMP-5.
            02 ACC2F PIC X(1).
            02 FILLER REDEFINES ACC2F.
              04 ACC2A PIC X.
            02 ACC2I PIC X(40).
            02 ACCSEL3L PIC S9(4) COMP-5.
            02 ACCSEL3F PIC X(1).
            02 FILLER REDEFINES ACCSEL3F.
              04 ACCSEL3A PIC X.
            02 ACCSEL3I PIC X(1).
            02 ACCNM3L PIC S9(4) COMP-5.
            02 ACCNM3F PIC X(1).
            02 FILLER REDEFINES ACCNM3F.
              04 ACCNM3A PIC X.
            02 ACCNM3I PIC X(16).
            02 ACC3L PIC S9(4) COMP-5.
            02 ACC3F PIC X(1).
            02 FILLER REDEFINES ACC3F.
              04 ACC3A PIC X.
            02 ACC3I PIC X(40).
            02 ACCSEL4L PIC S9(4) COMP-5.
            02 ACCSEL4F PIC X(1).
            02 FILLER REDEFINES ACCSEL4F.
              04 ACCSEL4A PIC X.
            02 ACCSEL4I PIC X(1).
            02 ACCNM4L PIC S9(4) COMP-5.
            02 ACCNM4F PIC X(1).
            02 FILLER REDEFINES ACCNM4F.
              04 ACCNM4A PIC X.
            02 ACCNM4I PIC X(16).
            02 ACC4L PIC S9(4) COMP-5.
            02 ACC4F PIC X(1).
            02 FILLER REDEFINES ACC4F.
              04 ACC4A PIC X.
            02 ACC4I PIC X(40).
            02 ACCSEL5L PIC S9(4) COMP-5.
            02 ACCSEL5F PIC X(1).
            02 FILLER REDEFINES ACCSEL5F.
              04 ACCSEL5A PIC X.
            02 ACCSEL5I PIC X(1).
            02 ACCNM5L PIC S9(4) COMP-5.
            02 ACCNM5F PIC X(1).
            02 FILLER REDEFINES ACCNM5F.
              04 ACCNM5A PIC X.
            02 ACCNM5I PIC X(16).
            02 ACC5L PIC S9(4) COMP-5.
            02 ACC5F PIC X(1).
            02 FILLER REDEFINES ACC5F.
              04 ACC5A PIC X.
            02 ACC5I PIC X(40).
            02 MSGL PIC S9(4) COMP-5.
            02 MSGF PIC X(1).
            02 FILLER REDEFINES MSGF.
              04 MSGA PIC X.
            02 MSGI PIC X(70).
        01 MAP001O REDEFINES MAP001I.
            02 FILLER PIC X(12).
            02 FILLER PIC X(3).
            02 CUSTO PIC X(6).
            02 FILLER PIC X(3).
            02 LASTO PIC X(40).
            02 FILLER PIC X(3).
            02 FIRSTO PIC X(40).
            02 FILLER PIC X(3).
            02 ZIPSELO PIC X(1).
            02 FILLER PIC X(3).
            02 ZIPO PIC X(10).
            02 FILLER PIC X(3).
            02 CITYO PIC X(20).
            02 FILLER PIC X(3).
            02 BDATEO PIC X(10).
            02 FILLER PIC X(3).
            02 ACCSEL1O PIC X(1).
            02 FILLER PIC X(3).
            02 ACCNM1O PIC X(16).
            02 FILLER PIC X(3).
            02 ACC1O PIC X(40).
            02 FILLER PIC X(3).
            02 ACCSEL2O PIC X(1).
            02 FILLER PIC X(3).
            02 ACCNM2O PIC X(16).
            02 FILLER PIC X(3).
            02 ACC2O PIC X(40).
            02 FILLER PIC X(3).
            02 ACCSEL3O PIC X(1).
            02 FILLER PIC X(3).
            02 ACCNM3O PIC X(16).
            02 FILLER PIC X(3).
            02 ACC3O PIC X(40).
            02 FILLER PIC X(3).
            02 ACCSEL4O PIC X(1).
            02 FILLER PIC X(3).
            02 ACCNM4O PIC X(16).
            02 FILLER PIC X(3).
            02 ACC4O PIC X(40).
            02 FILLER PIC X(3).
            02 ACCSEL5O PIC X(1).
            02 FILLER PIC X(3).
            02 ACCNM5O PIC X(16).
            02 FILLER PIC X(3).
            02 ACC5O PIC X(40).
            02 FILLER PIC X(3).
            02 MSGO PIC X(70).
