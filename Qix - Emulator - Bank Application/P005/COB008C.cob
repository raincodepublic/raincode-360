      *****************************************************************
      *                                                               *
      *   INPUT AMOUNT VALIDATION                                     *
      *                                                               *
      *   Validate a monetary amount that is entered by a user and    *
      *   return a result in the form of a display numeric number.    *
      *   If the input string contains a comma, it must also contain  *
      *   one or two decimals following it.                           *
      *   Thousand separators and signs are not accepted.             *
      *****************************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB008C.        
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
       DECIMAL-POINT IS COMMA.                                       
       DATA DIVISION.                                                   
       WORKING-STORAGE SECTION.   
       COPY COB008C.       
       10 LEN-IN PIC S9(4) COMP.
       10 LEN-OUT PIC S9(4) COMP.
       10 C0 PIC S9(4) COMP.
       10 N1 PIC S9(4) COMP.
       10 N2 PIC S9(4) COMP.
       10 I PIC S9(4) COMP.
       LINKAGE SECTION.                                                 
       01 DFHCOMMAREA PIC X(31).                                 
       PROCEDURE DIVISION.
       MOVE DFHCOMMAREA TO COB008C
       PERFORM VALIDATE-AMOUNT
       MOVE COB008C TO DFHCOMMAREA
       EXEC CICS RETURN
       END-EXEC
       .    
       VALIDATE-AMOUNT.
       MOVE 0 TO COB008C-ERRORCODE
       MOVE LENGTH OF COB008C-INFIELD TO LEN-IN
       MOVE LENGTH OF COB008C-OUTFIELD TO LEN-OUT
       MOVE 0 TO C0 N1 N2
       IF COB008C-INFIELD > SPACES
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > LEN-IN
               EVALUATE COB008C-INFIELD(I:1)
               WHEN '0' THRU '9'
               WHEN ' '
                   CONTINUE
               WHEN ','
                   IF C0 = 0
                       MOVE I TO C0
                   ELSE
                       MOVE 1 TO COB008C-ERRORCODE
                   END-IF
               WHEN OTHER
                   MOVE 2 TO COB008C-ERRORCODE
               END-EVALUATE
           END-PERFORM
       ELSE
           MOVE 3 TO COB008C-ERRORCODE
       END-IF
      
       IF COB008C-ERRORCODE = 0
           IF C0 = 1 OR C0 = LEN-IN
               MOVE 4 TO COB008C-ERRORCODE
           END-IF
       END-IF
      
       IF COB008C-ERRORCODE = 0
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > LEN-IN OR N1 > 0
               EVALUATE COB008C-INFIELD(I:1)
               WHEN '0' THRU '9'
                   MOVE I TO N1 
               END-EVALUATE
           END-PERFORM
           IF N1 = 0
               MOVE 5 TO COB008C-ERRORCODE
           END-IF
       END-IF
      
       IF COB008C-ERRORCODE = 0
           PERFORM VARYING I FROM LEN-IN BY -1 UNTIL I < 1 OR N2 > 0
               EVALUATE COB008C-INFIELD(I:1)
               WHEN '0' THRU '9'
                   MOVE I TO N2 
               END-EVALUATE
           END-PERFORM
           IF N2 = 0
               MOVE 6 TO COB008C-ERRORCODE
           END-IF
       END-IF
      
       IF COB008C-ERRORCODE = 0
           IF C0 > 0
               IF N2 - C0 > 2
                   MOVE 7 TO COB008C-ERRORCODE
               END-IF
           END-IF
       END-IF
      
       IF COB008C-ERRORCODE = 0
           MOVE ALL '0' TO COB008C-OUTFIELD
           IF C0 > 0
               MOVE COB008C-INFIELD(N1 : C0 - N1) 
                 TO COB008C-OUTFIELD(LEN-OUT - C0 + N1 - 1 : C0 - N1)
               MOVE COB008C-INFIELD(C0 + 1 : N2 - C0) 
                 TO COB008C-OUTFIELD(LEN-OUT - 1 : N2 - C0)
      	   ELSE
               MOVE COB008C-INFIELD(N1 : N2 - N1 + 1) 
                 TO COB008C-OUTFIELD
                   (LEN-OUT - N2 + N1 - 2 : N2 - N1 + 1)
           END-IF
       END-IF
       .
    