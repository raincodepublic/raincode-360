      ***********************************************************
      *                                                         *
      *   ZIP CODE SELECTION - SCREEN HANDLING                  *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB006.                                            
       DATA DIVISION.                                                   
       WORKING-STORAGE SECTION.          
       COPY COB006.
       COPY DFHAID.
       COPY DFHBMSCA.
       COPY SQLCA.
       COPY COMMAREA.
       01 W-LINE.
           10 W-ZIPCODE pic xxxx.
           10 filler pic x.
           10 W-city pic x(60).
           10 filler pic x.
       01 W-ATTR PIC X.
       01 W-ROW PIC 99.
       01 W-SEL PIC S9(4) COMP.
       01 W-COLUMN PIC 99.
       01 W-MSG PIC X(70).
       01 F-CITY.
           49 F-CITY-LEN PIC S9(4) COMP-5.
           49 F-CITY-TXT PIC X(60).
       EXEC SQL
           DECLARE CUR-F8 CURSOR FOR
           SELECT ZipCode, City
           FROM dbo.ZipCode
           WHERE ZIPCODE > :C006-LAST
           AND ZIPCODE LIKE RTRIM(:C006-ZIPCODE-FILTER) || '%'
           AND City LIKE RTRIM(:C006-CITY-FILTER) || '%'
           ORDER BY ZipCode
       END-EXEC.
       EXEC SQL
           DECLARE CUR-F7 CURSOR FOR
           SELECT ZipCode, City
           FROM dbo.ZipCode
           WHERE ZIPCODE < :C006-FIRST
           AND ZIPCODE LIKE RTRIM(:C006-ZIPCODE-FILTER) || '%'
           AND City LIKE RTRIM(:C006-CITY-FILTER) || '%'
           ORDER BY ZIPCODE desc
       END-EXEC.
       LINKAGE SECTION.                                                 
       COPY DFHCOMM.                                 
       PROCEDURE DIVISION.
       EXEC CICS IGNORE CONDITION MAPFAIL END-EXEC
       IF EIBCALEN > 0
           MOVE DFHCOMMAREA TO W-COMMAREA
       ELSE 
           MOVE LOW-VALUES TO W-COMMAREA
       END-IF
       EVALUATE TRUE
       WHEN EIBCALEN = 0
       WHEN EIBTRNID IS NOT EQUAL 'C006'
           INITIALIZE C006
           PERFORM F8
       WHEN EIBAID = DFHPF8
           PERFORM F8
       WHEN EIBAID = DFHPF7
           PERFORM F7
       WHEN EIBAID = DFHENTER
           PERFORM GET-MAP
           DIVIDE EIBCPOSN BY 80 GIVING W-ROW REMAINDER W-COLUMN
           ADD 1 TO W-ROW W-COLUMN
           MOVE W-ROW TO W-SEL
           SUBTRACT 4 FROM W-SEL 
           IF W-SEL > 0 AND W-SEL < 19
               IF C006-ZIPCODE(W-SEL) > SPACES
                   MOVE C006-ZIPCODE(W-SEL) TO C006-ZIPCODE-SELECTED
                   EXEC CICS XCTL
                       PROGRAM('COB005') COMMAREA(W-COMMAREA)
                   END-EXEC
               ELSE
                   MOVE 'Nothing to select' TO W-MSG
                   PERFORM PUT-MSG
               END-IF 
           ELSE
               MOVE SPACES TO C006-LAST
               PERFORM F8
      *         MOVE 'Invalid selection' TO W-MSG
      *         PERFORM PUT-MSG
           END-IF 
       WHEN EIBAID = DFHPF3
           EXEC CICS XCTL
               PROGRAM('COB005') COMMAREA(W-COMMAREA)
           END-EXEC 
       WHEN EIBAID = DFHCLEAR
           EXEC CICS SEND CONTROL ERASE FREEKB
           END-EXEC
           EXEC CICS RETURN
           END-EXEC
       WHEN OTHER
           EXEC CICS SEND CONTROL FREEKB
           END-EXEC
       END-EVALUATE
       EXEC CICS RETURN 
           TRANSID ('C006') COMMAREA (W-COMMAREA)
       END-EXEC
       GOBACK                                       
       .    
       GET-MAP.
       EXEC CICS RECEIVE
           MAP('MAP001')
           MAPSET('COB006')
       END-EXEC
       IF FZIPL > 0 
           MOVE FZIPI TO C006-ZIPCODE-FILTER
           INSPECT C006-ZIPCODE-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
       END-IF
       IF FCITYL > 0 
           MOVE FCITYI TO C006-CITY-FILTER
           INSPECT C006-CITY-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
       END-IF
       .
       F8.
       MOVE LOW-VALUE TO MAP001O
       PERFORM OPEN-F8
       EVALUATE SQLCODE
       WHEN 0
           PERFORM FETCH-F8
           IF SQLCODE = 100
               MOVE 'Bottom of list' TO MSGO
           END-IF
           MOVE W-ZIPCODE TO C006-ZIPCODE(1) C006-FIRST
           MOVE W-ATTR TO S01A C006-SELA(1)
           MOVE W-LINE TO L01O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(2)
           MOVE W-ATTR TO S02A C006-SELA(2)
           MOVE W-LINE TO L02O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(3)
           MOVE W-ATTR TO S03A C006-SELA(3)
           MOVE W-LINE TO L03O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(4)
           MOVE W-ATTR TO S04A C006-SELA(4)
           MOVE W-LINE TO L04O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(5)
           MOVE W-ATTR TO S05A C006-SELA(5)
           MOVE W-LINE TO L05O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(6)
           MOVE W-ATTR TO S06A C006-SELA(6)
           MOVE W-LINE TO L06O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(7)
           MOVE W-ATTR TO S07A C006-SELA(7)
           MOVE W-LINE TO L07O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(8)
           MOVE W-ATTR TO S08A C006-SELA(8)
           MOVE W-LINE TO L08O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(9)
           MOVE W-ATTR TO S09A C006-SELA(9)
           MOVE W-LINE TO L09O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(10)
           MOVE W-ATTR TO S10A C006-SELA(10)
           MOVE W-LINE TO L10O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(11)
           MOVE W-ATTR TO S11A C006-SELA(11)
           MOVE W-LINE TO L11O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(12)
           MOVE W-ATTR TO S12A C006-SELA(12)
           MOVE W-LINE TO L12O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(13)
           MOVE W-ATTR TO S13A C006-SELA(13)
           MOVE W-LINE TO L13O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(14)
           MOVE W-ATTR TO S14A C006-SELA(14)
           MOVE W-LINE TO L14O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(15)
           MOVE W-ATTR TO S15A C006-SELA(15)
           MOVE W-LINE TO L15O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(16)
           MOVE W-ATTR TO S16A C006-SELA(16)
           MOVE W-LINE TO L16O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(17)
           MOVE W-ATTR TO S17A C006-SELA(17)
           MOVE W-LINE TO L17O
           PERFORM FETCH-F8
           MOVE W-ZIPCODE TO C006-ZIPCODE(18)
           MOVE W-ATTR TO S18A C006-SELA(18)
           MOVE W-LINE TO L18O
           IF C006-ZIPCODE-FILTER > SPACES
               MOVE C006-ZIPCODE-FILTER TO FZIPO
           END-IF
           IF C006-CITY-FILTER > SPACES
               MOVE C006-CITY-FILTER TO FCITYO
           END-IF
           EXEC CICS SEND 
               MAP('MAP001')
               MAPSET('COB006') 
               ERASE 
      *        CURSOR
           END-EXEC
           PERFORM CLOSE-F8
       WHEN OTHER
           MOVE 'OPEN CUR-F8:' TO W-MSG
           MOVE SQLCODE TO W-MSG(14:)
       END-EVALUATE
       .
       OPEN-F8.
       EXEC SQL
         open CUR-F8
       END-EXEC
       .   
       FETCH-F8.
       IF SQLCODE = 0
         EXEC SQL
           FETCH CUR-F8 INTO
           :W-ZIPCODE,
           :F-CITY
         END-EXEC
         MOVE F-CITY-TXT(1:F-CITY-LEN) TO W-CITY
         EVALUATE SQLCODE
         WHEN 0
           MOVE W-ZIPCODE TO C006-LAST
           MOVE SPACE TO W-ATTR
         WHEN 100
           MOVE SPACES TO W-ZIPCODE
           MOVE SPACES TO W-LINE
           MOVE DFHPROTN TO W-ATTR
         WHEN OTHER
           MOVE 'FETCH CUR-F8: SQLCODE:' TO W-MSG
           MOVE SQLCODE TO W-MSG(24:)
         END-EVALUATE
       ELSE
         MOVE SPACES TO W-ZIPCODE
         MOVE SPACES TO W-LINE
         MOVE DFHPROTN TO W-ATTR
       END-IF
       .
       CLOSE-F8.
       EXEC SQL
         CLOSE CUR-F8
       END-EXEC
       .
       F7.
       MOVE LOW-VALUE TO MAP001O
       PERFORM OPEN-F7
       PERFORM FETCH-F7
       EVALUATE SQLCODE
       WHEN 0
           MOVE W-ZIPCODE TO C006-ZIPCODE(18) C006-LAST
           MOVE W-ATTR TO S18A C006-SELA(18)
           MOVE W-LINE TO L18O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(17)
           MOVE W-ATTR TO S17A C006-SELA(17)
           MOVE W-LINE TO L17O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(16)
           MOVE W-ATTR TO S16A C006-SELA(16)
           MOVE W-LINE TO L16O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(15)
           MOVE W-ATTR TO S15A C006-SELA(15)
           MOVE W-LINE TO L15O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(14)
           MOVE W-ATTR TO S14A C006-SELA(14)
           MOVE W-LINE TO L14O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(13)
           MOVE W-ATTR TO S13A C006-SELA(13)
           MOVE W-LINE TO L13O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(12)
           MOVE W-ATTR TO S12A C006-SELA(12)
           MOVE W-LINE TO L12O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(11)
           MOVE W-ATTR TO S11A C006-SELA(11)
           MOVE W-LINE TO L11O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(10)
           MOVE W-ATTR TO S10A C006-SELA(10)
           MOVE W-LINE TO L10O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(9)
           MOVE W-ATTR TO S09A C006-SELA(9)
           MOVE W-LINE TO L09O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(8)
           MOVE W-ATTR TO S08A C006-SELA(8)
           MOVE W-LINE TO L08O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(7)
           MOVE W-ATTR TO S07A C006-SELA(7)
           MOVE W-LINE TO L07O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(6)
           MOVE W-ATTR TO S06A C006-SELA(6)
           MOVE W-LINE TO L06O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(5)
           MOVE W-ATTR TO S05A C006-SELA(5)
           MOVE W-LINE TO L05O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(4)
           MOVE W-ATTR TO S04A C006-SELA(4)
           MOVE W-LINE TO L04O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(3)
           MOVE W-ATTR TO S03A C006-SELA(3)
           MOVE W-LINE TO L03O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(2)
           MOVE W-ATTR TO S02A C006-SELA(2)
           MOVE W-LINE TO L02O
           PERFORM FETCH-F7
           MOVE W-ZIPCODE TO C006-ZIPCODE(1)
           MOVE W-ATTR TO S01A C006-SELA(1)
           MOVE W-LINE TO L01O
           IF C006-ZIPCODE-FILTER > SPACES
               MOVE C006-ZIPCODE-FILTER TO FZIPO
           END-IF
           IF C006-CITY-FILTER > SPACES
               MOVE C006-CITY-FILTER TO FCITYO
           END-IF
           EXEC CICS SEND 
               MAP('MAP001')
               MAPSET('COB006') 
               ERASE 
      *        CURSOR
           END-EXEC
       WHEN 100
            MOVE 'Top of list' TO W-MSG
            PERFORM PUT-MSG
       END-EVALUATE
       PERFORM CLOSE-F7
       .
       OPEN-F7.
       EXEC SQL
         open CUR-F7
       end-exec
       .   
       FETCH-F7.
       IF SQLCODE = 0
         EXEC SQL
           FETCH CUR-F7 INTO
           :W-ZIPCODE,
           :W-CITY
         END-EXEC
         EVALUATE SQLCODE
         WHEN 0
           MOVE W-ZIPCODE TO C006-FIRST
           MOVE SPACE TO W-ATTR
         WHEN OTHER
           MOVE SPACES TO W-ZIPCODE
           MOVE SPACES TO W-LINE
           MOVE DFHPROTN TO W-ATTR
         END-EVALUATE
       ELSE
         MOVE SPACES TO W-ZIPCODE
         MOVE SPACES TO W-LINE
         MOVE DFHPROTN TO W-ATTR
       END-IF
       .
       CLOSE-F7.
       EXEC SQL
         CLOSE CUR-F7
       END-EXEC
       .
       PUT-MSG.
       MOVE LOW-VALUE TO MAP001O
       MOVE W-MSG TO MSGO
       MOVE C006-SELA(01) TO S01A 
       MOVE C006-SELA(02) TO S02A 
       MOVE C006-SELA(03) TO S03A 
       MOVE C006-SELA(04) TO S04A 
       MOVE C006-SELA(05) TO S05A 
       MOVE C006-SELA(06) TO S06A 
       MOVE C006-SELA(07) TO S07A 
       MOVE C006-SELA(08) TO S08A 
       MOVE C006-SELA(09) TO S09A 
       MOVE C006-SELA(10) TO S10A 
       MOVE C006-SELA(11) TO S11A 
       MOVE C006-SELA(12) TO S12A 
       MOVE C006-SELA(13) TO S13A 
       MOVE C006-SELA(14) TO S14A 
       MOVE C006-SELA(15) TO S15A 
       MOVE C006-SELA(16) TO S16A 
       MOVE C006-SELA(17) TO S17A 
       MOVE C006-SELA(18) TO S18A 
       EXEC CICS SEND 
           MAP('MAP001')
           MAPSET('COB006') 
           DATAONLY 
      *    CURSOR
       END-EXEC
       . 