      ***********************************************************
      *                                                         *
      *   ACCOUNT LIST                                          *
      *                                                         *
      ***********************************************************
       01 COB010A.
           10 COB010A-IN.
   1           20 COB010A-DIRECTION PIC X(01).
                   88 COB010A-DOWN VALUE 'D'.
                   88 COB010A-UP VALUE 'U'.
  20           20 COB010A-ACCOUNTNUMBER-FIRST PIC X(20). 
  20           20 COB010A-ACCOUNTNUMBER-LAST PIC X(20).
  20           20 COB010A-ACCOUNTNUMBER-FILTER PIC X(20).
  15           20 COB010A-LASTNAME-FILTER PIC X(15).
  15           20 COB010A-FIRSTNAME-FILTER PIC X(15).
   4           20 COB010A-ZIPCODE-FILTER PIC X(4).
  10           20 COB010A-CITY-FILTER PIC X(10).
   2           20 COB010A-LINECOUNT PIC S9(4) COMP.
           10 COB010A-OUT.
   4           20 COB010A-SQLCODE PIC S9(8) COMP-5.
   2           20 COB010A-FETCHED PIC S9(4) COMP.
 500           20 COB010A-ACCOUNTNUMBER PIC X(20) OCCURS 25.
 500           20 COB010A-LASTNAME PIC X(20) OCCURS 25.
 500           20 COB010A-FIRSTNAME PIC X(20) OCCURS 25.
 100           20 COB010A-ZIPCODE PIC X(4) OCCURS 25.
 500           20 COB010A-CITY PIC X(20) OCCURS 25.
----  
2213  
