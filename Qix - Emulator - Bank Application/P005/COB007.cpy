        01 MAP001I .
            02 FILLER PIC X(12).
            02 ACCTL PIC S9(4) COMP-5.
            02 ACCTF PIC X(1).
            02 FILLER REDEFINES ACCTF.
              04 ACCTA PIC X.
            02 ACCTI PIC X(19).
            02 BALL PIC S9(4) COMP-5.
            02 BALF PIC X(1).
            02 FILLER REDEFINES BALF.
              04 BALA PIC X.
            02 BALI PIC X(19).
            02 L01L PIC S9(4) COMP-5.
            02 L01F PIC X(1).
            02 FILLER REDEFINES L01F.
              04 L01A PIC X.
            02 L01I PIC X(80).
            02 L02L PIC S9(4) COMP-5.
            02 L02F PIC X(1).
            02 FILLER REDEFINES L02F.
              04 L02A PIC X.
            02 L02I PIC X(80).
            02 L03L PIC S9(4) COMP-5.
            02 L03F PIC X(1).
            02 FILLER REDEFINES L03F.
              04 L03A PIC X.
            02 L03I PIC X(80).
            02 L04L PIC S9(4) COMP-5.
            02 L04F PIC X(1).
            02 FILLER REDEFINES L04F.
              04 L04A PIC X.
            02 L04I PIC X(80).
            02 L05L PIC S9(4) COMP-5.
            02 L05F PIC X(1).
            02 FILLER REDEFINES L05F.
              04 L05A PIC X.
            02 L05I PIC X(80).
            02 L06L PIC S9(4) COMP-5.
            02 L06F PIC X(1).
            02 FILLER REDEFINES L06F.
              04 L06A PIC X.
            02 L06I PIC X(80).
            02 L07L PIC S9(4) COMP-5.
            02 L07F PIC X(1).
            02 FILLER REDEFINES L07F.
              04 L07A PIC X.
            02 L07I PIC X(80).
            02 L08L PIC S9(4) COMP-5.
            02 L08F PIC X(1).
            02 FILLER REDEFINES L08F.
              04 L08A PIC X.
            02 L08I PIC X(80).
            02 L09L PIC S9(4) COMP-5.
            02 L09F PIC X(1).
            02 FILLER REDEFINES L09F.
              04 L09A PIC X.
            02 L09I PIC X(80).
            02 L10L PIC S9(4) COMP-5.
            02 L10F PIC X(1).
            02 FILLER REDEFINES L10F.
              04 L10A PIC X.
            02 L10I PIC X(80).
            02 L11L PIC S9(4) COMP-5.
            02 L11F PIC X(1).
            02 FILLER REDEFINES L11F.
              04 L11A PIC X.
            02 L11I PIC X(80).
            02 L12L PIC S9(4) COMP-5.
            02 L12F PIC X(1).
            02 FILLER REDEFINES L12F.
              04 L12A PIC X.
            02 L12I PIC X(80).
            02 L13L PIC S9(4) COMP-5.
            02 L13F PIC X(1).
            02 FILLER REDEFINES L13F.
              04 L13A PIC X.
            02 L13I PIC X(80).
            02 L14L PIC S9(4) COMP-5.
            02 L14F PIC X(1).
            02 FILLER REDEFINES L14F.
              04 L14A PIC X.
            02 L14I PIC X(80).
            02 L15L PIC S9(4) COMP-5.
            02 L15F PIC X(1).
            02 FILLER REDEFINES L15F.
              04 L15A PIC X.
            02 L15I PIC X(80).
            02 MSGL PIC S9(4) COMP-5.
            02 MSGF PIC X(1).
            02 FILLER REDEFINES MSGF.
              04 MSGA PIC X.
            02 MSGI PIC X(80).
        01 MAP001O REDEFINES MAP001I.
            02 FILLER PIC X(12).
            02 FILLER PIC X(3).
            02 ACCTO PIC X(19).
            02 FILLER PIC X(3).
            02 BALO PIC X(19).
            02 FILLER PIC X(3).
            02 L01O PIC X(80).
            02 FILLER PIC X(3).
            02 L02O PIC X(80).
            02 FILLER PIC X(3).
            02 L03O PIC X(80).
            02 FILLER PIC X(3).
            02 L04O PIC X(80).
            02 FILLER PIC X(3).
            02 L05O PIC X(80).
            02 FILLER PIC X(3).
            02 L06O PIC X(80).
            02 FILLER PIC X(3).
            02 L07O PIC X(80).
            02 FILLER PIC X(3).
            02 L08O PIC X(80).
            02 FILLER PIC X(3).
            02 L09O PIC X(80).
            02 FILLER PIC X(3).
            02 L10O PIC X(80).
            02 FILLER PIC X(3).
            02 L11O PIC X(80).
            02 FILLER PIC X(3).
            02 L12O PIC X(80).
            02 FILLER PIC X(3).
            02 L13O PIC X(80).
            02 FILLER PIC X(3).
            02 L14O PIC X(80).
            02 FILLER PIC X(3).
            02 L15O PIC X(80).
            02 FILLER PIC X(3).
            02 MSGO PIC X(80).
