      ***********************************************************
      *                                                         *
      *   PAYMENT HISTORY                                       *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB007.                                              
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
       DECIMAL-POINT IS COMMA.                                       
       DATA DIVISION.                                                   
       WORKING-STORAGE SECTION.          
       COPY COB007.
       COPY DFHAID.
       COPY DFHBMSCA.
       COPY SQLCA.
       COPY COMMAREA.
       01 W-LINE.
           10 W-PAYMENTDATE PIC X(10).
           10 FILLER PIC XX VALUE SPACES.
           10 W-ACCOUNTNUMBER PIC X(18).
           10 FILLER PIC XX VALUE SPACES.
           10 W-DESCRIPTION PIC X(26).
           10 FILLER PIC X.
           10 W-AMOUNT PIC -----------9,99.
           10 FILLER PIC X.
           10 W-CURRENCY PIC X(3).
       01 W-AMOUNT-NP3 PIC S9(8)V9(2) COMP-3.
       01 W-ATTR PIC X.
       01 W-MSG PIC X(70).
       01 W-ROW PIC 99.
       01 W-COLUMN PIC 99.
       EXEC SQL
           DECLARE CUR-F8 CURSOR FOR
           SELECT 
               PaymentDate,
               DebitAccountNumber,
               DescriptionText,
               Amount
           FROM
               dbo.Payment
           WHERE
               CreditAccountNumber = :C007-ACCOUNTNUMBER
           UNION ALL
           SELECT 
               PaymentDate,
               CreditAccountNumber,
               DescriptionText,
               -1 * Amount
           FROM
               dbo.Payment
           WHERE
               DebitAccountNumber = :C007-ACCOUNTNUMBER
           ORDER BY
               PaymentDate DESC
       END-EXEC.
       LINKAGE SECTION.                                                 
       COPY DFHCOMM.                                 
       PROCEDURE DIVISION.
       EXEC CICS IGNORE CONDITION MAPFAIL END-EXEC
       IF EIBCALEN > 0
           MOVE DFHCOMMAREA TO W-COMMAREA
       ELSE 
           MOVE LOW-VALUES TO W-COMMAREA
       END-IF
       EVALUATE TRUE
       WHEN EIBTRNID = 'C008'
           MOVE C008-RETURN-MESSAGE TO W-MSG
           PERFORM F8
       WHEN EIBCALEN = 0
       WHEN EIBTRNID IS NOT EQUAL 'C007'
           PERFORM F8
       WHEN EIBAID = DFHENTER
           DIVIDE EIBCPOSN BY 80 GIVING W-ROW REMAINDER W-COLUMN
           ADD 1 TO W-ROW W-COLUMN
           IF W-ROW = 24 AND W-COLUMN >= 14 AND W-COLUMN <= 31
               MOVE C007-ACCOUNTNUMBER TO C008-ACCOUNTNUMBER-FROM
               EXEC CICS XCTL 
                   PROGRAM('COB008') COMMAREA (W-COMMAREA)
               END-EXEC
           ELSE
               EXEC CICS SEND CONTROL FREEKB
               END-EXEC
           END-IF
       WHEN EIBAID = DFHPF3
           EXEC CICS XCTL
               PROGRAM('COB005') COMMAREA(W-COMMAREA)
           END-EXEC 
       WHEN EIBAID = DFHPF6
           MOVE C007-ACCOUNTNUMBER TO C008-ACCOUNTNUMBER-FROM
           EXEC CICS XCTL 
               PROGRAM('COB008') COMMAREA (W-COMMAREA)
           END-EXEC
       WHEN EIBAID = DFHCLEAR
           EXEC CICS SEND CONTROL ERASE FREEKB
           END-EXEC
           EXEC CICS RETURN
           END-EXEC
       WHEN OTHER
           EXEC CICS SEND CONTROL FREEKB
           END-EXEC
       END-EVALUATE
       EXEC CICS RETURN 
           TRANSID ('C007') COMMAREA (W-COMMAREA)
       END-EXEC
       GOBACK                                       
       .
       F8.
       MOVE LOW-VALUE TO MAP001O
       MOVE C007-ACCOUNTNUMBER TO ACCTO
       PERFORM GET-ACCOUNT
       IF SQLCODE = 0
           MOVE C007-AMOUNT TO BALO
           MOVE 'EUR' TO BALO(17:)
           PERFORM OPEN-F8
           EVALUATE SQLCODE
           WHEN 0
               PERFORM FETCH-F8
               IF SQLCODE = 100
                   MOVE 'No payments found' TO MSGO
               END-IF
               MOVE W-LINE TO L01O
               PERFORM FETCH-F8
               MOVE W-LINE TO L02O
               PERFORM FETCH-F8
               MOVE W-LINE TO L03O
               PERFORM FETCH-F8
               MOVE W-LINE TO L04O
               PERFORM FETCH-F8
               MOVE W-LINE TO L05O
               PERFORM FETCH-F8
               MOVE W-LINE TO L06O
               PERFORM FETCH-F8
               MOVE W-LINE TO L07O
               PERFORM FETCH-F8
               MOVE W-LINE TO L08O
               PERFORM FETCH-F8
               MOVE W-LINE TO L09O
               PERFORM FETCH-F8
               MOVE W-LINE TO L10O
               PERFORM FETCH-F8
               MOVE W-LINE TO L11O
               PERFORM FETCH-F8
               MOVE W-LINE TO L12O
               PERFORM FETCH-F8
               MOVE W-LINE TO L13O
               PERFORM FETCH-F8
               MOVE W-LINE TO L14O
               PERFORM FETCH-F8
               MOVE W-LINE TO L15O
               PERFORM CLOSE-F8
           WHEN OTHER
               MOVE 'OPEN CUR-F8:' TO W-MSG
               MOVE SQLCODE TO W-MSG(14:)
           END-EVALUATE
       END-IF
       MOVE W-MSG TO MSGO
       EXEC CICS SEND 
           MAP('MAP001')
           MAPSET('COB007') 
           ERASE 
           CURSOR
       END-EXEC
       .
       OPEN-F8.
       EXEC SQL
         open CUR-F8
       END-EXEC
       .   
       FETCH-F8.
       IF SQLCODE = 0
           EXEC SQL
               FETCH CUR-F8 INTO
               :W-PAYMENTDATE,
               :W-ACCOUNTNUMBER,
               :W-DESCRIPTION,
               :W-AMOUNT-NP3
           END-EXEC
           EVALUATE SQLCODE
           WHEN 0
               MOVE W-AMOUNT-NP3 TO W-AMOUNT
               MOVE 'EUR' TO W-CURRENCY
           WHEN 100
              MOVE SPACES TO W-LINE
           WHEN OTHER
               MOVE 'FETCH CUR-F8: SQLCODE:' TO W-MSG
              MOVE SQLCODE TO W-MSG(24:)
           END-EVALUATE
       ELSE
           MOVE SPACES TO W-LINE
       END-IF
       .
       CLOSE-F8.
       EXEC SQL
         CLOSE CUR-F8
       END-EXEC
       .
       GET-ACCOUNT.
       EXEC SQL
           SELECT Amount
           INTO :W-AMOUNT-NP3
           FROM dbo.Account
           WHERE Accountnumber = :C007-ACCOUNTNUMBER
       END-EXEC
       EVALUATE SQLCODE
       WHEN 0
           MOVE W-AMOUNT-NP3 TO C007-AMOUNT
       WHEN OTHER
           MOVE 'SELECT ACCOUNT: SQLCODE:' TO W-MSG
           MOVE SQLCODE TO W-MSG(24:)
       END-EVALUATE
       .
