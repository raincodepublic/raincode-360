      ***********************************************************
      *                                                         *
      *   CUSTOMER LIST - SCREEN HANDLING                       *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB004.                            
       DATA DIVISION.                                                   
       WORKING-STORAGE SECTION.          
       COPY COB004.
       COPY DFHAID.
       COPY DFHBMSCA.
       COPY COB004A.
       01 W-LINE.
         10 CUSTOMERID PIC 999999.
         10 FILLER PIC X.
         10 LASTNAME PIC X(15).
         10 FILLER PIC X.
         10 FIRSTNAME PIC X(15).
         10 FILLER PIC X.
         10 ZIPCODE PIC XXXX.
         10 FILLER PIC X.
         10 CITY PIC X(10).
         10 FILLER PIC X.
         10 BIRTHDATE PIC X(10).
       01 W-LINECOUNT PIC S9(4).
       01 W-ATTR PIC X.
       01 W-ROW PIC 99.
       01 W-SEL PIC 99.
       01 W-COLUMN PIC 99.
       01 W-MSG PIC X(80).
       01 W-EIBCPOSN PIC S9(4) COMP VALUE 0.
       01 W-I PIC S9(4) COMP.
       01 W-J PIC S9(4) COMP.
       01 W-FILTERCHANGE PIC 9 VALUE 0.
       LINKAGE SECTION.                                                 
       COPY COMMAREA REPLACING W-COMMAREA BY DFHCOMMAREA.
       PROCEDURE DIVISION.
       EXEC CICS IGNORE CONDITION MAPFAIL END-EXEC
       IF EIBCALEN = 0
           EXEC CICS 
               GETMAIN SET(ADDRESS OF DFHCOMMAREA)
               FLENGTH(LENGTH OF DFHCOMMAREA) 
           END-EXEC
       ELSE
           IF EIBCALEN IS NOT EQUAL LENGTH OF DFHCOMMAREA
               EXEC CICS ABEND ABCODE('C004') END-EXEC
           END-IF
       END-IF
       EVALUATE TRUE
       WHEN EIBCALEN = 0
           INITIALIZE C004
           PERFORM F8
       WHEN EIBAID = DFHPF8
           PERFORM GET-MAP
           IF W-FILTERCHANGE > 0
               MOVE 0 TO C004-CUSTOMERID-LAST
           END-IF
           PERFORM F8
       WHEN EIBTRNID = 'C005'
           MOVE C004-EIBCPOSN TO W-EIBCPOSN
           SUBTRACT 1 FROM C004-CUSTOMERID-FIRST
           MOVE C004-CUSTOMERID-FIRST TO C004-CUSTOMERID-LAST
           PERFORM F8
       WHEN EIBAID = DFHPF7
           PERFORM GET-MAP
           IF W-FILTERCHANGE > 0
               MOVE 0 TO C004-CUSTOMERID-LAST
               PERFORM F8
           ELSE
               PERFORM F7
           END-IF
       WHEN EIBAID = DFHENTER
           PERFORM GET-MAP
           MOVE EIBCPOSN TO C004-EIBCPOSN
           DIVIDE EIBCPOSN BY 80 GIVING W-ROW REMAINDER W-COLUMN
           ADD 1 TO W-ROW W-COLUMN
           MOVE W-ROW TO W-SEL
           SUBTRACT 4 FROM W-SEL 
           IF W-SEL > 0 AND W-SEL < 19 AND C004-CUST(W-SEL) > 0
               MOVE C004-CUST(W-SEL) TO C004-CUSTOMERID-SELECTED
               EXEC CICS XCTL
                   PROGRAM('COB005') 
                   COMMAREA(DFHCOMMAREA)
                   LENGTH(LENGTH OF DFHCOMMAREA)
               END-EXEC 
           ELSE
               IF W-FILTERCHANGE > 0
                   MOVE 0 TO C004-CUSTOMERID-LAST
               ELSE
                   MOVE C004-CUSTOMERID-FIRST TO C004-CUSTOMERID-LAST
                   SUBTRACT 1 FROM C004-CUSTOMERID-LAST
               END-IF
               PERFORM F8
           END-IF 
       WHEN EIBAID = DFHPF3
       WHEN EIBAID = DFHCLEAR
         EXEC CICS SEND CONTROL ERASE FREEKB
         END-EXEC
         EXEC CICS RETURN
         END-EXEC
       WHEN OTHER
         EXEC CICS SEND CONTROL FREEKB
         END-EXEC
       END-EVALUATE
       EXEC CICS RETURN 
           TRANSID ('C004') 
           COMMAREA (DFHCOMMAREA)
           LENGTH(LENGTH OF DFHCOMMAREA)
       END-EXEC
       GOBACK                                       
       .    
       PUT-FILTER.
           IF C004-CUSTOMERID-FILTER > 0
               MOVE C004-CUSTOMERID-FILTER TO FCUSTO
           END-IF
           IF C004-LASTNAME-FILTER > SPACES
               MOVE C004-LASTNAME-FILTER TO FLASTO
           END-IF
           IF C004-FIRSTNAME-FILTER > SPACES
           MOVE C004-FIRSTNAME-FILTER TO FFIRSTO
           END-IF
           IF C004-ZIPCODE-FILTER > SPACES
           MOVE C004-ZIPCODE-FILTER TO FZIPO
           END-IF
           IF C004-CITY-FILTER > SPACES
           MOVE C004-CITY-FILTER TO FCITYO
           END-IF
           IF C004-BIRTHDATE-FILTER > SPACES
               MOVE C004-BIRTHDATE-FILTER TO FBDATEO
           END-IF
       .
       GET-MAP.
       EXEC CICS RECEIVE
           MAP('MAP001')
           MAPSET('COB004')
       END-EXEC
       IF FCUSTL > 0 
           MOVE 6 TO W-I W-J
           PERFORM UNTIL W-I < 1
               EVALUATE FCUSTI(W-I:1)
               WHEN '0' THRU '9'
                   MOVE FCUSTI(W-I:1) TO C004-CUSTOMERID-FILTER(W-J:1)
                   SUBTRACT 1 FROM W-J
               END-EVALUATE
               SUBTRACT 1 FROM W-I
           END-PERFORM
           PERFORM UNTIL W-J < 1
               MOVE '0' TO C004-CUSTOMERID-FILTER(W-J:1)
               SUBTRACT 1 FROM W-J
           END-PERFORM
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       IF FLASTL > 0 OR FLASTF = DFHBMEF OR FLASTF = DFHBMEOF
           MOVE FLASTI TO C004-LASTNAME-FILTER
           INSPECT C004-LASTNAME-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       IF FFIRSTL > 0 OR FFIRSTF = DFHBMEF OR FFIRSTF = DFHBMEOF
           MOVE FFIRSTI TO C004-FIRSTNAME-FILTER
           INSPECT C004-FIRSTNAME-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       IF FZIPL > 0 OR FZIPF = DFHBMEF OR FZIPF = DFHBMEOF
           MOVE FZIPI TO C004-ZIPCODE-FILTER
           INSPECT C004-ZIPCODE-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       IF FCITYL > 0 OR FCITYF = DFHBMEF OR FCITYF = DFHBMEOF
           MOVE FCITYI TO C004-CITY-FILTER
           INSPECT C004-CITY-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       IF FBDATEL > 0 OR FBDATEF = DFHBMEF OR FBDATEF = DFHBMEOF
           MOVE FBDATEI TO C004-BIRTHDATE-FILTER
           INSPECT C004-BIRTHDATE-FILTER REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE 1 TO W-FILTERCHANGE
       END-IF
       .
       F8.
       SET COB004A-DOWN TO TRUE
       PERFORM GET-LINES
       .
       F7.
       SET COB004A-UP TO TRUE
       PERFORM GET-LINES
       IF COB004A-SQLCODE = 0
           AND COB004A-FETCHED > 0
           AND COB004A-FETCHED < COB004A-LINECOUNT
               MOVE 0 TO C004-CUSTOMERID-FIRST
               PERFORM F8
       END-IF
       .
       GET-LINES.
       MOVE LOW-VALUE TO MAP001O
       PERFORM PUT-FILTER
       MOVE DFHBMFSE TO FCUSTA
       MOVE 18 TO COB004A-LINECOUNT
       MOVE C004-CUSTOMERID-FIRST TO COB004A-CUSTOMERID-FIRST
       MOVE C004-CUSTOMERID-LAST TO COB004A-CUSTOMERID-LAST
       MOVE C004-CUSTOMERID-FILTER TO COB004A-CUSTOMERID-FILTER
       MOVE C004-LASTNAME-FILTER TO COB004A-LASTNAME-FILTER
       MOVE C004-FIRSTNAME-FILTER TO COB004A-FIRSTNAME-FILTER
       MOVE C004-ZIPCODE-FILTER TO COB004A-ZIPCODE-FILTER
       MOVE C004-CITY-FILTER TO COB004A-CITY-FILTER
       MOVE C004-BIRTHDATE-FILTER TO COB004A-BIRTHDATE-FILTER
       SET COB004A-SORT-CUSTOMERID TO TRUE
       EXEC CICS
           LINK PROGRAM('COB004A') COMMAREA(COB004A)
       END-EXEC
       IF COB004A-SQLCODE = 0
           IF COB004A-FETCHED > 0
               MOVE COB004A-CUSTOMERID(1) TO C004-CUSTOMERID-FIRST
               MOVE COB004A-CUSTOMERID(COB004A-FETCHED) 
                 TO C004-CUSTOMERID-LAST
               PERFORM VARYING W-I FROM 1 BY 1 UNTIL W-I > 18
                   IF W-I <= COB004A-FETCHED
                       MOVE SPACE TO C004-SELA(W-I)
                       MOVE COB004A-CUSTOMERID(W-I) TO C004-CUST(W-I)
                       MOVE COB004A-CUSTOMERID(W-I) TO CUSTOMERID
                       MOVE COB004A-LASTNAME(W-I) TO LASTNAME
                       MOVE COB004A-FIRSTNAME(W-I) TO FIRSTNAME
                       MOVE COB004A-ZIPCODE(W-I) TO ZIPCODE
                       MOVE COB004A-CITY(W-I) TO CITY
                       MOVE COB004A-BIRTHDATE(W-I) TO BIRTHDATE
                   ELSE
                       MOVE DFHPROTN TO C004-SELA(W-I)
                       MOVE SPACE TO W-LINE
                   END-IF
                   PERFORM PUT-LINE
               END-PERFORM
               PERFORM PUT-SEL
               IF W-EIBCPOSN > 0
                   EXEC CICS SEND 
                       MAP('MAP001')
                       MAPSET('COB004') 
                       ERASE 
                       CURSOR(W-EIBCPOSN)
                   END-EXEC
               ELSE
                   EXEC CICS SEND 
                       MAP('MAP001')
                       MAPSET('COB004') 
                       ERASE 
                   END-EXEC
               END-IF
           ELSE
               IF EIBAID = DFHENTER
                   MOVE SPACES TO 
                       L01O L02O L03O L04O L05O L06O L07O L08O L09O 
                       L10O L11O L12O L13O L14O L15O L16O L17O L18O
                   PERFORM VARYING W-I FROM 1 BY 1 UNTIL W-I > 18
                       MOVE DFHPROTN TO C004-SELA(W-I)
                   END-PERFORM
                   PERFORM PUT-SEL
                   MOVE 'No matches' TO W-MSG
               ELSE
                   IF COB004A-DOWN
                       MOVE 'Bottom of list' TO W-MSG
                   ELSE
                       MOVE 'Top of list' TO W-MSG
                   END-IF
               END-IF
               PERFORM PUT-MSG
           END-IF
       ELSE
           MOVE 'COB004A-SQLCODE=' TO W-MSG
           MOVE COB004A-SQLCODE TO W-MSG(17:)
           PERFORM PUT-MSG
       END-IF
       .
       PUT-SEL.
       MOVE C004-SELA(01) TO S01A 
       MOVE C004-SELA(02) TO S02A 
       MOVE C004-SELA(03) TO S03A 
       MOVE C004-SELA(04) TO S04A 
       MOVE C004-SELA(05) TO S05A 
       MOVE C004-SELA(06) TO S06A 
       MOVE C004-SELA(07) TO S07A 
       MOVE C004-SELA(08) TO S08A 
       MOVE C004-SELA(09) TO S09A 
       MOVE C004-SELA(10) TO S10A 
       MOVE C004-SELA(11) TO S11A 
       MOVE C004-SELA(12) TO S12A 
       MOVE C004-SELA(13) TO S13A 
       MOVE C004-SELA(14) TO S14A 
       MOVE C004-SELA(15) TO S15A 
       MOVE C004-SELA(16) TO S16A 
       MOVE C004-SELA(17) TO S17A 
       MOVE C004-SELA(18) TO S18A 
       .
       PUT-LINE.
       EVALUATE W-I
       WHEN 1 MOVE W-LINE TO L01O
       WHEN 2 MOVE W-LINE TO L02O
       WHEN 3 MOVE W-LINE TO L03O
       WHEN 4 MOVE W-LINE TO L04O
       WHEN 5 MOVE W-LINE TO L05O
       WHEN 6 MOVE W-LINE TO L06O
       WHEN 7 MOVE W-LINE TO L07O
       WHEN 8 MOVE W-LINE TO L08O
       WHEN 9 MOVE W-LINE TO L09O
       WHEN 10 MOVE W-LINE TO L10O
       WHEN 11 MOVE W-LINE TO L11O
       WHEN 12 MOVE W-LINE TO L12O
       WHEN 13 MOVE W-LINE TO L13O
       WHEN 14 MOVE W-LINE TO L14O
       WHEN 15 MOVE W-LINE TO L15O
       WHEN 16 MOVE W-LINE TO L16O
       WHEN 17 MOVE W-LINE TO L17O
       WHEN 18 MOVE W-LINE TO L18O
       END-EVALUATE
       .
       PUT-MSG.
       MOVE W-MSG TO MSGO
       PERFORM PUT-SEL
       EXEC CICS SEND 
           MAP('MAP001')
           MAPSET('COB004') 
           DATAONLY 
           CURSOR
       END-EXEC
       .   