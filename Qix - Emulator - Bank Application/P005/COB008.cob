      ***********************************************************
      *                                                         *
      *   PAYMENT SCREEN HANDLING                               *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB008.        
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
       DECIMAL-POINT IS COMMA.                                       
       DATA DIVISION.                                                   
       WORKING-STORAGE SECTION.          
       COPY COB008.
       COPY PLI001.
       COPY DFHAID.
       COPY DFHBMSCA.
       COPY SQLCA.
       COPY COMMAREA.
       COPY COB008B.
       COPY COB008C.
       COPY COB008D.
       01 W-MSG PIC X(80).
       01 W-SQLCODE PIC -----9.
       01 AMOUNT-PIC PIC ZZZ9,99.
       01 PUT-DB-OK PIC 9.
       01 W-ACCOUNTNUMBER-OK PIC 9.
       01 W-AMOUNT-NP3 PIC S9(8)V9(2) COMP-3.
       01 W-AMOUNT PIC -----------9,99.
       01 DEC PIC S9(4) COMP.
       01 FILLER REDEFINES DEC.
           10 FILLER PIC X.
           10 BYTE PIC X.
       10 VALID PIC S9(4) COMP.
       LINKAGE SECTION.                                                 
       COPY DFHCOMM.                                 
       PROCEDURE DIVISION.
       EXEC CICS IGNORE CONDITION MAPFAIL END-EXEC
       MOVE DFHCOMMAREA TO W-COMMAREA
       EVALUATE TRUE
       WHEN EIBTRNID = 'C007'
           INITIALIZE C008-INTERNAL 
           PERFORM PUT-MAP
       WHEN EIBTRNID = 'C010'
           IF C010-ACCOUNTNUMBER-SELECTED > SPACES
               MOVE C010-ACCOUNTNUMBER-SELECTED 
                   TO C008-ACCOUNTNUMBER-TO
               PERFORM GET-ACCOUNT
           END-IF    
           PERFORM PUT-MAP
       WHEN EIBAID = DFHPF3
           MOVE 'Payment cancelled' TO C008-RETURN-MESSAGE
           EXEC CICS XCTL 
               PROGRAM('COB007') COMMAREA (W-COMMAREA)
           END-EXEC
       WHEN EIBAID = DFHENTER
           PERFORM GET-MAP
           EVALUATE EIBCPOSN
      *    ** (9-1)*80 + 14
           WHEN 654
               EXEC CICS XCTL 
                   PROGRAM('COB010') COMMAREA (W-COMMAREA)
               END-EXEC
           WHEN OTHER
               PERFORM VALIDATE
               IF VALID > 0
                   MOVE 'Press F5 to confirm' TO W-MSG
               END-IF
           END-EVALUATE
           PERFORM PUT-MAP
       WHEN EIBAID = DFHPF12
           PERFORM GET-MAP
           MOVE 'F:' TO W-MSG 
           MOVE COMMF TO DEC
           MOVE DEC TO W-MSG(3:)
           MOVE 'L:' TO W-MSG(10:)
           MOVE COMML TO W-MSG(12:)
           PERFORM PUT-MAP
       WHEN EIBAID = DFHPF5
           PERFORM GET-MAP
           PERFORM VALIDATE
           IF VALID > 0
               PERFORM PUT-DB
               IF PUT-DB-OK > 0
                   MOVE 'Payment successful' TO C008-RETURN-MESSAGE
                   EXEC CICS XCTL 
                       PROGRAM('COB007') COMMAREA (W-COMMAREA)
                   END-EXEC
               END-IF 
           END-IF
           PERFORM PUT-MAP
       WHEN EIBAID = DFHCLEAR
           EXEC CICS SEND CONTROL ERASE FREEKB
           END-EXEC
           EXEC CICS RETURN
           END-EXEC
       WHEN OTHER
           MOVE 'Invalid action' TO W-MSG
           PERFORM PUT-MAP
       END-EVALUATE
       EXEC CICS RETURN 
           TRANSID ('C008') COMMAREA (W-COMMAREA)
       END-EXEC
       GOBACK                                       
       .    
       PUT-MAP.
       MOVE LOW-VALUE TO MAP001O
       IF C008-DESCRIPTIONTEXT > SPACES
           MOVE C008-DESCRIPTIONTEXT TO COMMO
       END-IF
       IF C008-AMOUNT-ONSCREEN > SPACES
           MOVE C008-AMOUNT-ONSCREEN TO AMTO
       END-IF    
       IF C008-ACCOUNTNUMBER-TO > SPACES
           MOVE C008-ACCOUNTNUMBER-TO TO ACCTO
           MOVE C008-COUNTRY TO CTRYO
           MOVE C008-PAYMENT-TYPE TO PTYPEO
       END-IF
       IF C008-A-AMOUNT > SPACES
           MOVE 'H' TO AMTA
           MOVE -1 TO AMTL
       END-IF
       IF C008-A-ACCOUNTNUMBER-TO > SPACES
           MOVE 'H' TO ACCTA
           MOVE -1 TO ACCTL
       ELSE
           MOVE C008-LASTNAME TO LASTO
           MOVE C008-FIRSTNAME TO FIRSTO
           MOVE C008-ZIPCODE TO ZIPO
           MOVE C008-CITY TO CITYO
       END-IF
       MOVE C008-ACCOUNTNUMBER-FROM TO ACCFO
       PERFORM GET-ACCOUNT-FROM
       IF SQLCODE = 0
           MOVE W-AMOUNT TO BALO
           MOVE 'EUR' TO BALO(17:)
       END-IF
      
       EVALUATE TRUE
       WHEN C008-ACCOUNTNUMBER-TO <= SPACES
       WHEN C008-A-ACCOUNTNUMBER-TO > SPACES
       WHEN C008-A-AMOUNT > SPACES
           CONTINUE
       WHEN C008-AMOUNT-ONSCREEN <= SPACES
           MOVE -1 TO AMTL
       WHEN C008-DESCRIPTIONTEXT <= SPACES
           MOVE -1 TO COMML
       END-EVALUATE
        
       IF AMTL NOT = -1 AND COMML NOT = -1
           MOVE -1 TO ACCSELL
       END-IF
              
       MOVE W-MSG TO MSGO
       EXEC CICS SEND 
           MAP('MAP001')
           MAPSET('COB008') 
           ERASE 
           CURSOR
       END-EXEC
       .
       VALIDATE.
       MOVE 1 TO VALID
       IF C008-AMOUNT-ONSCREEN > SPACES
           MOVE C008-AMOUNT-ONSCREEN TO COB008C-INFIELD
           EXEC CICS LINK PROGRAM('COB008C') COMMAREA(COB008C)
           END-EXEC
           IF COB008C-ERRORCODE = 0
               IF COB008C-OUTFIELD <= 9999
                   MOVE COB008C-OUTFIELD TO C008-AMOUNT
                   MOVE COB008C-OUTFIELD TO AMOUNT-PIC 
                   MOVE FUNCTION TRIM(AMOUNT-PIC) 
                     TO C008-AMOUNT-ONSCREEN
                   MOVE SPACE TO C008-A-AMOUNT
               ELSE
                   MOVE 0 TO VALID
                   MOVE 'H' TO C008-A-AMOUNT
                   MOVE 'Maximum amount is 9999.99 EUR' TO W-MSG
               END-IF
           ELSE
               MOVE 0 TO VALID
               MOVE 'H' TO C008-A-AMOUNT
               MOVE 'Invalid Amount' TO W-MSG
           END-IF
       ELSE
           MOVE 0 TO VALID
           MOVE 'H' TO C008-A-AMOUNT
           MOVE 'Amount missing' TO W-MSG
       END-IF
       IF C008-ACCOUNTNUMBER-TO > SPACES
           IF C008-ACCOUNTNUMBER-TO = C008-ACCOUNTNUMBER-FROM
               MOVE 0 TO VALID
               MOVE 'H' TO C008-A-ACCOUNTNUMBER-TO
               MOVE 'Account numbers must be different' TO W-MSG
           ELSE
               MOVE C008-ACCOUNTNUMBER-TO TO COB008B-ACCOUNTNUMBER
               EXEC CICS 
                   LINK PROGRAM('COB008B') 
                   COMMAREA(COB008B) 
               END-EXEC
               IF COB008B-ERRORCODE = 0
                   MOVE C008-ACCOUNTNUMBER-TO TO COB008D-COUNTRY-CODE
                   EXEC CICS 
                       LINK PROGRAM('COB008D') 
                       COMMAREA(COB008D) 
                   END-EXEC
                   IF COB008D-ERRORCODE = 0
                       MOVE COB008D-PAYMENT-TYPE TO C008-PAYMENT-TYPE
                       MOVE COB008D-COUNTRY-NAME TO C008-COUNTRY
                       IF C008-PAYMENT-TYPE = 'LOCAL'
                           PERFORM GET-ACCOUNT
                           EVALUATE SQLCODE
                           WHEN 0
                               MOVE SPACE TO C008-A-ACCOUNTNUMBER-TO
                           WHEN 100
                               MOVE 0 TO VALID
                               MOVE 'H' TO C008-A-ACCOUNTNUMBER-TO
                               MOVE 'Account not found' TO W-MSG
                           WHEN OTHER
                               MOVE 0 TO VALID
                               MOVE 'H' TO C008-A-ACCOUNTNUMBER-TO
                               PERFORM PUT-SQLCODE
                           END-EVALUATE
                       ELSE
                           MOVE SPACE TO C008-A-ACCOUNTNUMBER-TO
                       END-IF
                   ELSE 
                       MOVE 0 TO VALID
                       MOVE 'H' TO C008-A-ACCOUNTNUMBER-TO
                       MOVE 'Invalid country prefix' TO W-MSG
                   END-IF
               ELSE
                   MOVE 0 TO VALID
                   MOVE 'H' TO C008-A-ACCOUNTNUMBER-TO
                   MOVE 'Invalid accountnumber' TO W-MSG
               END-IF
           END-IF
       ELSE
           MOVE 0 TO VALID
           MOVE 'H' TO C008-A-ACCOUNTNUMBER-TO
           MOVE 'Accountnumber missing' TO W-MSG
       END-IF
       .
       VALIDATE-ACCOUNTNUMBER-TO.
       IF C008-ACCOUNTNUMBER-TO(1:2) IS ALPHABETIC AND
          C008-ACCOUNTNUMBER-TO(3:2) IS NUMERIC AND
          C008-ACCOUNTNUMBER-TO(5:1) = '-' AND
          C008-ACCOUNTNUMBER-TO(6:4) IS NUMERIC AND
          C008-ACCOUNTNUMBER-TO(10:1) = '-' AND
          C008-ACCOUNTNUMBER-TO(11:4) IS NUMERIC AND
          C008-ACCOUNTNUMBER-TO(15:1) = '-' AND
          C008-ACCOUNTNUMBER-TO(16:4) IS NUMERIC
           MOVE 1 TO W-ACCOUNTNUMBER-OK
       ELSE
           MOVE 0 TO W-ACCOUNTNUMBER-OK
       END-IF
       .
       GET-ACCOUNT.
       EXEC SQL
           SELECT 
               C.LastName,
               C.FirstName,
               C.ZipCode,
               Z.City
           INTO
               :C008-LASTNAME,
               :C008-FIRSTNAME,
               :C008-ZIPCODE,
               :C008-CITY
           FROM
               dbo.Account A,
               dbo.Customer C,
               dbo.ZipCode Z
           WHERE
               A.AccountNumber = :C008-ACCOUNTNUMBER-TO AND
               C.CustomerId = A.CustomerId AND
               Z.ZipCode = C.ZipCode
       END-EXEC
       .
       GET-ACCOUNT-FROM.
       EXEC SQL
           SELECT Amount
           INTO :W-AMOUNT-NP3
           FROM dbo.Account
           WHERE Accountnumber = :C008-ACCOUNTNUMBER-FROM
       END-EXEC
       EVALUATE SQLCODE
       WHEN 0
           MOVE W-AMOUNT-NP3 TO W-AMOUNT
       WHEN OTHER
           MOVE 'SELECT ACCOUNT: SQLCODE:' TO W-MSG
           MOVE SQLCODE TO W-MSG(24:)
       END-EVALUATE
       .
       GET-MAP.
       EXEC CICS RECEIVE
           MAP('MAP001')
           MAPSET('COB008')
       END-EXEC
       IF ACCTL > 0 OR ACCTF = DFHBMEF OR ACCTF = DFHBMEOF
           MOVE FUNCTION UPPER-CASE(ACCTI) TO C008-ACCOUNTNUMBER-TO
           INSPECT C008-ACCOUNTNUMBER-TO REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
           MOVE SPACES TO C008-COUNTRY
           MOVE SPACES TO C008-PAYMENT-TYPE
       END-IF
       IF AMTL > 0 OR AMTF = DFHBMEF OR AMTF = DFHBMEOF
           MOVE AMTI TO C008-AMOUNT-ONSCREEN
           INSPECT C008-AMOUNT-ONSCREEN REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
       END-IF
       IF COMML > 0 OR COMMF = DFHBMEF OR COMMF = DFHBMEOF
           MOVE COMMI TO C008-DESCRIPTIONTEXT
           INSPECT C008-DESCRIPTIONTEXT REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
       END-IF
       .
       PUT-DB.
       MOVE C008-ACCOUNTNUMBER-FROM TO P001-ACCOUNTNUMBER-FROM
       MOVE C008-ACCOUNTNUMBER-TO TO P001-ACCOUNTNUMBER-TO
       MOVE C008-AMOUNT TO P001-AMOUNT
       MOVE C008-DESCRIPTIONTEXT TO P001-DESCRIPTIONTEXT
       EXEC CICS
           LINK PROGRAM('PLI001') COMMAREA(P001)
       END-EXEC
       IF P001-SQLCODE = 0
           MOVE 'Update successful' TO W-MSG
           MOVE 1 TO PUT-DB-OK
       ELSE
           MOVE P001-ACCOUNTNUMBER-FROM TO W-MSG
           MOVE P001-ACCOUNTNUMBER-TO TO W-MSG(11:)
           MOVE P001-DESCRIPTIONTEXT TO W-MSG(21:)
           MOVE P001-ERRTXT TO W-MSG(31:)
           MOVE 0 TO PUT-DB-OK
       END-IF
       .
       PUT-SQLCODE.
           MOVE SQLCODE TO W-SQLCODE
           MOVE 'Database error, SQLCODE:' TO W-MSG
           MOVE W-SQLCODE TO W-MSG(26:)
       .     