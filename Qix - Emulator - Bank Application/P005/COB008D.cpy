      ***********************************************************
      *                                                         *
      *   CHECK COUNTRY CODE AND RETURN PAYMENT TYPE            *
      *                                                         *
      *   Check if the country code in the input represents a   *
      *   valid European country and return a payment type if   *
      *   it does. The payment ype is 'LOCAL' for Belgium       *
      *   ('BE') and 'FOREIGN' for all other countries.         *
      *                                                         *
      ***********************************************************
       01 COB008D.
           10 COB008D-IN.
     2         20 COB008D-COUNTRY-CODE PIC X(2).
           10 COB008B-OUT.
    20         20 COB008D-COUNTRY-NAME PIC X(20).
    10         20 COB008D-PAYMENT-TYPE PIC X(10).
     2         20 COB008D-ERRORCODE PIC S9(4) COMP.
------
    34
