      ***********************************************************
      *                                                         *
      *   ACCOUNT LIST - DATABASE ACCESS                        *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB010A.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       COPY COB010A.
       01 I PIC S9(4) COMP.
       01 W-ACCOUNTNUMBER PIC X(20).
       01 W-LASTNAME PIC X(30).
       01 W-FIRSTNAME PIC X(30).
       01 W-ZIPCODE PIC X(4).
       01 W-CITY PIC X(30).
       01 W-BIRTHDATE PIC X(10).
       EXEC SQL
           DECLARE CUR-DOWN CURSOR FOR
           SELECT 
               A.AccountNumber,
               C.FirstName,
               C.LastName,
               Z.ZipCode, 
               Z.City
           FROM 
               dbo.Account A,
               dbo.Customer C,
               dbo.ZipCode Z
           WHERE 
               A.CUSTOMERID = C.CUSTOMERID AND
               C.Zipcode = Z.Zipcode AND
               A.AccountNumber > :COB010A-ACCOUNTNUMBER-LAST AND
               A.AccountNumber LIKE 
                   RTRIM(:COB010A-ACCOUNTNUMBER-FILTER) || '%' AND
               C.LastName LIKE
                   RTRIM(:COB010A-LASTNAME-FILTER) || '%' AND
               C.FirstName LIKE
                   RTRIM(:COB010A-FIRSTNAME-FILTER) || '%' AND
               Z.ZipCode LIKE RTRIM(:COB010A-ZIPCODE-FILTER) 
                   || '%' AND
               Z.City LIKE RTRIM(:COB010A-CITY-FILTER) || '%'
           ORDER BY 
               A.AccountNumber
       END-EXEC.
       EXEC SQL
           DECLARE CUR-UP CURSOR FOR
           SELECT 
               A.AccountNumber,
               C.FirstName,
               C.LastName,
               Z.ZipCode, 
               Z.City
           FROM 
               dbo.Account A,
               dbo.Customer C,
               dbo.ZipCode Z
           WHERE 
               A.CUSTOMERID = C.CUSTOMERID AND
               C.Zipcode = Z.Zipcode AND
               A.AccountNumber < :COB010A-ACCOUNTNUMBER-FIRST AND
               A.AccountNumber LIKE 
                   RTRIM(:COB010A-ACCOUNTNUMBER-FILTER) || '%' AND
               C.LastName LIKE
                   RTRIM(:COB010A-LASTNAME-FILTER) || '%' AND
               C.FirstName LIKE
                   RTRIM(:COB010A-FIRSTNAME-FILTER) || '%' AND
               Z.ZipCode LIKE RTRIM(:COB010A-ZIPCODE-FILTER) 
                   || '%' AND
               Z.City LIKE RTRIM(:COB010A-CITY-FILTER) || '%'
           ORDER BY 
               A.AccountNumber DESC
       END-EXEC.
       LINKAGE SECTION.
       01 DFHCOMMAREA PIC X(2213).                                 
       PROCEDURE DIVISION.
       MOVE DFHCOMMAREA TO COB010A
       MOVE -1 TO COB010A-SQLCODE
       MOVE 0 TO COB010A-FETCHED
       EVALUATE TRUE
       WHEN COB010A-DOWN
           PERFORM OPEN-DOWN
           MOVE SQLCODE TO COB010A-SQLCODE
           IF SQLCODE = 0
               MOVE 1 TO I
               PERFORM UNTIL I > COB010A-LINECOUNT 
                       OR SQLCODE IS NOT EQUAL 0
                   PERFORM FETCH-DOWN
                   EVALUATE SQLCODE
                   WHEN 0
                       ADD 1 TO COB010A-FETCHED
                       MOVE W-ACCOUNTNUMBER TO COB010A-ACCOUNTNUMBER(I)
                       MOVE W-LASTNAME TO COB010A-LASTNAME(I)
                       MOVE W-FIRSTNAME TO COB010A-FIRSTNAME(I)
                       MOVE W-ZIPCODE TO COB010A-ZIPCODE(I)
                       MOVE W-CITY TO COB010A-CITY(I)
                   WHEN 100
                       CONTINUE
                   WHEN OTHER
                       MOVE SQLCODE TO COB010A-SQLCODE
                   END-EVALUATE
                   ADD 1 TO I
               END-PERFORM
           END-IF
           IF SQLCODE = 0
               PERFORM CLOSE-DOWN
               MOVE SQLCODE TO COB010A-SQLCODE
           END-IF
       WHEN COB010A-UP
           PERFORM OPEN-UP
           MOVE SQLCODE TO COB010A-SQLCODE
           IF SQLCODE = 0
               MOVE COB010A-LINECOUNT TO I
               PERFORM UNTIL I < 1 OR SQLCODE IS NOT EQUAL 0
                   PERFORM FETCH-UP
                   EVALUATE SQLCODE
                   WHEN 0
                       ADD 1 TO COB010A-FETCHED
                       MOVE W-ACCOUNTNUMBER TO COB010A-ACCOUNTNUMBER(I)
                       MOVE W-LASTNAME TO COB010A-LASTNAME(I)
                       MOVE W-FIRSTNAME TO COB010A-FIRSTNAME(I)
                       MOVE W-ZIPCODE TO COB010A-ZIPCODE(I)
                       MOVE W-CITY TO COB010A-CITY(I)
                   WHEN 100
                       CONTINUE
                   WHEN OTHER
                       MOVE SQLCODE TO COB010A-SQLCODE
                   END-EVALUATE
                   SUBTRACT 1 FROM I
               END-PERFORM
           END-IF
           IF SQLCODE = 0
               PERFORM CLOSE-UP
               MOVE SQLCODE TO COB010A-SQLCODE
           END-IF
       END-EVALUATE
       MOVE COB010A TO DFHCOMMAREA
       EXEC CICS 
           RETURN
       END-EXEC
       .
       OPEN-DOWN.
       EXEC SQL
         OPEN CUR-DOWN
       END-EXEC
       .
       FETCH-DOWN.
       EXEC SQL
           FETCH CUR-DOWN INTO
           :W-ACCOUNTNUMBER,
           :W-FIRSTNAME,
           :W-LASTNAME,
           :W-ZIPCODE,
           :W-CITY
       END-EXEC
       .
       CLOSE-DOWN.
       EXEC SQL
           CLOSE CUR-DOWN
       END-EXEC
       .   
       OPEN-UP.
       EXEC SQL
         OPEN CUR-UP
       END-EXEC
       .
       FETCH-UP.
       EXEC SQL
           FETCH CUR-UP INTO
           :W-ACCOUNTNUMBER,
           :W-FIRSTNAME,
           :W-LASTNAME,
           :W-ZIPCODE,
           :W-CITY
       END-EXEC
       .
       CLOSE-UP.
       EXEC SQL
           CLOSE CUR-UP
       END-EXEC
       .
