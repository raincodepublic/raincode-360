        01 MAP001I .
            02 FILLER PIC X(12).
            02 ACCFL PIC S9(4) COMP-5.
            02 ACCFF PIC X(1).
            02 FILLER REDEFINES ACCFF.
              04 ACCFA PIC X.
            02 ACCFI PIC X(19).
            02 BALL PIC S9(4) COMP-5.
            02 BALF PIC X(1).
            02 FILLER REDEFINES BALF.
              04 BALA PIC X.
            02 BALI PIC X(19).
            02 ACCSELL PIC S9(4) COMP-5.
            02 ACCSELF PIC X(1).
            02 FILLER REDEFINES ACCSELF.
              04 ACCSELA PIC X.
            02 ACCSELI PIC X(1).
            02 ACCTL PIC S9(4) COMP-5.
            02 ACCTF PIC X(1).
            02 FILLER REDEFINES ACCTF.
              04 ACCTA PIC X.
            02 ACCTI PIC X(19).
            02 LASTL PIC S9(4) COMP-5.
            02 LASTF PIC X(1).
            02 FILLER REDEFINES LASTF.
              04 LASTA PIC X.
            02 LASTI PIC X(40).
            02 FIRSTL PIC S9(4) COMP-5.
            02 FIRSTF PIC X(1).
            02 FILLER REDEFINES FIRSTF.
              04 FIRSTA PIC X.
            02 FIRSTI PIC X(40).
            02 ZIPL PIC S9(4) COMP-5.
            02 ZIPF PIC X(1).
            02 FILLER REDEFINES ZIPF.
              04 ZIPA PIC X.
            02 ZIPI PIC X(10).
            02 CITYL PIC S9(4) COMP-5.
            02 CITYF PIC X(1).
            02 FILLER REDEFINES CITYF.
              04 CITYA PIC X.
            02 CITYI PIC X(20).
            02 CTRYL PIC S9(4) COMP-5.
            02 CTRYF PIC X(1).
            02 FILLER REDEFINES CTRYF.
              04 CTRYA PIC X.
            02 CTRYI PIC X(20).
            02 AMTL PIC S9(4) COMP-5.
            02 AMTF PIC X(1).
            02 FILLER REDEFINES AMTF.
              04 AMTA PIC X.
            02 AMTI PIC X(7).
            02 COMML PIC S9(4) COMP-5.
            02 COMMF PIC X(1).
            02 FILLER REDEFINES COMMF.
              04 COMMA PIC X.
            02 COMMI PIC X(40).
            02 PTYPEL PIC S9(4) COMP-5.
            02 PTYPEF PIC X(1).
            02 FILLER REDEFINES PTYPEF.
              04 PTYPEA PIC X.
            02 PTYPEI PIC X(20).
            02 MSGL PIC S9(4) COMP-5.
            02 MSGF PIC X(1).
            02 FILLER REDEFINES MSGF.
              04 MSGA PIC X.
            02 MSGI PIC X(70).
        01 MAP001O REDEFINES MAP001I.
            02 FILLER PIC X(12).
            02 FILLER PIC X(3).
            02 ACCFO PIC X(19).
            02 FILLER PIC X(3).
            02 BALO PIC X(19).
            02 FILLER PIC X(3).
            02 ACCSELO PIC X(1).
            02 FILLER PIC X(3).
            02 ACCTO PIC X(19).
            02 FILLER PIC X(3).
            02 LASTO PIC X(40).
            02 FILLER PIC X(3).
            02 FIRSTO PIC X(40).
            02 FILLER PIC X(3).
            02 ZIPO PIC X(10).
            02 FILLER PIC X(3).
            02 CITYO PIC X(20).
            02 FILLER PIC X(3).
            02 CTRYO PIC X(20).
            02 FILLER PIC X(3).
            02 AMTO PIC X(7).
            02 FILLER PIC X(3).
            02 COMMO PIC X(40).
            02 FILLER PIC X(3).
            02 PTYPEO PIC X(20).
            02 FILLER PIC X(3).
            02 MSGO PIC X(70).
