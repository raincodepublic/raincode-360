      ***********************************************************
      *                                                         *
      *   CHECK ACCOUNTNUMBER FORMAT (AA99-9999-9999-9999)      *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB008B.        
       ENVIRONMENT DIVISION.
       LINKAGE SECTION.                                                 
       COPY COB008B REPLACING COB008B BY DFHCOMMAREA.
       PROCEDURE DIVISION.
       IF EIBCALEN IS NOT EQUAL LENGTH OF DFHCOMMAREA
           EXEC CICS ABEND ABCODE('008B') END-EXEC
       END-IF
       PERFORM VALIDATE-ACCOUNTNUMBER
       EXEC CICS RETURN END-EXEC
       .
       VALIDATE-ACCOUNTNUMBER.
       IF COB008B-ACCOUNTNUMBER(1:2) IS ALPHABETIC AND
          COB008B-ACCOUNTNUMBER(3:2) IS NUMERIC AND
          COB008B-ACCOUNTNUMBER(5:1) = '-' AND
          COB008B-ACCOUNTNUMBER(6:4) IS NUMERIC AND
          COB008B-ACCOUNTNUMBER(10:1) = '-' AND
          COB008B-ACCOUNTNUMBER(11:4) IS NUMERIC AND
          COB008B-ACCOUNTNUMBER(15:1) = '-' AND
          COB008B-ACCOUNTNUMBER(16:4) IS NUMERIC
           MOVE 0 TO COB008B-ERRORCODE
       ELSE
           MOVE 1 TO COB008B-ERRORCODE
       END-IF
       .

