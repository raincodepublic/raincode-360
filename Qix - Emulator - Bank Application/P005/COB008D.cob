      ***********************************************************
      *                                                         *
      *   CHECK COUNTRY CODE AND RETURN PAYMENT TYPE            *
      *                                                         *
      *   Check if the country code in the input represents a   *
      *   valid European country and return a payment type if   *
      *   it does. The payment ype is 'LOCAL' for Belgium       *
      *   ('BE') and 'FOREIGN' for all other countries.         *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB008D.        
       ENVIRONMENT DIVISION.
       LINKAGE SECTION.                                                 
       COPY COB008D REPLACING COB008D BY DFHCOMMAREA.
       PROCEDURE DIVISION.
       IF EIBCALEN IS NOT EQUAL LENGTH OF DFHCOMMAREA
           EXEC CICS ABEND ABCODE('008D') END-EXEC
       END-IF
       PERFORM VALIDATE-COUNTRY
       EXEC CICS RETURN END-EXEC
       .
       VALIDATE-COUNTRY.
       MOVE 0 TO COB008D-ERRORCODE
       EVALUATE COB008D-COUNTRY-CODE
       WHEN 'AT' MOVE 'Austria' TO COB008D-COUNTRY-NAME
       WHEN 'BE' MOVE 'Belgium' TO COB008D-COUNTRY-NAME
       WHEN 'BG' MOVE 'Bulgaria' TO COB008D-COUNTRY-NAME
       WHEN 'CY' MOVE 'Cyprus' TO COB008D-COUNTRY-NAME
       WHEN 'CZ' MOVE 'Czech Republic' TO COB008D-COUNTRY-NAME
       WHEN 'DE' MOVE 'Germany' TO COB008D-COUNTRY-NAME
       WHEN 'DK' MOVE 'Denmark' TO COB008D-COUNTRY-NAME
       WHEN 'EE' MOVE 'Estonia' TO COB008D-COUNTRY-NAME
       WHEN 'ES' MOVE 'Spain' TO COB008D-COUNTRY-NAME
       WHEN 'FI' MOVE 'Finland' TO COB008D-COUNTRY-NAME
       WHEN 'FR' MOVE 'France' TO COB008D-COUNTRY-NAME
       WHEN 'GR' MOVE 'Greece ' TO COB008D-COUNTRY-NAME
       WHEN 'HR' MOVE 'Croatia' TO COB008D-COUNTRY-NAME
       WHEN 'HU' MOVE 'Hungary' TO COB008D-COUNTRY-NAME
       WHEN 'IE' MOVE 'Ireland' TO COB008D-COUNTRY-NAME
       WHEN 'IT' MOVE 'Italy' TO COB008D-COUNTRY-NAME
       WHEN 'LT' MOVE 'Lithuania' TO COB008D-COUNTRY-NAME
       WHEN 'LU' MOVE 'Luxembourg' TO COB008D-COUNTRY-NAME
       WHEN 'LV' MOVE 'Latvia' TO COB008D-COUNTRY-NAME
       WHEN 'MT' MOVE 'Malta' TO COB008D-COUNTRY-NAME
       WHEN 'NL' MOVE 'Netherlands' TO COB008D-COUNTRY-NAME
       WHEN 'PO' MOVE 'Poland' TO COB008D-COUNTRY-NAME
       WHEN 'PT' MOVE 'Portugal' TO COB008D-COUNTRY-NAME
       WHEN 'RO' MOVE 'Romania' TO COB008D-COUNTRY-NAME
       WHEN 'SE' MOVE 'Sweden' TO COB008D-COUNTRY-NAME
       WHEN 'SI' MOVE 'Slovenia' TO COB008D-COUNTRY-NAME
       WHEN 'SK' MOVE 'Slovakia' TO COB008D-COUNTRY-NAME
       WHEN OTHER
           MOVE 1 TO COB008D-ERRORCODE
       END-EVALUATE
       IF COB008D-ERRORCODE = 0
           IF COB008D-COUNTRY-CODE = 'BE'
               MOVE 'LOCAL' TO COB008D-PAYMENT-TYPE
           ELSE
               MOVE 'FOREIGN' TO COB008D-PAYMENT-TYPE
           END-IF
       END-IF
       .

