      ***********************************************************
      *                                                         *
      *   CUSTOMER LIST - DATABASE ACCESS                       *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB004A.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       COPY COB004A.
       01 W-CUSTOMERID-MIN PIC S9(9) COMP-5.
       01 W-CUSTOMERID-MAX PIC S9(9) COMP-5.
       01 I PIC S9(4) COMP.
       01 W-CUSTOMERID PIC S9(4) COMP.
       01 W-LASTNAME PIC X(30).
       01 W-FIRSTNAME PIC X(30).
       01 W-ZIPCODE PIC X(4).
       01 W-CITY PIC X(30).
       01 W-BIRTHDATE PIC X(10).
       EXEC SQL
         DECLARE CUR-DOWN-C CURSOR FOR
         SELECT CustomerId, FirstName, LastName, C.ZipCode, 
         BirthDate, Z.City
         FROM 
             dbo.Customer C,
             dbo.ZipCode Z
         WHERE 
             CustomerId > :COB004A-CUSTOMERID-LAST AND
             CustomerId 
                 BETWEEN 
                     :W-CUSTOMERID-MIN AND 
                     :W-CUSTOMERID-MAX AND
             LastName
                 LIKE RTRIM(:COB004A-LASTNAME-FILTER) || '%' AND
             Firstname
                 LIKE RTRIM(:COB004A-FIRSTNAME-FILTER) || '%' AND
             C.ZipCode
                 LIKE RTRIM(:COB004A-ZIPCODE-FILTER) || '%' AND
             BirthDate
                 LIKE RTRIM(:COB004A-BIRTHDATE-FILTER) || '%' AND
             Z.City
                 LIKE RTRIM(:COB004A-CITY-FILTER) || '%' AND
             Z.ZipCode = C.ZipCode
         ORDER BY CustomerId
       END-EXEC.
       EXEC SQL
         DECLARE CUR-DOWN-F CURSOR FOR
         SELECT CustomerId, FirstName, LastName, C.ZipCode, 
         BirthDate, Z.City
         FROM 
             dbo.Customer C,
             dbo.ZipCode Z
         WHERE 
             FirstName >= :COB004A-FIRSTNAME-LAST AND
             ( FirstName > :COB004A-FIRSTNAME-LAST OR
               FirstName = :COB004A-FIRSTNAME-LAST AND
               CustomerId > :COB004A-CUSTOMERID-LAST
             ) AND
             CustomerId > :COB004A-CUSTOMERID-LAST AND
             CustomerId 
                 BETWEEN 
                     :W-CUSTOMERID-MIN AND 
                     :W-CUSTOMERID-MAX AND
             LastName
                 LIKE RTRIM(:COB004A-LASTNAME-FILTER) || '%' AND
             Firstname
                 LIKE RTRIM(:COB004A-FIRSTNAME-FILTER) || '%' AND
             C.ZipCode
                 LIKE RTRIM(:COB004A-ZIPCODE-FILTER) || '%' AND
             BirthDate
                 LIKE RTRIM(:COB004A-BIRTHDATE-FILTER) || '%' AND
             Z.City
                 LIKE RTRIM(:COB004A-CITY-FILTER) || '%' AND
             Z.ZipCode = C.ZipCode
         ORDER BY FirstName, LastName, CustomerId
       END-EXEC.
       EXEC SQL
         DECLARE CUR-DOWN-L CURSOR FOR
         SELECT CustomerId, FirstName, LastName, C.ZipCode, 
         BirthDate, Z.City
         FROM 
             dbo.Customer C,
             dbo.ZipCode Z
         WHERE 
             LastName >= :COB004A-LASTNAME-LAST AND
             ( LastName > :COB004A-LASTNAME-LAST OR
               LastName = :COB004A-LASTNAME-LAST AND
               CustomerId > :COB004A-CUSTOMERID-LAST
             ) AND
             CustomerId 
                 BETWEEN 
                     :W-CUSTOMERID-MIN AND 
                     :W-CUSTOMERID-MAX AND
             LastName
                 LIKE RTRIM(:COB004A-LASTNAME-FILTER) || '%' AND
             Firstname
                 LIKE RTRIM(:COB004A-FIRSTNAME-FILTER) || '%' AND
             C.ZipCode
                 LIKE RTRIM(:COB004A-ZIPCODE-FILTER) || '%' AND
             BirthDate
                 LIKE RTRIM(:COB004A-BIRTHDATE-FILTER) || '%' AND
             Z.City
                 LIKE RTRIM(:COB004A-CITY-FILTER) || '%' AND
             Z.ZipCode = C.ZipCode
         ORDER BY LastName, FirstName, CustomerId
       END-EXEC.
       EXEC SQL
         DECLARE CUR-UP-C CURSOR FOR
         SELECT CustomerId, FirstName, LastName, C.ZipCode, 
         BirthDate, Z.City
         FROM 
             dbo.Customer C,
             dbo.ZipCode Z
         WHERE 
             CustomerId < :COB004A-CUSTOMERID-FIRST AND
             CustomerId 
                 BETWEEN 
                     :W-CUSTOMERID-MIN AND 
                     :W-CUSTOMERID-MAX AND
             LastName
                 LIKE RTRIM(:COB004A-LASTNAME-FILTER) || '%' AND
             Firstname 
                 LIKE RTRIM(:COB004A-FIRSTNAME-FILTER) || '%' AND
             C.ZipCode
                 LIKE RTRIM(:COB004A-ZIPCODE-FILTER) || '%' AND
             BirthDate
                 LIKE RTRIM(:COB004A-BIRTHDATE-FILTER) || '%' AND
             Z.City
                 LIKE RTRIM(:COB004A-CITY-FILTER) || '%' AND
             Z.ZipCode = C.ZipCode
         ORDER BY CustomerId desc
       END-EXEC.
       EXEC SQL
         DECLARE CUR-UP-F CURSOR FOR
         SELECT CustomerId, FirstName, LastName, C.ZipCode, 
         BirthDate, Z.City
         FROM 
             dbo.Customer C,
             dbo.ZipCode Z
         WHERE 
             FirstName <= :COB004A-FIRSTNAME-LAST AND
             ( FirstName < :COB004A-FIRSTNAME-LAST OR
               FirstName = :COB004A-FIRSTNAME-LAST AND
               CustomerId < :COB004A-CUSTOMERID-LAST
             ) AND
             CustomerId 
                 BETWEEN 
                     :W-CUSTOMERID-MIN AND 
                     :W-CUSTOMERID-MAX AND
             LastName
                 LIKE RTRIM(:COB004A-LASTNAME-FILTER) || '%' AND
             Firstname 
                 LIKE RTRIM(:COB004A-FIRSTNAME-FILTER) || '%' AND
             C.ZipCode
                 LIKE RTRIM(:COB004A-ZIPCODE-FILTER) || '%' AND
             BirthDate
                 LIKE RTRIM(:COB004A-BIRTHDATE-FILTER) || '%' AND
             Z.City
                 LIKE RTRIM(:COB004A-CITY-FILTER) || '%' AND
             Z.ZipCode = C.ZipCode
         ORDER BY FirstName desc, LastName desc, CustomerId desc
       END-EXEC.
       EXEC SQL
         DECLARE CUR-UP-L CURSOR FOR
         SELECT CustomerId, FirstName, LastName, C.ZipCode, 
         BirthDate, Z.City
         FROM 
             dbo.Customer C,
             dbo.ZipCode Z
         WHERE 
             LastName <= :COB004A-LASTNAME-LAST AND
             ( LastName < :COB004A-LASTNAME-LAST OR
               LastName = :COB004A-LASTNAME-LAST AND
               CustomerId < :COB004A-CUSTOMERID-LAST
             ) AND
             CustomerId 
                 BETWEEN 
                     :W-CUSTOMERID-MIN AND 
                     :W-CUSTOMERID-MAX AND
             LastName
                 LIKE RTRIM(:COB004A-LASTNAME-FILTER) || '%' AND
             Firstname 
                 LIKE RTRIM(:COB004A-FIRSTNAME-FILTER) || '%' AND
             C.ZipCode
                 LIKE RTRIM(:COB004A-ZIPCODE-FILTER) || '%' AND
             BirthDate
                 LIKE RTRIM(:COB004A-BIRTHDATE-FILTER) || '%' AND
             Z.City
                 LIKE RTRIM(:COB004A-CITY-FILTER) || '%' AND
             Z.ZipCode = C.ZipCode
         ORDER BY LastName desc, FirstName desc, CustomerId desc
       END-EXEC.
       LINKAGE SECTION.
       01 DFHCOMMAREA PIC X(2034).                                 
       PROCEDURE DIVISION.
       IF LENGTH OF DFHCOMMAREA IS NOT EQUAL LENGTH OF COB004A
           EXEC CICS ABEND ABCODE('XX4A') END-EXEC
       END-IF
       MOVE DFHCOMMAREA TO COB004A
       MOVE -1 TO COB004A-SQLCODE
       MOVE 0 TO COB004A-FETCHED
       EVALUATE TRUE
       WHEN COB004A-SORT-CUSTOMERID
       WHEN COB004A-SORT-FIRSTNAME
       WHEN COB004A-SORT-LASTNAME
           CONTINUE
       WHEN OTHER
           EXEC CICS 
               RETURN
           END-EXEC
       END-EVALUATE
       
       PERFORM SET-CUSTOMER-FILTER
       EVALUATE TRUE
       WHEN COB004A-DOWN
           PERFORM OPEN-DOWN
           MOVE SQLCODE TO COB004A-SQLCODE
           IF SQLCODE = 0
               MOVE 1 TO I
               PERFORM UNTIL I > COB004A-LINECOUNT 
                       OR SQLCODE IS NOT EQUAL 0
                   PERFORM FETCH-DOWN
                   EVALUATE SQLCODE
                   WHEN 0
                       ADD 1 TO COB004A-FETCHED
                       MOVE W-CUSTOMERID TO COB004A-CUSTOMERID(I)
                       MOVE W-LASTNAME TO COB004A-LASTNAME(I)
                       MOVE W-FIRSTNAME TO COB004A-FIRSTNAME(I)
                       MOVE W-ZIPCODE TO COB004A-ZIPCODE(I)
                       MOVE W-CITY TO COB004A-CITY(I)
                       MOVE W-BIRTHDATE TO COB004A-BIRTHDATE(I)
                   WHEN 100
                       CONTINUE
                   WHEN OTHER
                       MOVE SQLCODE TO COB004A-SQLCODE
                   END-EVALUATE
                   ADD 1 TO I
               END-PERFORM
           END-IF
           IF SQLCODE = 0
               PERFORM CLOSE-DOWN
               MOVE SQLCODE TO COB004A-SQLCODE
           END-IF
       WHEN COB004A-UP
           PERFORM OPEN-UP
           MOVE SQLCODE TO COB004A-SQLCODE
           IF SQLCODE = 0
               MOVE COB004A-LINECOUNT TO I
               PERFORM UNTIL I < 1 OR SQLCODE IS NOT EQUAL 0
                   PERFORM FETCH-UP
                   EVALUATE SQLCODE
                   WHEN 0
                       ADD 1 TO COB004A-FETCHED
                       MOVE W-CUSTOMERID TO COB004A-CUSTOMERID(I)
                       MOVE W-LASTNAME TO COB004A-LASTNAME(I)
                       MOVE W-FIRSTNAME TO COB004A-FIRSTNAME(I)
                       MOVE W-ZIPCODE TO COB004A-ZIPCODE(I)
                       MOVE W-CITY TO COB004A-CITY(I)
                       MOVE W-BIRTHDATE TO COB004A-BIRTHDATE(I)
                   WHEN 100
                       CONTINUE
                   WHEN OTHER
                       MOVE SQLCODE TO COB004A-SQLCODE
                   END-EVALUATE
                   SUBTRACT 1 FROM I
               END-PERFORM
           END-IF
           IF SQLCODE = 0
               PERFORM CLOSE-UP
               MOVE SQLCODE TO COB004A-SQLCODE
           END-IF
       END-EVALUATE
       MOVE COB004A TO DFHCOMMAREA
       EXEC CICS 
           RETURN
       END-EXEC
       .
       SET-CUSTOMER-FILTER.
       IF COB004A-CUSTOMERID-FILTER = 0
           MOVE -1 TO W-CUSTOMERID-MIN
           MOVE 2147483647 TO W-CUSTOMERID-MAX
       ELSE
           MOVE COB004A-CUSTOMERID-FILTER 
             TO W-CUSTOMERID-MIN W-CUSTOMERID-MAX
       END-IF
       .
       OPEN-DOWN.
       EVALUATE TRUE
       WHEN COB004A-SORT-CUSTOMERID
           EXEC SQL
             OPEN CUR-DOWN-C
           END-EXEC
       WHEN COB004A-SORT-FIRSTNAME
           EXEC SQL
             OPEN CUR-DOWN-F
           END-EXEC
       WHEN COB004A-SORT-LASTNAME
           EXEC SQL
             OPEN CUR-DOWN-L
           END-EXEC
       END-EVALUATE
       .
       FETCH-DOWN.
       EVALUATE TRUE
       WHEN COB004A-SORT-CUSTOMERID
           EXEC SQL
               FETCH CUR-DOWN-C INTO
               :W-CUSTOMERID,
               :W-FIRSTNAME,
               :W-LASTNAME,
               :W-ZIPCODE,
               :W-BIRTHDATE,
               :W-CITY
           END-EXEC
       WHEN COB004A-SORT-FIRSTNAME
           EXEC SQL
               FETCH CUR-DOWN-F INTO
               :W-CUSTOMERID,
               :W-FIRSTNAME,
               :W-LASTNAME,
               :W-ZIPCODE,
               :W-BIRTHDATE,
               :W-CITY
           END-EXEC
       WHEN COB004A-SORT-LASTNAME
           EXEC SQL
               FETCH CUR-DOWN-L INTO
               :W-CUSTOMERID,
               :W-FIRSTNAME,
               :W-LASTNAME,
               :W-ZIPCODE,
               :W-BIRTHDATE,
               :W-CITY
           END-EXEC
       END-EVALUATE
       .
       CLOSE-DOWN.
       EVALUATE TRUE
       WHEN COB004A-SORT-CUSTOMERID
           EXEC SQL
             CLOSE CUR-DOWN-C
           END-EXEC
       WHEN COB004A-SORT-FIRSTNAME
           EXEC SQL
             CLOSE CUR-DOWN-F
           END-EXEC
       WHEN COB004A-SORT-LASTNAME
           EXEC SQL
             CLOSE CUR-DOWN-L
           END-EXEC
       END-EVALUATE
       .   
       OPEN-UP.
       EVALUATE TRUE
       WHEN COB004A-SORT-CUSTOMERID
           EXEC SQL
             OPEN CUR-UP-C
           END-EXEC
       WHEN COB004A-SORT-FIRSTNAME
           EXEC SQL
             OPEN CUR-UP-F
           END-EXEC
       WHEN COB004A-SORT-LASTNAME
           EXEC SQL
             OPEN CUR-UP-L
           END-EXEC
       END-EVALUATE
       .
       FETCH-UP.
       EVALUATE TRUE
       WHEN COB004A-SORT-CUSTOMERID
           EXEC SQL
               FETCH CUR-UP-C INTO
               :W-CUSTOMERID,
               :W-FIRSTNAME,
               :W-LASTNAME,
               :W-ZIPCODE,
               :W-BIRTHDATE,
               :W-CITY
           END-EXEC
       WHEN COB004A-SORT-FIRSTNAME
           EXEC SQL
               FETCH CUR-UP-F INTO
               :W-CUSTOMERID,
               :W-FIRSTNAME,
               :W-LASTNAME,
               :W-ZIPCODE,
               :W-BIRTHDATE,
               :W-CITY
           END-EXEC
       WHEN COB004A-SORT-LASTNAME
           EXEC SQL
               FETCH CUR-UP-L INTO
               :W-CUSTOMERID,
               :W-FIRSTNAME,
               :W-LASTNAME,
               :W-ZIPCODE,
               :W-BIRTHDATE,
               :W-CITY
           END-EXEC
       END-EVALUATE
       .
       CLOSE-UP.
       EVALUATE TRUE
       WHEN COB004A-SORT-CUSTOMERID
           EXEC SQL
             CLOSE CUR-UP-C
           END-EXEC
       WHEN COB004A-SORT-FIRSTNAME
           EXEC SQL
             CLOSE CUR-UP-F
           END-EXEC
       WHEN COB004A-SORT-LASTNAME
           EXEC SQL
             CLOSE CUR-UP-L
           END-EXEC
       END-EVALUATE
       .   
