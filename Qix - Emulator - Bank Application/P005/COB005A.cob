      ***********************************************************
      *                                                         *
      *   GET CUSTOMER DETAILS AND ACCOUNTS                     *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB005A.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       COPY COB005A.
       01 I PIC S9(4) COMP.
       EXEC SQL
           DECLARE CUR-CUST CURSOR FOR
           SELECT 
               FirstName, 
               LastName, 
               C.ZipCode, 
               BirthDate, 
               Z.City,
               A.AccountNumber,
               A.Amount
           FROM 
               dbo.Customer C,
               dbo.ZipCode Z,
               dbo.Account A
           WHERE 
               Z.ZipCode = C.ZipCode AND
               A.CustomerId = C.CustomerId AND
               C.CustomerId = :COB005A-CustomerId
           ORDER BY
               A.AccountNumber
       END-EXEC.
       LINKAGE SECTION.
       01 DFHCOMMAREA PIC X(262).                                 
       PROCEDURE DIVISION.
       MOVE DFHCOMMAREA TO COB005A
       INITIALIZE COB005A-OUT
       PERFORM OPEN-CUST
       IF COB005A-ERROR-CODE = 0
           MOVE 1 TO I
           PERFORM UNTIL I > 5 OR SQLCODE IS NOT EQUAL 0
               PERFORM FETCH-CUST
               IF COB005A-ERROR-CODE = 0
                   ADD 1 TO COB005A-ACCOUNTS-FETCHED
               END-IF
               ADD 1 TO I
           END-PERFORM
       END-IF
       IF COB005A-ERROR-CODE = 0
           PERFORM CLOSE-CUST
       END-IF
       MOVE COB005A TO DFHCOMMAREA
       EXEC CICS 
           RETURN
       END-EXEC
       .
       OPEN-CUST.
       EXEC SQL
         OPEN CUR-CUST
       END-EXEC
       IF SQLCODE IS NOT EQUAL 0
           MOVE 'COB005A: OPEN CUR-CUST: SQLCODE='  TO 
               COB005A-ERROR-MSG
           MOVE SQLCODE TO 
               COB005A-ERROR-MSG(33:) 
               COB005A-ERROR-CODE
       END-IF
       .
       FETCH-CUST.
       EXEC SQL
           FETCH 
               CUR-CUST 
           INTO
               :COB005A-FirstName, 
               :COB005A-LastName, 
               :COB005A-ZipCode, 
               :COB005A-BirthDate, 
               :COB005A-City,
               :COB005A-AccountNumber(I),
               :COB005A-Amount(I)
       END-EXEC
       IF SQLCODE IS NOT EQUAL 0 AND SQLCODE IS NOT EQUAL 100
           MOVE 'COB005A: FETCH CUR-CUST: SQLCODE=' TO
               COB005A-ERROR-MSG
           MOVE SQLCODE TO 
               COB005A-ERROR-MSG(34:) 
               COB005A-ERROR-CODE
       END-IF
       .
       CLOSE-CUST.
       EXEC SQL
           CLOSE CUR-CUST
       END-EXEC
       IF SQLCODE IS NOT EQUAL 0
           MOVE 'COB005A: CLOSE CUR-CUST: SQLCODE=' TO
               COB005A-ERROR-MSG
           MOVE SQLCODE TO 
               COB005A-ERROR-MSG(34:) 
               COB005A-ERROR-CODE
       END-IF
       .   
