      ***********************************************************
      *                                                         *
      *   DISPLAY CUSTOMER DETAILS AND ACCOUNTS                 *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB005.        
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
       DECIMAL-POINT IS COMMA.                                       
       DATA DIVISION.                                                   
       WORKING-STORAGE SECTION.          
       COPY COB005.
       COPY COB005A.
       COPY DFHAID.
       COPY DFHBMSCA.
       COPY SQLCA.
       COPY COMMAREA.
       01 W-MSG PIC X(80).
       01 W-SQLCODE PIC -----9.
       01 L-ZIPCODE PIC XXXX.
       01 W-ABSTIME PIC S9(15) COMP-3.
       01 W-DATE PIC X(10).
       01 W-FOUND PIC S9(4) COMP.
       LINKAGE SECTION.                                                 
       COPY DFHCOMM.                                 
       PROCEDURE DIVISION.
       EXEC CICS IGNORE CONDITION MAPFAIL END-EXEC
       MOVE DFHCOMMAREA TO W-COMMAREA
       EVALUATE TRUE
       WHEN EIBTRNID = 'C006'
           IF C006-ZIPCODE-SELECTED > SPACES
               MOVE C006-ZIPCODE-SELECTED TO C005-ZIPCODE
           END-IF
           PERFORM VALIDATE
           PERFORM PUT-MAP
       WHEN EIBTRNID = 'C004'
           MOVE SPACES TO C005-A
           MOVE C004-CUSTOMERID-SELECTED TO C005-CUSTOMERID
           PERFORM GET-CUST
           PERFORM PUT-MAP
       WHEN EIBTRNID = 'C007'
           PERFORM GET-CUST
           PERFORM PUT-MAP
       WHEN EIBAID = DFHPF3
           EXEC CICS XCTL 
               PROGRAM('COB004') COMMAREA (W-COMMAREA)
           END-EXEC
       WHEN EIBAID = DFHPF6
           EXEC CICS XCTL 
               PROGRAM('COB006') COMMAREA (W-COMMAREA)
           END-EXEC
       WHEN EIBAID = DFHENTER
           PERFORM GET-MAP
           EVALUATE EIBCPOSN
      *    zipcode (11-1)*80 + 14
           WHEN 814
               EXEC CICS XCTL 
                   PROGRAM('COB006') COMMAREA (W-COMMAREA)
               END-EXEC
      *    account 1 (17 -1)*80 + 14
           WHEN 1294
               MOVE C005-ACCOUNTNUMBER-1 TO C007-ACCOUNTNUMBER
               MOVE C005-AMOUNT-1 TO C007-AMOUNT
               EXEC CICS XCTL 
                   PROGRAM('COB007') COMMAREA (W-COMMAREA)
               END-EXEC
      *    account 2 (18 -1)*80 + 14
           WHEN 1374
               MOVE C005-ACCOUNTNUMBER-2 TO C007-ACCOUNTNUMBER
               MOVE C005-AMOUNT-2 TO C007-AMOUNT
               EXEC CICS XCTL 
                   PROGRAM('COB007') COMMAREA (W-COMMAREA)
               END-EXEC
      *    account 3 (19 -1)*80 + 14
           WHEN 1454
               MOVE C005-ACCOUNTNUMBER-3 TO C007-ACCOUNTNUMBER
               MOVE C005-AMOUNT-3 TO C007-AMOUNT
           EXEC CICS XCTL 
                   PROGRAM('COB007') COMMAREA (W-COMMAREA)
               END-EXEC
           WHEN OTHER
               PERFORM VALIDATE
           END-EVALUATE
           PERFORM PUT-MAP
       WHEN EIBAID = DFHPF5
           PERFORM GET-MAP
           PERFORM VALIDATE
           IF C005-A = SPACES
               PERFORM PUT-DB
           ELSE
               MOVE 'Errors present' TO W-MSG
           END-IF
           PERFORM PUT-MAP
       WHEN OTHER
           MOVE 'Invalid action' TO W-MSG
           PERFORM PUT-MAP
       END-EVALUATE
       EXEC CICS RETURN 
           TRANSID ('C005') COMMAREA (W-COMMAREA)
       END-EXEC
       GOBACK                                       
       .    
       GET-CUST.
       MOVE C005-CUSTOMERID TO COB005A-CUSTOMERID
       EXEC CICS
           LINK PROGRAM('COB005A') COMMAREA(COB005A)
       END-EXEC
       IF COB005A-ERROR-CODE = 0
           MOVE COB005A-LASTNAME TO C005-LASTNAME
           MOVE COB005A-FIRSTNAME TO C005-FIRSTNAME
           MOVE COB005A-ZIPCODE TO C005-ZIPCODE
           MOVE COB005A-CITY TO C005-CITY
           MOVE COB005A-BIRTHDATE TO C005-BIRTHDATE
           MOVE COB005A-ACCOUNTNUMBER(1) TO C005-ACCOUNTNUMBER-1
           MOVE COB005A-ACCOUNTNUMBER(2) TO C005-ACCOUNTNUMBER-2
           MOVE COB005A-ACCOUNTNUMBER(3) TO C005-ACCOUNTNUMBER-3
           MOVE COB005A-ACCOUNTNUMBER(4) TO C005-ACCOUNTNUMBER-4
           MOVE COB005A-ACCOUNTNUMBER(5) TO C005-ACCOUNTNUMBER-5
           MOVE COB005A-AMOUNT(1) TO C005-AMOUNT-1
           MOVE COB005A-AMOUNT(2) TO C005-AMOUNT-2
           MOVE COB005A-AMOUNT(3) TO C005-AMOUNT-3
           MOVE COB005A-AMOUNT(4) TO C005-AMOUNT-4
           MOVE COB005A-AMOUNT(5) TO C005-AMOUNT-5
       ELSE
           MOVE COB005A-ERROR-MSG TO W-MSG
       END-IF
       .
       GET-ZIP.
       EXEC SQL
           SELECT CAST(City AS CHAR(60)), Country
               INTO :C005-CITY, :C005-COUNTRY 
           FROM dbo.zipcode
           WHERE zipcode = :C005-ZIPCODE
       END-EXEC
       .
       PUT-MAP.
           MOVE LOW-VALUE TO MAP001O
           MOVE C005-CUSTOMERID TO CUSTO
           IF C005-LASTNAME > SPACES
               MOVE C005-LASTNAME TO LASTO
           END-IF
           IF C005-FIRSTNAME > SPACES
               MOVE C005-FIRSTNAME TO FIRSTO
           END-IF
           IF C005-ZIPCODE > SPACES
               MOVE C005-ZIPCODE TO ZIPO
           END-IF
           MOVE C005-CITY TO CITYO
           IF C005-BIRTHDATE > SPACES
               MOVE C005-BIRTHDATE TO BDATEO
           END-IF
           IF C005-ACCOUNTNUMBER-1 > SPACES
               MOVE ' EUR' TO C005-EUR-1
               MOVE C005-ACCOUNT-1 TO ACC1O
           ELSE
               MOVE DFHPROTN TO ACCSEL1A ACCNM1A
           END-IF
           IF C005-ACCOUNTNUMBER-2 > SPACES
               MOVE ' EUR' TO C005-EUR-2
               MOVE C005-ACCOUNT-2 TO ACC2O
           ELSE
               MOVE DFHPROTN TO ACCSEL2A ACCNM2A
           END-IF
           IF C005-ACCOUNTNUMBER-3 > SPACES
               MOVE ' EUR' TO C005-EUR-3
               MOVE C005-ACCOUNT-3 TO ACC3O
           ELSE
               MOVE DFHPROTN TO ACCSEL3A ACCNM3A
           END-IF
           IF C005-ACCOUNTNUMBER-4 > SPACES
               MOVE ' EUR' TO C005-EUR-4
               MOVE C005-ACCOUNT-4 TO ACC4O
           ELSE
               MOVE DFHPROTN TO ACCSEL4A ACCNM4A
           END-IF
           IF C005-ACCOUNTNUMBER-5 > SPACES
               MOVE ' EUR' TO C005-EUR-5
               MOVE C005-ACCOUNT-5 TO ACC5O
           ELSE
               MOVE DFHPROTN TO ACCSEL5A ACCNM5A
           END-IF
           MOVE W-MSG TO MSGO
           PERFORM PUT-ATTRIBUTES
      
           EXEC CICS SEND 
               MAP('MAP001')
               MAPSET('COB005') 
               ERASE 
               CURSOR
           END-EXEC
       .
       PUT-ATTRIBUTES.
           IF A-BIRTHDATE > SPACE
               MOVE A-BIRTHDATE TO BDATEA
               MOVE -1 TO BDATEL
           END-IF
           IF A-ZIPCODE > SPACE
               MOVE A-ZIPCODE TO ZIPA
               MOVE -1 TO ZIPL
           END-IF
           IF A-LASTNAME > SPACE
               MOVE A-LASTNAME TO LASTA
               MOVE -1 TO LASTL
           END-IF
           IF A-FIRSTNAME > SPACE
               MOVE A-FIRSTNAME TO FIRSTA
               MOVE -1 TO FIRSTL
           END-IF
           IF C005-A <= SPACES 
               IF C005-ACCOUNTNUMBER-1 > SPACES
                   MOVE -1 TO ACCSEL1L
               ELSE
                   MOVE -1 TO ZIPL
               END-IF
           END-IF 
       .
       VALIDATE.
       PERFORM VALIDATE-BIRTHDATE
       PERFORM VALIDATE-ZIPCODE
       PERFORM VALIDATE-FIRSTNAME
       PERFORM VALIDATE-LASTNAME
       .
       VALIDATE-BIRTHDATE.
       MOVE SPACES TO A-BIRTHDATE
       IF C005-BIRTHDATE = SPACES
           MOVE 'Birthdate must be filled' TO W-MSG
           MOVE 'H' TO A-BIRTHDATE
       ELSE
           IF C005-BIRTHDATE(1:1) >= '0' AND
               C005-BIRTHDATE(1:1) <= '9' AND
               C005-BIRTHDATE(2:1) >= '0' AND
               C005-BIRTHDATE(2:1) <= '9' AND
               C005-BIRTHDATE(3:1) >= '0' AND
               C005-BIRTHDATE(3:1) <= '9' AND
               C005-BIRTHDATE(4:1) >= '0' AND
               C005-BIRTHDATE(4:1) <= '9' AND
               C005-BIRTHDATE(5:1) = '-' AND
               C005-BIRTHDATE(6:1) >= '0' AND
               C005-BIRTHDATE(6:1) <= '9' AND
               C005-BIRTHDATE(7:1) >= '0' AND
               C005-BIRTHDATE(7:1) <= '9' AND
               C005-BIRTHDATE(8:1) = '-' AND
               C005-BIRTHDATE(9:1) >= '0' AND
               C005-BIRTHDATE(9:1) <= '9' AND
               C005-BIRTHDATE(10:1) >= '0' AND
               C005-BIRTHDATE(10:1) <= '9' AND
               C005-BIRTHDATE(6:2) <= '12' AND
               C005-BIRTHDATE(9:2) <= '31'
                   EXEC CICS ASKTIME ABSTIME(W-ABSTIME)
                   END-EXEC
                   EXEC CICS FORMATTIME ABSTIME(W-ABSTIME)
                       DATESEP('-') YYYYMMDD(W-DATE)
                   END-EXEC
                   IF W-DATE <= C005-BIRTHDATE
                       MOVE 'Date must be in the past' TO W-MSG
                       MOVE 'H' to A-BIRTHDATE
                   END-IF
           ELSE
               MOVE 'Invalid date' TO W-MSG
               MOVE 'H' TO A-BIRTHDATE
           END-IF
       END-IF
       .
       VALIDATE-ZIPCODE.
       MOVE SPACE TO A-ZIPCODE
       MOVE SPACES TO C005-CITY
       IF C005-ZIPCODE = SPACES
           MOVE 'Zipcode must be filled' TO W-MSG
           MOVE 'H' TO A-ZIPCODE
       ELSE
           PERFORM GET-ZIP
           EVALUATE SQLCODE
           WHEN 0
               MOVE SPACE TO A-ZIPCODE
           WHEN 100
               MOVE 'Unknown zipcode' TO W-MSG
               MOVE 'H' TO A-ZIPCODE
           WHEN OTHER
               PERFORM PUT-SQLCODE
               MOVE 'H' TO A-ZIPCODE
           END-EVALUATE
       END-IF
       .
       VALIDATE-FIRSTNAME.
       MOVE SPACE TO A-FIRSTNAME
       IF C005-FIRSTNAME = SPACES
           MOVE 'First name must be filled' TO W-MSG
           MOVE 'H' TO A-FIRSTNAME
       END-IF
       .
       VALIDATE-LASTNAME.
       MOVE SPACE TO A-LASTNAME
       IF C005-LASTNAME = SPACES
           MOVE 'Last name must be filled' TO W-MSG
           MOVE 'H' TO A-LASTNAME
       END-IF
       .
       GET-MAP.
       EXEC CICS RECEIVE
           MAP('MAP001')
           MAPSET('COB005')
       END-EXEC
       IF BDATEL > 0 OR BDATEF = DFHBMEF OR BDATEF = DFHBMEOF
           MOVE BDATEI TO C005-BIRTHDATE
           INSPECT C005-BIRTHDATE REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
       END-IF
       IF ZIPL > 0 OR ZIPF = DFHBMEF OR ZIPF = DFHBMEOF
           MOVE ZIPI TO C005-ZIPCODE
           INSPECT C005-ZIPCODE REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
       END-IF
       IF FIRSTL > 0 OR FIRSTF = DFHBMEF OR FIRSTF = DFHBMEOF
           MOVE FIRSTI TO C005-FIRSTNAME
           INSPECT C005-FIRSTNAME REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
       END-IF
       IF LASTL > 0 OR LASTF = DFHBMEF OR LASTF = DFHBMEOF
           MOVE LASTI TO C005-LASTNAME
           INSPECT C005-LASTNAME REPLACING 
               ALL '_' BY SPACE ALL LOW-VALUE BY SPACE
       END-IF
       .
       PUT-DB.
       EXEC SQL
           UPDATE dbo.Customer
               SET LastName = :C005-LASTNAME,
               FirstName = :C005-FIRSTNAME,
               ZipCode = :C005-ZIPCODE,
               BirthDate = :C005-BIRTHDATE 
           WHERE CustomerId = :C005-CUSTOMERID
       END-EXEC
       IF SQLCODE = 0
           MOVE 'Update successful' TO W-MSG
       ELSE
           PERFORM PUT-SQLCODE
       END-IF
       .
       EVALUATE-SQLCODE.
       EVALUATE SQLCODE
       WHEN 0
       WHEN 100
           CONTINUE
       WHEN OTHER
           PERFORM PUT-SQLCODE
       END-EVALUATE
       .
       PUT-SQLCODE.
           MOVE SQLCODE TO W-SQLCODE
           MOVE 'An database error has occurred, SQLCODE:' TO W-MSG
           MOVE W-SQLCODE TO W-MSG(33:)
       .
           