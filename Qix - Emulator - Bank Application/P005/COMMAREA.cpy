       01 W-COMMAREA.
           10 C004.
     2         20 C004-EIBCPOSN PIC S9(4) COMP.
     2         20 C004-CUSTOMERID-FIRST PIC S9(4) COMP VALUE 0.
     2         20 C004-CUSTOMERID-LAST PIC S9(4) COMP VALUE 0.
    20         20 C004-SELA PIC X OCCURS 20.
    40         20 C004-CUST PIC S9(4) COMP OCCURS 20.
     4         20 C004-CUSTOMERID-SELECTED PIC S9(4).
     6         20 C004-CUSTOMERID-FILTER PIC 999999.
    15         20 C004-LASTNAME-FILTER PIC X(15).
    15         20 C004-FIRSTNAME-FILTER PIC X(15).
     4         20 C004-ZIPCODE-FILTER PIC X(4).
    10         20 C004-CITY-FILTER PIC X(10).
    10         20 C004-BIRTHDATE-FILTER PIC X(10).
           10 C005.
     2         20 C005-CUSTOMERID PIC S9(4).
    40         20 C005-LASTNAME PIC X(40).
    40         20 C005-FIRSTNAME PIC X(40).
     4         20 C005-ZIPCODE PIC XXXX.
    60         20 C005-CITY PIC X(60).
     2        20 C005-COUNTRY PIC XX.
    30         20 C005-BIRTHDATE PIC X(30).
               20 C005-A.
     1             30 A-LASTNAME PIC X.
     2             30 A-FIRSTNAME PIC X.
     2             30 A-ZIPCODE PIC X.
     1             30 A-BIRTHDATE PIC X.
               20 C005-ACCOUNT
                   30 C005-ACCOUNT-1.
    20                 40 C005-ACCOUNTNUMBER-1 PIC X(20).
    15                  40 C005-AMOUNT-1 PIC -----------9,99.
     4                 40 C005-EUR-1 PIC X(4) VALUE ' EUR'.
                   30 C005-ACCOUNT-2.
    20                 40 C005-ACCOUNTNUMBER-2 PIC X(20).
    15                 40 C005-AMOUNT-2 PIC -----------9,99.
     4                 40 C005-EUR-2 PIC X(4) VALUE ' EUR'.
                   30 C005-ACCOUNT-3.
    20                 40 C005-ACCOUNTNUMBER-3 PIC X(20).
    15                 40 C005-AMOUNT-3 PIC -----------9,99.
     4                 40 C005-EUR-3 PIC X(4) VALUE ' EUR'.
                   30 C005-ACCOUNT-4
    20                 40 C005-ACCOUNTNUMBER-4 PIC X(20).
    15                 40 C005-AMOUNT-4 PIC -----------9,99.
     4                 40 C005-EUR-4 PIC X(4) VALUE ' EUR'.
                   30 C005-ACCOUNT-5.
    20                 40 C005-ACCOUNTNUMBER-5 PIC X(20).
    15                 40 C005-AMOUNT-5 PIC -----------9,99.
     4                 40 C005-EUR-5 PIC X(4) VALUE ' EUR'.
           10 C006.
     4         20 C006-FIRST PIC XXXX.
     4         20 C006-LAST PIC XXXX.
    20         20 C006-SELA PIC X OCCURS 20.
    80         20 C006-ZIPCODE PIC XXXX OCCURS 20.
     4         20 C006-ZIPCODE-SELECTED PIC XXXX.
     4         20 C006-ZIPCODE-FILTER PIC X(4).
    60         20 C006-CITY-FILTER PIC X(60).
           10 C007.
    20         20 C007-ACCOUNTNUMBER PIC X(20).
    15         20 C007-AMOUNT PIC -----------9,99.
           10 C008.
               20 C008-PARAMETERS.
    20             30 C008-ACCOUNTNUMBER-FROM PIC X(20).
    20         20 C008-INTERNAL.
    20             30 C008-ACCOUNTNUMBER-TO PIC X(20).
    40             30 C008-LASTNAME PIC X(40).
    40             30 C008-FIRSTNAME PIC X(40).
     4             30 C008-ZIPCODE PIC XXXX.
    60             30 C008-CITY PIC X(60).
    30             30 C008-COUNTRY PIC X(30).
     6             30 C008-AMOUNT PIC S9(8)V9(2) COMP-3.
     7             30 C008-AMOUNT-ONSCREEN PIC X(7).
    40             30 C008-DESCRIPTIONTEXT PIC X(40).
    20             30 C008-PAYMENT-TYPE PIC X(20).
                   30 C008-A.
     1                 40 C008-A-ACCOUNTNUMBER-TO PIC X.
     1                 40 C008-A-AMOUNT PIC X.
    30             30 C008-RETURN-MESSAGE PIC X(30). 
           10 C010.
    20         20 C010-ACCOUNTNUMBER-FIRST PIC X(20).
    20         20 C010-ACCOUNTNUMBER-LAST PIC X(20).
    20         20 C010-SELA PIC X OCCURS 20.
   400         20 C010-ACCOUNTNUMBER PIC X(20) OCCURS 20.
    20         20 C010-ACCOUNTNUMBER-SELECTED PIC X(20).
    20         20 C010-ACCOUNTNUMBER-FILTER PIC X(20).
    20         20 C010-LASTNAME-FILTER PIC X(20).
    20         20 C010-FIRSTNAME-FILTER PIC X(20).
     4         20 C010-ZIPCODE-FILTER PIC X(4).
    20         20 C010-CITY-FILTER PIC X(20).
------
  1623      