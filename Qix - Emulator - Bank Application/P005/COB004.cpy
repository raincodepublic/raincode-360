        01 MAP001I .
            02 FILLER PIC X(12).
            02 FCUSTL PIC S9(4) COMP-5.
            02 FCUSTF PIC X(1).
            02 FILLER REDEFINES FCUSTF.
              04 FCUSTA PIC X.
            02 FCUSTI PIC X(6).
            02 FLASTL PIC S9(4) COMP-5.
            02 FLASTF PIC X(1).
            02 FILLER REDEFINES FLASTF.
              04 FLASTA PIC X.
            02 FLASTI PIC X(15).
            02 FFIRSTL PIC S9(4) COMP-5.
            02 FFIRSTF PIC X(1).
            02 FILLER REDEFINES FFIRSTF.
              04 FFIRSTA PIC X.
            02 FFIRSTI PIC X(15).
            02 FZIPL PIC S9(4) COMP-5.
            02 FZIPF PIC X(1).
            02 FILLER REDEFINES FZIPF.
              04 FZIPA PIC X.
            02 FZIPI PIC X(4).
            02 FCITYL PIC S9(4) COMP-5.
            02 FCITYF PIC X(1).
            02 FILLER REDEFINES FCITYF.
              04 FCITYA PIC X.
            02 FCITYI PIC X(10).
            02 FBDATEL PIC S9(4) COMP-5.
            02 FBDATEF PIC X(1).
            02 FILLER REDEFINES FBDATEF.
              04 FBDATEA PIC X.
            02 FBDATEI PIC X(10).
            02 S01L PIC S9(4) COMP-5.
            02 S01F PIC X(1).
            02 FILLER REDEFINES S01F.
              04 S01A PIC X.
            02 S01I PIC X(1).
            02 L01L PIC S9(4) COMP-5.
            02 L01F PIC X(1).
            02 FILLER REDEFINES L01F.
              04 L01A PIC X.
            02 L01I PIC X(70).
            02 S02L PIC S9(4) COMP-5.
            02 S02F PIC X(1).
            02 FILLER REDEFINES S02F.
              04 S02A PIC X.
            02 S02I PIC X(1).
            02 L02L PIC S9(4) COMP-5.
            02 L02F PIC X(1).
            02 FILLER REDEFINES L02F.
              04 L02A PIC X.
            02 L02I PIC X(70).
            02 S03L PIC S9(4) COMP-5.
            02 S03F PIC X(1).
            02 FILLER REDEFINES S03F.
              04 S03A PIC X.
            02 S03I PIC X(1).
            02 L03L PIC S9(4) COMP-5.
            02 L03F PIC X(1).
            02 FILLER REDEFINES L03F.
              04 L03A PIC X.
            02 L03I PIC X(70).
            02 S04L PIC S9(4) COMP-5.
            02 S04F PIC X(1).
            02 FILLER REDEFINES S04F.
              04 S04A PIC X.
            02 S04I PIC X(1).
            02 L04L PIC S9(4) COMP-5.
            02 L04F PIC X(1).
            02 FILLER REDEFINES L04F.
              04 L04A PIC X.
            02 L04I PIC X(70).
            02 S05L PIC S9(4) COMP-5.
            02 S05F PIC X(1).
            02 FILLER REDEFINES S05F.
              04 S05A PIC X.
            02 S05I PIC X(1).
            02 L05L PIC S9(4) COMP-5.
            02 L05F PIC X(1).
            02 FILLER REDEFINES L05F.
              04 L05A PIC X.
            02 L05I PIC X(70).
            02 S06L PIC S9(4) COMP-5.
            02 S06F PIC X(1).
            02 FILLER REDEFINES S06F.
              04 S06A PIC X.
            02 S06I PIC X(1).
            02 L06L PIC S9(4) COMP-5.
            02 L06F PIC X(1).
            02 FILLER REDEFINES L06F.
              04 L06A PIC X.
            02 L06I PIC X(70).
            02 S07L PIC S9(4) COMP-5.
            02 S07F PIC X(1).
            02 FILLER REDEFINES S07F.
              04 S07A PIC X.
            02 S07I PIC X(1).
            02 L07L PIC S9(4) COMP-5.
            02 L07F PIC X(1).
            02 FILLER REDEFINES L07F.
              04 L07A PIC X.
            02 L07I PIC X(70).
            02 S08L PIC S9(4) COMP-5.
            02 S08F PIC X(1).
            02 FILLER REDEFINES S08F.
              04 S08A PIC X.
            02 S08I PIC X(1).
            02 L08L PIC S9(4) COMP-5.
            02 L08F PIC X(1).
            02 FILLER REDEFINES L08F.
              04 L08A PIC X.
            02 L08I PIC X(70).
            02 S09L PIC S9(4) COMP-5.
            02 S09F PIC X(1).
            02 FILLER REDEFINES S09F.
              04 S09A PIC X.
            02 S09I PIC X(1).
            02 L09L PIC S9(4) COMP-5.
            02 L09F PIC X(1).
            02 FILLER REDEFINES L09F.
              04 L09A PIC X.
            02 L09I PIC X(70).
            02 S10L PIC S9(4) COMP-5.
            02 S10F PIC X(1).
            02 FILLER REDEFINES S10F.
              04 S10A PIC X.
            02 S10I PIC X(1).
            02 L10L PIC S9(4) COMP-5.
            02 L10F PIC X(1).
            02 FILLER REDEFINES L10F.
              04 L10A PIC X.
            02 L10I PIC X(70).
            02 S11L PIC S9(4) COMP-5.
            02 S11F PIC X(1).
            02 FILLER REDEFINES S11F.
              04 S11A PIC X.
            02 S11I PIC X(1).
            02 L11L PIC S9(4) COMP-5.
            02 L11F PIC X(1).
            02 FILLER REDEFINES L11F.
              04 L11A PIC X.
            02 L11I PIC X(70).
            02 S12L PIC S9(4) COMP-5.
            02 S12F PIC X(1).
            02 FILLER REDEFINES S12F.
              04 S12A PIC X.
            02 S12I PIC X(1).
            02 L12L PIC S9(4) COMP-5.
            02 L12F PIC X(1).
            02 FILLER REDEFINES L12F.
              04 L12A PIC X.
            02 L12I PIC X(70).
            02 S13L PIC S9(4) COMP-5.
            02 S13F PIC X(1).
            02 FILLER REDEFINES S13F.
              04 S13A PIC X.
            02 S13I PIC X(1).
            02 L13L PIC S9(4) COMP-5.
            02 L13F PIC X(1).
            02 FILLER REDEFINES L13F.
              04 L13A PIC X.
            02 L13I PIC X(70).
            02 S14L PIC S9(4) COMP-5.
            02 S14F PIC X(1).
            02 FILLER REDEFINES S14F.
              04 S14A PIC X.
            02 S14I PIC X(1).
            02 L14L PIC S9(4) COMP-5.
            02 L14F PIC X(1).
            02 FILLER REDEFINES L14F.
              04 L14A PIC X.
            02 L14I PIC X(70).
            02 S15L PIC S9(4) COMP-5.
            02 S15F PIC X(1).
            02 FILLER REDEFINES S15F.
              04 S15A PIC X.
            02 S15I PIC X(1).
            02 L15L PIC S9(4) COMP-5.
            02 L15F PIC X(1).
            02 FILLER REDEFINES L15F.
              04 L15A PIC X.
            02 L15I PIC X(70).
            02 S16L PIC S9(4) COMP-5.
            02 S16F PIC X(1).
            02 FILLER REDEFINES S16F.
              04 S16A PIC X.
            02 S16I PIC X(1).
            02 L16L PIC S9(4) COMP-5.
            02 L16F PIC X(1).
            02 FILLER REDEFINES L16F.
              04 L16A PIC X.
            02 L16I PIC X(70).
            02 S17L PIC S9(4) COMP-5.
            02 S17F PIC X(1).
            02 FILLER REDEFINES S17F.
              04 S17A PIC X.
            02 S17I PIC X(1).
            02 L17L PIC S9(4) COMP-5.
            02 L17F PIC X(1).
            02 FILLER REDEFINES L17F.
              04 L17A PIC X.
            02 L17I PIC X(70).
            02 S18L PIC S9(4) COMP-5.
            02 S18F PIC X(1).
            02 FILLER REDEFINES S18F.
              04 S18A PIC X.
            02 S18I PIC X(1).
            02 L18L PIC S9(4) COMP-5.
            02 L18F PIC X(1).
            02 FILLER REDEFINES L18F.
              04 L18A PIC X.
            02 L18I PIC X(70).
            02 MSGL PIC S9(4) COMP-5.
            02 MSGF PIC X(1).
            02 FILLER REDEFINES MSGF.
              04 MSGA PIC X.
            02 MSGI PIC X(70).
        01 MAP001O REDEFINES MAP001I.
            02 FILLER PIC X(12).
            02 FILLER PIC X(3).
            02 FCUSTO PIC X(6).
            02 FILLER PIC X(3).
            02 FLASTO PIC X(15).
            02 FILLER PIC X(3).
            02 FFIRSTO PIC X(15).
            02 FILLER PIC X(3).
            02 FZIPO PIC X(4).
            02 FILLER PIC X(3).
            02 FCITYO PIC X(10).
            02 FILLER PIC X(3).
            02 FBDATEO PIC X(10).
            02 FILLER PIC X(3).
            02 S01O PIC X(1).
            02 FILLER PIC X(3).
            02 L01O PIC X(70).
            02 FILLER PIC X(3).
            02 S02O PIC X(1).
            02 FILLER PIC X(3).
            02 L02O PIC X(70).
            02 FILLER PIC X(3).
            02 S03O PIC X(1).
            02 FILLER PIC X(3).
            02 L03O PIC X(70).
            02 FILLER PIC X(3).
            02 S04O PIC X(1).
            02 FILLER PIC X(3).
            02 L04O PIC X(70).
            02 FILLER PIC X(3).
            02 S05O PIC X(1).
            02 FILLER PIC X(3).
            02 L05O PIC X(70).
            02 FILLER PIC X(3).
            02 S06O PIC X(1).
            02 FILLER PIC X(3).
            02 L06O PIC X(70).
            02 FILLER PIC X(3).
            02 S07O PIC X(1).
            02 FILLER PIC X(3).
            02 L07O PIC X(70).
            02 FILLER PIC X(3).
            02 S08O PIC X(1).
            02 FILLER PIC X(3).
            02 L08O PIC X(70).
            02 FILLER PIC X(3).
            02 S09O PIC X(1).
            02 FILLER PIC X(3).
            02 L09O PIC X(70).
            02 FILLER PIC X(3).
            02 S10O PIC X(1).
            02 FILLER PIC X(3).
            02 L10O PIC X(70).
            02 FILLER PIC X(3).
            02 S11O PIC X(1).
            02 FILLER PIC X(3).
            02 L11O PIC X(70).
            02 FILLER PIC X(3).
            02 S12O PIC X(1).
            02 FILLER PIC X(3).
            02 L12O PIC X(70).
            02 FILLER PIC X(3).
            02 S13O PIC X(1).
            02 FILLER PIC X(3).
            02 L13O PIC X(70).
            02 FILLER PIC X(3).
            02 S14O PIC X(1).
            02 FILLER PIC X(3).
            02 L14O PIC X(70).
            02 FILLER PIC X(3).
            02 S15O PIC X(1).
            02 FILLER PIC X(3).
            02 L15O PIC X(70).
            02 FILLER PIC X(3).
            02 S16O PIC X(1).
            02 FILLER PIC X(3).
            02 L16O PIC X(70).
            02 FILLER PIC X(3).
            02 S17O PIC X(1).
            02 FILLER PIC X(3).
            02 L17O PIC X(70).
            02 FILLER PIC X(3).
            02 S18O PIC X(1).
            02 FILLER PIC X(3).
            02 L18O PIC X(70).
            02 FILLER PIC X(3).
            02 MSGO PIC X(70).
