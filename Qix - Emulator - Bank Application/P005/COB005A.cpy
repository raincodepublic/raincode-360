      ***********************************************************
      *                                                         *
      *   GET CUSTOMER DETAILS AND ACCOUNTS                     *
      *                                                         *
      ***********************************************************
       01 COB005A.
           10 COB005A-IN.
   2           20 COB005A-CUSTOMERID pic S9(4) COMP. 
           10 COB005A-OUT.
   4           20 COB005A-ERROR-CODE PIC S9(8) COMP-5.
  50           20 COB005A-ERROR-MSG PIC X(50).
   2           20 COB005A-ACCOUNTS-FETCHED PIC S9(4) COMP.
  20           20 COB005A-LASTNAME PIC X(20).
  20           20 COB005A-FIRSTNAME PIC X(20).
   4           20 COB005A-ZIPCODE PIC X(4).
  20           20 COB005A-CITY PIC X(20).
  10           20 COB005A-BIRTHDATE PIC X(10).
               20 COB005A-ACCOUNT OCCURS 5.
 100               30 COB005A-ACCOUNTNUMBER PIC X(20).
  30               30 COB005A-AMOUNT PIC S9(8)V9(2) COMP-3.
----  
 262  

