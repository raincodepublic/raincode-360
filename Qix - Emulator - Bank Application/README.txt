
PURPOSE
. Illustrate the use of CICS by means of a functional bank application

FEATURES
. Use of CICS commands such as XCTL, LINK and RETURN
. Use of BMS maps
. Use of microsrvices (DAPR)

RUNNING THE APPLICATION
. Via the developer command prompt
  . Go to tools/Command Line/Developer Command Prompt
  . enter 'startcics'
  . there new windows titled 'PS', 'TS' and 'Raincode Bank) open up. 'PS' and 'TS' are the CICS emulator (aka Raincode Qix)
  . enter 'C004' in the 3270 emulator window ('Raincode Bank') to start the bank app
. Via the Developer Powershell 
  . Go to Tools/Command Line/Developer Powershell
  . enter '.\run'
  . wait for the Qix windows and the 3270 emulator to open up
  . the bank application starts automatically

OPENING ADDITIONAL TERMINAL WINDOWS
. the Qix server (PS/TS) can support multiple terminals as long as it is running
. enter 'start3270' (command prompt) or '.\run3270' (Powershell) to open up another window
. entering 'C004' to start the app

ABOUT THE 3270 TERMINAL EMULATOR
. The product that comes with this demo (wc3270) is license-free
. Do resize the window by dragging a corner while the application is running
. Adjust the font size by pressing control and scrolling the mouse wheel. This may not work in a VM.
. Clicking on the terminal icon in the title bar opens a menu. The properties option allows you to adjust the font manually along with some other settings.
. Some handy shortcut keys
  . Alt-K : keypad
  . Alt-N : menu bar
  . Alt-J : return
. see http://x3270.bgp.nu/wc3270-man.html for more information

HOW TO DEBUG
. Place a breakpoint at line 44 of program COB004.cob ('GET-MAP') by clicking in the left margin. A red dot appears.
. Make sure that Qix is running and connected.
. Start the application in the 3270 window
. In Visual Studio, select Debug/Attach to Process on the main menu.
. A list of processes opens up. Select 'ProcessingServerRunner.exe' and click on the 'Attach' button.
. Visual Studio switches to debug mode. Because of the psudoconversational nature of the application the program COB004.cob is not running at this point.
. Press <F8> on the main screen of the bank application to scroll down. This will activate COB004.cob and the execution will stop at the brakpoint we put in earlier.
. Hover over any variable in the source text of COB004.cob to see it contents.
. Check the 'locals' window in Visual Studio to see a list of local variables an their corresponding values.
. Use the 'step' buttons to step through the program.
. Stepping over the 'EXEC CICS RETURN' will trigger an exception. This normal. Pressing continue will end the program without detaching the debug session. Continue debugging by pressing <F8> or <enter> in the application.
. Stop debugging by pressing the stop icon in the debug toolbar.

CLOSING QIX
. Close the Qix windows.

RESTARTING QIX
. The startcics and run scripts will automatically close any Qix that are already open

PROJECT (.CPROJ) FILE NOTES
. The target framework must be net6.0
  <TargetFrameworkVersion>net6.0</TargetFrameworkVersion>
. BMS maps are compiled via a pre-build event
  <PreBuildEvent>$(SolutionDir)buildbms</PreBuildEvent>

