#!/bin/sh
dotnet /opt/raincode/net8.0/QIX.TerminalServerRunner.dll -LogLevel=TRACE -TcpPort=$PORT_NO -Region=$REGION_ID -ConfigConnectionString="$CONN_STRING" -LogsDirectory="$LOGS_DIR"