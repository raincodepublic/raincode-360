//JCL001   JOB  CLASS=A,MSGCLASS=C
//STEP001  EXEC PGM=EZTPA00
//SYSOUT   DD SYSOUT=*
//PERSNL  DD *
Bonita Pepper            123
Kathie Rosabella         001
Lynsey Sidney            888
Lotus Lorrin             789
Ami Loren                336
Sherie Lynwood           911
Jake Karen               529
Izabelle Chanelle        630
Evvie Walker             111
Lilac Mia                345
/*
//SYSIN DD *
PARM DEBUG(FLOW)
*
FILE PERSNL FB(80 800)
NAME 1 25 A
DEPT 26 3 N
*
JOB INPUT PERSNL
PRINT DDOUT
*
REPORT DDOUT LINESIZE 60 SPREAD NODATE NOPAGE 
SEQUENCE DEPT
CONTROL DEPT
TITLE 1 'SALARY REPORT'
LINE DEPT NAME
/*
//