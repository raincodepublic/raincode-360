﻿CREATE OR ALTER FUNCTION [dbo].[CURRENTDATE]()
    RETURNS DATE
AS
BEGIN
  RETURN cast(dbo.CURRENTTIMESTAMP() as DATE);
END

GO

CREATE OR ALTER FUNCTION [dbo].[CURRENTTIME]()
    RETURNS TIME
AS
BEGIN
  RETURN cast(dbo.CURRENTTIMESTAMP() as TIME);
END

GO

CREATE OR ALTER FUNCTION [dbo].[CURRENTTIMESTAMP] ()
RETURNS datetime2
AS
BEGIN
    DECLARE @dateTimeResult datetime2
	DECLARE @starting_date datetime2 = CURRENT_TIMESTAMP
	DECLARE @ticks bigint = CONVERT(BIGINT, SESSION_CONTEXT(N'RAINCODE_DATE'))
	IF (@ticks IS NULL)
	    RETURN @starting_date
    IF (@ticks < 0)
    BEGIN
        -- Hours
        SET @dateTimeResult = DATEADD(HOUR, CEILING(@ticks / 36000000000.0), @starting_date)
        SET @ticks = @ticks % 36000000000
        -- Seconds
        SET @dateTimeResult = DATEADD(SECOND, CEILING(@ticks / 10000000.0), @dateTimeResult)
        SET @ticks = @ticks % 10000000
        -- Nanoseconds
        SET @dateTimeResult = DATEADD(NANOSECOND, @ticks * 100, @dateTimeResult)
    END
    ELSE
    BEGIN
        -- Hours
        SET @dateTimeResult = DATEADD(HOUR, FLOOR(@ticks / 36000000000.0), @starting_date)
        SET @ticks = @ticks % 36000000000
        -- Seconds
        SET @dateTimeResult = DATEADD(SECOND, FLOOR(@ticks / 10000000.0), @dateTimeResult)
        SET @ticks = @ticks % 10000000
        -- Nanoseconds
        SET @dateTimeResult = DATEADD(NANOSECOND, @ticks * 100, @dateTimeResult)
    END

    RETURN @dateTimeResult
END
