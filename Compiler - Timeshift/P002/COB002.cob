       IDENTIFICATION DIVISION.
       PROGRAM-ID.     COB002.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 W-TS PIC X(26).
       01 W-DTTM.
          10  YYYY PIC 9(4).
          10  MO   PIC 9(2).
          10  DD   PIC 9(2).
          10  HH   PIC 9(2).
          10  MI   PIC 9(2).
          10  SS   PIC 9(2).
          10  XX   PIC 9(2).
          10  ZZ   PIC S9(4).  
       COPY SQLCA.
       PROCEDURE DIVISION.
           MOVE FUNCTION CURRENT-DATE TO W-DTTM
           EXEC SQL
               SELECT CURRENT TIMESTAMP
               INTO :W-TS
               FROM SYSIBM.SYSDUMMY1
           END-EXEC
           DISPLAY 'COBOL TS: ' W-DTTM
           DISPLAY 'SQL   TS: ' W-TS
           GOBACK
           .