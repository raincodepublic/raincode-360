-- Register: TIMESTAMP_VCHAR26
EXECUTE Raincode.RComp_drop_udf 'TIMESTAMP_VCHAR26';
GO

CREATE FUNCTION Raincode.TIMESTAMP_VCHAR26
(
	@Input VARCHAR(26)
)
RETURNS DATETIME2
AS
BEGIN
    DECLARE @timestamp VARCHAR(26)
	DECLARE @date DATE
	DECLARE @hour VARCHAR(2)
	SET @timestamp = 
	CASE
	WHEN PATINDEX('__[.-]__[.-]______[.:]__[.:]%', @Input) <> 0 THEN
	  SUBSTRING(@Input,7,4)+'-'+SUBSTRING(@Input,4,2)+'-'+SUBSTRING(@Input,1,2)+'T'
	  +SUBSTRING(@Input,11,2)+':'+SUBSTRING(@Input,14,2)+':'+SUBSTRING(@Input,17,10)
	WHEN PATINDEX('____[.-]__[.-]____[.:]__[.:]%', @Input) <> 0 THEN
	  SUBSTRING(@Input,1,4)+'-'+SUBSTRING(@Input,6,2)+'-'+SUBSTRING(@Input,9,2)+'T'
	  +SUBSTRING(@Input,11,2)+':'+SUBSTRING(@Input,14,2)+':'+SUBSTRING(@Input,17,10)
	WHEN PATINDEX('__[.-]__[.-]____[- T]__[.:]__[.:]%', @Input) <> 0 THEN
	  SUBSTRING(@Input,7,4)+'-'+SUBSTRING(@Input,4,2)+'-'+SUBSTRING(@Input,1,2)+'T'
	  +SUBSTRING(@Input,12,2)+':'+SUBSTRING(@Input,15,2)+':'+SUBSTRING(@Input,18,9)
	WHEN PATINDEX('____[.-]__[.-]__[- T]__[.:]__[.:]%', @Input) <> 0 THEN
	  SUBSTRING(@Input,1,4)+'-'+SUBSTRING(@Input,6,2)+'-'+SUBSTRING(@Input,9,2)+'T'
	  +SUBSTRING(@Input,12,2)+':'+SUBSTRING(@Input,15,2)+':'+SUBSTRING(@Input,18,9)
	WHEN @Input = '' THEN '-' -- Invalid timestamp
	ELSE @Input
	END

	SET @hour = SUBSTRING(@timestamp, 12, 2)
	SET @date = CONVERT(DATE, SUBSTRING(@timestamp, 1, 10))

	IF (@hour = '24')
		IF (@date = '9999-12-31')
			BEGIN
				SET @timestamp = CONVERT(VARCHAR(10), @date) + 'T' + '23:59:59.999999'
			END
		ELSE
			BEGIN
				SET @date = DATEADD(day, 1, @date)
				SET @hour = '00'
				SET @timestamp = CONVERT(VARCHAR(10), @date) + 'T' + @hour + SUBSTRING(@timestamp, 14, 13)
			END

	RETURN CONVERT(DATETIME2, @timestamp)
END

GO

-- Register: CHAR26_TIMESTAMP
EXECUTE Raincode.RComp_drop_udf 'CHAR26_TIMESTAMP';
GO

CREATE FUNCTION Raincode.CHAR26_TIMESTAMP
(
	@Input DATETIME2
)
RETURNS CHAR(26)
AS
BEGIN
    DECLARE @tmp varchar(26)
    SET @tmp = convert(varchar, @Input, 127)
    DECLARE @usec char(6)
    RETURN SUBSTRING(@tmp, 1, 4) + '-' + SUBSTRING(@tmp, 6, 2) + '-' + SUBSTRING(@tmp, 9, 2) + '-' + SUBSTRING(@tmp, 12, 2) + '.' + SUBSTRING(@tmp, 15, 2) + '.' + SUBSTRING(@tmp, 18, 2) + '.' + (CASE WHEN LEN(@tmp) = 26 THEN SUBSTRING(@tmp, 21, 6) ELSE '000000' END)END
GO

-- Register: CHAR10_DATE
EXECUTE Raincode.RComp_drop_udf 'CHAR10_DATE';
GO

CREATE FUNCTION Raincode.CHAR10_DATE
(
	@Input DATETIME2
)
RETURNS CHAR(10)
AS
BEGIN
    DECLARE @tmp varchar(26)
    SET @tmp = convert(varchar, @Input, 127)
    RETURN SUBSTRING(@tmp, 1, 4) + '-' + SUBSTRING(@tmp, 6, 2) + '-' + SUBSTRING(@tmp, 9, 2) + '-' + SUBSTRING(@tmp, 12, 2)
END
GO

-- Register: DATE_CHAR10
EXECUTE Raincode.RComp_drop_udf 'DATE_CHAR10';
GO

CREATE FUNCTION Raincode.DATE_CHAR10
(
	@Input CHAR(10)
)
RETURNS DATE
AS
BEGIN
	RETURN CONVERT(DATE,
            CASE
                WHEN @Input = '' THEN '-' -- Invalid timestamp
                ELSE @Input
            END,
            CASE
                WHEN PATINDEX('____[.-]__[.-]__', @Input) <> 0 THEN 102
                WHEN @Input = '' THEN 102
                ELSE 104
            END
        )
END
GO

-- Register: CHAR8_TIME
EXECUTE Raincode.RComp_drop_udf 'CHAR8_TIME';
GO

CREATE FUNCTION Raincode.CHAR8_TIME
(
	@Input DATETIME2
)
RETURNS CHAR(8)
AS
BEGIN
    DECLARE @tmp varchar(26)
    SET @tmp = convert(varchar, @Input, 127)
    RETURN SUBSTRING(@tmp, 12, 2) + '.' + SUBSTRING(@tmp, 15, 2) + '.' + SUBSTRING(@tmp, 18, 2)
END
GO

-- Register: TIME_CHAR8
EXECUTE Raincode.RComp_drop_udf 'TIME_CHAR8';
GO

CREATE FUNCTION Raincode.TIME_CHAR8
(
	@Input CHAR(8)
)
RETURNS TIME(0)
AS
BEGIN
	RETURN CONVERT(TIME(0),
            CASE
                WHEN PATINDEX('__.__.__', @Input) <> 0 THEN SUBSTRING(@Input, 1, 2) + ':' + SUBSTRING(@Input, 4, 2) + ':' + SUBSTRING(@Input, 7, 2)
                WHEN @Input = '' THEN '-' -- Invalid timestamp
                ELSE @Input
            END
        )
END
GO

-- Register: DATE_FROM_STRING
EXECUTE Raincode.RComp_drop_udf 'DATE_FROM_STRING';
GO

CREATE FUNCTION Raincode.DATE_FROM_STRING(@Str VARCHAR(MAX))
    RETURNS DATE
AS
BEGIN
    RETURN Raincode.DATE_CHAR10(@Str);
END
GO

-- Register: VARCHAR_FORMAT_DATE
EXECUTE Raincode.RComp_drop_udf 'VARCHAR_FORMAT_DATE';
GO

CREATE FUNCTION [Raincode].[VARCHAR_FORMAT_DATE]
(
@theDate VARCHAR(10), @format VARCHAR(10)
)
RETURNS VARCHAR(10)
AS
BEGIN
	RETURN FORMAT(CONVERT(DATE,
        CASE
            WHEN @theDate = '' THEN '-' -- Invalid timestamp
            ELSE @theDate
        END), @format)
END
GO

-- Register: VARCHAR_FORMAT_DATETIME2
EXECUTE Raincode.RComp_drop_udf 'VARCHAR_FORMAT_DATETIME2';
GO

CREATE FUNCTION [Raincode].[VARCHAR_FORMAT_DATETIME2]
(
@theDate VARCHAR(26), @format VARCHAR(26)
)
RETURNS VARCHAR(26)
AS
BEGIN
	RETURN FORMAT(Raincode.TIMESTAMP_VCHAR26(@theDate), @format)
END
GO

-- Register: VARCHAR_FORMAT_NUMERIC
EXECUTE Raincode.RComp_drop_udf 'VARCHAR_FORMAT_NUMERIC';
GO

CREATE FUNCTION [Raincode].[VARCHAR_FORMAT_NUMERIC]
(
@theNumber FLOAT, @format VARCHAR(26)
)
RETURNS VARCHAR(255)
AS
BEGIN
  IF SIGN(@theNumber) < 0
	RETURN FORMAT(@theNumber, @format)
  ELSE
	RETURN ' ' + FORMAT(@theNumber, @format)
  RETURN ''
END
GO


