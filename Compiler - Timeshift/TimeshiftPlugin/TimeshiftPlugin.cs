﻿using RainCode.Core.Plugin;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Sql;
using System;
using System.Numerics;

[assembly: PluginProvider(typeof(TimeshiftPlugin.TimeshiftPlugin),
    nameof(TimeshiftPlugin.TimeshiftPlugin.Register))]

namespace TimeshiftPlugin
{
    public class TimeshiftPlugin
    {
        public static void Register()
        {
            ExecutionContext.ConstructorPlugin.Provide(1000, Initialize);
        }

        private static ExecutionContext Initialize(ExecutionContextArgs args)
        {
            ExecutionContext ctx = new ExecutionContext(args);
            _ = new TimeshiftPlugin(ctx);
            return ctx;
        }

        private TimeshiftPlugin(ExecutionContext ctx)
        {
            ctx.Hooks.PostOpenConnectionHandler += handleConnection;
        }

        private void handleConnection(SqlRuntime SqlRuntime)
        {
            if (SqlRuntime.DbConnection.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    Microsoft.Data.SqlClient.SqlCommand cmd = (Microsoft.Data.SqlClient.SqlCommand)SqlRuntime.DbConnection.CreateCommand();
                    cmd.CommandText = "EXEC sys.sp_set_session_context @key = N'RAINCODE_DATE' , @value = @RaincodeDate";
                    cmd.Parameters.AddWithValue("@RaincodeDate", RainCode.Core.RcDateTime.DateTimeOffset.Ticks);
                    cmd.ExecuteNonQuery();
                }
                catch (System.Exception e)
                {
                    System.Console.WriteLine(e);
                }
            }
        }
    }
}
