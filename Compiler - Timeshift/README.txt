T I M E S H I F T

0. Running the test
"""""""""""""""""""
In Raincode 360: 
. Execute the script 'Stored Procedures_TimeConversion_Raincode.sql' in the 'Bankdemo' database
. Execute the script 'Stored Procedures.sql' in the 'Bankdemo' database
. Press 'Start' on the main menu

Expected output on the console:
   COBOL TS: 2032041304392589+020
   SQL   TS: 2032-04-13 04:39:26.246666

1. Priciple
"""""""""""
. Timeshift is a feature that enables you to run programs at any particular point in time. It accomplishes this by manipulating the current date and time that is built into the Raincode runtime. With some additional programming, this mechanism can be further expanded to include the current date and time in embedded SQL. The remainder of this text descibes a possible implementation for Sql Server.
. The script 'Stored Procedures_TimeConversion_Raincode.sql' is a general script that should be run to support the execution of rewritten sql.
. The script 'Stored Procedures.sql' implements procedures that specifically support timeshift.

2. Initial Value and Offset
"""""""""""""""""""""""""""
The runtime offers a number of options that directly affect all date and time functions in COBOL and PL/1, macros in Assembler and embedded CICS statements that depend on the current date and/or time. Common examples are 'ACCEPT FROM' in COBOL, 'ASKTIME' command in CICS and the 'STCK' macro in assembler. Raincode lets you specify either an initial value or an offset to the current datetime that these functions base their result on.

   Method            Initial Value       Offset
   ---------------   ------------------  -----------------
   rclrun option     -InitialDateTime    -DateTimeOffset
   submit option     -InitialDateTime              
   envvar            RC_INITIALDATETIME  RC_DATETIMEOFFSET

The format of these values is that of datetime and timespan in C#.

   Type       Example
   --------   ---------------------------
   DateTime   2009-06-15T13:45:30.0000000
   Timespan   -1.12:24:02

The current datetime is set onnce at the beginning of the program and will evolve during the lifetime of the program. When using the submit option, it is set once at the beginning of the first legacy being executed.

3. Embedded SQL
"""""""""""""""
Embedded SQL comes with its own set of functions, also known as 'special registers' in DB2. The full list is listed below:

    DB2 datetime functions
    ------------------------
    CURRENT TIMESTAMP
    CURRENT_TIMESTAMP
    CURRENT DATE
    CURRENT_DATE
    CURRENT TIME
    CURRENT_TIME

Their values are not immediately affected by the initial value or offset options of the runtime. Making this work is a two-step process. First, we need to make sure that any reference to these special registers is replaced by an explicit function call. Raincode already provides the following script for this purpose.

    $Env:RCDIR\scripts\TimeShift.rcs

The 'RewriteSourceSQLProc' compiler option activates the script at compile time.

    :RewriteSourceSQLProc=TimeShift.RewriteSql

The script will then replace the datetime references as follows :

    ORIGINAL SQL        REWRITTEN SQL
    -----------------   ------------------
    CURRENT TIMESTAMP   CURRENTTIMESTAMP()
    CURRENT_TIMESTAMP   CURRENTTIMESTAMP()
    CURRENT DATE        CURRENTDATE()
    CURRENT_DATE        CURRENTDATE()
    CURRENT TIME        CURRENTTIME()
    CURRENT_TIME        CURRENTTIME()

When executed, the runtime will now look for stored procedures names CURRENTTIMESTAMP, CURRENTDATE and CURRENTTIME, for which we do need to provide an implementation ourselves. Ideally, this implementation should rely on the -InitialDateTime and -DateTimeOffset options that we discussed earlier. The following plugin will store the datetime offset in a session variable named 'RAINCODE_DATE'.

using RainCode.Core.Plugin;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Sql;
using System;
using System.Numerics;
[assembly: PluginProvider(typeof(TimeshiftPlugin.TimeshiftPlugin),
    nameof(TimeshiftPlugin.TimeshiftPlugin.Register))]
namespace TimeshiftPlugin
{
    public class TimeshiftPlugin
    {
        public static void Register()
        {
            ExecutionContext.ConstructorPlugin.Provide(1000, Initialize);
        }
        private static ExecutionContext Initialize(ExecutionContextArgs args)
        {
            ExecutionContext ctx = new ExecutionContext(args);
            _ = new TimeshiftPlugin(ctx);
            return ctx;
        }
        private TimeshiftPlugin(ExecutionContext ctx)
        {
            ctx.Hooks.PostOpenConnectionHandler += handleConnection;
        }
        private void handleConnection(SqlRuntime SqlRuntime)
        {
            if (SqlRuntime.DbConnection.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    Microsoft.Data.SqlClient.SqlCommand cmd = (Microsoft.Data.SqlClient.SqlCommand)SqlRuntime.DbConnection.CreateCommand();
                    cmd.CommandText = "EXEC sys.sp_set_session_context @key = N'RAINCODE_DATE' , @value = @RaincodeDate";
                    cmd.Parameters.AddWithValue("@RaincodeDate", RainCode.Core.RcDateTime.DateTimeOffset.Ticks);
                    cmd.ExecuteNonQuery();
                }
                catch (System.Exception e)
                {
                    System.Console.WriteLine(e);
                }
            }
        }
    }
}

Each session will maintain its own private value of RAINCODE_DATE, which is ideal for our purpose. Note that the runtime stores the timeshift value as an offset, even when it was originally specified as an initial value. Also, we are storing the offset as 'Ticks', whereby 

    1 Tick = 0.0000001 s

This overcomes the limitation that timespans are limited to 24 hours in Sql Server and also provides us with a unit that can be easily added to thecurrent datetime value via the DATEADD function in Sql Server. To activate the plugin, provide the following option to rclrun.

    -Plugin=TimeshiftPlugin

Finally we need to implement the stored procedures. The following script will do exactly that.

CREATE OR ALTER FUNCTION [dbo].[CURRENTDATE]()
    RETURNS DATE
AS
BEGIN
  RETURN cast(dbo.CURRENTTIMESTAMP() as DATE);
END

GO

CREATE OR ALTER FUNCTION [dbo].[CURRENTTIME]()
    RETURNS TIME
AS
BEGIN
  RETURN cast(dbo.CURRENTTIMESTAMP() as TIME);
END

GO

CREATE OR ALTER FUNCTION [dbo].[CURRENTTIMESTAMP] ()
RETURNS datetime2
AS
BEGIN
    DECLARE @dateTimeResult datetime2
       DECLARE @starting_date datetime2 = CURRENT_TIMESTAMP
       DECLARE @ticks bigint = CONVERT(BIGINT, SESSION_CONTEXT(N'RAINCODE_DATE'))
       IF (@ticks IS NULL)
           RETURN @starting_date
    IF (@ticks < 0)
    BEGIN
        -- Hours
        SET @dateTimeResult = DATEADD(HOUR, CEILING(@ticks / 36000000000.0), @starting_date)
        SET @ticks = @ticks % 36000000000
        -- Seconds
        SET @dateTimeResult = DATEADD(SECOND, CEILING(@ticks / 10000000.0), @dateTimeResult)
        SET @ticks = @ticks % 10000000
        -- Nanoseconds
        SET @dateTimeResult = DATEADD(NANOSECOND, @ticks * 100, @dateTimeResult)
    END
    ELSE
    BEGIN
        -- Hours
        SET @dateTimeResult = DATEADD(HOUR, FLOOR(@ticks / 36000000000.0), @starting_date)
        SET @ticks = @ticks % 36000000000
        -- Seconds
        SET @dateTimeResult = DATEADD(SECOND, FLOOR(@ticks / 10000000.0), @dateTimeResult)
        SET @ticks = @ticks % 10000000
        -- Nanoseconds
        SET @dateTimeResult = DATEADD(NANOSECOND, @ticks * 100, @dateTimeResult)
    END

    RETURN @dateTimeResult
END























These are also known as 'special registers' in DB2.


because 
The initial values or offset will not automatically carry over to embedded SQL, however. 






The absolute value is converted to an offset when the program or job first starts, and is then added to 'DateTime.Now' with each request, meaning that the result will evolve during the lifespan of the program.



JCL 



