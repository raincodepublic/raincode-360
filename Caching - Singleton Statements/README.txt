*********************************************
Compiler - SQL Caching - Singleton Statements
*********************************************

PURPOSE
. Demonstrate SQL Caching via a plugin
. Show that statements that do not retrieve all the columns of the table should not populate the cache

RUNNING THE DEMO
. Place a breakpoint on the GOBACK statement and start the Raincode debugger.
. You will see the SQL cache log on the console.
