       IDENTIFICATION DIVISION.
       PROGRAM-ID. SqlCacheSingleton.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       01 ZIP-CODE PIC 9(4).
       01 ZIP-CITY PIC X(30).
       01 ZIP-COUNTRY PIC X(2).
       PROCEDURE DIVISION.
           PERFORM TEST-CACHING-1A.
           PERFORM TEST-CACHING-1B.
           PERFORM TEST-CACHING-1C.
           PERFORM TEST-CACHING-4.
           PERFORM TEST-CACHING-3.
           PERFORM TEST-CACHING-2.
           PERFORM TEST-CACHING-2.
           PERFORM TEST-CACHING-3.
           GOBACK.
        TEST-CACHING-1A.
           DISPLAY "## TEST-CACHING-1A ##"
           MOVE 1030 TO ZIP-CODE
           MOVE SPACES TO ZIP-CITY ZIP-COUNTRY
           EXEC SQL
              SELECT CITY, COUNTRY
                INTO :ZIP-CITY, :ZIP-COUNTRY
                FROM ZIPCODE 
               WHERE ZIPCODE = :ZIP-CODE
           END-EXEC
           DISPLAY "[" ZIP-CODE "][" ZIP-CITY "]["
                       ZIP-COUNTRY "][" SQLCODE "]"
           .
        TEST-CACHING-1B.
           DISPLAY "## TEST-CACHING-1B ##"
           MOVE 1030 TO ZIP-CODE
           MOVE SPACES TO ZIP-CITY ZIP-COUNTRY
           EXEC SQL
              SELECT CITY, COUNTRY
                INTO :ZIP-CITY, :ZIP-COUNTRY
                FROM ZIPCODE 
               WHERE ZIPCODE = :ZIP-CODE
           END-EXEC
           DISPLAY "[" ZIP-CODE "][" ZIP-CITY "]["
                       ZIP-COUNTRY "][" SQLCODE "]"
           .
        TEST-CACHING-1C.
           DISPLAY "## TEST-CACHING-1C ##"
           MOVE 1040 TO ZIP-CODE
           MOVE SPACES TO ZIP-CITY ZIP-COUNTRY
           EXEC SQL
              SELECT CITY
                INTO :ZIP-CITY
                FROM ZIPCODE 
               WHERE ZIPCODE = :ZIP-CODE
           END-EXEC
           DISPLAY "[" ZIP-CODE "][" ZIP-CITY "]["
                       ZIP-COUNTRY "][" SQLCODE "]"
           .
        TEST-CACHING-1D.
           DISPLAY "## TEST-CACHING-1D ##"
           MOVE 1030 TO ZIP-CODE
           MOVE SPACES TO ZIP-CITY ZIP-COUNTRY
           EXEC SQL
              SELECT CITY
                INTO :ZIP-CITY
                FROM ZIPCODE 
               WHERE ZIPCODE = :ZIP-CODE
           END-EXEC
           DISPLAY "[" ZIP-CODE "][" ZIP-CITY "]["
                       ZIP-COUNTRY "][" SQLCODE "]"
           .
        TEST-CACHING-2.
           DISPLAY " "
           DISPLAY "## TEST-CACHING-2 ##".
           MOVE 0 TO SQLCODE.
           PERFORM
              VARYING ZIP-CODE FROM 1020 BY 10
              UNTIL (ZIP-CODE > 1120) OR (SQLCODE NOT = 0)
              WITH TEST BEFORE
              EXEC SQL
                 SELECT ZIPCODE, CITY, COUNTRY
                   INTO :ZIP-CODE, :ZIP-CITY, :ZIP-COUNTRY
                   FROM ZIPCODE 
                   WHERE ZIPCODE = :ZIP-CODE
              END-EXEC
              DISPLAY "[" ZIP-CODE "][" ZIP-CITY "]["
                          ZIP-COUNTRY "][" SQLCODE "]"
           END-PERFORM.
        TEST-CACHING-3.
           DISPLAY " "
           DISPLAY "## TEST-CACHING-3 ##".
           MOVE 0 TO SQLCODE.
           PERFORM
              VARYING ZIP-CODE FROM 1020 BY 10
              UNTIL (ZIP-CODE > 1090) OR (SQLCODE NOT = 0)
              WITH TEST BEFORE
              EXEC SQL
                 SELECT CITY, COUNTRY
                   INTO :ZIP-CITY, :ZIP-COUNTRY
                   FROM ZIPCODE 
                   WHERE ZIPCODE = :ZIP-CODE
              END-EXEC
              DISPLAY "[" ZIP-CODE "][" ZIP-CITY "]["
                          ZIP-COUNTRY "][" SQLCODE "]"
           END-PERFORM.
        TEST-CACHING-4.
           DISPLAY " "
           DISPLAY "## TEST-CACHING-4 ##".
           MOVE 0 TO SQLCODE.
           PERFORM
              VARYING ZIP-CODE FROM 1020 BY 10
              UNTIL (ZIP-CODE > 1090) OR (SQLCODE NOT = 0)
              WITH TEST BEFORE
              EXEC SQL
                  SELECT CITY
                    INTO :ZIP-CITY
                    FROM ZIPCODE 
                   WHERE ZIPCODE = :ZIP-CODE
              END-EXEC
              DISPLAY "[" ZIP-CODE "][" ZIP-CITY "][" SQLCODE "]"
           END-PERFORM.
                      

