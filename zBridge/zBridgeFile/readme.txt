zBridge/File demo
*****************
This demo show how to load a file into a VsamSql table and how to query the database.
Contains of the directory:
* catalog/Raincode.Catalog.xml: the catalog configuration. It contains the mapping between the file names (DSN) and the tables.
* catalog/RcDbConnections.csv: file containing the plans (the connection string).
* copy/ACCOUNT.cpy: the copybook describing the data of the ACCOUNT.dat file.
* data/ACCOUNT.dat: the data file transferred from the mainframe.
* generateTable.ps1: script to generate the table creation script based on the catalog configuration.
* generateView.ps1: script to generate the view creation script based on the copybook.
* file.LoadData.ps1: script to load the data into the table

To execute this demo, first execute the script “generateTable.ps1” that will generate the tables creation script based on the catalog configuration. The script is “output/table_creation.sql”. The sql script didn’t need to be execute because the tables are already created in the DB.
The second step is to be executed is “generateViews.ps1” to generate the sql view’s creation script based on the copybook. The sql script is “ACCOUNT_view.sql”. The sql script didn’t need to be executed because the view is already create in the DB.
And the third step is to execute “file.LoadData.ps1”. This script load the data into [VsamSql].[REC_ACCOUNT] table. But this table contains binary/EBCDIC data (see zBridge documentation). The view [VsamSql].[REC_ACCOUNT] can be used to query the data in a more user-friendly way.

file.LoadData.ps1
----------------------
The first part of the script defines variables with path and options:
* $configFile: Path to the configuration file
*$srcDir: root directory of data files
* $wrkDir: directory where temporary files will be created
* $bcpOptions: options used by bcp (connection and database name)
* $sqlcmdOptions: options used by sqlcmd (connection and database name)
* $toolDir : directory of VsamSql.Load
* $todo: list of files to be loaded in a csv format: <DSN>,<record length>,<file format: FB or VB),<data file relatif to $srcDir>

Then the processing of the files:
* Transformation of the data file into a format accepted by “bcp” using “VsamSql.Load”: a data file and a format file
* delete the existing data from the table
* loading the data into the table using “bcp”

catalog/Raincode.Catalog.xml
--------------------------------------
The important element  in this file, <datasetTemplate>, is the mapping between the file and the table:
    <datasetTemplate tableName="REC_ACCOUNT" connection="VSAMSQL">
      <pattern>
        <DSN>VSAMSQL\.DATA\.ACCOUNT</DSN>
      </pattern>
      <parameters length="36">
        <key start="0" length="19" />
      </parameters>
    </datasetTemplate>
