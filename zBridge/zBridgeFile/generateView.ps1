#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "VsamSql"
)

# Path to the directory containing the copybooks
$copyDir= Join-Path -Path $PSScriptRoot -ChildPath "copy"
# Path to the directory where intermediate files will be generated
$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output"
# Path to the directory where view creation scripts will be generated
$OutputDirView= Join-Path -Path $PSScriptRoot -ChildPath "Output"

$DbConnectionString = "Server=$DbServerString,1433;User ID=$DbUser;Password=$DbPassword;Persist Security Info=False;Encrypt=False;Connection Timeout=30;Initial Catalog=$DbName;"

# create output directories
if (!(Test-Path -Path $OutputDir)) {
   New-Item $OutputDir -ItemType "directory"
}
if (!(Test-Path -Path $OutputDirView)) {
   New-Item $OutputDirView -ItemType "directory"
}

# function to generate a view
function GenerateView($copyName, $tableName)
{
	# transformation of the copybook (.cpy) into a declaration description file (.xml)
	$proc = Start-Process -NoNewWindow -Wait -FilePath "cobrc" `
		-ArgumentList ":FirstUsableColumn=7", ":LastUsableColumn=72", ":MaxMem=1G",
		":DeclDescriptors=$outputDir/$copyName.xml", ":OutputDir=$outputDir",  "$copyDir/$copyName.cpy"
	# generate the view creation script
    $proc = Start-Process -PassThru -NoNewWindow -Wait -FilePath copybookViewGenerator `
        -ArgumentList "-conn=""$DbConnectionString""", "-OnlyTech", "-table=$tableName", "-output=$OutputDirView/${copyName}_view.sql", "-xml=$outputDir/$copyName.xml", "-readonly=true"
    if ($proc.ExitCode -ne 0) {
		Write-Error "copybookViewGenerator failed for  with code $($proc.ExitCode)."
	}
}

# generate the view using the "ACCOUT.cpy" copybook
GenerateView "ACCOUNT" "REC_ACCOUNT"