#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "VsamSql"
)

# Path to the configuration file
$configFile = "$PSScriptRoot/catalog/Raincode.Catalog.xml"
# directory of VsamSql.Load
$toolDir = "$env:RCBIN"
# Path to the directory where tables creation script will be generated
$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output"

$DbConnectionString = "Server=$DbServerString,1433;User ID=$DbUser;Password=$DbPassword;Persist Security Info=False;Encrypt=False;Connection Timeout=30;Initial Catalog=$DbName;"

# create output directories
if (!(Test-Path -Path $OutputDir)) {
   New-Item $OutputDir -ItemType "directory"
}

# Generation of the tables creation script
& "$tooldir/VsamSql.DbGenerator.exe" -CatalogConfiguration="$configFile" -OutputFile="$(Join-Path $OutputDir "table_creation.sql")"