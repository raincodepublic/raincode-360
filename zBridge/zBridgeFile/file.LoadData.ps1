﻿#Example of script to load data using VsamSql.Load and bcp

# Path to the configuration file
$configFile = "$PSScriptRoot/catalog/Raincode.Catalog.xml"
# root directory of data files
$srcDir = "$PSScriptRoot/Data"
# directory where temporary files will be created
$wrkDir = "$PSScriptRoot/wrk"
# options used by bcp (connection and database name)
$bcpOptions = "-S $env:DB_SERVER -U $env:DB_USER -P $env:DB_PASSWORD -d vsamsql -q -h ""CHECK_CONSTRAINTS"" ".Split(' ')
# options used by sqlcmd (connection and database name)
$sqlcmdOptions = "-S $env:DB_SERVER -U $env:DB_USER -P $env:DB_PASSWORD -d vsamsql -I".Split(' ')
# directory of VsamSql.Load
$toolDir = "$env:RCBIN"

# list of files to be loaded in a csv format: <DSN>,<record length>,<file format: FB or VB),<data file relatif to $srcDir>
$todo =
@'
dsn,recSize,recType,dataFile
VSAMSQL.DATA.ACCOUNT,36,FB,ACCOUNT.dat
'@ | ConvertFrom-Csv

foreach ($e in $todo) {
# convert the data file into bcp file
    if (!(Test-Path -Path $wrkDir)) {
      New-Item $wrkDir -ItemType "directory"
	}
	& $toolDir/VsamSql.Load -CatalogConfiguration="$configFile" -DataFile="$(Join-Path $srcDir $e.dataFile)" -OutputDirectory="$wrkDir" -TargetDsn="$($e.dsn)" -LogLevel=TRACE  -RecordLength="$($e.recSize)" -RecordFormat="$($e.recType)"
	write-host "after VsamSql.Load"
	$tableFile = $(Join-Path $wrkDir $($e.dsn)) + ".table"
	$tableName = Get-Content -Path $tableFile
	write-host "table: $tableName"
# delete data (before loading)
    sqlcmd  -e -Q "DELETE FROM $tableName" $sqlcmdOptions	
# load the data using bcp
	$data=$(Join-Path $wrkDir $e.table)
	write-host "bcp dbo.$tableName in $wrkDir/$($e.dsn).bcpData -f $wrkDir/$($e.dsn).fmt -e $wrkDir/err.txt -h TABLOCK $bcpOptions "
	& bcp "dbo.$tableName" "in" "$wrkDir/$($e.dsn).bcpData" -f "$wrkDir/$($e.dsn).fmt" -e "$wrkDir/err.txt" -h "TABLOCK" $bcpOptions 
	write-host "after bcp"
}
