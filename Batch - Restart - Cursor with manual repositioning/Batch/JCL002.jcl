//*********************************************************************
//*   
//*   JCL002 : Main process
//* 
//*********************************************************************
//JCL002 JOB COND=(4,LT)
//*--------------------------------------------------------------------
//*   COB001 : Update accounts and produce main output file
//*--------------------------------------------------------------------
//STEP001   EXEC PGM=COB001,COND=(4,LT)
//SYSOUT      DD SYSOUT=*
//SYSPRINT    DD SYSOUT=*
//SYSTSPRT    DD SYSOUT=*
//OUTFILE     DD DISP=OLD,DSN=ARC02.OUTFILE
//ARCSYSIN DD *
TRMBEFORCKP=3
/*