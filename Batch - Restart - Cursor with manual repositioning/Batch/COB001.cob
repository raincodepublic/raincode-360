       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB001.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT OUTFILE ASSIGN TO OUTFILE
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE IS SEQUENTIAL
           FILE STATUS IS W-OUTFILE-STATUS.
       DATA DIVISION.
       FILE SECTION.
       FD OUTFILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS R-OUTFILE.
       01 R-OUTFILE PIC X(80).
       WORKING-STORAGE SECTION.
      *********************
      *     VARIABLES     *
      *********************
       01 W-ACCOUNTNUMBER PIC X(19) VALUE SPACES.
       01 W-AMOUNT PIC S9(8)V9(2) COMP-3. 
       01 W-AMOUNT-X PIC ---------9,99.
       01 W-OUTFILE-STATUS PIC XX.
       01 W-RESTART PIC X VALUE 'N'
       01 FILLER PIC X(30) VALUE '** CHKP AREA END FOR AR/CTL **' 
          VOLATILE.
      *********************
      *     COPYBOOKS     *
      *********************
       COPY SQLCA.
      *********************
      *     CURSORS       *
      *********************
   	   EXEC SQL
   	       DECLARE CUR01 CURSOR WITH HOLD FOR
      	   SELECT 
              AccountNumber, 
              Amount
           FROM 
              dbo.Account
          WHERE
             AccountNumber LIKE 'ARC02%' AND
             AccountNumber > :W-ACCOUNTNUMBER
           ORDER BY
              AccountNumber
           FOR UPDATE OF
              Amount
   	   END-EXEC.
      *********************
      *   PROGRAM         *
      *********************
       PROCEDURE DIVISION.
          PERFORM OPEN-OUTFILE
          IF W-RESTART = 'N'
             PERFORM PUT-HEADER
             MOVE 'Y' TO W-RESTART
          END-IF
          PERFORM OPEN-CUR01
   	      PERFORM FETCH-CUR01
          PERFORM UNTIL SQLCODE > 0
             MOVE SPACES TO R-OUTFILE
             MOVE W-ACCOUNTNUMBER TO R-OUTFILE
             MOVE W-AMOUNT TO W-AMOUNT-X
             MOVE W-AMOUNT-X TO R-OUTFILE(20:)
             ADD 10 TO W-AMOUNT
             PERFORM UPDATE-AMOUNT
             MOVE W-AMOUNT TO W-AMOUNT-X
             MOVE W-AMOUNT-X TO R-OUTFILE(33:)
             PERFORM WRITE-OUTFILE
   	         PERFORM FETCH-CUR01
          END-PERFORM
          PERFORM CLOSE-CUR01
          PERFORM CLOSE-OUTFILE
          STOP RUN
          .
       PUT-HEADER.
          MOVE 'COB001 UPDATES' 
             TO R-OUTFILE
          PERFORM WRITE-OUTFILE
          MOVE '---------------------------------------------' 
             TO R-OUTFILE
          PERFORM WRITE-OUTFILE
          MOVE 'Accountnumber        Amnt Before   Amnt After' 
             TO R-OUTFILE
          PERFORM WRITE-OUTFILE
          MOVE '---------------------------------------------' 
             TO R-OUTFILE
          PERFORM WRITE-OUTFILE
          .
       OPEN-CUR01.
   	      EXEC SQL
   	         OPEN CUR01
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
             CONTINUE
          WHEN OTHER
             DISPLAY '** COB001: OPEN CUR01: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       FETCH-CUR01.
   	      EXEC SQL
   	         FETCH CUR01 INTO
                :W-ACCOUNTNUMBER,
                :W-AMOUNT
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
          WHEN 100
             CONTINUE
          WHEN OTHER
             DISPLAY '** COB001: FETCH CUR01: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       CLOSE-CUR01.
          EXEC SQL
   	         CLOSE CUR01
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
             CONTINUE
          WHEN OTHER
             DISPLAY '** COB001: CLOSE CUR01: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       UPDATE-AMOUNT.
          EXEC SQL
   	         UPDATE
                dbo.Account
             SET
                Amount = :W-Amount
             WHERE CURRENT OF
                CUR01
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
             CONTINUE
          WHEN OTHER
             DISPLAY '** COB001: UPDATE ACCOUNT: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             PERFORM CLOSE-OUTFILE
             STOP RUN
          END-EVALUATE
          .
       OPEN-OUTFILE.
          OPEN OUTPUT OUTFILE
          IF W-OUTFILE-STATUS IS NOT EQUAL '00'
             DISPLAY '** COB001: OPEN OUTFILE: STATUS: ' 
                     W-OUTFILE-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       WRITE-OUTFILE.
          WRITE R-OUTFILE
          IF W-OUTFILE-STATUS IS NOT EQUAL '00'
             DISPLAY '** COB001: WRITE OUTFILE: STATUS: ' 
                     W-OUTFILE-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       CLOSE-OUTFILE.
          CLOSE OUTFILE
          IF W-OUTFILE-STATUS IS NOT EQUAL '00'
             DISPLAY '** COB001: CLOSE OUTFILE: STATUS: ' 
                     W-OUTFILE-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
