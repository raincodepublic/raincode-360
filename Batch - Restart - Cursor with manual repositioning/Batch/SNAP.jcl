//*********************************************************************
//*   
//*   SNAPnn : Create snaphot nn=01,02,.. 
//* 
//*********************************************************************
//SNAP%%NN JOB COND=(4,LT)
//*--------------------------------------------------------------------
//*   Output database contents
//*--------------------------------------------------------------------
//STEP003   EXEC PGM=DBDUMP,COND=(4,LT)
//SYSOUT      DD SYSOUT=*
//SYSPRINT    DD SYSOUT=*
//SYSTSPRT    DD SYSOUT=*
//DBDUMP      DD DSN=ARC02.SNAP%%NN.DBDUMP,
//               DISP=(NEW,CATLG,DELETE),RECFM=LSEQ
//*--------------------------------------------------------------------
//*   Make a line sequential copy of the main output file
//*--------------------------------------------------------------------
//STEP003   EXEC PGM=IEBGENER
//SYSPRINT    DD  SYSOUT=*
//SYSIN       DD  DUMMY
//SYSUT1      DD  DSNAME=ARC02.OUTFILE,DISP=SHR
//SYSUT2      DD  DSN=ARC02.SNAP%%NN.OUTFILE,
//                DISP=(NEW,CATLG,DELETE),RECFM=LSEQ
//