$FilePath = "$($Env:RCDIR)\bin\cobrc"
$InputFile = "db2example.sql"
$QuotedSqlServerConnString = "`"$SQLSERVERCONNECTIONSTRING$`""
if($QuotedSqlServerConnString -eq "`"$`""){
   $QuotedSqlServerConnString = "`"$($Env:RC360_CONNSTRING)Initial Catalog=BankDemo;`""
}
$RewriteArgs = ":IgnoreClassification :SQL=SQLServer :RewriteSQLProc=RewriteDoubleExcl.Rewrite,RewriteIfNull.Rewrite"
$OutputDir = ":OutputDir=."
$ArgumentList = "$($OutputDir) $($RewriteArgs) :MaxMem=1G :SQLServerSchemaCacheFile=schemacache.xml -SQLServerConnectionString=$($QuotedSqlServerConnString) $($InputFile)"
$p = Start-Process -PassThru -Wait -NoNewWindow -FilePath $FilePath -ArgumentList $ArgumentList
if($p.ExitCode -ne 0){ exit $p.ExitCode }