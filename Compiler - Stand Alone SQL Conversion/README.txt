********************************************************************
*                                                                  *
*   Compiler Demo : Standalone SQL Conversion                      *
*                                                                  *
********************************************************************

PURPOSE
  . Illustrate how the compiler can be used to convert SQL statements that are not part of a program
  . Useful for SQLs that apear inside scripts or JCLs
  . Also incudes an examples of script-based SQL transformation

MANUAL REFERENCES
  . Compiler User Guide
    . � 6.3.8 Script-based SQL transformation
    . � 6.3.9 Standalone SQL Conversion

RUNNING THE DEMO
  . Go to Tools/Command Line/Developer Command Prompt
  . enter 'pwsh'
  . enter '.\run'

NOTES
  . Input is taken from the file db2example.sql
  . Output is written to db2example.tran.sql
  . The impact of the rewrite procedures is not visible in the output file, as they convert the input before conversion.




