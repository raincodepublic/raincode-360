EXEC SQL
    SELECT 
        CHAR(C.CustomerId), 
        SUBSTR(C.FirstName, 1, 5), 
        SUBSTR(C.FirstName, 1, 1) !! '.', 
        VALUE(C.LastName, ' '), 
        C.ZipCode, 
        C.BirthDate, 
        Z.City
    FROM 
        dbo.Customer C,
        dbo.ZipCode Z
    WHERE 
        Z.ZipCode = '1000'
    ORDER BY 
        C.CustomerId
END-EXEC