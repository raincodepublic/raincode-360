       IDENTIFICATION DIVISION.
       PROGRAM-ID.     MAIN.
       ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
       DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 W-ABSTIME PIC S9(15) USAGE COMP-3 VALUE 3650022310610.
        01 W-TIME PIC X(8).
        LINKAGE SECTION.
         01 DFHCOMMAREA.
           10 MSG PIC X(20).
       PROCEDURE DIVISION.
           DISPLAY EIBCALEN
           
           EXEC CICS
               FORMATTIME
               ABSTIME(W-ABSTIME)
               TIME(W-TIME)
               TIMESEP(":")
           END-EXEC
           DISPLAY MSG
           DISPLAY W-TIME
           EXEC CICS
               RETURN
           END-EXEC