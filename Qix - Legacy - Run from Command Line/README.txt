********************************************************************
*                                                                  *
*   Qix Demo : Running a CICS program from the Command Line        *
*                                                                  *
********************************************************************

PURPOSE
  . Run a COBOL/Qix program from the command line

HOW TO RUN
  . Two possibilities:
    . Place a breakpoint on the last line of of MAIN.cob and start the Raincode debugger
      . the commarea is taken from the input file input.txt
      . the filename is specified by the QixFile property
    . Run run.bat from the command line
      . the commarea is taken from the command line
      . the commarea contents is specified by the -QixCommarea parameter

NOTES
  . QixFile and QixCommarea are mutually exclusive
