      ***********************************************************
      *                                                         *
      *   Perform financial transaction with CICS LINK          *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COB001.        
       DATA DIVISION.                                                   
       WORKING-STORAGE SECTION.          
       COPY COB001.
       LINKAGE SECTION.                                                 
       PROCEDURE DIVISION.
           PERFORM PUT-DB
           GOBACK
           .
       PUT-DB.
           MOVE 'BE00-1000-0000-1438' TO C001-ACCOUNTNUMBER-FROM
           MOVE 'BE00-1000-0000-2643' TO C001-ACCOUNTNUMBER-TO
           MOVE 0.01 TO C001-AMOUNT
           MOVE 'No Emulator' TO C001-DESCRIPTIONTEXT
           EXEC CICS
               LINK PROGRAM('PLI001') COMMAREA(C001)
           END-EXEC
           IF C001-SQLCODE = 0
               DISPLAY 'Transaction ok' 
           ELSE
               DISPLAY C001-ERRTXT 
               MOVE -1 TO RETURN-CODE
           END-IF
           .
