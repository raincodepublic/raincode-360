
*****************************************************************************************
*                                                                                       *
*   Batch Demo : Legacy Calling Assembler                                               *
*                                                                                       *
*****************************************************************************************

PURPOSE OF THIS DEMO
  . Elementary example of a JCL executing a COBOL program with no database access.
  . The COBOL program illustrates how to call an subroutine written in Assembler.

WHAT THE DEMO DOES
  . The assembler routine calculates the difference in years between two dates, that are passed as parameters by the COBOL main program. 

HOW TO RUN THE DEMO
  . Start the Raincode Debugger

REFERENCES
  . All Raincode manuals
    . https://www.raincode.com/docs/#_raincode_manuals
