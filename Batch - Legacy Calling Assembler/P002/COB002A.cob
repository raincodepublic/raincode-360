       IDENTIFICATION DIVISION.
       PROGRAM-ID.     COB002A.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 PARMS usage DISPLAY.
           10 DT1 PIC 9(8).
           10 DT2 PIC 9(8).
           10 AGE PIC 9(8).
       PROCEDURE DIVISION.
           MOVE 19651124 TO DT1
           MOVE 20201124 TO DT2
           DISPLAY 'BEFORE: ' DT1 ' ' DT2
           CALL 'ASM002A' USING PARMS
           DISPLAY 'AFTER : ' DT1 ' ' DT2 ' ' AGE
           DISPLAY RETURN-CODE.
           GOBACK
           .
