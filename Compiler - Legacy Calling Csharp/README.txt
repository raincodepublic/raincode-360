*---------------------------------------------------------------------------------------*
*                                                                                       *
*   Compiler - Legacy Calling Csharp                                                    *
*                                                                                       *
*---------------------------------------------------------------------------------------*

PURPOSE
  . Demonstrate how a COBOL program can call a C# routine in a transparent way (as if it were COBOL)

HOW TO RUN
  . Optionally, run helper.bat in the solution directory to re-generate the helper class (WStruct.cs in the 'csharp' folder).
  . Press Ctrl-F5 to start. 

EXPECTED OUTPUT
  . Console 
      00000001
      00000004
      00000009
      00000016
      00000025
      00000036
      00000049
      00000064
      00000081
      00000100
      Press any key ...
