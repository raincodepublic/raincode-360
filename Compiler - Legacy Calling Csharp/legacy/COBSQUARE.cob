       IDENTIFICATION DIVISION.
       PROGRAM-ID.     COBSQUARE.
      * Computes the square of numbers from 1 to 10
       ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
       DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 W-ACCEPT PIC X.
        01 W-CNT PIC S9(8) USAGE IS BINARY.
        01 W-STRUCT.
          10 W-MSG PIC X(20).
          10 W-ARRAY OCCURS 10 TIMES.
            15 W-NUMBER1 PIC S9(8) USAGE IS BINARY.
            15 W-SQUARE PIC S9(8) USAGE IS BINARY.
        LINKAGE SECTION.
       PROCEDURE DIVISION.
           MOVE 1 TO W-CNT
           PERFORM UNTIL W-CNT > 10
               MOVE W-CNT TO W-NUMBER1(W-CNT)
               ADD 1 TO W-CNT
           END-PERFORM
           CALL "CSSQUARE" USING W-STRUCT
           DISPLAY W-MSG OF W-STRUCT
           MOVE 1 TO W-CNT
           PERFORM UNTIL W-CNT > 10
               DISPLAY W-SQUARE(W-CNT)
               ADD 1 TO W-CNT
           END-PERFORM    
           DISPLAY 'Press any key ...'
           GOBACK.