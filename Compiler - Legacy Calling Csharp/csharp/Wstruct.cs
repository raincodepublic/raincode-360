namespace RainCode.Samples
{
    using RainCodeLegacyRuntime.Core;
    using RainCodeLegacyRuntime.Descriptor;
    using RainCodeLegacyRuntime.Types;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Generated on 23/11/2022 at 15:37:23
    /// by Raincode COBOL compiler version 4.2.135.0 (Build nb_v4.2.134.0-128-g3ee5b02a44)
    /// with arguments -HelperClassName=RainCode.Samples.WStruct -HelperStructure=W-STRUCT ./legacy/cobsquare.cob
    ///
    /// Input program path: 'C:\repositories\raincode360\Compiler - Legacy Calling Csharp\legacy\cobsquare.cob'
    /// Structure was originally in files:
    ///   cobsquare.cob
    /// </summary>
    public partial class WStruct : BaseHelper, System.IDisposable
    {
        public const int StructureSize = 100;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public WStruct(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public WStruct(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static WStruct StackAllocate(ExecutionContext ctxt)
        {
            return new WStruct(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static WStruct Allocate(ExecutionContext ctxt)
        {
            WStruct result = new WStruct(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea W_MSG => theMemoryArea.Substr(0, 20);
            public MemoryArea W_ARRAY => theMemoryArea.Substr(20, 80);
        }

        /// <summary>
        /// name:   W_MSG
        /// type:   string
        /// offset: 0
        /// size:   20
        /// </summary> 
        public string W_MSG
        {
            get
            {
                return Convert.cnv_char_string_to_string(theContext, Raw.W_MSG);
            }
            set
            {
                Convert.move_string_to_char_string(theContext, value, Raw.W_MSG);
            }
        }

        private TypedLValue theCachedW_MSG_TypedLValue = TypedLValue.Null;
        public TypedLValue W_MSG_TypedLValue
        {
            get
            {
                if (theCachedW_MSG_TypedLValue.IsNull)
                {
                    theCachedW_MSG_TypedLValue = new TypedLValue(Raw.W_MSG, CharacterStringType.Get(20,0,false));
                }
                return theCachedW_MSG_TypedLValue;
            }
        }

        /// <summary>
        /// name:   W_ARRAY
        /// type:   W_STRUCT_W_ARRAY_ARRAY
        /// offset: 20
        /// size:   80
        /// </summary> 
        private W_STRUCT_W_ARRAY_ARRAY theW_ARRAY_1;
        public W_STRUCT_W_ARRAY_ARRAY W_ARRAY
        {
            get
            {
                if (theW_ARRAY_1 == null)
                {
                    theW_ARRAY_1 = new W_STRUCT_W_ARRAY_ARRAY(theContext, Raw.W_ARRAY);
                }
                return theW_ARRAY_1;
            }
        }

        public readonly int W_ARRAY_Size = 10;
        [System.Obsolete]
        public W_STRUCT_W_ARRAY GetW_ARRAY_At(int idx)
        {
            return W_ARRAY[idx];
        }

        [System.Obsolete]
        public IEnumerable<W_STRUCT_W_ARRAY> GetW_ARRAY()
        {
            return W_ARRAY;
        }

        private TypedLValue theCachedW_ARRAY_TypedLValue = TypedLValue.Null;
        public TypedLValue W_ARRAY_TypedLValue
        {
            get
            {
                if (theCachedW_ARRAY_TypedLValue.IsNull)
                {
                    theCachedW_ARRAY_TypedLValue = new TypedLValue(Raw.W_ARRAY, W_STRUCT_W_ARRAY_ARRAY.GetStaticDescriptor());
                }
                return theCachedW_ARRAY_TypedLValue;
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {
              var temp_0 = StructTypeDescriptor.Get(new FieldDescriptor[] {
                      new FieldDescriptor ("W-NUMBER1",0,FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP())),
                      new FieldDescriptor ("W-SQUARE",4,FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()))
              });

                theCachedDesc =
                    StructTypeDescriptor.Get(new FieldDescriptor[] {
                            new FieldDescriptor ("W-MSG",0,CharacterStringType.Get(20,0,false)),
                            new FieldDescriptor ("W-ARRAY",20,temp_0.GetArray(1,10,8,false))
                    });
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class W_STRUCT_W_ARRAY_ARRAY : BaseHelper, System.IDisposable, IEnumerable<W_STRUCT_W_ARRAY>
    {
        public const int StructureSize = 80;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public W_STRUCT_W_ARRAY_ARRAY(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public W_STRUCT_W_ARRAY_ARRAY(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static W_STRUCT_W_ARRAY_ARRAY StackAllocate(ExecutionContext ctxt)
        {
            return new W_STRUCT_W_ARRAY_ARRAY(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static W_STRUCT_W_ARRAY_ARRAY Allocate(ExecutionContext ctxt)
        {
            W_STRUCT_W_ARRAY_ARRAY result = new W_STRUCT_W_ARRAY_ARRAY(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData: IEnumerable<MemoryArea>
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            internal const int Count0 = 10;
            internal int Count => Count0;

            public MemoryArea this[int idx] => theMemoryArea.Substr(idx * 8, 8);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public IEnumerator<MemoryArea> GetEnumerator()
            {
                for (int idx = 0; idx < Count; ++idx)
                {
                    yield return this[idx];
                }
            }

        }

        public const int Count0 = RawData.Count0;
        public int Count => Raw.Count;

        private W_STRUCT_W_ARRAY[] theW_ARRAY_1;
        public W_STRUCT_W_ARRAY this[int idx]
        {
            get
            {
                if (theW_ARRAY_1 == null)
                {
                    theW_ARRAY_1 = new W_STRUCT_W_ARRAY[Count];
                }
                if (theW_ARRAY_1[idx] == null)
                {
                    theW_ARRAY_1[idx] = new W_STRUCT_W_ARRAY(theContext, Raw[idx]);
                }
                return theW_ARRAY_1[idx];
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<W_STRUCT_W_ARRAY> GetEnumerator()
        {
            for (int idx = 0; idx < Count; ++idx)
            {
                yield return this[idx];
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {
              var temp_0 = StructTypeDescriptor.Get(new FieldDescriptor[] {
                      new FieldDescriptor ("W-NUMBER1",0,FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP())),
                      new FieldDescriptor ("W-SQUARE",4,FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()))
              });

                theCachedDesc =
                    temp_0.GetArray(1,10,8,false);
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class W_STRUCT_W_ARRAY : BaseHelper, System.IDisposable
    {
        public const int StructureSize = 8;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public W_STRUCT_W_ARRAY(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public W_STRUCT_W_ARRAY(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static W_STRUCT_W_ARRAY StackAllocate(ExecutionContext ctxt)
        {
            return new W_STRUCT_W_ARRAY(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static W_STRUCT_W_ARRAY Allocate(ExecutionContext ctxt)
        {
            W_STRUCT_W_ARRAY result = new W_STRUCT_W_ARRAY(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea W_NUMBER1 => theMemoryArea.Substr(0, 4);
            public MemoryArea W_SQUARE => theMemoryArea.Substr(4, 4);
        }

        /// <summary>
        /// name:   W_NUMBER1
        /// type:   int
        /// offset: 0
        /// size:   4
        /// </summary> 
        public int W_NUMBER1
        {
            get
            {
                return Convert.cnv_fixed_dec_to_int32(theContext, Raw.W_NUMBER1, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
            }
            set
            {
                Convert.move_int32_to_fixed_dec(theContext, value, Raw.W_NUMBER1, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
            }
        }

        private TypedLValue theCachedW_NUMBER1_TypedLValue = TypedLValue.Null;
        public TypedLValue W_NUMBER1_TypedLValue
        {
            get
            {
                if (theCachedW_NUMBER1_TypedLValue.IsNull)
                {
                    theCachedW_NUMBER1_TypedLValue = new TypedLValue(Raw.W_NUMBER1, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
                }
                return theCachedW_NUMBER1_TypedLValue;
            }
        }

        /// <summary>
        /// name:   W_SQUARE
        /// type:   int
        /// offset: 4
        /// size:   4
        /// </summary> 
        public int W_SQUARE
        {
            get
            {
                return Convert.cnv_fixed_dec_to_int32(theContext, Raw.W_SQUARE, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
            }
            set
            {
                Convert.move_int32_to_fixed_dec(theContext, value, Raw.W_SQUARE, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
            }
        }

        private TypedLValue theCachedW_SQUARE_TypedLValue = TypedLValue.Null;
        public TypedLValue W_SQUARE_TypedLValue
        {
            get
            {
                if (theCachedW_SQUARE_TypedLValue.IsNull)
                {
                    theCachedW_SQUARE_TypedLValue = new TypedLValue(Raw.W_SQUARE, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
                }
                return theCachedW_SQUARE_TypedLValue;
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    StructTypeDescriptor.Get(new FieldDescriptor[] {
                            new FieldDescriptor ("W-NUMBER1",0,FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP())),
                            new FieldDescriptor ("W-SQUARE",4,FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()))
                    });
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

}
