-- Generated at 27/12/2023 13:06:21

drop view if exists [VSQLBENCH_V];
go
Create or alter View [VSQLBENCH_V]
  -- WITH SCHEMABINDING
as select
[VSQLBENCH].*
  , cast (dbo.RC_EBCDIC_TO_ASCII(substring([data],1,64)) as VARCHAR(64)) as [B_KEY]
  , cast (dbo.RC_EBCDIC_TO_ASCII(substring([data],65,200)) as VARCHAR(200)) as [B_DATA01]
  , cast (dbo.RC_EBCDIC_TO_ASCII(substring([data],265,200)) as VARCHAR(200)) as [B_DATA02]
  , cast (dbo.RC_EBCDIC_TO_ASCII(substring([data],465,48)) as VARCHAR(48)) as [B_DATA03]
  from [VSQLBENCH];
go
--------------------------------------------
create or alter trigger [UPDATE_VSQLBENCH_V] on [VSQLBENCH_V] instead of update as
  begin
  create table #tt (
    [rid] int,
    [data] varbinary (max)
  );
  insert into #tt (rid,[data])
    select deleted.rid, deleted.[data]
      from deleted;
if (update([B_KEY]))
  update #tt set
              [data].Write(CAST (dbo.RC_ASCII_TO_EBCDIC(Inserted.[B_KEY],64) AS BINARY(64)),
                       0,64)
    from #tt inner join inserted on inserted.rid = #tt.rid
if (update([B_DATA01]))
  update #tt set
              [data].Write(CAST (dbo.RC_ASCII_TO_EBCDIC(Inserted.[B_DATA01],200) AS BINARY(200)),
                       64,200)
    from #tt inner join inserted on inserted.rid = #tt.rid
if (update([B_DATA02]))
  update #tt set
              [data].Write(CAST (dbo.RC_ASCII_TO_EBCDIC(Inserted.[B_DATA02],200) AS BINARY(200)),
                       264,200)
    from #tt inner join inserted on inserted.rid = #tt.rid
if (update([B_DATA03]))
  update #tt set
              [data].Write(CAST (dbo.RC_ASCII_TO_EBCDIC(Inserted.[B_DATA03],48) AS BINARY(48)),
                       464,48)
    from #tt inner join inserted on inserted.rid = #tt.rid
  update [VSQLBENCH] set [VSQLBENCH].[data] = #tt.[data]

    from [VSQLBENCH] inner join #tt on [VSQLBENCH].rid = #tt.rid;
  end
  go
--------------------------------------------
create or alter trigger [Insert_VSQLBENCH_V] on [VSQLBENCH_V] instead of insert as
  begin
declare @data varbinary(max) = cast (0x00 as binary(512))
declare @B_KEY VARCHAR(64) = null;
declare @B_DATA01 VARCHAR(200) = null;
declare @B_DATA02 VARCHAR(200) = null;
declare @B_DATA03 VARCHAR(48) = null;
declare curs cursor local for select 
      [B_KEY],
      [B_DATA01],
      [B_DATA02],
      [B_DATA03]
    from inserted;
  open curs;
    fetch next from curs into
      @B_KEY,
      @B_DATA01,
      @B_DATA02,
      @B_DATA03
  while @@FETCH_STATUS = 0
    begin
      if (update([B_KEY]) and @B_KEY is not null)
        begin
        -- len(@data)>=0 [1]
        set @data.Write(CAST (dbo.RC_ASCII_TO_EBCDIC(@B_KEY,64) AS BINARY(64)),
                               0,64);
        end
      if (update([B_DATA01]) and @B_DATA01 is not null)
        begin
        -- len(@data)>=64 [2]
        set @data.Write(CAST (dbo.RC_ASCII_TO_EBCDIC(@B_DATA01,200) AS BINARY(200)),
                               64,200);
        end
      if (update([B_DATA02]) and @B_DATA02 is not null)
        begin
        -- len(@data)>=264 [3]
        set @data.Write(CAST (dbo.RC_ASCII_TO_EBCDIC(@B_DATA02,200) AS BINARY(200)),
                               264,200);
        end
      if (update([B_DATA03]) and @B_DATA03 is not null)
        begin
        -- len(@data)>=464 [4]
        set @data.Write(CAST (dbo.RC_ASCII_TO_EBCDIC(@B_DATA03,48) AS BINARY(48)),
                               464,48);
        end
    -- Insert into master table
    insert into [VSQLBENCH] ([Data])
      Values (@data);
    fetch next from curs into
      @B_KEY,
      @B_DATA01,
      @B_DATA02,
      @B_DATA03
    if @@FETCH_STATUS = 0
      begin
      -- Reset temp record
      set @data = cast (0x00 as binary(512))
      end;
    end
  end;
go
---------------------------------------
--------------------------------------------
create or alter trigger [delete_VSQLBENCH_V] on [VSQLBENCH_V] instead of delete as
  begin
  delete from [VSQLBENCH] where rid in (select rid from deleted);
  end;
go
