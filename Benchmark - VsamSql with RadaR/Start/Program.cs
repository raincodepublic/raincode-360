﻿using System.Diagnostics;
Console.WriteLine("Processing, this may take a few minutes ...");
var processStartInfo = new ProcessStartInfo
{
    FileName = "pwsh.exe",
    Arguments = $"-Command \"..\\..\\..\\..\\RadaR_VsamSql\\Run.ps1\"",
    UseShellExecute = false,
    RedirectStandardOutput = true
};
using var process = new Process();
process.StartInfo = processStartInfo;
process.Start();
string output = process.StandardOutput.ReadToEnd();
Console.WriteLine(output);
