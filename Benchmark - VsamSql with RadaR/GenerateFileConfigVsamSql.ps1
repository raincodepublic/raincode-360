Write-Host "Generating FileConfigVsamSql.xml from template ..."
$ConnectionString = "$($Env:RC360_CONNSTRING)Initial Catalog=VsamSql"
(Get-Content "..\..\..\..\FileConfigVsamSqlTemplate.xml").Replace('[ConnectionString]', $ConnectionString) | Set-Content "..\..\..\..\FileConfigVsamSql.xml"