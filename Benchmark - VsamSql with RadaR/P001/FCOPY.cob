       IDENTIFICATION DIVISION.
       PROGRAM-ID. FCOPY.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT FILE-I ASSIGN TO DDIN
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE  IS SEQUENTIAL 
           FILE STATUS  IS W-FS-I.
           SELECT FILE-O ASSIGN TO DDOUT
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE  IS SEQUENTIAL 
           FILE STATUS  IS W-FS-O.
       DATA DIVISION.
       FILE SECTION.
       FD FILE-I
           RECORD CONTAINS 80 CHARACTERS 
           RECORDING MODE  IS  F 
           DATA RECORD     IS REC-I.
       01 REC-I PIC X(80).
       FD FILE-O 
           RECORD CONTAINS 80 CHARACTERS 
           RECORDING MODE  IS  F 
           DATA RECORD     IS REC-O.
       01 REC-O PIC X(80).
       WORKING-STORAGE SECTION.
       01 W-FS-I PIC 99.
       01 W-FS-O PIC 99.
       01 W-EOF-I PIC 9 VALUE 0.
       PROCEDURE DIVISION.
           OPEN INPUT FILE-I 
           OPEN OUTPUT FILE-O
           PERFORM UNTIL W-EOF-I > 0
              READ FILE-I 
                AT END 
                   SET W-EOF-I TO 1
                NOT AT END 
                   MOVE REC-I TO REC-O
                   WRITE REC-O
                END-READ
           END-PERFORM
           CLOSE FILE-O
           CLOSE FILE-I
           STOP RUN.