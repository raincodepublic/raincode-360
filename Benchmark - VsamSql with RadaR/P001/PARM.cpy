      *** DEMO PARAMETERS ***

      ** Number of records updated. Increasing this value will produce
      ** results that are more stable and accurate but will also affect
      ** the runtime of the demo.
       01 PARM-UPDATES  PIC 9(9) COMP-5 VALUE 3000.
      ** Number of intermediate output LINES.
       01 PARM-BREAKS   PIC 99          VALUE 10.
      ** Total number of rows in the VSQLBENCH table.
       01 PARM-RECORDS  PIC 9(9) COMP-5 VALUE 25000.
      ** FileConfig name. Do not change.
       01 PARM-FC pic   X(80)           VALUE 'none'.
