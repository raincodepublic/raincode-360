       IDENTIFICATION DIVISION.
       PROGRAM-ID.     RDDATA.
       ENVIRONMENT DIVISION.
        INPUT-OUTPUT SECTION.
         FILE-CONTROL.
           SELECT BDATA-FILE
               ASSIGN       TO  BDATA
               ORGANIZATION IS  INDEXED
               RECORD KEY   IS  B-KEY OF BDATA-REC
               ACCESS MODE  IS  random.
       DATA DIVISION.
       FILE SECTION.
        FD BDATA-FILE.
           COPY BDATA.
       WORKING-STORAGE SECTION.
        01 W-CNT  PIC 9(9) COMP-5.
        01 W-CNT2  PIC 9(4) COMP-5.
        01 W-MAX  PIC 9(9) COMP-5.
        01 W-RND  PIC 9(9).
        01 W-MS   PIC ZZZZZZZZZ9V99.
        COPY PARM.
        COPY TIMER.
       PROCEDURE DIVISION.
           COMPUTE W-MAX = PARM-UPDATES / PARM-BREAKS
           ACCEPT PARM-FC from COMMAND-LINE
           DISPLAY 'FileConfig: ' PARM-FC
           DISPLAY "Performing " PARM-UPDATES " random updates on " 
                   PARM-RECORDS  " records"
           MOVE 'START' TO TIMER-MODE
           CALL 'TIMER' USING TIMER
           DISPLAY TIMER-DTTM ' Start'
           OPEN I-O BDATA-FILE
           PERFORM UNTIL W-CNT > PARM-UPDATES
               ADD 1 TO W-CNT
               ADD 1 TO W-CNT2
               COMPUTE W-RND = (FUNCTION RANDOM) * PARM-RECORDS
               MOVE W-RND TO B-KEY
               READ BDATA-FILE KEY IS B-KEY
                 INVALID KEY DISPLAY "NOT FOUND" W-RND
               END-READ
               IF W-CNT2 >= W-MAX 
                   MOVE 'TIME' TO TIMER-MODE
                   CALL 'TIMER' USING TIMER
                   DISPLAY TIMER-DTTM ' ' W-CNT ' records updated'
                   MOVE 0 TO W-CNT2
               end-if
               MOVE TIMER-DTTM TO B-DATA01 B-DATA02
               REWRITE BDATA-REC
           END-PERFORM
           CLOSE BDATA-FILE
           MOVE 'STOP' TO TIMER-MODE
           CALL 'TIMER' USING TIMER
           DISPLAY TIMER-DTTM ' Stop'
           DISPLAY 'Elapsed (s):' TIMER-DIFF-S
           GOBACK
           .

