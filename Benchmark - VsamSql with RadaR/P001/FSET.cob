       IDENTIFICATION DIVISION.
       PROGRAM-ID. FSET.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT FILE-O ASSIGN TO DDOUT
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE  IS SEQUENTIAL 
           FILE STATUS  IS W-FS-O.
       DATA DIVISION.
       FILE SECTION.
       FD FILE-O 
           RECORD CONTAINS 80 CHARACTERS 
           RECORDING MODE  IS  F 
           DATA RECORD     IS REC-O.
       01 REC-O PIC X(80).
       WORKING-STORAGE SECTION.
       01 W-FS-O PIC 99.
       PROCEDURE DIVISION.
           PERFORM 100 TIMES
               OPEN OUTPUT FILE-O
               display 'open [' w-fs-o ']'
               MOVE 'FSET' TO REC-O
               PERFORM 4000 TIMES
                  WRITE REC-O
               END-PERFORM
               display 'write [' w-fs-o ']'
               CLOSE FILE-O
               display 'close [' w-fs-o ']'
           END-PERFORM
           DISPLAY 'FSET DONE'
           STOP RUN.