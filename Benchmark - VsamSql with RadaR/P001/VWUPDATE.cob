       IDENTIFICATION DIVISION.
       PROGRAM-ID. VWUPDATE.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
        01 W-CNT  PIC 9(9) COMP-5.
        01 W-CNT2  PIC 9(4) COMP-5.
        01 W-MAX  PIC 9(9) COMP-5.
        01 W-RND  PIC 9(9).
        01 W-MS   PIC ZZZZZZZZZ9V99.
        01 W-1 PIC 9.
        01 W-SQLCODE PIC ---------9.
        COPY PARM.
        COPY TIMER.
        COPY BDATA.
        COPY ENCODING.
        COPY SQLCA.
       PROCEDURE DIVISION.
           COMPUTE W-MAX = PARM-UPDATES / PARM-BREAKS
           DISPLAY 'FileConfig: ' PARM-FC
           DISPLAY "Performing " PARM-UPDATES " random updates on " 
                   PARM-RECORDS  " records"
           MOVE 'START' TO TIMER-MODE
           CALL 'TIMER' USING TIMER
           DISPLAY TIMER-DTTM ' Start'
           PERFORM UNTIL W-CNT > PARM-UPDATES
               ADD 1 TO W-CNT
               ADD 1 TO W-CNT2
               COMPUTE W-RND = (FUNCTION RANDOM) * PARM-RECORDS
               MOVE W-RND TO B-KEY
       
      *         EXEC SQL
      *           select 'ABC'
      *             into :B-DATA01
      *             from vsqlbench
      *            WHERE "FILE" = 2
      *              and "KEY"  = CONVERT(VARBINARY(64),:B-KEY,0)
      *         END-EXEC

               IF W-RND < 1000
                 DISPLAY 'RND:' W-RND
               END-IF

               EXEC SQL
                   UPDATE VSQLBENCH_V
                      SET B_DATA01 = :TIMER-DTTM  
                   WHERE "FILE" = 2
                     AND "KEY"  = :B-KEY
               END-EXEC

               IF SQLCODE NOT = 0
                 MOVE SQLCODE TO W-SQLCODE
                 DISPLAY '** SQLCODE   : ' W-SQLCODE
                 DISPLAY '** SQLERRMC  : ' SQLERRMC
                 COMPUTE W-1 = 1/0
               END-IF

               IF W-CNT2 >= W-MAX 
                   MOVE 'TIME' TO TIMER-MODE
                   CALL 'TIMER' USING TIMER
                   DISPLAY TIMER-DTTM ' ' W-CNT ' records updated'
                   MOVE 0 TO W-CNT2
               end-if
            END-PERFORM
           MOVE 'STOP' TO TIMER-MODE
           CALL 'TIMER' USING TIMER
           DISPLAY TIMER-DTTM ' Stop'
           COMPUTE W-MS = TIMER-DIFF-S * 1000 / PARM-UPDATES
           DISPLAY '      Time(s)        ms/upd' 
           DISPLAY '------------- -------------'
           DISPLAY TIMER-DIFF-S ' ' W-MS
           GOBACK
           .

