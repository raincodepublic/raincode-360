       IDENTIFICATION DIVISION.
        PROGRAM-ID. CRDATA.
       ENVIRONMENT DIVISION.
        INPUT-OUTPUT SECTION.
         FILE-CONTROL.
           SELECT BDATA-FILE
               ASSIGN       TO  BDATA
               ORGANIZATION IS  INDEXED
               RECORD KEY   IS  B-KEY OF BDATA-REC
               ACCESS MODE  IS  SEQUENTIAL.
       DATA DIVISION.
       FILE SECTION.
        FD BDATA-FILE.
           COPY BDATA.
       WORKING-STORAGE SECTION.
        01 W-CNT  PIC 9(9) COMP-5.
        01 W-CNT2 PIC 9(9) COMP-5.
        01 W-MAX PIC 9(9) COMP-5.
        01 W-RND  PIC 9(9).
        01 W-STR  PIC X(200).
        COPY PARM.
        COPY TIMER.
       PROCEDURE DIVISION.
           accept PARM-FC from COMMAND-LINE
           DISPLAY 'FileConfig: ' PARM-FC
           MOVE 0 TO W-CNT
           move 0 TO W-CNT2
           COMPUTE W-MAX = PARM-RECORDS / PARM-BREAKS
           DISPLAY "Inserting " PARM-RECORDS " records (4)"
           MOVE 'START' TO TIMER-MODE
           CALL 'TIMER' USING TIMER
           DISPLAY TIMER-DTTM
           OPEN OUTPUT BDATA-FILE
           PERFORM UNTIL W-CNT > PARM-RECORDS
               move W-CNT TO B-KEY
               move W-CNT TO B-DATA01
               COMPUTE W-RND = PARM-RECORDS - W-CNT
               MOVE W-RND TO B-DATA02
               ADD 1 TO W-CNT               
               ADD 1 TO W-CNT2
               IF W-CNT2 >= W-MAX
                 DISPLAY W-CNT
                 MOVE 0 TO W-CNT2
               END-IF
               WRITE BDATA-REC
           END-PERFORM       
           CLOSE  BDATA-FILE .  
           MOVE 'STOP' TO TIMER-MODE
           CALL 'TIMER' USING TIMER
           DISPLAY TIMER-DTTM
           DISPLAY 'Time(s) : ' TIMER-DIFF-S
           GOBACK
           .