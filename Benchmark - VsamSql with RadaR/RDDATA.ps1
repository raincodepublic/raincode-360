$ServerDir = "$Env:USERPROFILE\Documents" 
$FileConfig = "FileConfigRadaR.xml"
$OutFile = 'runme.sql'
#Remove-Item "$OutFile*" 

$Env:SQLCMDSERVER = $Env:DB_SERVER
$Env:SQLCMDPASSWORD = $Env:DB_PASSWORD
$Env:SQLCMDUSER = $Env:DB_USER

$Flags = @(
    "-LogLevel=WARNING",
    "-ProgramArguments=`"$FileConfig`"",
    "-FileConfigEncoding=Windows-1252",
    "-AddAssemblySearchDir=$ServerDir",
    "-FileConfig=`"$ServerDir\$FileConfig`"",
    "-StringRuntimeEncoding=IBM037"
) -join ' '

& "C:\Program Files\Raincode\Crossbow\net8.0\scripts\RadaR\radar.ps1" -SqlFile $OutFile  -RunArgs "$Flags RDDATA" 

#Get-Content "$Outfile.out"
