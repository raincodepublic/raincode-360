$ServerDir = "$Env:USERPROFILE\Documents" 

$Env:SQLCMDSERVER = $Env:DB_SERVER
$Env:SQLCMDPASSWORD = $Env:DB_PASSWORD
$Env:SQLCMDUSER = $Env:DB_USER

$Flags = @(
    "-LogLevel=WARNING",
    "-AddAssemblySearchDir=$ServerDir"
) -join ' '

& "C:\Program Files\Raincode\Crossbow\net8.0\scripts\RadaR\radar.ps1" -RunArgs "$Flags FCOPY" 
