       IDENTIFICATION DIVISION.
       PROGRAM-ID.     CHAR2HEX.
       ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
       DATA DIVISION.
        WORKING-STORAGE SECTION.
        01  HEXSTR   PIC X(16) VALUE "0123456789ABCDEF".
        01  DEC      PIC S9(4) COMP VALUE 0.                    
        01  FILLER   REDEFINES DEC.                    
            02  FILLER PIC X.                          
            02  DECBYTE PIC X.                          
        01  I   PIC S9(8) COMP.                        
        01  J   PIC S9(8) COMP.                        
        01  Q   PIC S9(8) COMP.                        
        01  R   PIC S9(8) COMP.                        
        01  J1  PIC S9(8) COMP.                        
        01  Q1  PIC S9(8) COMP.                        
        01  R1  PIC S9(8) COMP.  
        LINKAGE SECTION.
        COPY CHAR2HEX.
       PROCEDURE DIVISION USING CHAR2HEX.
           MOVE '0x' TO CHAR2HEX-HEX
           COMPUTE CHAR2HEX-HEX-LEN = CHAR2HEX-CHAR-LEN * 2 + 2
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > CHAR2HEX-CHAR-LEN
              COMPUTE J = 2 * I + 1                  
              MOVE CHAR2HEX-CHAR(I:1) TO DECBYTE            
              DIVIDE DEC BY 16 GIVING Q REMAINDER R  
              COMPUTE J1 = J + 1                      
              COMPUTE Q1 = Q + 1                      
              COMPUTE R1 = R + 1                      
              MOVE HEXSTR(Q1:1) TO CHAR2HEX-HEX(J:1)        
              MOVE HEXSTR(R1:1) TO CHAR2HEX-HEX(J1:1)      
           END-PERFORM.
           GOBACK.