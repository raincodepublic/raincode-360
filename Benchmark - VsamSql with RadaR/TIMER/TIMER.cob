       IDENTIFICATION DIVISION.
       PROGRAM-ID.     TIMER.
       ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
       DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 W-TIMER OCCURS 2.
           10  YYYY PIC 9(4).
           10  MO   PIC 9(2).
           10  DD   PIC 9(2).
           10  HH   PIC 9(2).
           10  MI   PIC 9(2).
           10  SS   PIC 9(2).
           10  XX   PIC 9(2).
           10  FILL PIC S9(4).  
        01 W-DT PIC 9(7) OCCURS 2.
        01 W-TM PIC 9(7)V99 OCCURS 2.
        01 N8 PIC 9(8).
        01 I PIC 9.
        01 FORMAT-DTTM-I PIC 9.
      * 01 W-DIFF PIC ZZZZZZZZZ9.99
        LINKAGE SECTION.
        COPY TIMER.
       PROCEDURE DIVISION USING TIMER.
           EVALUATE TIMER-MODE
           WHEN 'TIME'
             MOVE FUNCTION CURRENT-DATE TO W-TIMER(2)
             MOVE 2 TO FORMAT-DTTM-I
             PERFORM FORMAT-DTTM
           WHEN 'START'
             MOVE FUNCTION CURRENT-DATE TO W-TIMER(1)
             MOVE 1 TO FORMAT-DTTM-I
             PERFORM FORMAT-DTTM
           WHEN 'STOP'
             MOVE FUNCTION CURRENT-DATE TO W-TIMER(2)
             MOVE 2 TO FORMAT-DTTM-I
             PERFORM FORMAT-DTTM
             PERFORM VARYING I FROM 1 BY 1 UNTIL I > 2
               MOVE W-TIMER(I)(1:8) TO N8
               COMPUTE W-DT(I) = FUNCTION INTEGER-OF-DATE(N8)
               COMPUTE W-TM(I) 
                     = HH(I)*3600 + MI(I)*60 + SS(I) + XX(I)/100
             END-PERFORM
             COMPUTE TIMER-DIFF 
                   = (W-DT(2) - W-DT(1))*86400 + W-TM(2) - W-TM(1)
             MOVE TIMER-DIFF TO TIMER-DIFF-S
      *      MOVE FUNCTION TRIM(W-DIFF) TO TIMER-DIFF-S
      *      MOVE FUNCTION TRIM(W-DIFF) TO TIMER-DIFF-S
           WHEN OTHER
             DISPLAY 'TIMER.cob: unknown input:' TIMER-MODE
             COMPUTE I = 1/0
           END-EVALUATE
           GOBACK
           .
      
       FORMAT-DTTM.
           MOVE YYYY(FORMAT-DTTM-I) TO TIMER-DTTM
           MOVE '-'                 TO TIMER-DTTM(5:1) TIMER-DTTM(8:1)
           MOVE MO(FORMAT-DTTM-I)   TO TIMER-DTTM(6:2)
           MOVE DD(FORMAT-DTTM-I)   TO TIMER-DTTM(9:2)
           MOVE HH(FORMAT-DTTM-I)   TO TIMER-DTTM(12:2)
           MOVE ':'                 TO TIMER-DTTM(14:1) TIMER-DTTM(17:1)
           MOVE MI(FORMAT-DTTM-I)   TO TIMER-DTTM(15:2)
           MOVE SS(FORMAT-DTTM-I)   TO TIMER-DTTM(18:2)
           MOVE '.'                 TO TIMER-DTTM(20:1)
           MOVE XX(FORMAT-DTTM-I)   TO TIMER-DTTM(21:2)
           .
