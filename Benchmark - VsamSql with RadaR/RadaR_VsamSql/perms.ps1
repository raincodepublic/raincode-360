<#
.SYNOPSIS
 Set permissions on the given directory such that rclrun can access them when called from DbLoCo (e.g. with -AddAssemblySearchDir=<x> ).

 Note that DbLoCo can only work on SQL Server for windows. See https://github.com/microsoft/sql-server-language-extensions/tree/main/language-extensions/dotnet-core-CSharp
 Hence this script is windows-only, for brevity.
#>

param (
# The Assembly directory
    [Parameter(Mandatory)]
    [string]
    $AssemblyDir
)

& icacls "$AssemblyDir" /grant `"SQLRUserGroup`":`(OI`)`(CI`)RX /T /Q
& icacls "$AssemblyDir" /grant *S-1-15-2-1:`(OI`)`(CI`)RX /T /Q