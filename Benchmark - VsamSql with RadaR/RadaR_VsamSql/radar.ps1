<#
.SYNOPSIS
 Raincode Database Runner (RadaR). Run Raincode compiled code on SQL Server, making use of the MSSQLRunner. It will produce a .sql file with the call, and a .sql.out file that contains the output of that SQL call.
 
 Example:  .\radar.ps1 -SqlFile "c:\temp\runme.sql" -ProgName "HELLO" -Flags  "-AddAssemblySearchDir=C:\\tests\\sqlcobprog"

 Prerequisites: a working sqlcmd that can converse with the database (e.g. with working windows authentication or alternatively with correct SQLCMDSERVER and SQLCMDUSER environment variables set)
#>

param (
# The SQL file that will be created, containing the call to the DB
    [Parameter(Mandatory)]
    [string]
    $SqlFile,

# Any flags to be passed to rclrun (e.g. assembly search directory)
    [Parameter(Mandatory)]
    [string]
    $Flags,

# The program to be called by rclrun
    [Parameter(Mandatory)]
    [string]
    $ProgName
)

# If there are problems, give up.
$ErrorActionPreference = 'Stop'


# Create and fill the file
New-Item $Sqlfile -ItemType File -Value ("declare @result int"+[Environment]::NewLine) | out-null

Add-Content $Sqlfile "EXEC sp_execute_external_script @language = N'Dotnet', @script = N'rcrun.dll;MSSQLRunner.MSSQLRunner', @input_data_1 = N'SELECT null', @params = N'@result int OUTPUT, @progName varchar(20), @flags varchar(1000)', @result = @result OUTPUT, @progName = '$ProgName', @flags ='$Flags';"

Add-Content $Sqlfile ":EXIT(select @result)"

# Execute it
#& sqlcmd -i $Sqlfile -h -1 -W -o "$Sqlfile.out"
& sqlcmd -S tcp:sqlserver -i $Sqlfile -h -1 -W -o "$Sqlfile.out"
$Result = $LASTEXITCODE

$Result

# $q = "declare @result int
# EXEC sp_execute_external_script @language = N'Dotnet', @script = N'rcrun.dll;MSSQLRunner.MSSQLRunner', @input_data_1 = N'SELECT null', @params = N'@result int OUTPUT, @progName varchar(20), @flags varchar(1000)', @result = @result OUTPUT, @progName = '$ProgName', @flags ='$Flags';
# :EXIT(select @result)"
# Invoke-Sqlcmd -Query $q -ConnectionString $Env:RC360_CONNSTRING
# $LASTEXITCODE
