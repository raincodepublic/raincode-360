******************************************************
*                                                    *
*   Benchmark - VsamSql with RadaR                   *
*                                                    *
******************************************************

PURPOSE
  . Compare traditional client/server approach with RadaR (Raincode Database Runner).
  . RadaR allows you to run applications on the database server without having access to its filesystem.

HOW TO RUN
  . Select 'Release' in the main toolbar of Visual Studio.
  . Press 'start' on the Visual Studio main menu. This will launch the C# project named 'Start', which launches the script 'run.ps1'.
  . Alternatively, open a developer command prompt (Tools/Command Line/Developer Command Prompt) and type "pwsh Radar_Vsamsql\run.ps1".
    . Running the script likes this produces a colourful graph in addition to the statistics.

PREREQUISITES
  . To run this demo, you need to be able to obtain a seperate copy of the Raincode Stack Installer from the Raincode website.
  . A few manual actions must be performed once before running this demo.
    . Raincode Stack needs to be installed on the database server.
      . The database server is the VM named 'sqlserver' in the resource group.
      . You can connect to it by launching an rdp session from within the client ('this' machine, or the one that runs the demos).
        . Search for 'rdp' in the Windows search bar.
        . Launch the app using 'sqlserver' as the name of the machine and the userid and password that were specified during the installation of Raincode 360.
        . You end up on the desktop of the database server.
        . Install the .NET SDK from https://dotnet.microsoft.com/en-us/download/dotnet/8.0
        . Transfer the license file found in C:\Program Files\Raincode\Crossbow\net8.0\RaincodeMetal.rclic and out it in the exact same location on the target machine.
        . Transfer the Raincode Stack installer and run the install.
    . Make sure the necessary features are switched on in the database server.
      . Navigate to the server desktop via rdp as explained earlier.
      . Launch the 'Sql Server Configuration Manager' via the start menu.
        . Go to 'Sql Native Client Configuration / Client Protocols'.
        . Make sure the services listed there are enabled.
          . Shared Memory
          . TCP/IP
          . Named Pipes
        . If you need to enable one or more of these sevices, you also need to restart Sql Server before your changes will take effect.

DEMO PARAMETERS
  . The COBOL copybook PARM.cpy contains basic parameters that allow you to tweak the demo.

      *** DEMO PARAMETERS ***

      ** Number of records updated. Increasing this value will produce
      ** results that are more stable and accurate but will also affect
      ** the runtime of the demo.
       01 PARM-UPDATES  PIC 9(9) COMP-5 VALUE 5000.
      ** Number of intermediate output LINES.
       01 PARM-BREAKS   PIC 99          VALUE 10.
      ** Total number of rows in the VSQLBENCH table.
       01 PARM-RECORDS  PIC 9(9) COMP-5 VALUE 1000000.
      ** FileConfig name. Do not change.
       01 PARM-FC pic   X(80)           VALUE 'none'.

TROUBLESHOOTING
  . Message
    . IO operation status is 23: 'STATUS_CODE_23_REC_NOT_FOUND'
  . Reason
    . Mismatch between the table layout and the program.
  . Solution
    . Delete the table VSQLBENCH in the VsamSql database and run the test again.

EXPECTED OUTPUT
  . Console window (exact timings will be different)

        Benchmark - VsamSql with RadaR>pwsh Radar_Vsamsql\run.ps1

        +++++++++++++++++++++++++
        +++ CREATE TABLE      +++
        +++++++++++++++++++++++++
        Table VSQLBENCH already exists
        +++++++++++++++++++++++++
        +++ INSERT TEST DATA  +++
        +++++++++++++++++++++++++
        Table is already filled
        +++++++++++++++++++++++++
        +++ VSAMSQL ON CLIENT +++
        +++++++++++++++++++++++++
        FileConfig: FileConfigVsamSql.xml
        Performing 0000005000 random updates on 0001000000 records
        2024-02-19 13:42:23.14 Start
        2024-02-19 13:42:24.29 0000000500 records updated
        2024-02-19 13:42:25.11 0000001000 records updated
        2024-02-19 13:42:25.97 0000001500 records updated
        2024-02-19 13:42:26.76 0000002000 records updated
        2024-02-19 13:42:27.62 0000002500 records updated
        2024-02-19 13:42:28.57 0000003000 records updated
        2024-02-19 13:42:29.43 0000003500 records updated
        2024-02-19 13:42:30.32 0000004000 records updated
        2024-02-19 13:42:31.13 0000004500 records updated
        2024-02-19 13:42:32.16 0000005000 records updated
        2024-02-19 13:42:32.18 Stop
            Time(s)        ms/upd
        ------------- -------------
                9.04          1.80
        ++++++++++++++++++++++++++
        +++ VSAMSQL WITH RADAR +++
        ++++++++++++++++++++++++++
        Successfully processed 642 files; Failed processing 0 files
        Successfully processed 642 files; Failed processing 0 files
        Successfully processed 675 files; Failed processing 0 files
        Configuration option 'external scripts enabled' changed from 1 to 1. Run the RECONFIGURE statement to install.
        Successfully processed 36 files; Failed processing 0 files
        Successfully processed 36 files; Failed processing 0 files
        0

        (0 rows affected)
        STDOUT message(s) from external script:
        FileConfig: FileConfigRadaR.xml
        Performing 0000005000 random updates on 0001000000 records
        2024-02-19 13:42:38.01 Start
        2024-02-19 13:42:38.51 0000000500 records updated
        2024-02-19 13:42:38.73 0000001000 records updated
        2024-02-19 13:42:38.95 0000001500 records updated
        2024-02-19 13:42:39.16 0000002000 records updated
        2024-02-19 13:42:39.38 0000002500 records updated
        2024-02-19 13:42:39.59 0000003000 records updated
        2024-02-19 13:42:39.81 0000003500 records updated
        2024-02-19 13:42:40.02 0000004000 records updated
        2024-02-19 13:42:40.24 0000004500 records updated
        2024-02-19 13:42:40.46 0000005000 records updated
        2024-02-19 13:42:40.47 Stop
            Time(s)        ms/upd
        ------------- -------------
                2.46          0.49



        0

        (1 rows affected)
        Client Elapsed:  12198
        Radar  Elapsed:  4050

        ELAPSED TIMES
        ----------------------------------------------------
                            Rows         Rows        Time
        Test                 Total        Updated     (s)
        ----------------------------------------------------
        VsamSql on Client    1000000      5000        12.20
        VsamSql with RadaR   1000000      5000         4.05



