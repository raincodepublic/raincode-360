param (
  [switch] $Client,
  [switch] $Radar
)
if ($Client -or $Radar) {
} else {
    ## do both if none is specified
    $Client = $True
    $Radar  = $True
}

$ErrorActionPreference = 'Stop'
Push-Location $PSScriptRoot ## $SolutionDir\Radar_VsamSql

Write-Host "++++++++++++++++++++++++++"
Write-Host "+++ PREPARE FILECONFIG +++"
Write-Host "++++++++++++++++++++++++++"
$ConnectionString = "$($Env:RC360_CONNSTRING)Initial Catalog=VsamSql"
Write-Host "Preparing FileConfigVsamSql.xml ..."
(Get-Content "..\FileConfigVsamSqlTemplate.xml").Replace('[ConnectionString]', $ConnectionString) | Set-Content "..\FileConfigVsamSql.xml"
Write-Host "Preparing FileConfigRadar.xml ..."
(Get-Content "..\FileConfigVsamSqlTemplate.xml").Replace('[ConnectionString]', $ConnectionString) | Set-Content "FileConfigRadaR.xml"

Write-Host "+++++++++++++++++++++++++++++++++++++++++++++"
Write-Host "+++ CREATE CATALOG CONFIGURATION FILE     +++"
Write-Host "+++++++++++++++++++++++++++++++++++++++++++++"
$Catalog = New-TemporaryFile
Write-Host "Writing output to $Catalog ..."
@'
<catalogConfiguration defaultRecordLength="80" defaultSysoutRecordLength="200"
					  defaultSysprintRecordLength="133" truncateRedirectedSysoutRecord="false" 
					  ebcdicCompareCodePage="870" defaultOutputClass="A" 
					  defaultCodePage="1252"
					  sysoutFolderPath="C:\ProgramData\Raincode\Batch\SYSOUT" sysoutVolumeName="#SYSOUT" sysoutSyslogVersion="1" 
					  autoEditCalendarFolderPath="C:\Raincode360\Repositories\Demos\Batch - VsamSql\catalog\AutoEditCalendars" 
					  sysoutFolderNamingPattern="JOB{ID}-{NAME}" defaultRecordFormat="FB" defaultSysoutRecordFormat="LSEQ" 
					  defaultSysprintRecordFormat="LSEQ" instreamDataRecordFormat="FB" instreamDataRecordLength="80" SDSN_Wait="3600" 
					  enforceMemoryRestriction="true" enforceUpdateLastReference="false" dbConnectionDataProvider="C:\ProgramData\Raincode\Batch\RcDbConnections.csv"
					  >
  <units>
    <unit name="SYSDA" isDefault="true">
      <volumes>
        <volume name="DEFAULT" path="C:\ProgramData\Raincode\Batch\DefaultVolume" isDefault="true" />
      </volumes>
    </unit>
  </units>
  <defaultProcLibraries>
    <dsn>SYS1.PROCLIB</dsn>
  </defaultProcLibraries>
  <codePages jclAlternate="1252" jclRewrite="Default" />
  <datasetSqlMapping>
    <datasetTemplate tableName="VSQLBENCH" connection="VSAMSQL">
      <pattern>
        <DSN>VSAMSQL\.BENCH\.FB80</DSN>
      </pattern>
      <parameters length="512">
        <key start="0" length="64" />
      </parameters>
    </datasetTemplate>
  </datasetSqlMapping>
</catalogConfiguration>
'@ > $Catalog

Write-Host "++++++++++++++++++++++++++++++++++++++++++++++++++++"
Write-Host "+++ CREATE TABLE FROM CATALOG CONFIGURATION FILE +++"
Write-Host "++++++++++++++++++++++++++++++++++++++++++++++++++++"
$OutFile = New-TemporaryFile
Write-Host "Writing output to $Outfile ..."
& "$($env:rcbin)\VsamSql.DbGenerator.exe" -CatalogConfiguration="$Catalog" -OutputFile="$OutFile"
if ($LastExitCode -ne 0) { 
    Write-Host "VsamSql.DbGenerator.exe: rc=$LastExitCode"
    Exit
}
Write-Host "Invoking sql ..."
Invoke-Sqlcmd -InputFile "$OutFile" -ServerInstance sqlserver -Database VsamSql -ErrorAction Stop

Write-Host "++++++++++++++++++++++++++"
Write-Host "+++ INSERT TEST DATA   +++"
Write-Host "++++++++++++++++++++++++++"
#if ($Res[0] -eq 0) {
    Write-Host "Inserting test data ..."
    Push-Location "..\P001\bin\Debug\net8.0"
    & "$env:rcbin\rclrun" -ProgramArguments="FileConfigVsamSql.xml" -FileConfigEncoding=Windows-1252 -FileConfig="..\..\..\..\FileConfigVsamSql.xml" -LogLevel=WARNING  CRDATA -StringRuntimeEncoding=IBM037
    Pop-Location
#} else {
#    Write-Host "Table is already filled"
#}

# ++++++++++++++++++++++++++++++++++++++
# +++ Extract Elapsed Time from File +++
# ++++++++++++++++++++++++++++++++++++++
Function ExtractElapsed {
    param (
        [Parameter(Mandatory=$true)]
        [String]$File
    )
    $b = Get-Content $File | Select-String -Pattern '^Elapsed \(s\):' 
    if ($b.Count -ne 1) {
        throw "*** Elapsed Time Mismatch"
    }
    [Decimal]$b.Line.Substring(12)
}

if ($Client) {
    Write-Host "++++++++++++++++++++++++++"
    Write-Host "+++ VSAMSQL ON CLIENT  +++"
    Write-Host "++++++++++++++++++++++++++"
    $AssemblyDir = "..\P001\bin\Debug\net8.0"
    $OutFile = (New-TemporaryFile).FullName
    $Stopwatch = [System.Diagnostics.Stopwatch]::new()
    $Stopwatch.Start()
    & "$env:rcbin\rclrun" -ProgramArguments="FileConfigVsamSql.xml" -FileConfigEncoding=Windows-1252 -FileConfig="..\FileConfigVsamSql.xml" -LogLevel=WARNING  RDDATA -StringRuntimeEncoding=IBM037 -AddAssemblySearchDir="$AssemblyDir" | Tee-Object $OutFile
    $Stopwatch.Stop()
    $ClientInternal = ExtractElapsed -File $OutFile
    $ClientExternal = $Stopwatch.ElapsedMilliseconds / 1000
    Write-Host "Client Internal (s):" $ClientInternal
    Write-Host "Client External (s):" $ClientExternal
}

if ($Radar) {
    Write-Host "+++++++++++++++++++++++++++"
    Write-Host "+++ VSAMSQL WITH RADAR  +++"
    Write-Host "+++++++++++++++++++++++++++"
    $ServerDir = "$Env:USERPROFILE\Documents" 
    $FileConfig = "FileConfigRadaR.xml"
    $RadaRDir = "$Env:RCDIR\scripts\RadaR"
    $ScriptDir = $RadaRDir #"." ## Use local scripts
    $BinDir = "..\P001\bin\Debug\net8.0"
    $MssqlDir = "C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL"
    $OutFile = 'runme.sql'

    Set-Item wsman:\localhost\Client\TrustedHosts -Value '*' -Force
    $Session = New-PSSession -ComputerName 'sqlserver'
    $FilesToCopy = @(
        "$RadarDir\dotnet-core-CSharp-lang-extension.zip",
        "$RadarDir\MSSQLRunner.dll", 
        #"$BinDir\RDDATA.dll",
        #"$BinDir\FCOPY.dll",
        #"$BinDir\TIMER.dll",
        "$BinDir\*.dll",
        "$FileConfig"
        )
    Copy-Item -Path $FilesToCopy -ToSession $Session -Destination $ServerDir
    ## Troubleshooting: get contents of server folder
    ## Invoke-Command -Session $Session -ScriptBlock { Get-ChildItem | Select-Object Mode, LastWriteTime, Length, Name, PSComputername }
    Invoke-Command -Session $Session -ScriptBlock { net start MSSQLLaunchpad } -ErrorAction SilentlyContinue
    #Invoke-Sqlcmd -Query "DROP EXTERNAL LIBRARY rcrun.dll" -ServerInstance sqlserver -Database VsamSql -ErrorAction SilentlyContinue
    Invoke-Command -FilePath $ScriptDir\perms.ps1 -Session $Session -ArgumentList $ServerDir
    Invoke-Command -FilePath $ScriptDir\install.ps1 -Session $Session -ArgumentList $MssqlDir , "Server=127.0.0.1;Database=master;Trusted_Connection=True;TrustServerCertificate=True;Initial Catalog=VsamSql"
    Remove-Item "$OutFile*" 
    $Flags = @(
        "-LogLevel=WARNING",
        "-ProgramArguments=`"$FileConfig`"",
        "-FileConfigEncoding=Windows-1252",
        "-AddAssemblySearchDir=$ServerDir",
        "-FileConfig=`"$ServerDir\$FileConfig`"",
        "-StringRuntimeEncoding=IBM037"
    ) -join ' '
    $Stopwatch = [System.Diagnostics.Stopwatch]::new()
    $Stopwatch.Start()
    #& "$ScriptDir\radar.ps1" -SqlFile $OutFile -ProgName "RDDATA" -Flags  $Flags 

    $Env:SQLCMDSERVER = $Env:DB_SERVER
    $Env:SQLCMDPASSWORD = $Env:DB_PASSWORD
    $Env:SQLCMDUSER = $Env:DB_USER
    $Env:SQLCMDDBNAME = "VSAMSQL"
    & "C:\Program Files\Raincode\Crossbow\net8.0\scripts\RadaR\radar.ps1" -SqlFile $OutFile  -RunArgs "$Flags RDDATA" 
    $Stopwatch.Stop()
    Get-Content "$Outfile.out"
    $Session | Remove-PSSession
    $RadaRInternal = ExtractElapsed -File "$Outfile.out"
    $RadarExternal = $Stopwatch.ElapsedMilliseconds / 1000
    Write-Host "RadaR Internal (s):" $RadaRInternal
    Write-Host "RadaR External (s):" $RadaRExternal
}

Pop-Location

if ($Client -and $Radar) {
    Write-Host "+++++++++++++++++++++++++++"
    Write-Host "+++ FINAL REPORT        +++"
    Write-Host "+++++++++++++++++++++++++++"
    function Write-Result    
    {
        param([String]$TestName, [Double]$Time, [Double]$ItemTotal, [Double]$ItemProcessed, [string]$Color)
        Write-Host -NoNewLine $TestName.PadRight(8) 
        Write-Host -NoNewLine $ItemTotal.ToString().PadLeft(10)
        Write-Host -NoNewLine $ItemProcessed.ToString().PadLeft(10) 
        Write-Host -NoNewLine $Time.tostring("0.00").PadLeft(13)
        Write-Host -NoNewLine "    "
        For ($i=0; $i -lt [math]::Round($Time * 10); $i++) { write-host -nonewline -BackgroundColor $Color ' '}
        Write-Host 
    }
    Write-Host "`nELAPSED TIMES"
    Write-Host "----------------------------------------------------"
    Write-Host "                     Rows         Rows        Time"
    Write-Host "Test                 Total        Updated     (s)"
    Write-Host "----------------------------------------------------"
    Write-Result -TestName "VsamSql on Client " -Time "$($ClientInternal)" -ItemTotal 25000 -ItemProcessed 3000 -Color "Green"
    Write-Result -TestName "VsamSql with RadaR" -Time "$($RadarInternal)"  -ItemTotal 25000 -ItemProcessed 3000 -Color "DarkYellow"
    Write-Host
}
