<#
.SYNOPSIS
 Install Db Local Code -- DbLoCo. This script needs to run as administrator.

 Note that DbLoCo can only work on SQL Server for windows. See https://github.com/microsoft/sql-server-language-extensions/tree/main/language-extensions/dotnet-core-CSharp
 Hence this script is windows-only, for brevity.

 Prerequisites: A working install of the raincode stack, with the RCBIN env variable set. A working sqlcmd that can converse with the database (e.g. with working windows authentication or alternatively with correct SQLCMDSERVER and SQLCMDUSER environment variables set).

#>

param (
# The MSSQL directory, e.g. "C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL"
    [Parameter(Mandatory)]
    [string]
    $MSSSqlDir
)

# If there are problems, give up.
$ErrorActionPreference = 'Stop'

#Grant required permissions on different directories

# SQL server needs to be able to run rclrun (here assuming the standard name of the group)
& icacls "$Env:RCBIN" /grant `"SQLRUserGroup`":`(OI`)`(CI`)RX /T /Q 
& icacls "$Env:RCBIN" /grant *S-1-15-2-1:`(OI`)`(CI`)RX /T /Q 

# See https://github.com/microsoft/sql-server-language-extensions/blob/main/language-extensions/dotnet-core-CSharp/sample/regex/README.md#permissions-to-execute-external-language
& icacls "$MSSSqlDir" /grant *S-1-15-2-1:`(OI`)`(CI`)RX /T /Q 
& sqlcmd -Q "SP_CONFIGURE 'external scripts enabled', 1; RECONFIGURE WITH OVERRIDE;"
& sqlcmd -Q "IF NOT EXISTS (SELECT * FROM sys.external_languages WHERE language = 'Dotnet') CREATE EXTERNAL LANGUAGE Dotnet FROM (CONTENT = N'$pwd\dotnet-core-CSharp-lang-extension-windows-release.zip', FILE_NAME = 'nativecsharpextension.dll' );"
& sqlcmd -Q "IF NOT EXISTS (SELECT * FROM sys.external_libraries WHERE name = 'rcrun.dll') CREATE EXTERNAL LIBRARY [rcrun.dll] FROM (CONTENT = N'$pwd\MSSQLRunner.dll') WITH(LANGUAGE = 'Dotnet'); "
