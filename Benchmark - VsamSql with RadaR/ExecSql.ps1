[CmdletBinding()]
Param (
    [Parameter(Mandatory)]
    [string]
    $ConnectionString,
    [Parameter(Mandatory)]
    [string]
    $InputFile
)
Write-Host "Executing `"$InputFile`""
Invoke-Sqlcmd -AbortOnError -ConnectionString $ConnectionString -InputFile $InputFile
