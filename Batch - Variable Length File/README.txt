CATALOG EXPLORER DEMO: VARIABLE LENGTH FILE

PURPOSE
. This demo creates a variable length file that can be vieuwed in the Catalog Explorer

HOW TO RUN
. Start the Raincode Debugger in Visual Studio
. Launch the Catalog Explorer to look at the result

NOTES
. The JCL starts by deleting the output from the previous run (if any)

