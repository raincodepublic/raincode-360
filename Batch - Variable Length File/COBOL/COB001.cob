       IDENTIFICATION DIVISION.
        PROGRAM-ID. COB001.
       ENVIRONMENT DIVISION.
        INPUT-OUTPUT SECTION.
         FILE-CONTROL.
           SELECT VB-FILE
               ASSIGN TO CUSTFILE
               ORGANIZATION IS SEQUENTIAL
               FILE STATUS  IS W-STAT.
       DATA DIVISION.
        FILE SECTION.
        FD VB-FILE
            RECORDING MODE IS V
            RECORD IS VARYING IN SIZE
            FROM 4 TO 128 DEPENDING ON W-LEN.
        01 VB-RECORD.
           02 W-CUSTOMERID PIC 9(4). 
           02 W-FULLNAME PIC X(124).
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       01 W-LEN PIC 9999.
       01 W-STAT PIC XX.
   	   EXEC SQL
   	       DECLARE CUR01 CURSOR FOR
      	   SELECT 
               C.CUSTOMERID, 
               ' ' || UPPER(TRIM(C.LASTNAME)) || 
               ', ' || TRIM(C.FIRSTNAME),
               LENGTH(TRIM(C.LASTNAME)) + LENGTH(TRIM(C.FIRSTNAME)) + 7
           FROM 
               DBO.CUSTOMER C,
               DBO.ZIPCODE Z
           WHERE 
               Z.ZIPCODE = C.ZIPCODE
   	   END-EXEC.
       PROCEDURE DIVISION.
           OPEN OUTPUT VB-FILE
           IF W-STAT IS NOT EQUAL TO '00'
               DISPLAY '**** OPEN VB-FILE: ' W-STAT
               STOP RUN
           END-IF
           EXEC SQL
   	           OPEN CUR01
   	       END-EXEC
   	       PERFORM FETCH-CUR01
           PERFORM UNTIL SQLCODE > 0
               WRITE VB-RECORD
   	           PERFORM FETCH-CUR01
           END-PERFORM
           EXEC SQL
   	           CLOSE CUR01
   	       END-EXEC
           CLOSE VB-FILE.
           GOBACK
           .
       FETCH-CUR01.
   	       EXEC SQL
   	           FETCH 
                   CUR01 
               INTO
                   :W-CUSTOMERID,
                   :W-FULLNAME,
                   :W-LEN
   	       END-EXEC
           .
