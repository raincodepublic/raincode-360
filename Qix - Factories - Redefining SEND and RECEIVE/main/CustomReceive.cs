﻿using RainCodeLegacyRuntime.Core;
using RainCodeQixRuntimeInterface.QixInstances;

namespace RainCode.Samples
{
    class CustomReceive : ReceiveInstance
    {
        public override void Execute()
        {
            ExecutionContext ec = (ExecutionContext)Context;

            if (CustomSend.Data.Count > 0)
            {
                var data = CustomSend.Data.Dequeue();
                if (Variant == Variants.Into)
                {
                    // EXEC CICS RECEIVE INTO(WS-INTO) END-EXEC

                    // Variants.Into means that the memory already exists in the address space and does not need to allocated
                    // it can simply be copied.

                    // Make sure to use the correct string encoding when needed
                    DATA.Write(0, data);
                }
                else
                {
                    // EXEC CICS RECEIVE SET(WS-PTR) LENGTH(WS-LENGTH) END-EXEC

                    // Memory is allocated by QIX itself
                    DATA = Context.QixReallocate(data.Length, this);
                    // Copy the data to the program
                    DATA.Write(0, data);
                    // The QIX API takes care of setting the correct value in the LENGTH option of the command
                }
                LENGTH = data.Length;
            }
            else
            {
                ec.RaincodeQixContext.DfheiblkHelper.EIBRESP = RainCodeQixInterface.Constants.Resp.QZERO;
            }
        }
    }
}
