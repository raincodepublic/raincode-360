﻿using RainCode.Core.Logging;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions.QIX;
using RainCodeLegacyRuntime.Sql.SqlServer;
using System;

namespace RainCode.Samples
{
    class main
    {
        private static void Link(ExecutionContext ec, string program)
        {
            var link = ec.QixInterface.Factory.CreateLink();
            link.Context = ec;
            link.PROGRAM = program;
            ec.QixInterface.Operation.Link(link);
            if (ec.RaincodeQixContext.DfheiblkHelper.EIBRESP != RainCodeQixInterface.Constants.Resp.NORMAL)
            {
                throw new Exception(string.Format("Program exited with RESP {0}", ec.RaincodeQixContext.DfheiblkHelper.EIBRESP));
            }
        }
        private static ExecutionContext SetupQix()
        {
            ExecutionContextArgs ecArgs = new ExecutionContextArgs()
            {
                QixMode = true,
                QixFactory = new CustomQixFactory()
            };
            ExecutionContext ec = new ExecutionContext(ecArgs);
            ec.SqlRuntime = new SqlServerRuntime(ec, GetConnectionString());
            ec.SqlRuntime.ExecutionStrategy = RainCodeLegacyRuntime.Sql.SqlRuntime.ExecutionModeStrategy.DynamicOnly; 
            ec.IOOperation.ConfigureSystemDatasets(); 
            return ec;
        }
        static void Main(string[] args)
        {
            var ec = SetupQix();
            Link(ec, "COB001");
        }
        #region connectionstring
        private static string GetConnectionString()
        {
            string NightbuildConnectionString = "$SQLSERVERCONNECTIONSTRING$"; //Automated test framework substitution
            string Rc360ConnectionString = Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog=BankDemo;";
            return !NightbuildConnectionString.Contains("$SQLSERVERCONNECTIONSTRING") ? NightbuildConnectionString : Rc360ConnectionString;
        }
        #endregion connectionstring
   }
}
