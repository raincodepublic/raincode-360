﻿using RainCodeQixRuntimeInterface.QixInstances;
using System.Collections.Generic;

namespace RainCode.Samples
{
    class CustomSend : SendInstance
    {
        public readonly static Queue<byte[]> Data = new Queue<byte[]>();

        public override void Execute()
        {
            Data.Enqueue(FROM.Bytes);
        }
    }
}
