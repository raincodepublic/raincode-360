
**************************************************************
*                                                            *
*   QIX Demo : Factories                                     *
*              Redefining SEND and RECEIVE                   *
*                                                            *
**************************************************************

PURPOSE
  . Redefine the behaviour of the standard CICS commands SEND and RECEIVE by providing custom factories.
  . SEND and RECEIVE are traditionally used to send and receive text messages to and from the terminal.
  . The behaviour of thes commands is changed so that they can send and receive data structures to and from an in-memory queue.

WHAT THE DEMO DOES
  . The main program asks the user for a city prefix and calls a subroutine (via CICS LINK) to retrieve all cities matching this prefix from the database.
  . The subroutines pushes a variable number of results on the internal queue.
  . The main routine readsthe results from the queue and shows them on the screen.

HOW TO RUN THE DEMO
  . Select Debug/Start Without Debugging on the main menu.
  . Starting the debugger in this way will avoid automatic brakpoints and will also keep the console window open after the program ends.
  . Enter a prefix of a Belgian city ('BR' for Brussels will work just fine).
  . Watch the result on the screen.
  . Close the console window.
