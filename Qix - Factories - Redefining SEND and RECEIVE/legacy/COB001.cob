       IDENTIFICATION DIVISION.
       PROGRAM-ID.     COB001.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY COB002.
       01 W-INTO PIC X(30).
       01 W-FROM PIC X(30).
       01 W-PTR POINTER.
       01 W-LENGTH PIC S9(4) USAGE IS BINARY.
       01 W-OTHER PIC X(24).
       01 W-DATA-FIXED PIC X(45).
       LINKAGE SECTION.
       01 W-DATA PIC X(42).
       PROCEDURE DIVISION.
      *   Passing BR as the city prefix    
          MOVE 'BR' TO COB002-PREFIX
          EXEC CICS 
             IGNORE CONDITION QZERO
          END-EXEC
          EXEC CICS
             LINK POGRAM('COB002') COMMAREA(COB002-COMMAREA)
          END-EXEC
          IF COB002-RC = 0
             PERFORM COB002-ROWS TIMES
                EXEC CICS
                   RECEIVE
                   SET(W-PTR)
                   LENGTH(W-LENGTH)
                END-EXEC
                EVALUATE EIBRESP
                WHEN 0
                   SET ADDRESS OF W-DATA TO W-PTR
                   MOVE W-DATA(1:W-LENGTH) TO W-DATA-FIXED
                   DISPLAY W-DATA-FIXED "Length: " W-LENGTH
                WHEN 23
                   DISPLAY "+++ Error: unexpected end of queue"
                   EXIT PERFORM
                WHEN OTHER
                   DISPLAY "+++ Error: EIBRESP: " EIBRESP
                   EXIT PERFORM
                END-EVALUATE
             END-PERFORM
          END-IF
          EXEC CICS 
             RETURN 
          END-EXEC
          .
