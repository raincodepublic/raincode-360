       IDENTIFICATION DIVISION.
       PROGRAM-ID.     COB002.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 W-CUR01-END PIC S9(4) COMP.
       01 W-CITY.
         49 W-CITY-LEN PIC S9(4) COMP.
         49 W-CITY-TEXT PIC X(30).
       COPY SQLCA.
   	   EXEC SQL
   	       DECLARE CUR01 CURSOR FOR
      	   SELECT DISTINCT
               UPPER(Z.CITY)
           FROM 
               DBO.ZIPCODE Z
           WHERE 
               UPPER(Z.CITY) LIKE UPPER(TRIM(:COB002-PREFIX)) || '%'
           ORDER BY 1
   	   END-EXEC.
       LINKAGE SECTION.
       COPY COB002 REPLACING COB002-COMMAREA BY DFHCOMMAREA.       
       PROCEDURE DIVISION.
           PERFORM OPEN-CUR01
           INITIALIZE COB002-OUT
   	       PERFORM WITH TEST AFTER UNTIL W-CUR01-END > 0
               PERFORM FETCH-CUR01
               IF W-CUR01-END = 0
                   ADD 1 TO COB002-ROWS
                   EXEC CICS
                       SEND FROM(W-CITY-TEXT(1:W-CITY-LEN))
                   END-EXEC
               END-IF
           END-PERFORM
           PERFORM CLOSE-CUR01
           EXEC CICS 
               RETURN 
           END-EXEC
           .
       OPEN-CUR01.
   	       EXEC SQL
   	           OPEN CUR01
   	       END-EXEC
           EVALUATE SQLCODE
           WHEN 0
               CONTINUE
           WHEN OTHER
               PERFORM RETURN-SQL-ERROR
           END-EVALUATE
           .
       FETCH-CUR01.
   	       EXEC SQL
   	           FETCH CUR01 INTO :W-CITY
   	       END-EXEC
           EVALUATE SQLCODE
           WHEN 0
               MOVE 0 TO W-CUR01-END
           WHEN 100
               MOVE 1 TO W-CUR01-END
           WHEN OTHER
               PERFORM RETURN-SQL-ERROR
           END-EVALUATE
           .
       CLOSE-CUR01.
   	       EXEC SQL
   	           CLOSE CUR01
   	       END-EXEC
           EVALUATE SQLCODE
           WHEN 0
               CONTINUE
           WHEN OTHER
               PERFORM RETURN-SQL-ERROR
           END-EVALUATE
           .
       RETURN-SQL-ERROR.
           MOVE -1 TO COB002-RC
           MOVE SQLCODE TO COB002-SQLCODE
           MOVE SQLERRM TO COB002-SQLERRM
           EXEC CICS 
               RETURN 
           END-EXEC
           .
           
