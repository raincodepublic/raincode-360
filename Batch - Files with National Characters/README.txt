********************************************************************
*                                                                  *
*   DEMO : National Characters in Files                            *
*                                                                  *
********************************************************************

PURPOSE
  . Show how national characters can be written to files.
  . Demonstrate how the Record Editor can be used to view these files.

CONCEPTS
  . National character string are declared as PIC N(n) USAGE NATIONAL.
  . Each character is represented by two or four bytes.
  . Numeric edited variables can also be declared with USAGE NATIONAL.
  . Supported operations are computations, output to the console, viewing values in the debugger, and reading and writing to a database.
  . To compile programs with embedded national characters, as in 
          MOVE N'世界！' TO REC
    The compilation command line must contain the following option:
        :StringSourceFileEncoding=utf-8
   . National characters are encoded in UTF-16 big endian format.
   . The console is not able to display unicode characters. Change the language to 'Japanese' in
        [configuration/time and language/language/administrative language settings/change system locale]
     to make the the Japanese characters visible. This requires rebooting your machine.

ABOUT THE DEMO
  . The demo reads English and Japanese descriptions from a table file.
  . The output file is 'NATIONAL.FILE01'. It contains both character fields and national fields.

RUNNING THE DEMO IN VISUAL STUDIO
  . Run 'gendd.bat' in the solution directory to produce a file named 'DeclarationDescripors.xml'.
  . Press F5 or Ctrl-F5 to submit the JCL.
  . Look at the contents of the output file with the Record Editor. 
  . Switch to field view using the declaration descriptor or the copybook to make the Japanese characters visible.
