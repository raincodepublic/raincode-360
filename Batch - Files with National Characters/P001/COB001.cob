﻿       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB001.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT FILE01 ASSIGN TO FILE01
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE IS SEQUENTIAL.
       CONFIGURATION SECTION.
       DATA DIVISION.
       FILE SECTION.
       FD FILE01.
       COPY REC01.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       01 ITEM.
           10 ITEM-NM-ENG PIC X(50).
           10 ITEM-NM-JPN PIC N(50) USAGE NATIONAL.
           10 ITEM-QTY PIC S9(4) COMP.
           10 ITEM-PRICE PIC S9(7)V99 COMP-3.
   	   EXEC SQL
   	       DECLARE CUR01 CURSOR FOR
      	   SELECT 
               ITEM_NM_ENG,
               ITEM_NM_JPN,
               ITEM_QTY,
               ITEM_PRICE
           FROM 
               DBO.OFFICESUPPLIES
   	   END-EXEC.
       PROCEDURE DIVISION.
           DISPLAY N'在庫レベル'
           DISPLAY N'---------'
           OPEN OUTPUT FILE01
           EXEC SQL
   	           OPEN CUR01
   	       END-EXEC
   	       PERFORM FETCH-CUR01
           PERFORM UNTIL SQLCODE NOT = 0
               MOVE ITEM-NM-ENG TO REC01-NM-ENG
               MOVE ITEM-NM-JPN TO REC01-NM-JPN
               MOVE ITEM-QTY TO REC01-QTY
               MOVE ITEM-PRICE TO REC01-PRICE
               MOVE N'¥' TO REC01-CURRENCY
               WRITE REC01
               DISPLAY REC01-NM-JPN N' 量: ' REC01-QTY
   	           PERFORM FETCH-CUR01
           END-PERFORM
           EXEC SQL
   	           CLOSE CUR01
   	       END-EXEC
           CLOSE FILE01
           GOBACK
           .
       FETCH-CUR01.
   	       EXEC SQL
   	           FETCH CUR01 INTO
                   :ITEM-NM-ENG,
                   :ITEM-NM-JPN,
                   :ITEM-QTY,
                   :ITEM-PRICE
   	       END-EXEC
           .
