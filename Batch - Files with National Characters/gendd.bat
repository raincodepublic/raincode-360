@echo off
echo ######################
echo ## copybook version ##
echo ######################
"%RCBIN%\cobrc.exe" :DeclDescriptors=DeclDescriptors1.xml "P001\REC01.cpy"  
echo ######################
echo ## program version  ##
echo ######################
"%RCBIN%\cobrc.exe" :DeclDescriptors=DeclDescriptors.xml :IncludeSearchDir=P001 :IncludeSearchPath="%RCDIR%/includes/cobol" :SQL=sqlserver "P001\COB001.cob"  
