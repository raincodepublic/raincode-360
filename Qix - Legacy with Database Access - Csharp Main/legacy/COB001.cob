       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB001.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       01 W-ZIPCODE PIC X(4).
       PROCEDURE DIVISION.
          EXEC SQL
   	         SELECT 
                MIN(ZIPCODE)
             INTO
                :W-ZIPCODE
             FROM
                ZIPCODE
          END-EXEC
          IF SQLCODE = 0
             DISPLAY "Minimum zipcode: " W-ZIPCODE
          ELSE
             DISPLAY "SQLCODE:" SQLCODE
          END-IF   
          EXEC CICS
             RETURN
          END-EXEC
          .