﻿using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions;
using RainCodeLegacyRuntime.Exceptions.QIX;
using RainCodeLegacyRuntime.Module;
using RainCodeLegacyRuntime.Sql.SqlServer;
using System;

namespace RainCode.Samples
{
    class main
    {
        static void Main(string[] args)
        {
            var ecargs = new ExecutionContextArgs()
            {
                QixMode = true,
            };
            var ec = new ExecutionContext(ecargs);
            ec.SqlRuntime = new SqlServerRuntime(ec, GetConnectionString());
            ec.SqlRuntime.ExecutionStrategy = RainCodeLegacyRuntime.Sql.SqlRuntime.ExecutionModeStrategy.DynamicOnly;
            ec.IOOperation.ConfigureSystemDatasets();
            var link = ec.QixInterface.Factory.CreateLink();
            link.Context = ec;
            link.PROGRAM = "COB001";
            try
            {
                ec.QixInterface.Operation.Link(link);
            }
            catch (AbendException e)
            {
                Console.WriteLine("Abend {0} detected", e.Message);
            }
            Environment.Exit(ec.ReturnCode);
        }
        #region connectionstring
        private static string GetConnectionString()
        {
            string NightbuildConnectionString = "$SQLSERVERCONNECTIONSTRING$"; //Automated test framework substitution
            string Rc360ConnectionString = Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog=BankDemo;";
            return !NightbuildConnectionString.Contains("$SQLSERVERCONNECTIONSTRING") ? NightbuildConnectionString : Rc360ConnectionString;
        }
        #endregion connectionstring
      }
}
