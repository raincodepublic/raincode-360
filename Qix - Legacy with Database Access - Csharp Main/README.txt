
*****************************************************************************************
*                                                                                       *
*   Qix Demo : C# calling legacy with database access using CICS LINK                   *
*                                                                                       *
*****************************************************************************************

PURPOSE OF THIS DEMO
  . Show how a COBOL program can be called from a C# main program (a proxy).
  . The C# program uses the CICS LINK mechanism to incoke the COBOL program.
  . Show how to prepare an execution context with a database connection at the C# level.

WHAT THE DEMO DOES
  . The legacy program fetches an item from the database, displays it on the console and exits.

HOW TO RUN THE DEMO
  . Select 'Start Without Debugging' in the Debug menu.
  . This will avoid automatic breakpoints and will also keep the console window open after the program ends.

REFERENCES
  . All Raincode manuals
    . https://www.raincode.com/docs/#_raincode_manuals
  . Linking to a legacy program
    . https://www.raincode.com/docs/Compiler-document/GettingStarted/GettingStarted.html#link-legacy