﻿using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions;
using RainCodeLegacyRuntime.Module;
using RainCodeLegacyRuntime.Sql.SqlServer;
using System;

namespace RainCode.Samples
{
    class main
    {
        private static Executable LoadExecutable(string name)
        {
            var exec = ModuleDictionary.FindExecutable(name);
            ModuleDictionary.EnsureModuleIsLoaded(name);
            return exec;
        }
        static void Main(string[] args)
        {
            var exec = LoadExecutable("COB001");
            var ec = new ExecutionContext();
            ec.SqlRuntime = new SqlServerRuntime(ec, GetSqlConnectionString());
            ec.SqlRuntime.ExecutionStrategy = RainCodeLegacyRuntime.Sql.SqlRuntime.ExecutionModeStrategy.DynamicOnly;
            ec.IOOperation.ConfigureSystemDatasets();
            try
            {
                exec.Execute(ec);
            }
            catch (StopRunUnitException)
            {
                Console.WriteLine("Program ended");
            }

            Environment.Exit(ec.ReturnCode);
        }
        #region ReadConnectionString
        private static string GetSqlConnectionString()
        {
            var connectionString = "$SQLSERVERCONNECTIONSTRING$"; //Automated test framework substitution
            if(connectionString.Contains("$SQLSERVERCONNECTIONSTRING"))
               connectionString = Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog = BankDemo;";
            return connectionString;
        }
        #endregion
   }
}
