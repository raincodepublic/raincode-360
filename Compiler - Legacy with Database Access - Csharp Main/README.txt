
*****************************************************************************************
*                                                                                       *
*   Compiler Demo : C# Calling Legacy with database access Using a COBOL Call           *
*                                                                                       *
*****************************************************************************************

PURPOSE OF THIS DEMO
  . Show how a COBOL program can be called from a C# main program in two different ways.
  . These two methods are implemented in the projects 'main' and 'othermain'.
  . Program.cs in 'main' creates a new executioncontext, sets the connectionstring property in that executioncontext and then calls 'COB001' with that executioncontext.
  . Program.cs in 'othermain' creates a custom runner that is derived from the BaseRunner class, and passes the connection string as an argument to that custom runner.
  . The 'othermain.exe' executable that is produced by the 'othermain' procect accepts all the command line parameters that are accepted by 'rclrun.exe' and is therefore a true custom version of that program. Note that any connection string that is passed to 'othermain.exe' via the command line may cause a conflict, because it will appear to the program as having  been passed two connection strings.
  . The 'main.exe' executable that is produced by the 'main' project ignores all parameters that are passed to it.

WHAT THE DEMO DOES
  . The legacy program fetches an item from the database, displays it on the console and exits. 
  . 'main' and 'othermain' both run COB001 with the exact same connection string and produce the exact same output.

HOW TO RUN THE DEMO
  . Make either 'main' or 'othermain' the statup project by right-clicking on the project name and selecting 'Set as Startup Project'. The default startup project is 'main'. 
  . Select 'Debug/Start Without Debugging' or press Ctrl+F5 to run the selected startup project.
  . Alternatively, run either project by right-clicking on it and selecting 'Debug/Start New Instance'. This has the same effect as making it the main project and clicking on 'Start'.

REFERENCES
  . All Raincode manuals
    . https://www.raincode.com/docs/#_raincode_manuals
  . Calling a legacy program
    . https://www.raincode.com/docs/Compiler-document/GettingStarted/GettingStarted.html#call_legacy