﻿using System;
using System.Linq;

namespace othermain
{
    class othermain
    {
        static void Main(string[] args)
        {
            int returnCode = -1;
            try
            {
                var newArgs = new string[args.Length + 4];
                newArgs[0] = "-SqlExecutionMode=DynamicOnly";
                newArgs[1] = "-Sql=SqlServer";
                newArgs[2] = $"-SqlServer={GetSqlConnectionString()}";
                newArgs[3] = $"COB001"; //Module name
                args.CopyTo(newArgs, 4);
                var myRunner = new Runner(newArgs);
                returnCode = myRunner.Execute();
            }
            catch (Exception e)
            {
                // Here add your own errorhandling. We end here if there was an exception.
                // Logger.LogError(LogSource, "{0}", e);
                Console.WriteLine("There was an error");
            }
            finally
            {
                // We are here anyway, but the returncode provided by the program might still be different from 0.
                // add behaviour to handle error codes different from 0.
                if (returnCode != 0 && args.FirstOrDefault(arg => string.Equals(arg.Substring(1), "PauseOnAbnormalExit", StringComparison.InvariantCultureIgnoreCase)) != null)
                {
                    Console.WriteLine($"Program ended with return code {returnCode}. Press enter to continue");
                    Console.ReadLine();
                }
                Environment.Exit(returnCode);
            }

        }
        #region ReadConnectionString
        private static string GetSqlConnectionString()
        {
            var connectionString = "$SQLSERVERCONNECTIONSTRING$"; //Automated test framework substitution
            if(connectionString.Contains("$SQLSERVERCONNECTIONSTRING"))
               connectionString = Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog = BankDemo;";
            return connectionString;
        }
        #endregion
    }
}
