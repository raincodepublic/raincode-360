==========================
LOADING DATA WITH DSNUTILB
==========================

PURPOSE
  . Illustrate the use of DSNTUTILB for loading data in a JCL context.

HOW TO RUN
  . Start the Raincode Debugger in Visual Studio
  . Examine the contents of the ZIPCOPY table in the BANKDEMO database

DATABASE CONNECTION
  . The database connection relies on the following elements:
    . Planname
      . defined in Raincode.Catalog.xml
           <programDBConnectionDefinitions>
               <dbConnection JobId=".*" >DEFAULTPLAN</dbConnection >
           </programDBConnectionDefinitions>
   . Connection string
     . defined in RcDbConnections.csv
          .*,SqlServer,Data Source=<datasource>;Uid=<user>;Pwd=<password>;Initial Catalog=BankDemo;

NOTES
  . For the load to work, the input fields must span the record exactly, so no filler is allowed after the last record
  . The sort step creates a file with the required record length of 16
  . The input fields can be shorter than the fields in the database
