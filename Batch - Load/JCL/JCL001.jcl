//*********************************************************
//*                                                       *
//*   LOAD EXAMPLE                                        *
//*                                                       *
//*********************************************************
//JCL001   JOB CLASS=A,MSGCLASS=C
//*********************************************************
//* CLEAN UP FILES FROM PREVIOUS RUN                      *
//*********************************************************
//STEP001  EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
 DELETE LOAD.ZIPCOPY.*
 DELETE DELETE.QUERY.TEMP
 SET MAXCC = 0
/*
//*********************************************************
//* CLEANUP TABLE FROM PREVIOUS RUN                       *
//*********************************************************
//STEP002   EXEC PGM=IKJEFT01
//SYSTSPRT  DD SYSOUT=*
//SYSTSIN   DD *  
 RUN PROGRAM(DSNTIAUL) PLAN(DSNTIAUL_PLAN) PARM('SQL') 
 END
//SYSPRINT  DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//SYSREC00  DD DSN=DELETE.QUERY.TEMP,
//        DISP=(NEW,CATLG,DELETE),RECFM=FB,VOL=SER=DEFAULT,LRECL=57
//SYSPUNCH  DD SYSOUT=*                                           
//SYSIN     DD *
 TRUNCATE TABLE ZIPCOPY;
/*
//*********************************************************
//* PRODUCE LOADFILE                                      *
//*********************************************************
//STEP002  EXEC PGM=SORT                                  
//SYSOUT   DD SYSOUT=*
//SORTIN   DD *
1000BRUSSELS  BE
2000ANTWERP   BE
3000GHENT     BE
/*
//SORTOUT  DD DSN=LOAD.ZIPCOPY.INPUT,
//        DISP=(NEW,CATLG,DELETE),RECFM=FB,VOL=SER=DEFAULT,LRECL=16
//SYSIN    DD *
  SORT FIELDS=COPY
/*
//*********************************************************
//* PERFORM LOAD                                          *
//*********************************************************
//STEP003  EXEC PGM=DSNUTILB
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//UTPRINT  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSIN    DD *
  LOAD DATA RESUME YES INDDN SYSREC
  INTO TABLE dbo.ZIPCOPY
  (ZIPCODE POSITION( 1) CHAR( 4),
   CITY    POSITION( 5) CHAR(10),
   COUNTRY POSITION(14) CHAR( 2))
//SYSREC DD DSN=LOAD.ZIPCOPY.INPUT,DISP=SHR
