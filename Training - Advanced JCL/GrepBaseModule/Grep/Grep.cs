﻿namespace Raincode.csharp
{
    using RainCodeLegacyRuntime.Core;
    using RainCodeLegacyRuntime.Module;
    using RainCodeLegacyRuntime.Module.Attribute;
    using System.Text;
    using System;
    using RainCodeLegacyRuntime.IO;
    using RainCodeLegacyRuntimeUtils;

    [RainCodeExport]
    public class Grep : BaseModule<Grep>
    {
        private static Encoding ebcdic = Encoding.GetEncoding("IBM037");
        private static Encoding ascii = Encoding.GetEncoding("windows-1250");

        RecordBasedFile grepin, grepout, sysin;
        MemoryArea statusArea;
        MemoryArea grepinBuffer, grepoutBuffer, sysinBuffer;

        protected override void Run(RainCodeLegacyRuntime.Core.ExecutionContext ec, MemoryArea workMem, CallParameters parms)
        {
            ec.IOOperation.ConfigureSystemDatasets();
            InitFiles(ec);

            //read SYSIN
            sysin.set_status_memoryarea(statusArea);
            sysin.set_IOBuffer(sysinBuffer);
            sysin.read_sequential(OperationLockType.NotSpecified);
            var lastSize = sysin.get_last_record_size();

            var arr = sysinBuffer.RetrieveBytes(0, sysinBuffer.Size);
            string sysinLine = Encoding.ASCII.GetString(arr).Trim();

            while (true)
            {
                grepin.set_status_memoryarea(statusArea);
                grepin.set_IOBuffer(grepinBuffer);
                grepin.read_sequential(OperationLockType.NotSpecified);

                if (grepin.is_status_class(RecordBasedFile.STATUS_CLASS_1)) 
                {
                    // end of file
                    grepin.close();
                    grepout.close();
                    sysin.close();
                    return;
                }

                arr = grepinBuffer.RetrieveBytes(0, grepinBuffer.Size);
                string line = ascii.GetString(arr);
                if (line.Contains(sysinLine))
                {
                    //TODO adapt for VB, trim or pad
                    grepinBuffer.CopyTo(grepoutBuffer);
                    grepout.set_current_record_size(lastSize);
                    grepout.set_data(grepoutBuffer);
                    grepout.write(0);
                }
            }
        }

        private void InitFiles(RainCodeLegacyRuntime.Core.ExecutionContext ec)
        {
            grepin = ec.IOOperation.get_file(ec.IOOperation.get_handle("grepin"));
            FileFormatType ff = grepin.get_file_format();
            FileOrganizationType fo = grepin.get_file_organization();
            int fs = grepin.get_maximum_record_size();
            grepinBuffer = ec.Allocate(fs);

            MemoryArea statusArea = ec.Allocate(2);
            grepin.set_status_memoryarea(statusArea);
            grepin.set_open_mode(FileEnums.OpenModeType_Input());
            grepin.open();

            grepout = ec.IOOperation.get_file(ec.IOOperation.get_handle("grepout"));
            ff = grepout.get_file_format();
            fo = grepout.get_file_organization();
            fs = grepout.get_maximum_record_size();
            grepoutBuffer = ec.Allocate(fs);

            grepout.set_status_memoryarea(statusArea);
            grepout.set_open_mode(FileEnums.OpenModeType_Output());
            grepout.open();

            sysin = ec.IOOperation.get_file(ec.IOOperation.get_handle("sysin"));
            ff = sysin.get_file_format();
            fo = sysin.get_file_organization();
            fs = sysin.get_maximum_record_size();
            sysinBuffer = ec.Allocate(fs);

            sysin.set_status_memoryarea(statusArea);
            sysin.set_open_mode(FileEnums.OpenModeType_Output());
            sysin.open();
        }
    }
}
