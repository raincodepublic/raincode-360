//JCL001   JOB COND=(4,LT)
//********************************************************************
//* GREP Demo                                                        *
//********************************************************************
//*------------------------------------------------------------------*
//* Clean up output files from previous run.                         *
//*------------------------------------------------------------------*
//STEP001 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSOUT DD SYSOUT=*
//SYSIN DD *
 DELETE GREP.OUT
      SET LASTCC=0
      SET MAXCC=0
/*
//*------------------------------------------------------------------*
//* Copy lines containing 'ER' to GREPOUT.                           *
//*------------------------------------------------------------------*
//STEP002  EXEC PGM=GREP
//GREPIN   DD *
AACHEN
AMSTERDAM
ANTWERP
BRUSSELS
CHARLEROI
/*
//SYSIN    DD *
ER
/*
//GREPOUT  DD DSN=GREP.OUT,
//         DISP=(NEW,CATLG,DELETE),RECFM=LSEQ,VOL=SER=DEFAULT,LRECL=80
