To create a repository in SQLite format:
""""""""""""""""""""""""""""""""""""""""
. Open DB Browser for SQLite.
. Select File/Import/Database from SQL File from the main menu.
. Open the file repository.db3.sql in this folder.
. Choose a name and a location for the resulting database.
. Go to the 'Execute SQL' tab.
. Open the files JclExec.sql and PgmCall.sql in this folder.
