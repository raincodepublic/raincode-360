@ECHO OFF

ECHO Cleaning up repository ...

del repository.db3

ECHO RCBATCHDIR: %rcbatchdir%
ECHO RCDIR: %rcdir%

ECHO Compiling COBOL ...

"%RCDIR%bin\cobrc" @CobOptions.txt @CobSource.txt

ECHO Scanning JCL ...

"%rcbatchdir%"submit -File=SOURCE\JCL\JCL001.jcl -ScanOnly -DBDriver=SQLite -DBConnectString=repository.db3
"%rcbatchdir%"submit -File=SOURCE\JCL\JCL002.jcl -ScanOnly -DBDriver=SQLite -DBConnectString=repository.db3
"%rcbatchdir%"submit -File=SOURCE\JCL\JCL003.jcl -ScanOnly -DBDriver=SQLite -DBConnectString=repository.db3
"%rcbatchdir%"submit -File=SOURCE\JCL\JCL004.jcl -ScanOnly -DBDriver=SQLite -DBConnectString=repository.db3
"%rcbatchdir%"submit -File=SOURCE\JCL\JCL005.jcl -ScanOnly -DBDriver=SQLite -DBConnectString=repository.db3

