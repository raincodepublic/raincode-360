//SETUP002 JOB
//*******************************************************************
//*                                                                 *
//*  Create pdslib MYLIB.PROC.JCL and add MYPROC as a member        *
//*                                                                 *
//*******************************************************************
//*-----------------------------------------------------------------*
//*  STEP001: Cleanup files from previous run                       *
//*-----------------------------------------------------------------*
//STEP001 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSOUT DD SYSOUT=*
//SYSIN DD *
 DELETE MYLIB.PROC.JCL
      SET LASTCC=0
      SET MAXCC=0
/*
//*-----------------------------------------------------------------*
//*  STEP002: Create PDSLIB                                         *
//*-----------------------------------------------------------------*
//STEP002 EXEC PGM=IEFBR14
//DD1     DD DSN=MYLIB.PROC.JCL,DSNTYPE=PDS,
//        DISP=(NEW,CATLG,DELETE),RECFM=FB
//*-----------------------------------------------------------------*
//*  STEP003: Add member from file                                  *
//*-----------------------------------------------------------------*
//STEP003 EXEC PGM=IEFBR14
//DD1     DD DSN=MYLIB.PROC.JCL(MYPROC),DISP=SHR,
//        PATH=('.\SOURCE\PROC\MYPROC.jcl',COPY)
//