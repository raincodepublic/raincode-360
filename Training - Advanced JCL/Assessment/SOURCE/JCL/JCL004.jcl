//JCL004 JOB ACCT,'NAME',COND=(4,LT)
//*******************************************************************
//*                                                                 *
//*  Utility Example (GREP)                                         *
//*                                                                 *
//*******************************************************************
//*-----------------------------------------------------------------*
//*  Clean up files from previous run                               *
//*-----------------------------------------------------------------*
//STEP001 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSOUT DD SYSOUT=*
//SYSIN DD *
 DELETE JCL004.*
      SET LASTCC=0
      SET MAXCC=0
/*
//*-----------------------------------------------------------------*
//*  Produce GREPIN                                                 *
//*-----------------------------------------------------------------*
//STEP002  EXEC PGM=SORT                                  
//SYSOUT   DD SYSOUT=*
//SORTIN   DD *
AACHEN
AMSTERDAM
ANTWERP
BRUSSELS
CHARLEROI
/*
//SORTOUT  DD DSN=JCL004.GREPIN,
//        DISP=(NEW,CATLG,DELETE),RECFM=LSEQ,LRECL=16
//SYSIN    DD *
  SORT FIELDS=COPY
/*
//*-----------------------------------------------------------------*
//*  Run GREP                                                       *
//*-----------------------------------------------------------------*
//STEP003  EXEC PGM=GREP                                  
//SYSOUT   DD DSN=JCL004.SYSOUT,
//         DISP=(NEW,CATLG,DELETE),RECFM=LSEQ
//GREPIN   DD DSN=JCL004.GREPIN,DISP=SHR
//GREPOUT  DD DSN=JCL004.GREPOUT,
//         DISP=(NEW,CATLG,DELETE),RECFM=LSEQ,LRECL=16
//SYSIN    DD *
ER
/*
//
