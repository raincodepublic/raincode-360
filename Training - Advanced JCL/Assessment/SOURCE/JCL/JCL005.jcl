//*******************************************************************
//*                                                                 *
//*  DSNTIAUL Example                                               *
//*                                                                 *
//*******************************************************************
//JCL005   JOB  JOB ACCT,'NAME',COND=(4,LT)
//*-----------------------------------------------------------------*
//*  PERFORM INLINE QUERY                                           *
//*-----------------------------------------------------------------*
//STEP002   EXEC PGM=IKJEFT01
//SYSTSPRT  DD SYSOUT=*
//SYSTSIN   DD *  
 RUN PROGRAM(DSNTIAUL) PLAN(DSNTIAUL_PLAN) PARM('SQL') 
 END
//SYSPRINT  DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//SYSREC00  DD DSN=INLINE.QUERY.SEQ,
//          DISP=(NEW,CATLG,DELETE),RECFM=FB,VOL=SER=DEFAULT,LRECL=57
//SYSPUNCH  DD SYSOUT=*                                           
//SYSIN     DD *
 SELECT ZIPCODE, 
        CAST(CITY AS CHAR(50)), 
        COUNTRY 
   FROM DBO.ZIPCODE;
/*