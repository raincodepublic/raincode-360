//SETUP001 JOB
//*******************************************************************
//*                                                                 *
//*  Create pdslib STEP.LIB and add COB002.dll as a member          *
//*                                                                 *
//*******************************************************************
//*-----------------------------------------------------------------*
//*  STEP001: Cleanup files from previous run                       *
//*-----------------------------------------------------------------*
//STEP001 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSOUT DD SYSOUT=*
//SYSIN DD *
 DELETE STEP.*
      SET LASTCC=0
      SET MAXCC=0
/*
//*-----------------------------------------------------------------*
//*  STEP002: Create PDSLIB                                         *
//*-----------------------------------------------------------------*
//STEP002 EXEC PGM=IEFBR14
//DD1     DD DSN=STEP.LIB,DSNTYPE=PDS,
//        DISP=(NEW,CATLG,DELETE),RECFM=FB
//*-----------------------------------------------------------------*
//*  STEP003: Add member from file                                  *
//*-----------------------------------------------------------------*
//STEP003 EXEC PGM=IEFBR14
//DD1     DD DSN=STEP.LIB(COB002),DISP=SHR,
//        PATH=('.\BIN\COB002.dll',COPY)
//