//JCL002 JOB
//*******************************************************************
//*                                                                 *
//*  PROCEDURE example                                              *
//*  """""""""""""""""                                              *
//*  The default proclib is SYS1.PROCLIB                            *
//*  The JCLLIB statement makes that MYLIB.PROC.JCL is searched     *
//*  before SYS1.PROCLIB                                            *
//*                                                                 *
//*******************************************************************
//LIB001   JCLLIB ORDER=MYLIB.PROC.JCL
//*-----------------------------------------------------------------*
//*  Clean up files                                                 *
//*-----------------------------------------------------------------*
//STEP001 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSOUT DD SYSOUT=*
//SYSIN DD *
 DELETE JCL002.*
      SET LASTCC=0
      SET MAXCC=0
/*
//*-----------------------------------------------------------------*
//*  Call procedure MYPROC in MYLIB.PROC.JCL                        *
//*-----------------------------------------------------------------*
//STEP002  EXEC MYPROC,PR=COB002,HLQ=JCL002
//*FILEA    DD DSN=JCL002.FILEA,
//*         DISP=(NEW,CATLG,DELETE),RECFM=FB,LRECL=80
//*FILEC    DD DSN=JCL002.FILEC,
//*         DISP=(NEW,CATLG,DELETE),RECFM=LSEQ

