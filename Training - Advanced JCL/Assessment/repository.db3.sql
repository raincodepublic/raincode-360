BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "COB_CALL" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"COB_FILE"	VARCHAR(255),
	"COB_START_LINE"	INTEGER,
	"COB_LOCAL"	INTEGER,
	"COB_ARITY"	INTEGER,
	"COB_TO"	TEXT,
	"COB_VERB"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "COB_PARAM" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"COB_NAME"	TEXT,
	"COB_MODE"	TEXT,
	"COB_SIZE"	INTEGER,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "COB_CALL_PARAM" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"COB_CALL"	INTEGER,
	"COB_INDEX"	INTEGER,
	"COB_SIZE"	INTEGER,
	"COB_IMAGE"	TEXT,
	"COB_VALUE"	TEXT,
	"COB_MODE"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "COB_PERFORM" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"COB_FILE"	VARCHAR(255),
	"COB_START_LINE"	INTEGER,
	"COB_BEGIN_PARAGRAPH"	TEXT,
	"COB_END_PARAGRAPH"	TEXT,
	"COB_VERB"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "COB_ENTRY" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"COB_FILE"	VARCHAR(255),
	"COB_START_LINE"	INTEGER,
	"COB_NAME"	TEXT,
	"COB_FATHER"	INTEGER,
	"COB_ARITY"	INTEGER,
	"COB_ENTRY"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_PROGRAM" (
	"RC_ID"	INTEGER,
	"RC_NAME"	VARCHAR(255),
	"RC_FILENAME"	VARCHAR(255),
	"RC_CVERSION"	TEXT,
	"RC_DEBUG"	TEXT,
	"RC_DATE"	VARCHAR(255),
	"RC_COMP_FLAGS"	TEXT,
	"RC_OUTPUT_FILENAME"	VARCHAR(255),
	"RC_RET_CODE"	INTEGER,
	"RC_INPUT_LINES"	INTEGER,
	"RC_EXPANDED_LINES"	INTEGER,
	"RC_STATIC_SIZE"	INTEGER,
	"RC_OBJECT_SIZE"	INTEGER,
	"RC_TOTAL_TIME"	INTEGER,
	"RC_PREPRO_TIME"	INTEGER,
	"RC_PARSING_TIME"	INTEGER,
	"RC_TAGGING_TIME"	INTEGER,
	"RC_CG_TIME"	INTEGER,
	"RC_ASSEMBLY_TIME"	INTEGER,
	"RC_VERIFICATION_TIME"	INTEGER,
	"RC_USER"	VARCHAR(255),
	"RC_LANG"	VARCHAR(255),
	PRIMARY KEY("RC_ID" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "RC_ERROR_CONTEXT" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_ERROR_SEQ_ID"	INTEGER,
	"RC_LINE"	INTEGER,
	"RC_MODULE"	TEXT,
	"RC_TEXT"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_ERROR" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_LINE"	INTEGER,
	"RC_COLUMN"	INTEGER,
	"RC_MODULE"	TEXT,
	"RC_MESSAGE"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_CICS" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_LINE"	INTEGER,
	"RC_COMMAND"	TEXT,
	"RC_USED"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_DLI" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_LINE"	INTEGER,
	"RC_COMMAND"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_SQL" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_START_LINE"	INTEGER,
	"RC_END_LINE"	INTEGER,
	"RC_START_COL"	INTEGER,
	"RC_END_COL"	INTEGER,
	"RC_COMMAND"	TEXT,
	"RC_USED"	TEXT,
	"RC_SINGLETON"	TEXT,
	"RC_GHOST_UPDATE"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_SYMBOLS" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_FILE"	VARCHAR(255),
	"RC_LINE"	INTEGER,
	"RC_COL"	INTEGER,
	"RC_END_COL"	INTEGER,
	"RC_NAME"	VARCHAR(255),
	"RC_KIND"	VARCHAR(255),
	"RC_DESC"	TEXT,
	"RC_PARENT"	INTEGER,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_TAGS" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_FILE"	VARCHAR(255),
	"RC_LINE"	INTEGER,
	"RC_COL"	INTEGER,
	"RC_END_COL"	INTEGER,
	"RC_REF"	INTEGER,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_SQL_VERSION" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_STATEMENT"	INTEGER,
	"RC_DBNAME"	VARCHAR(255),
	"RC_VERSION"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_SQL_TABLE" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_STATEMENT"	INTEGER,
	"RC_OPERATION"	TEXT,
	"RC_NAME"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_FILE" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_NAME"	TEXT,
	"RC_ENV"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_INCLUDE" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_NAME"	TEXT,
	"RC_PATH"	VARCHAR(255),
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_IDMS_STMT" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_START_LINE"	INTEGER,
	"RC_END_LINE"	INTEGER,
	"RC_START_COL"	INTEGER,
	"RC_END_COL"	INTEGER,
	"RC_COMMAND"	TEXT,
	"RC_BODY"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_IDMS_CTRL" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_SCHEMA"	TEXT,
	"RC_SUBSCHEMA"	TEXT,
	"RC_RECORDS"	TEXT,
	"RC_MODE"	TEXT,
	"RC_DEBUG"	TEXT,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_JCL_JOB" (
	"RC_ID"	INTEGER,
	"RC_JOB_NAME"	VARCHAR(255),
	"RC_JOB_PATH"	VARCHAR(255),
	"RC_JOB_COND"	VARCHAR(255),
	"RC_JOB_TIME"	VARCHAR(255),
	PRIMARY KEY("RC_ID" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "RC_JCL_PGM_STEP" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_STEP_NAME"	VARCHAR(255),
	"RC_PROG_NAME"	VARCHAR(255),
	"RC_PROG_PARAMS"	VARCHAR(255),
	"RC_PROG_COND"	VARCHAR(255),
	"RC_PROG_RUNNER"	VARCHAR(255),
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_JCL_PROC_STEP" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_STEP_NAME"	VARCHAR(255),
	"RC_PROC_NAME"	VARCHAR(255),
	"RC_PROC_PARAMS"	VARCHAR(255),
	"RC_PROC_COND"	VARCHAR(255),
	"RC_PROC_INPUT_CARDS"	VARCHAR(255),
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_JCL_DD" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_DD_NAME"	VARCHAR(255),
	"RC_DSN_NAME"	VARCHAR(255),
	"RC_IS_DUMMY"	BOOLEAN,
	"RC_INSTREAM_DATA"	VARCHAR(255),
	"RC_PROG_STEP_REF"	INTEGER,
	"RC_PROC_STEP_REF"	INTEGER,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_JCL_ERR" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_ERR_TYPE"	VARCHAR(255),
	"RC_ERR_MSG"	VARCHAR(255),
	"RC_ERR_LINE"	INTEGER,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_JCL_VAR" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_VAR_NAME"	VARCHAR(255),
	"RC_VAR_FOUND"	BOOLEAN,
	"RC_VAR_VALUE"	VARCHAR(255),
	"RC_VAR_CONTROLM"	BOOLEAN,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
CREATE TABLE IF NOT EXISTS "RC_UTILITIES_PARSING_STATUS" (
	"RC_ID"	INTEGER NOT NULL,
	"RC_SEQ"	INTEGER NOT NULL,
	"RC_STEP_NAME"	VARCHAR(255),
	"RC_UTILITY_NAME"	VARCHAR(255),
	"RC_COMMAND_TEXT"	VARCHAR(255),
	"RC_STATUS_TEXT"	VARCHAR(255),
	"RC_COLUMN_NUMBER"	INTEGER,
	"RC_COMMAND_PARSE_STATUS"	BOOLEAN,
	PRIMARY KEY("RC_ID","RC_SEQ")
);
INSERT INTO "COB_CALL" VALUES (3,2,'C:\Users\Martin\Desktop\20220331 Advanced JCL\Assessment\SOURCE\COBOL\COB003.cob',12,0,1,'ASM001A','CALL');
INSERT INTO "COB_CALL_PARAM" VALUES (3,3,2,1,24,'PARMS',NULL,' ');
INSERT INTO "COB_PERFORM" VALUES (1,2,'C:\Users\Martin\Desktop\20220331 Advanced JCL\Assessment\SOURCE\COBOL\COB001.cob',41,'FETCH-CUR01',NULL,'PERFORM');
INSERT INTO "COB_PERFORM" VALUES (1,3,'C:\Users\Martin\Desktop\20220331 Advanced JCL\Assessment\SOURCE\COBOL\COB001.cob',46,'FETCH-CUR01',NULL,'PERFORM');
INSERT INTO "COB_ENTRY" VALUES (1,1,'C:\Users\Martin\Desktop\20220331 Advanced JCL\Assessment\SOURCE\COBOL\COB001.cob',37,'COB001',0,0,'N');
INSERT INTO "COB_ENTRY" VALUES (2,1,'C:\Users\Martin\Desktop\20220331 Advanced JCL\Assessment\SOURCE\COBOL\COB002.cob',7,'COB002',0,0,'N');
INSERT INTO "COB_ENTRY" VALUES (3,1,'C:\Users\Martin\Desktop\20220331 Advanced JCL\Assessment\SOURCE\COBOL\COB003.cob',8,'COB003',0,0,'N');
INSERT INTO "RC_PROGRAM" VALUES (1,'COB001','SOURCE\COBOL\COB001.COB','4.2.80.0, Build nb_v4.2.79.0-2-gfe1d1139bcd-dirty','N','2022:09:05 07:42:29',':MaxMem=4000 :IncludeSearchPath=.\SOURCE\CPY :IncludeSearchPath=%RCDIR%includes\cobol :DBDriver=SQLITE :DBConnectString=repository.db3 :SQL=DB2IBM :SQLSourceDialect=DB2 :Margins=7,72 :OutputDir=.\BIN :GenerateStaticSQL=False','.\BIN\COB001.dll',0,69,118,616,15872,1700,5,516,2,711,90,360,'Martin','COBOL');
INSERT INTO "RC_PROGRAM" VALUES (2,'COB002','SOURCE\COBOL\COB002.COB','4.2.80.0, Build nb_v4.2.79.0-2-gfe1d1139bcd-dirty','N','2022:09:05 07:42:29',':MaxMem=4000 :IncludeSearchPath=.\SOURCE\CPY :IncludeSearchPath=%RCDIR%includes\cobol :DBDriver=SQLITE :DBConnectString=repository.db3 :SQL=DB2IBM :SQLSourceDialect=DB2 :Margins=7,72 :OutputDir=.\BIN :GenerateStaticSQL=False','.\BIN\COB002.dll',0,9,9,72,8704,360,2,1,0,207,74,66,'Martin','COBOL');
INSERT INTO "RC_PROGRAM" VALUES (3,'COB003','SOURCE\COBOL\COB003.COB','4.2.80.0, Build nb_v4.2.79.0-2-gfe1d1139bcd-dirty','N','2022:09:05 07:42:30',':MaxMem=4000 :IncludeSearchPath=.\SOURCE\CPY :IncludeSearchPath=%RCDIR%includes\cobol :DBDriver=SQLITE :DBConnectString=repository.db3 :SQL=DB2IBM :SQLSourceDialect=DB2 :Margins=7,72 :OutputDir=.\BIN :GenerateStaticSQL=False','.\BIN\COB003.dll',0,16,16,104,10752,349,1,1,0,194,70,68,'Martin','COBOL');
INSERT INTO "RC_SQL" VALUES (1,4,21,32,15,37,'DECLARE','Y','N',NULL);
INSERT INTO "RC_SQL" VALUES (1,9,39,39,15,24,'OPEN','Y','N',NULL);
INSERT INTO "RC_SQL" VALUES (1,10,50,50,15,25,'CLOSE','Y','N',NULL);
INSERT INTO "RC_SQL" VALUES (1,11,53,54,11,39,'SELECT','Y','N',NULL);
INSERT INTO "RC_SQL" VALUES (1,15,61,67,14,17,'FETCH','Y','N',NULL);
INSERT INTO "RC_SQL_VERSION" VALUES (1,5,4,'Original','DECLARE CUR01 CURSOR FOR SELECT C.CUSTOMERID, C.FIRSTNAME, C.LASTNAME, C.ZIPCODE, Z.CITY, 1 FROM DBO.CUSTOMER C, DBO.ZIPCODE Z WHERE Z.ZIPCODE = C.ZIPCODE');
INSERT INTO "RC_SQL_VERSION" VALUES (1,6,4,'DB2IBM','DECLARE CUR01 CURSOR FOR SELECT C.CUSTOMERID, C.FIRSTNAME, C.LASTNAME, C.ZIPCODE, Z.CITY, 1 FROM DBO.CUSTOMER C, DBO.ZIPCODE Z WHERE(Z.ZIPCODE=C.ZIPCODE)');
INSERT INTO "RC_SQL_VERSION" VALUES (1,12,11,'Original','SELECT ''*** COB003A FINISHED ****'' FROM SYSIBM.SYSDUMMY1 INTO :A');
INSERT INTO "RC_SQL_VERSION" VALUES (1,13,11,'DB2IBM','DECLARE "@OUTCUR" CURSOR WITH RETURN FOR SELECT ''*** COB003A FINISHED ****'' FROM SYSIBM.SYSDUMMY1');
INSERT INTO "RC_SQL_TABLE" VALUES (1,7,4,'READ','DBO.CUSTOMER');
INSERT INTO "RC_SQL_TABLE" VALUES (1,8,4,'READ','DBO.ZIPCODE');
INSERT INTO "RC_SQL_TABLE" VALUES (1,14,11,'READ','SYSIBM.SYSDUMMY1');
INSERT INTO "RC_FILE" VALUES (1,1,'DD1',NULL);
INSERT INTO "RC_INCLUDE" VALUES (1,2,'SQLCA','.\SOURCE\CPY\SQLCA.cpy');
INSERT INTO "RC_INCLUDE" VALUES (1,3,'COB001A','.\SOURCE\CPY\COB001A.cpy');
INSERT INTO "RC_JCL_JOB" VALUES (1,'JCL001','SOURCE\JCL\JCL001.jcl','','09/05/22 7:42:31');
INSERT INTO "RC_JCL_JOB" VALUES (2,'JCL002','SOURCE\JCL\JCL002.jcl','','09/05/22 7:42:33');
INSERT INTO "RC_JCL_JOB" VALUES (3,'JCL003','SOURCE\JCL\JCL003.jcl','(4,LT)','09/05/22 7:42:35');
INSERT INTO "RC_JCL_JOB" VALUES (4,'JCL004','SOURCE\JCL\JCL004.jcl','(4,LT)','09/05/22 7:42:37');
INSERT INTO "RC_JCL_JOB" VALUES (5,'JCL005','SOURCE\JCL\JCL005.jcl','','09/05/22 7:42:40');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (1,1,'STEP001','COB002','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (2,1,'STEP001','IDCAMS','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (3,1,'STEP001','IDCAMS','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (3,2,'STEP002','IKJEFT01','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (3,3,'STEP002','COB001',NULL,'','IKJEFT');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (3,6,'STEP003','SORT','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (3,9,'STEP004','SORT','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (4,1,'STEP001','IDCAMS','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (4,2,'STEP002','SORT','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (4,4,'STEP003','GREP','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (5,1,'STEP002','IKJEFT01','','','');
INSERT INTO "RC_JCL_PGM_STEP" VALUES (5,2,'STEP002','DSNTIAUL','SQL','','IKJEFT');
INSERT INTO "RC_JCL_PROC_STEP" VALUES (2,1,'STEP002','MYPROC','PR=COB002|HLQ=JCL002|','','');
INSERT INTO "RC_JCL_DD" VALUES (1,1,'STEPLIB','STEP.LIB',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (2,1,'SYSPRINT','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (2,2,'SYSOUT','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (2,3,'SYSIN','',0,' DELETE JCL002.*                                                                                                                     
      SET LASTCC=0                                                                                                                   
      SET MAXCC=0                                                                                                                    ',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (2,4,'FILEA','JCL002.FILEA',0,'',-1,1);
INSERT INTO "RC_JCL_DD" VALUES (2,5,'FILEC','JCL002.FILEC',0,'',-1,1);
INSERT INTO "RC_JCL_DD" VALUES (3,1,'SYSPRINT','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,2,'SYSOUT','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,3,'SYSIN','',0,' DELETE JCL003A.FILE*                                                                                                                
      SET LASTCC=0                                                                                                                   
      SET MAXCC=0                                                                                                                    ',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,4,'SYSPRINT','',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,5,'SYSABOUT','',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,6,'SYSDBOUT','',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,7,'SYSUDUMP','',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,8,'DISPLAY','',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,9,'SYSOUT','',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,10,'SYSTSPRT','',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,11,'DD1','JCL003A.FILE01',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,12,'SYSTSIN','',0,'    RUN PROGRAM(COB001) PLAN(COB001)                                                                                                 
    END                                                                                                                              ',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,13,'SYSOUT','',0,'',6,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,14,'SORTIN','JCL003A.FILE01',0,'',6,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,15,'SORTOUT','JCL003A.FILE02',0,'',6,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,16,'SYSIN','',0,'  SORT FIELDS=(85,4,CH,A)                                                                                                            
  SUM FIELDS=(129,9,ZD)                                                                                                              
  OUTREC FIELDS=(85,4,C''   '',89,15,C''   '',129,9)                                                                                     ',6,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,17,'SYSOUT','',0,'',9,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,18,'SORTIN','JCL003A.FILE01',0,'',9,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,19,'SORTOUT','JCL003A.FILE03',0,'',9,-1);
INSERT INTO "RC_JCL_DD" VALUES (3,20,'SYSIN','',0,'  SORT FIELDS=(85,4,CH,A)                                                                                                            
  SUM FIELDS=(129,9,ZD)                                                                                                              
  OUTREC FIELDS=(85,4,C''   '',89,15,C''   '',129,9)                                                                                     ',9,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,1,'SYSPRINT','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,2,'SYSOUT','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,3,'SYSIN','',0,' DELETE JCL004.*                                                                                                                     
      SET LASTCC=0                                                                                                                   
      SET MAXCC=0                                                                                                                    ',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,4,'SYSOUT','',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,5,'SORTIN','',0,'AACHEN                                                                                                                               
AMSTERDAM                                                                                                                            
ANTWERP                                                                                                                              
BRUSSELS                                                                                                                             
CHARLEROI                                                                                                                            ',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,6,'SORTOUT','JCL004.GREPIN',0,'',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,7,'SYSIN','',0,'  SORT FIELDS=COPY                                                                                                                   ',2,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,8,'SYSOUT','JCL004.SYSOUT',0,'',4,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,9,'GREPIN','JCL004.GREPIN',0,'',4,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,10,'GREPOUT','JCL004.GREPOUT',0,'',4,-1);
INSERT INTO "RC_JCL_DD" VALUES (4,11,'SYSIN','',0,'ER                                                                                                                                   ',4,-1);
INSERT INTO "RC_JCL_DD" VALUES (5,1,'SYSTSPRT','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (5,2,'SYSTSIN','',0,' RUN PROGRAM(DSNTIAUL) PLAN(DSNTIAUL_PLAN) PARM(''SQL'')                                                                               
 END                                                                                                                                 ',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (5,3,'SYSPRINT','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (5,4,'SYSUDUMP','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (5,5,'SYSREC00','INLINE.QUERY.SEQ',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (5,6,'SYSPUNCH','',0,'',1,-1);
INSERT INTO "RC_JCL_DD" VALUES (5,7,'SYSIN','',0,' SELECT ZIPCODE,                                                                                                                     
        CAST(CITY AS CHAR(50)),                                                                                                      
        COUNTRY                                                                                                                      
   FROM DBO.ZIPCODE;                                                                                                                 ',1,-1);
INSERT INTO "RC_JCL_ERR" VALUES (2,1,'PARSE_ERR','Unable to find referenced PROC MYPROC in the catalog',26);
INSERT INTO "RC_UTILITIES_PARSING_STATUS" VALUES (3,4,'STEP002','IKJEFT','    RUN PROGRAM(COB001) PLAN(COB001)','',0,1);
INSERT INTO "RC_UTILITIES_PARSING_STATUS" VALUES (3,5,'STEP002','IKJEFT','    END','',0,1);
INSERT INTO "RC_UTILITIES_PARSING_STATUS" VALUES (3,7,'STEP003','SORT','  SORT FIELDS=(85,4,CH,A)                                                                                                            
  SUM FIELDS=(129,9,ZD)                                                                                                              
  OUTREC FIELDS=(85,4,C''   '',89,15,C''   '',129,9)                                                                                     ','',0,1);
INSERT INTO "RC_UTILITIES_PARSING_STATUS" VALUES (3,8,'STEP003','SORT','  SORT FIELDS=(85,4,CH,A)                                                                                                            
  SUM FIELDS=(129,9,ZD)                                                                                                              
  OUTREC FIELDS=(85,4,C''   '',89,15,C''   '',129,9)                                                                                     ','',0,1);
INSERT INTO "RC_UTILITIES_PARSING_STATUS" VALUES (3,10,'STEP004','SORT','  SORT FIELDS=(85,4,CH,A)                                                                                                            
  SUM FIELDS=(129,9,ZD)                                                                                                              
  OUTREC FIELDS=(85,4,C''   '',89,15,C''   '',129,9)                                                                                     ','',0,1);
INSERT INTO "RC_UTILITIES_PARSING_STATUS" VALUES (3,11,'STEP004','SORT','  SORT FIELDS=(85,4,CH,A)                                                                                                            
  SUM FIELDS=(129,9,ZD)                                                                                                              
  OUTREC FIELDS=(85,4,C''   '',89,15,C''   '',129,9)                                                                                     ','',0,1);
INSERT INTO "RC_UTILITIES_PARSING_STATUS" VALUES (4,3,'STEP002','SORT','  SORT FIELDS=COPY                                                                                                                   ','',0,1);
INSERT INTO "RC_UTILITIES_PARSING_STATUS" VALUES (5,3,'STEP002','IKJEFT',' RUN PROGRAM(DSNTIAUL) PLAN(DSNTIAUL_PLAN) PARM(''SQL'')','',0,1);
INSERT INTO "RC_UTILITIES_PARSING_STATUS" VALUES (5,4,'STEP002','IKJEFT',' END','',0,1);
CREATE VIEW jcl_common_error as select rc_job_path, RC_ERR_MSG, count(*) as cnt from rc_jcl_err e left join rc_jcl_job j on j.rc_id = e.rc_id group by RC_ERR_MSG order by cnt desc;
CREATE VIEW jcl_program_called as select RC_PROG_NAME, count(*) as cnt from RC_JCL_PGM_STEP group by RC_PROG_NAME order by cnt desc;
COMMIT;
