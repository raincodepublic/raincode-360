SELECT P1.RC_NAME, 
       C.COB_START_LINE,
       C.COB_VERB,
       C.COB_TO
  FROM COB_CALL C,
       RC_PROGRAM P1
 WHERE C.RC_ID = P1.RC_ID AND
       NOT EXISTS (
           SELECT 1 
		     FROM RC_PROGRAM P2 
			WHERE P2.RC_NAME = C.COB_TO
	   )
