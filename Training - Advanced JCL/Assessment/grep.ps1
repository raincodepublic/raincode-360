#############################################################################
##                                                                         ##
##   GREP utility (Powershell version)                                     ##
##                                                                         ##
#############################################################################

# Write list of environment variables to SYSOUT

Get-ChildItem Env:* -Include *GREPIN | Sort-Object Name
Write-Host 
Get-ChildItem Env:* -Include *GREPOUT | Sort-Object Name
Write-Host 
Get-ChildItem Env:* -Include *SYSIN | Sort-Object Name
Write-Host 
Get-ChildItem Env:* -Include RC* | Sort-Object Name

# Search GREPIN for matches with SYSIN and wite result to GREPOUT

Get-Content $Env:dd_sysin | 
    Foreach-Object { Select-String -Path $Env:dd_grepin $_.trim() } | 
    ForEach-Object {$_.Line} > $env:dd_grepout

