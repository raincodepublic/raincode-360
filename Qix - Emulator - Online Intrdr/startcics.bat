@echo off
set vsbin="P005\bin\Debug\net6.0"

taskkill /F /IM RegionManagerRunner.exe 2> nul
taskkill /F /IM TerminalServerRunner.exe 2> nul
taskkill /F /IM ProcessingServerRunner.exe 2> nul

SET QIXCONN="%RC360_CONNSTRING%initial catalog=RaincodeQIXMS;MultipleActiveResultSets=True;"
SET APPCONN="%RC360_CONNSTRING%initial catalog=BankDemo;"
"RegionCreator.exe" -qixconnectionstring=%QIXCONN% -region=BANKDEMO -mapspaths=%vsbin% -programspaths=%vsbin% -qixtransactions=C004:cob004;C005:cob005;C006:cob006;C007:COB007;C008:COB008;C010:COB010;CSPO:COBSPO -ApplicationConnectionString=%APPCONN% -Append=FALSE

echo Starting Terminal Server ...
start "TS" TerminalServerRunner.exe -LogLevel=TRACE -Region=BANKDEMO -TcpPort=23 -TCTUASIZE=512 -ConfigConnectionString=%QIXCONN%
timeout /T 10 > nul

echo Starting Processing Server ...
start "PS" ProcessingServerRunner.exe -Region=BANKDEMO -MaxThreads=200 -TraceAll=true -SafeMemoryMode=true -LogLevel=TRACE -ConfigConnectionString=%QIXCONN%
timeout /T 10 > nul

echo Starting Terminal ...
start "" "C:\Program Files\wc3270\wc3270.exe" +S -model 2 -title "Raincode Bank" 127.0.0.1
