$folderpath="C:\Raincode360\Repositories\Demos\Qix - Emulator - Online Intrdr\Watch\*.jcl"
$workpath="C:\Raincode360\Repositories\Demos\Qix - Factories - Intrdr with Watchdog\main\bin\Debug\net8.0"
$loopc=0;
$fname = ""

while ($loopc -le 60)
{
    $loopc= $loopc+1
    $Items = (Get-ChildItem $folderpath)

    if( $Items.Count -ge 1){
        Write-Host Found Item $Items[0].Name

        $fname = $Items[0].FullName 

        Push-Location
        cd $workpath
        Start-process -FilePath "submit.exe" -ArgumentList "-File=`"$fname`"" -Wait
        pop-Location
        Start-Sleep -Seconds 1
        remove-item $Items[0]
        $fname= ""
    }
    else{
        Write-Host "Waiting"
        Start-Sleep -Seconds 5
    }
}