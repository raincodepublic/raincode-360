      ***********************************************************
      *                                                         *
      *   YEAR JCL REPORT                                       *
      *                                                         *
      ***********************************************************
       IDENTIFICATION DIVISION.                                         
       PROGRAM-ID. COBSPO.        
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
       DECIMAL-POINT IS COMMA.                                       
       DATA DIVISION.                                                   
       WORKING-STORAGE SECTION.          
       COPY COBSPO.
       COPY DFHAID.
       COPY DFHBMSCA.
       COPY COMMAREA.
       01 RESP PIC 9(8) BINARY.
       01 RESP2 PIC 9(8) BINARY.
       01 TOKEN PIC X(8).
       01 OUTLEN PIC S9(8) BINARY VALUE +80.
       77 OUTPRT PIC X(80).
       01 YEARFROM PIC X(80).
       01 YEARTO PIC X(80).
       01 W-MSG PIC X(80).
       01 PARMSPTR POINTER.
       01 PARMS-AREA.
       03 PARMSLEN PIC S9(8) BINARY VALUE 14.
       03 PARMSINF PIC X(14) VALUE 'WRITER(MYPROG)'.
       03 PARMADDR POINTER.
       LINKAGE SECTION.                                                 
       COPY DFHCOMM.                                 
       PROCEDURE DIVISION.
       EXEC CICS IGNORE CONDITION MAPFAIL END-EXEC
       IF EIBCALEN = 0
           EXEC CICS 
               GETMAIN SET(ADDRESS OF DFHCOMMAREA)
               FLENGTH(LENGTH OF DFHCOMMAREA) 
           END-EXEC
       ELSE
           IF EIBCALEN IS NOT EQUAL LENGTH OF DFHCOMMAREA
               EXEC CICS ABEND ABCODE('CSPO') END-EXEC
           END-IF
       END-IF

       MOVE DFHCOMMAREA TO W-COMMAREA
       EVALUATE TRUE
       WHEN EIBCALEN = 0
           PERFORM PUT-MAP
       WHEN EIBAID = DFHENTER
           PERFORM GET-MAP
           PERFORM PUT-JCL
           MOVE 'Started JCL REPORT' TO W-MSG
           PERFORM PUT-MAP
       WHEN EIBAID = DFHPF5
           PERFORM GET-MAP
           PERFORM PUT-JCL
           PERFORM PUT-MAP
       WHEN EIBAID = DFHCLEAR
           EXEC CICS SEND CONTROL ERASE FREEKB
           END-EXEC
           EXEC CICS RETURN
           END-EXEC
       WHEN OTHER
           MOVE 'Invalid action' TO W-MSG
           PERFORM PUT-MAP
       END-EVALUATE
       EXEC CICS RETURN 
           TRANSID ('CSPO') 
           COMMAREA (DFHCOMMAREA)
           LENGTH(LENGTH OF DFHCOMMAREA)
       END-EXEC
       GOBACK                                       
       .    
       PUT-MAP.
       MOVE LOW-VALUE TO MAP001O
              
       MOVE W-MSG TO MSGO
       EXEC CICS SEND 
           MAP('MAP001')
           MAPSET('COBSPO') 
           ERASE 
           CURSOR
       END-EXEC
       .
       GET-MAP.
       EXEC CICS RECEIVE
           MAP('MAP001')
           MAPSET('COBSPO')
       END-EXEC
       IF YEARFL > 0 
           MOVE YEARFI TO YEARFROM
       END-IF
       IF YEARTL > 0 
           MOVE YEARTI TO YEARTO
       END-IF
       .
       PUT-JCL.
       SET PARMSPTR TO ADDRESS OF PARMS-AREA
       SET PARMADDR TO PARMSPTR
       SET PARMSPTR TO ADDRESS OF PARMADDR
       EXEC CICS SPOOLOPEN OUTPUT
           NODE ('*')
           USERID ('*')
           RESP(RESP) RESP2(RESP2)
           OUTDESCR(PARMSPTR)
           TOKEN(TOKEN)
       END-EXEC
       MOVE '//JCL001   JOB CLASS=A,MSGCLASS=C' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//STEP001  EXEC PGM=IDCAMS' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//SYSPRINT DD  SYSOUT=*' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//SYSIN    DD  *' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE ' DELETE UNLOAD.CUST.*' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE ' SET MAXCC = 0' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '/*' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//STEP002  EXEC PGM=DSNUTILB' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//SYSPRINT DD SYSOUT=*' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//SYSUDUMP DD SYSOUT=*' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//UTPRINT  DD SYSOUT=*' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//SYSOUT   DD SYSOUT=*' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//SYSIN    DD *' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE 
          '  UNLOAD TABLESPACE PRIMARY FROM TABLE DBO.CUSTOMER LIMIT 25'
            TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE "  WHEN (BIRTHDATE > '" TO OUTPRT
       MOVE YEARFROM TO OUTPRT(22:)
       MOVE "-01-01'" TO OUTPRT(26:)
       MOVE " AND BIRTHDATE < '" TO OUTPRT(33:)
       MOVE YEARTO TO OUTPRT(51:)
       MOVE "-01-01')" TO OUTPRT(55:)
       PERFORM PUT-JCL-LINE
       MOVE '//SYSREC DD DSN=UNLOAD.CUST.SEQ,' TO OUTPRT
       PERFORM PUT-JCL-LINE
       MOVE '//         DISP=(NEW,CATLG,DELETE),LRECL=120' TO OUTPRT
       PERFORM PUT-JCL-LINE
       EXEC CICS SPOOLCLOSE
           TOKEN(TOKEN)
           RESP(RESP) RESP2(RESP2)
       END-EXEC
       .

       PUT-JCL-LINE.
       EXEC CICS SPOOLWRITE
           FROM(OUTPRT)
           RESP(RESP) RESP2(RESP2)
           FLENGTH(OUTLEN)
           TOKEN(TOKEN)
       END-EXEC
       .