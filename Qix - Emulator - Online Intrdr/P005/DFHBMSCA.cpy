************************************************************************
******* Standard DFHBMSCA copybook, defining symbolic constants to
******* represent BMS equates in CICS.
************************************************************************
       01  DFHBMSCA.
         05  DFHBLUE  PIC X(1) VALUE X'F1'.
         05  DFHBMASB PIC X(1) VALUE X'F8'.
         05  DFHBMASF PIC X(1) VALUE X'F1'.
         05  DFHBMASK PIC X(1) VALUE X'F0'.
         05  DFHBMBRY PIC X(1) VALUE X'C8'.
         05  DFHBMCUR PIC X(1) VALUE X'02'.
         05  DFHBMDAR PIC X(1) VALUE X'4C'.
         05  DFHBMEF  PIC X(1) VALUE X'82'.
         05  DFHBMEOF PIC X(1) VALUE X'80'.
         05  DFHBMFSE PIC X(1) VALUE X'C1'.
         05  DFHBMPRF PIC X(1) VALUE X'61'.
         05  DFHBMPRO PIC X(1) VALUE X'60'.
         05  DFHBMUNN PIC X(1) VALUE X'50'.
         05  DFHBMUNP PIC X(1) VALUE X'40'.
         05  DFHDFCOL PIC X(1) VALUE X'00'.
         05  DFHGREEN PIC X(1) VALUE X'F4'.
         05  DFHNEUTR PIC X(1) VALUE X'F7'.
         05  DFHPINK  PIC X(1) VALUE X'F3'.
         05  DFHPROTI PIC X(1) VALUE X'E8'.
         05  DFHPROTN PIC X(1) VALUE X'6C'.
         05  DFHRED   PIC X(1) VALUE X'F2'.
         05  DFHTURQ  PIC X(1) VALUE X'F5'.
         05  DFHUNIMD PIC X(1) VALUE X'C9'.
         05  DFHUNINT PIC X(1) VALUE X'D9'.
         05  DFHUNNOD PIC X(1) VALUE X'4D'.
         05  DFHUNNON PIC X(1) VALUE X'5D'.
         05  DFHUNNUB PIC X(1) VALUE X'D8'.
         05  DFHUNNUM PIC X(1) VALUE X'D1'.
         05  DFHYELLO PIC X(1) VALUE X'F6'.
         05  DFHDFHI  PIC X(1) VALUE X'00'.
************************************************************************
