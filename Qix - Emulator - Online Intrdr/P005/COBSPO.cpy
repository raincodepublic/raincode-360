        01 MAP001I .
            02 FILLER PIC X(12).
            02 YEARFL PIC S9(4) COMP-5.
            02 YEARFF PIC X(1).
            02 FILLER REDEFINES YEARFF.
              04 YEARFA PIC X.
            02 YEARFI PIC X(4).
            02 YEARTL PIC S9(4) COMP-5.
            02 YEARTF PIC X(1).
            02 FILLER REDEFINES YEARTF.
              04 YEARTA PIC X.
            02 YEARTI PIC X(4).
            02 MSGL PIC S9(4) COMP-5.
            02 MSGF PIC X(1).
            02 FILLER REDEFINES MSGF.
              04 MSGA PIC X.
            02 MSGI PIC X(70).
        01 MAP001O REDEFINES MAP001I.
            02 FILLER PIC X(12).
            02 FILLER PIC X(3).
            02 YEARFO PIC X(4).
            02 FILLER PIC X(3).
            02 YEARTO PIC X(4).
            02 FILLER PIC X(3).
            02 MSGO PIC X(70).
