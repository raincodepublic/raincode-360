
PURPOSE
. Illustrate how a Batch can be started from an online CICS application using SPOOL commands

FEATURES
. Use of CICS commands such as CICS SPOOLOPEN SPOOLWRITE SPOOLCLOSE
. Use of BMS maps
. Executes a JCL from a CICS application
. Extracts the customers from the DB born between a start and an end year into the dataset UNLOAD.CUST.SEQ
. Demonstrates a watchdog can trigger a JCL execution when a file is put in a directory

RUNNING THE APPLICATION
. Via the Developer Powershell 
  . Go to Tools/Command Line/Developer Powershell
  . enter '.\run'
  . wait for the Qix windows and the 3270 emulator to open up
  . the QiX application starts automatically

OPENING ADDITIONAL TERMINAL WINDOWS
. the Qix server (PS/TS) can support multiple terminals as long as it is running
. enter '.\run3270' (Powershell) to open up another window
. entering 'CSPO' to start the app
. Enter a start year > 1940
. Enter a end year < 2000
. open a windows explorer and check results in "C:\ProgramData\Raincode\Batch\DefaultVolume\UNLOAD\CUST" 
. double click SEQ.meta, this should open the dataset in the record editor

ABOUT THE 3270 TERMINAL EMULATOR
. The product that comes with this demo (wc3270) is license-free
. Do resize the window by dragging a corner while the application is running
. Adjust the font size by pressing control and scrolling the mouse wheel. This may not work in a VM.
. Clicking on the terminal icon in the title bar opens a menu. The properties option allows you to adjust the font manually along with some other settings.
. Some handy shortcut keys
  . Alt-K : keypad
  . Alt-N : menu bar
  . Alt-J : return
. see http://x3270.bgp.nu/wc3270-man.html for more information

HOW TO DEBUG
. Place a breakpoint at line 46 of program COBSPO.cob ('MOVE') by clicking in the left margin. A red dot appears.
. Make sure that Qix is running and connected.
. Start the application in the 3270 window
. In Visual Studio, select Debug/Attach to Process on the main menu.
. A list of processes opens up. Select 'ProcessingServerRunner.exe' and click on the 'Attach' button.
. Visual Studio switches to debug mode. Because of the psudoconversational nature of the application the program COB004.cob is not running at this point.
. Press <F8> on the main screen of the bank application to scroll down. This will activate COB004.cob and the execution will stop at the brakpoint we put in earlier.
. Hover over any variable in the source text of COBSPO.cob to see it contents.
. Check the 'locals' window in Visual Studio to see a list of local variables an their corresponding values.
. Use the 'step' buttons to step through the program.
. Stepping over the 'EXEC CICS RETURN' will trigger an exception. This normal. Pressing continue will end the program without detaching the debug session. Continue debugging by pressing <F8> or <enter> in the application.
. Stop debugging by pressing the stop icon in the debug toolbar.

CLOSING QIX
. Close the Qix windows.

RESTARTING QIX
. The startcics and run scripts will automatically close any Qix that are already open

PROJECT (.CPROJ) FILE NOTES
. The target framework must be net8.0
  <TargetFrameworkVersion>net8.0</TargetFrameworkVersion>
. BMS maps are compiled via a pre-build event
  <PreBuildEvent>$(SolutionDir)buildbms</PreBuildEvent>

