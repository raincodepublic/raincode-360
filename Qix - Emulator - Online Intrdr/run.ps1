
##  1. Start Qix
##  2. redefine transactions in region BANKDEMO
##  3. open a 3270 window
##  4. launch transaction 'C004'

if (Get-Process TerminalServerRunner -ErrorAction SilentlyContinue) { Stop-Process -processname TerminalServerRunner  }
if (Get-Process ProcessingServerRunner -ErrorAction SilentlyContinue) { Stop-Process -processname ProcessingServerRunner  }

$vsbin = Join-Path -Path "P005" -ChildPath "bin/Debug/net8.0"
##### Use if service broker not activated in RaincodeQIXMS #####
# $QIXCONN = '"' + $Env:RC360_CONNSTRING + 'initial catalog=RaincodeQXIMSNew;MultipleActiveResultSets=True;"'
################################################################
$demodir = ""
$QIXCONN = '"' + $Env:RC360_CONNSTRING + 'initial catalog=RaincodeQIXMS;MultipleActiveResultSets=True;"'
$APPCONN = '"' + $Env:RC360_CONNSTRING + 'initial catalog=BankDemo;"'
$tranlist = 'C004:cob004;C005:cob005;C006:cob006;C007:COB007;C008:COB008;C010:COB010;CSPO:COBSPO'
$rcargs = "-qixconnectionstring=$QIXCONN -region=BANKDEMO -mapspaths=$vsbin -programspaths=$vsbin -qixtransactions=$tranlist -ApplicationConnectionString=$APPCONN -Append=FALSE"
Start-Process -FilePath "$Env:RCBIN\QIX.RegionCreator.exe" -ArgumentList $rcargs -NoNewWindow -Wait

"Copying plugin ..."
Copy-Item "$PSScriptRoot\ContextPlugin\bin\Debug\net8.0\ContextPlugin.dll" -Destination "$env:RCDIR\plugins\QIX Processing Server\"

"Starting Watchdog ..."
Invoke-Item (start powershell ((Split-Path $MyInvocation.InvocationName) + "\Scripts\WatchJcl.ps1"))

"Starting Terminal Server ..."
Start-Process -FilePath "$Env:RCBIN\QIX.TerminalServerRunner.exe" -ArgumentList "-LogLevel=TRACE -Region=BANKDEMO -TcpPort=23 -TCTUASIZE=512 -ConfigConnectionString=$QIXCONN" 
Start-Sleep -Seconds 10

"Starting Processing Server ..."
Start-Process -FilePath "$Env:RCBIN\QIX.ProcessingServerRunner.exe" -ArgumentList "-Region=BANKDEMO -MaxThreads=200 -TraceAll=true -SafeMemoryMode=true -LogLevel=TRACE -ConfigConnectionString=$QIXCONN" 
Start-Sleep -Seconds 10

"Starting Terminal ..."
Start-process -FilePath "C:\Program Files\wc3270\wc3270.exe" -ArgumentList "+S -model 2 -title `"Raincode Bank`" 127.0.0.1"
