#!/usr/bin/env pwsh

Param (
    [Parameter(Mandatory)]
    [string]
    $SolutionDir,

    [Parameter(Mandatory)]
    [string]
    $Configuration
)

$ErrorActionPreference = 'Stop'

$rcbms = Join-Path -Path $env:RCBIN -ChildPath "rcbms"
$ProjectDir = Join-Path -Path $SolutionDir -ChildPath "P005"
$OutputDir = Join-Path -Path $ProjectDir -ChildPath "bin/$Configuration/net8.0"

Push-Location $SolutionDir
& $rcbms -GenBasedSymbolicMap -Language=cobol -IncludeDirectory="$SolutionDir" -CopybooksOutputDirectory="$ProjectDir" -MapsOutputDirectory="$OutputDir" cobspo.bms
Pop-Location
if ($LASTEXITCODE -ne 0) {
    Write-Error "rcbms failed with code $LASTEXITCODE."
}
