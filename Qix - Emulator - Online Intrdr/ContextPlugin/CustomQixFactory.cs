﻿using RainCodeQixRuntimeInterface.QixInstances;

namespace RainCode.Samples
{
    /// <summary>
    /// Inheriting from RainCodeLegacyRuntime.QIX.Interface.Factory allows to reuse all QIX commands
    /// already implemented in the RainCode Legacy Runtime
    /// </summary>
    class CustomQixFactory : QIX.ProcessingServer.QIX.Statements.Factory
    {
        public override SpoolOpenInstance CreateSpoolOpen()
        {
            return new CustomSpoolOpen();
        }

        public override SpoolWriteInstance CreateSpoolWrite()
        {
            return new CustomSpoolWrite();
        }

        public override SpoolCloseInstance CreateSpoolClose()
        {
            return new CustomSpoolClose();
        }
    }
}
