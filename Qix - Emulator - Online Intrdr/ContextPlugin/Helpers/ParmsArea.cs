namespace Raincode.Samples
{
    using RainCodeLegacyRuntime.Core;
    using RainCodeLegacyRuntime.Descriptor;
    using RainCodeLegacyRuntime.Types;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Generated on 31/12/2024 at 16:29:30
    /// by Raincode COBOL compiler version 4.2.621.0 (Build nb_v4.2.620.0-20-gf60e621428e-dirty)
    /// with arguments -HelperClassName=Raincode.Samples.ParmsArea -HelperStructure=PARMS-AREA COBSPOOL.cob
    ///
    /// Input program path: 'C:\Raincode360\Repositories\Demos\Qix - Factories - Writer\legacy\COBSPOOL.cob'
    /// Structure was originally in files:
    ///   COBSPOOL.cob
    /// </summary>
    public partial class ParmsArea : BaseHelper, System.IDisposable
    {
        public override string Name => "PARMS-AREA";
        public const int StructureSize = 22;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public ParmsArea(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public ParmsArea(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static ParmsArea StackAllocate(ExecutionContext ctxt)
        {
            return new ParmsArea(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static ParmsArea Allocate(ExecutionContext ctxt)
        {
            ParmsArea result = new ParmsArea(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea PARMSLEN => theMemoryArea.Substr(0, 4);
            public MemoryArea PARMSINF => theMemoryArea.Substr(4, 14);
            public MemoryArea PARMADDR => theMemoryArea.Substr(18, 4);
        }

        /// <summary>
        /// name:   PARMSLEN
        /// type:   int
        /// offset: 0
        /// size:   4
        /// </summary> 
        public int PARMSLEN
        {
            get
            {
                return Convert.cnv_fixed_dec_to_int32(theContext, Raw.PARMSLEN, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
            }
            set
            {
                Convert.move_int32_to_fixed_dec(theContext, value, Raw.PARMSLEN, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
            }
        }

        private TypedLValue theCachedPARMSLEN_TypedLValue = TypedLValue.Null;
        public TypedLValue PARMSLEN_TypedLValue
        {
            get
            {
                if (theCachedPARMSLEN_TypedLValue.IsNull)
                {
                    theCachedPARMSLEN_TypedLValue = new TypedLValue(Raw.PARMSLEN, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
                }
                return theCachedPARMSLEN_TypedLValue;
            }
        }

        /// <summary>
        /// name:   PARMSINF
        /// type:   string
        /// offset: 4
        /// size:   14
        /// </summary> 
        public string PARMSINF
        {
            get
            {
                return Convert.cnv_char_string_to_string(theContext, Raw.PARMSINF);
            }
            set
            {
                Convert.move_string_to_char_string(theContext, value, Raw.PARMSINF);
            }
        }

        private TypedLValue theCachedPARMSINF_TypedLValue = TypedLValue.Null;
        public TypedLValue PARMSINF_TypedLValue
        {
            get
            {
                if (theCachedPARMSINF_TypedLValue.IsNull)
                {
                    theCachedPARMSINF_TypedLValue = new TypedLValue(Raw.PARMSINF, CharacterStringType.Get(14,0,false));
                }
                return theCachedPARMSINF_TypedLValue;
            }
        }

        /// <summary>
        /// name:   PARMADDR
        /// type:   MemoryArea
        /// offset: 18
        /// size:   4
        /// </summary> 
        public MemoryArea PARMADDR
        {
            get
            {
                return Convert.cnv_generic_pointer_to_memory_area(theContext, Raw.PARMADDR);
            }
            set
            {
                Convert.move_memory_area_to_generic_pointer(theContext, value, Raw.PARMADDR);
            }
        }

        private TypedLValue theCachedPARMADDR_TypedLValue = TypedLValue.Null;
        public TypedLValue PARMADDR_TypedLValue
        {
            get
            {
                if (theCachedPARMADDR_TypedLValue.IsNull)
                {
                    theCachedPARMADDR_TypedLValue = new TypedLValue(Raw.PARMADDR, PointerTypeDescriptor.Get());
                }
                return theCachedPARMADDR_TypedLValue;
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    StructTypeDescriptor.Get(new FieldDescriptor[] {
                            new FieldDescriptor ("PARMSLEN",0,FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP())),
                            new FieldDescriptor ("PARMSINF",4,CharacterStringType.Get(14,0,false)),
                            new FieldDescriptor ("PARMADDR",18,PointerTypeDescriptor.Get())
                    });
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

}
