namespace Raincode.Samples
{
    using RainCodeLegacyRuntime.Core;
    using RainCodeLegacyRuntime.Descriptor;
    using RainCodeLegacyRuntime.Types;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Generated on 31/12/2024 at 16:47:38
    /// by Raincode COBOL compiler version 4.2.621.0 (Build nb_v4.2.620.0-20-gf60e621428e-dirty)
    /// with arguments -HelperClassName=Raincode.Samples.PrintedLine -HelperStructure=OUTPRT COBSPOOL.cob
    ///
    /// Input program path: 'C:\Raincode360\Repositories\Demos\Qix - Factories - Writer\legacy\COBSPOOL.cob'
    /// Structure was originally in files:
    ///   COBSPOOL.cob
    /// </summary>
    public partial class PrintedLine : BaseHelper, System.IDisposable
    {
        public override string Name => "OUTPRT";
        public const int StructureSize = 80;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public PrintedLine(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public PrintedLine(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static PrintedLine StackAllocate(ExecutionContext ctxt)
        {
            return new PrintedLine(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static PrintedLine Allocate(ExecutionContext ctxt)
        {
            PrintedLine result = new PrintedLine(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea OUTPRT => theMemoryArea.Substr(0, 80);
        }

        /// <summary>
        /// name:   OUTPRT
        /// type:   string
        /// offset: 0
        /// size:   80
        /// </summary> 
        public string OUTPRT
        {
            get
            {
                return Convert.cnv_char_string_to_string(theContext, Raw.OUTPRT);
            }
            set
            {
                Convert.move_string_to_char_string(theContext, value, Raw.OUTPRT);
            }
        }

        private TypedLValue theCachedOUTPRT_TypedLValue = TypedLValue.Null;
        public TypedLValue OUTPRT_TypedLValue
        {
            get
            {
                if (theCachedOUTPRT_TypedLValue.IsNull)
                {
                    theCachedOUTPRT_TypedLValue = new TypedLValue(Raw.OUTPRT, CharacterStringType.Get(80,0,false));
                }
                return theCachedOUTPRT_TypedLValue;
            }
        }

        public override ITypeDescriptor Descriptor => CharacterStringType.Get(80,0,false);

    }

}
