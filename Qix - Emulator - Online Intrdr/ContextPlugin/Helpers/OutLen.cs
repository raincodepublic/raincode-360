namespace Raincode.Samples
{
    using RainCodeLegacyRuntime.Core;
    using RainCodeLegacyRuntime.Descriptor;
    using RainCodeLegacyRuntime.Types;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Generated on 31/12/2024 at 16:45:43
    /// by Raincode COBOL compiler version 4.2.621.0 (Build nb_v4.2.620.0-20-gf60e621428e-dirty)
    /// with arguments -HelperClassName=Raincode.Samples.OutLen -HelperStructure=OUTLEN COBSPOOL.cob
    ///
    /// Input program path: 'C:\Raincode360\Repositories\Demos\Qix - Factories - Writer\legacy\COBSPOOL.cob'
    /// Structure was originally in files:
    ///   COBSPOOL.cob
    /// </summary>
    public partial class OutLen : BaseHelper, System.IDisposable
    {
        public override string Name => "OUTLEN";
        public const int StructureSize = 4;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public OutLen(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public OutLen(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static OutLen StackAllocate(ExecutionContext ctxt)
        {
            return new OutLen(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static OutLen Allocate(ExecutionContext ctxt)
        {
            OutLen result = new OutLen(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea OUTLEN => theMemoryArea.Substr(0, 4);
        }

        /// <summary>
        /// name:   OUTLEN
        /// type:   int
        /// offset: 0
        /// size:   4
        /// </summary> 
        public int OUTLEN
        {
            get
            {
                return Convert.cnv_fixed_dec_to_int32(theContext, Raw.OUTLEN, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
            }
            set
            {
                Convert.move_int32_to_fixed_dec(theContext, value, Raw.OUTLEN, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
            }
        }

        private TypedLValue theCachedOUTLEN_TypedLValue = TypedLValue.Null;
        public TypedLValue OUTLEN_TypedLValue
        {
            get
            {
                if (theCachedOUTLEN_TypedLValue.IsNull)
                {
                    theCachedOUTLEN_TypedLValue = new TypedLValue(Raw.OUTLEN, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP()));
                }
                return theCachedOUTLEN_TypedLValue;
            }
        }

        public override ITypeDescriptor Descriptor => FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP());

    }

}
