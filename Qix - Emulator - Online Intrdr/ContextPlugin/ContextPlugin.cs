﻿using System;
using RainCode.Core.Plugin;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Language.HlAsm.Utils;
using RainCodeLegacyRuntime.Module;
using RainCodeQixRuntimeInterface.QixInstances;
using ExecutionContext = RainCodeLegacyRuntime.Core.ExecutionContext;

[assembly: PluginProvider(typeof(RainCode.Samples.ContextPlugin), "RegisterContextPlugin")]
namespace RainCode.Samples
{
    public class ContextPlugin
    {
        public static void RegisterContextPlugin()
        {
            RainCodeQixRuntimeInterface.QixInstances.QixFactory.ConstructorPlugin.Provide(30, NewFactory);
        }

        public static QixFactory NewFactory(RainCodeQixRuntimeInterface.IQixContext  theCtxt)
        {
            return new CustomQixFactory();
        }
    }
}
