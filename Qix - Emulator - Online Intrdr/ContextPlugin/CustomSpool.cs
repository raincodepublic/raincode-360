﻿using Raincode.Samples;
using RainCodeLegacyBatchCatalog;
using RainCodeLegacyRuntime.Core;
using RainCodeQixRuntimeInterface.QixInstances;
using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using ExecutionContext = RainCodeLegacyRuntime.Core.ExecutionContext;

namespace RainCode.Samples
{
    class CustomSpoolOpen : SpoolOpenInstance
    {
        public override void Execute()
        {
            //if (this.OUTDESCR == null)
            //    throw new ArgumentException();
            ////correctly setup the resp and do the appropriate treament

            //ExecutionContext ec = (ExecutionContext)Context;
            //MemoryArea mem = new MemoryArea(ec.AddressSpace, OUTDESCR.Offset, OUTDESCR.Size);

            //var pa = new ParmsArea(ec, mem);
            //if (pa.PARMSINF != "WRITER(INTRDR)") {
            //    Log.Trace("Ignoring spools other than internal reader.");
            //    return;
            //}

            SpoolContainer.Accumulator.Clear();
        }
    }

    class CustomSpoolWrite : SpoolWriteInstance
    {
        public override void Execute()
        {
            ExecutionContext ec = (ExecutionContext)Context;
            MemoryArea mem = new MemoryArea(ec.AddressSpace, FROM.Offset, FROM.Size);
            var pl = new PrintedLine(ec, mem);
            SpoolContainer.Accumulator.Add(pl.OUTPRT);
        }
    }

    class CustomSpoolClose : SpoolCloseInstance
    {
        public override void Execute()
        {
            var tempFile = System.IO.Path.GetTempFileName();
            tempFile = Path.GetFileName(tempFile);
            tempFile = Path.ChangeExtension(tempFile, "jcl");
            tempFile = @"C:\Raincode360\Repositories\Demos\Qix - Emulator - Online Intrdr\Watch\"+tempFile;
            File.WriteAllLines(tempFile, SpoolContainer.Accumulator);
        }
        
        // If you wanted to execute the JCL directly the method would work as such
        // public override void Execute()
        // {
            // var tempFile = System.IO.Path.GetTempFileName();
            // File.WriteAllLines(tempFile, SpoolContainer.Accumulator);
            // var args = $"-File={tempFile}";

            // var psi = new ProcessStartInfo("submit.exe");
            // psi.ArgumentList.Add(args);
            // psi.WindowStyle = ProcessWindowStyle.Hidden;
            // psi.CreateNoWindow = true;
            // psi.UseShellExecute = false;
            // Process p = new Process { StartInfo = psi };
            // p.Start();
        // }
    }

    class SpoolContainer
    {
        public static List<string> Accumulator = new List<string>();
    }
}
