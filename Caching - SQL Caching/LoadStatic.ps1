[CmdletBinding()]
Param (
    [Parameter(Mandatory)]
    [string]
    $ConnectionString
)
$ErrorActionPreference = "Stop"
Set-Variable maxRetries -Option Constant -Value 4
Set-Variable retryWait -Option Constant -Value 10

function Invoke-RobustSqlcmd {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory)]
        [string]
        $ConnectionString,

        [Parameter(Mandatory)]
        [string]
        $InputFile
    )
    Process {
        $retries = 0
        while ($true) {
            try {
                Invoke-Sqlcmd -AbortOnError -ConnectionString $ConnectionString -InputFile $InputFile
                break
            }
            catch [System.Data.SqlClient.SqlException], [System.InvalidOperationException] {
                if ($retries -lt $maxRetries) {
                    Write-Host "SQL command failed. $($maxRetries - $retries) retries left..."
                    $retries += 1
                    Start-Sleep -Seconds $retryWait
                }
                else {
                    Write-Host "SQL command failed too many times. Giving up!"
                    throw 
                }
            }
        }
    }
}

Get-ChildItem  -Name -Filter *.sp.sql | 
Foreach-Object {
    Invoke-RobustSqlcmd -InputFile $_ -ConnectionString $ConnectionString
    Write-Host ($_ + ": bind successful")
}

