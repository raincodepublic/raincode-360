       IDENTIFICATION DIVISION.
       PROGRAM-ID.     ZIPSQLCACHE.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       LINKAGE SECTION.
       COPY ZIPSQLCACHE.
       PROCEDURE DIVISION USING 
           ZIP-CODE, ZIP-CITY, ZIP-COUNTRY, ZIP-SQLCODE.
       EXEC SQL
           SELECT COUNTRY, CITY
           INTO :ZIP-COUNTRY, :ZIP-CITY
           FROM ZIPCODE 
           WHERE ZIPCODE = :ZIP-CODE
       END-EXEC
       MOVE SQLCODE TO ZIP-SQLCODE
       GOBACK
       .
