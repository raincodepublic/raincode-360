$SqlCached = 400
$SqlTotal  = 500

$rclrun = "$($Env:RCBIN)\rclrun"
$cobrc = "$($Env:RCBIN)\cobrc"
$SolutionDir = Get-Location 
$ProgName = "MAINSQLCACHE"
$Configuration = "Debug"
$quotedconn = "`"$($Env:RC360_CONNSTRING)Initial Catalog=BankDemo;`""
$ArgCommon = "-SqlServer=$($quotedconn) $($ProgName).dll"
$ArgDynamic = "-SQLExecutionMode=DynamicOnly "
$ArgStatic = "-SQLExecutionMode=StaticOnly "
$ArgSqlCache = "-PluginPath=`"$($Solutiondir)\SqlCachingPlugin\bin\$($Configuration)\net8.0`" -Plugin=SqlCachingPlugin "
$ArgProgramCache = "-PluginPath=`"$($Solutiondir)\ProgramCachingPlugin\bin\$($Configuration)\net8.0`" -Plugin=ProgramCachingPlugin "

function Start-Main    
{
    param([string]$CmdArgs)
    $p = Start-Process -PassThru -Wait -NoNewWindow -RedirectStandardOutput "NUL" -FilePath $rclrun -ArgumentList $CmdArgs
    if ($p.ExitCode -ne 0) {
        Set-Location -Path $SolutionDir
        throw "program $($ProgName) failed with code $($p.ExitCode)`n$cmdargs"
    }
}

if (-Not (Test-Path "Caching - SQL Caching.sln")) { 
    throw "not running in solution directory" 
}
Set-Location "$($SolutionDir)\Legacy\bin\$($Configuration)\net8.0"

#####################################################
## Static SQL with no caching                      ##
#####################################################
Write-Host "Test 1: Static SQL with no caching"
$Elapsed = Measure-Command { Start-Main -CmdArgs ($ArgStatic + $ArgCommon) } 
$TimeStatic = [int](Get-Content timings.txt)[0] / 1000

#####################################################
## Static SQL with SQL caching                     ##
#####################################################
Write-Host "Test 2: Static SQL with SQL caching"
$Elapsed = Measure-Command { Start-Main -CmdArgs ($ArgStatic + $ArgSqlCache + $ArgCommon) } 
$TimeStaticSqlCache = [int](Get-Content timings.txt)[0] / 1000

############################
## REPORT                 ##
############################
function Output-Result    
{
    param([String]$TestName, [Double]$Time, [Double]$SqlTotal, [Double]$SqlCached, [string]$Color)
    Write-Host -NoNewLine $TestName.PadRight(8) 
    Write-Host -NoNewLine $SqlTotal.ToString().PadLeft(10)
    Write-Host -NoNewLine $SqlCached.ToString().PadLeft(10) 
    Write-Host -NoNewLine $Time.tostring("0.00").PadLeft(13)
    Write-Host -NoNewLine "    "
    For ($i=0; $i -lt [math]::Round($Time * 4); $i++) { write-host -nonewline -BackgroundColor $Color ' '}
    Write-Host 
}
Write-Host "`nELAPSED TIMES"
Write-Host "-------------------------------------------"
Write-Host "               SQLs      SQLs        Time"
Write-Host "Test           Total     Cached      (s)"
Write-Host "-------------------------------------------"
Output-Result -TestName "Test 1" -Time $TimeStatic              -SqlTotal $SqlTotal -SqlCached 0          -Color "Green"
Output-Result -TestName "Test 2" -Time $TimeStaticSqlCache      -SqlTotal $SqlTotal -SqlCached $SqlCached -Color "DarkYellow"
Write-Host
Set-Location $SolutionDir
