********************************************************************
*                                                                  *
*   DEMO : SQL CACHING                                             *
*                                                                  *
********************************************************************

PURPOSE
  . Test the performance of SQL caching
  . Illustrate the use of static sql

RUNNING THE DEMO
  . Open a developer command prompt
  . Type 'pwsh' <enter>
  . Type '.\run' <enter>

NOTES
  . 'pwsh' starts version 7.0.2 (or higher) of powershell 
  . The use of static SQL requires that the COBOL program names are unique for the database that they connect to
  . When the cache is active, the first 50 SQLs will conect to the database to fill up the cache
  . Both tests run the same compiled code, the only difference being the -plugin argument of rclrun
  . The script 'LoadStatic.ps1' is run during the build phase via a post-build event, which is defined in the build properties of the COBOL project
  . When launching the program from the menu, it will run in cached mode
  . The run script assumes that the solution was built in debug mode