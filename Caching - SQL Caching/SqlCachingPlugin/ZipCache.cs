﻿using RainCode.Core.Extensions;
using System;
using System.Collections.Generic;

namespace RainCodeLegacyRuntime.Sql.SqlCachingPlugin
{
    public class ZipCache
    {

        private Dictionary<int, Zip> theDictionary = new Dictionary<int, Zip>();
        public ZipCache()
        {

        }

        internal SqlCode find(ParameterizedSqlComponent concreteStatement,
                              SqlImmediateStatement iStmt)
        {
            var inputs = iStmt.GetSingletonInputVariables();
            if (inputs.Count == 1 && inputs[0].EqualsCaseInsensitive("ZIPCODE"))
            {
                var h = concreteStatement.InputHostVariables[0];
                int ZipCode = (int)h.ValueAsObject(System.Data.DbType.Int32);
                if (theDictionary.ContainsKey(ZipCode))
                {
                    Console.WriteLine("Found in cache: {0}", ZipCode);
                    Zip z = theDictionary[ZipCode];
                    var outputs = iStmt.GetSingletonOutputVariables();
                    for (int i = 0; i < outputs.Count; i++)
                    {
                        object v = z.extractFieldAsObject(outputs[i]);
                        HostVariable.TransferValueToOutputVariable(v, concreteStatement.OutputHostVariables[i]);
                    }
                    return SqlCode.NoError;
                }
            }
            return SqlCode.NoDataFound;
        }

        internal void populate(ParameterizedSqlComponent concreteStatement, SqlImmediateStatement iStmt)
        {
            Zip z = new Zip();
            if (z.populate(concreteStatement, iStmt))
            {
                if (theDictionary.ContainsKey(z.ZipCode))
                {
                    Console.WriteLine("Duplicate key: {0}", z.ZipCode);
                }
                else
                {
                    Console.WriteLine("Population successful: {0}:{1}:{2}", z.ZipCode, z.City, z.Country);
                    theDictionary.Add(z.ZipCode, z);
                }
            }
            else
                Console.WriteLine("Incomplete record, populate cancelled");
        }
    }
}