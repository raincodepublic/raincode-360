﻿using RainCode.Core.Extensions;
using RainCode.Core.Plugin;
using RainCodeLegacyRuntime.Core;

[assembly: PluginProvider(typeof(RainCodeLegacyRuntime.Sql.SqlCachingPlugin.SqlCachingPlugin),
    nameof(RainCodeLegacyRuntime.Sql.SqlCachingPlugin.SqlCachingPlugin.Register))]

namespace RainCodeLegacyRuntime.Sql.SqlCachingPlugin
{
    public class SqlCachingPlugin
    {
        public static void Register()
        {
            ExecutionContext.PreparePlugin.Provide(1000, Initialize);
        }

        private static ZipCache zipCache = new ZipCache();

        private static void Initialize(ExecutionContext ctx, string entry)
        {
            ctx.SqlRuntime.ExecuteCommandInterceptor = (Command concreteStatement) =>
            {
                SqlStatement stmt = concreteStatement.IntermediateObject;
                bool populateCache = false;
                SqlCode res;
                SqlImmediateStatement iStmt = null;

                if (stmt is SqlImmediateStatement)
                {
                    iStmt = (SqlImmediateStatement)stmt;
                    string table = iStmt.GetSingletonTable();
                    if (table != null)
                    {
                        if (table.EqualsCaseInsensitive("ZIPCODE"))
                        {
                            res = zipCache.find(concreteStatement, iStmt);
                            if (res == SqlCode.NoError)
                                return res;
                            populateCache = res == SqlCode.NoDataFound;
                        }
                    }
                }
                res = concreteStatement.Execute();
                if (populateCache && res == SqlCode.NoError)
                {
                    zipCache.populate(concreteStatement, iStmt);
                }
                return res;
            };
        }
    }
}
