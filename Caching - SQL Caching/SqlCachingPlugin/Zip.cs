﻿namespace RainCodeLegacyRuntime.Sql.SqlCachingPlugin
{
    public class Zip
    {
        public int ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        internal bool populate(ParameterizedSqlComponent concreteStatement, SqlImmediateStatement iStmt)
        {
            var h = concreteStatement.InputHostVariables[0];
            ZipCode = (int)h.ValueAsObject(System.Data.DbType.Int32);
            var vars = iStmt.GetSingletonOutputVariables();
            for (int i = 0; i < vars.Count; i++)
            {
                var hv = concreteStatement.OutputHostVariables[i];
                switch (vars[i].ToUpper())
                {
                    case "ZIPCODE":
                        if (ZipCode == 0)
                            ZipCode = (int)hv.ValueAsObject(System.Data.DbType.Int32);
                        break;
                    case "COUNTRY":
                        if (Country == null)
                            Country = (string)hv.ValueAsObject(System.Data.DbType.String);
                        break;
                    case "CITY":
                        if (City == null)
                            City = (string)hv.ValueAsObject(System.Data.DbType.String);
                        break;
                    default:
                        return false;
                }
            }
            return (ZipCode > 0) && (City != null) && (Country != null);
        }

        internal object extractFieldAsObject(string v)
        {
            switch (v.ToUpper())
            {
                case "ZIPCODE":
                    return ZipCode;
                case "COUNTRY":
                    return Country;
                case "CITY":
                    return City;
            }
            return null;
        }
    }
}
