
********************************************************
*                                                      *
*   Compiler Demo : User defined function to implement *
*                   a coding guideline                 *
*                                                      *
********************************************************

PURPOSE
  . By using the Raincode scripting language create a user defined function to implement a coding guideline.
  . This UDF adds extra validation to compiler so that it throws an error when "GO TO" statement is encountered in Cobol module.

HOW TO RUN
  Error flow:
   . Right click on "CobolGotoUDF" from the solution explorer and chose "Rebuild" option
   . The compilation fails and you see the error message "Goto statements are not allowed when working for XXXX Corporation!" in the Output tab.
  Normal flow:
   . Click on "CobolGotoUDF" from the solution explorer.
   . From the displayed xml lines, comment below line i.e add tags <!-- and --> 
     <UserDefinedParameters>:ScriptPaths="$(SolutionDir)" :UserDefinedValidationProc="GotoUdf.Validate"</UserDefinedParameters> ".
   . Save the changes by pressing cltrl+s.
   . Right click on "CobolGotoUDF" from the solution explorer and chose "Rebuild" option, the build should be successful
   . Open GOTOCBL.cob from solution explorer and set a break point at GOBACK statement in PARA-1.
   . Start the Raincode Debugger.
   . The execution will print "IN PARA-0" and "IN PARA-1" in consecutive lines.
   . Close the application window to exit the debugger.
