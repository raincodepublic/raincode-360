*---------------------------------------------------------------------------------------*
*                                                                                       *
*   Compiler - Legacy Calling Csharp - Basic                                            *
*                                                                                       *
*---------------------------------------------------------------------------------------*

PURPOSE
  . Demonstrate how a COBOL program can call a C# routine

HOW TO RUN
  . Press Ctrl-F5 or start the Raincode Debugger via the menu.

EXPECTED OUTPUT
  . Console window
      Hello C#
      Hello COBOL
      Press any key ...

ABOUT THE DEMO
  . HELLO.cob calls CSHELLO.cs without a helper class. The parameter being passed is a simple string.
