﻿using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Module;
using RainCodeLegacyRuntime.Module.Attribute;
using System;

namespace RainCode.Samples
{
    /// <summary>
    /// Add the RainCodeExport (with the optional module name) and inherit from BaseModule to make the class callable from a COBOL program
    /// </summary>
    [RainCodeExport]
    public class CSHELLO : BaseModule<CSHELLO>
    {
        /// <summary>
        /// This method will be called automatically by the Raincode legacy runtime whenever a COBOL CALL to this program is issued
        /// </summary>
        /// <param name="ec">The execution context of the current run</param>
        /// <param name="workMem">The static memory of the program</param>
        /// <param name="args">The arguments to the program (COBOL USING clause)</param>
        /// <param name="retValue">The return value of the program (unused here)</param>
        protected override void Run(ExecutionContext ec, MemoryArea workMem, CallParameters parms)
        {
            // extract the first (and only) parameter: a 12 bytes strings
            MemoryArea param1 = parms.GetParameterAddress(ec, 0).Substr(0, 12);

            // Convert it to the appropriate type
            string str = RainCodeLegacyRuntime.Types.Convert.cnv_char_string_to_string(ec, param1);
            Console.WriteLine(str);

            // It is also possible to write back a value to the parameter (a MemoryArea is like a pointer, so we can write at that location)
            RainCodeLegacyRuntime.Types.Convert.move_string_to_char_string(ec, "Hello COBOL", param1);
        }
    }
}
