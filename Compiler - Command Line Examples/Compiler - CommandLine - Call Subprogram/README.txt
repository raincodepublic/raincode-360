
********************************************************
*                                                      *
*   Compiler Demo : CommandLine- Call Subprogram       *
********************************************************

PURPOSE

This demo compiles and executes COBOL programs to showcase the concept of "calling a subprogram".


HOW TO RUN
1. Right click on the script RunCommandLineDemo_Compiler - Call Subprogram.ps1
2. Select Run with Powershell. 
3. First part of script compiles, hello1.cob and hello2.cob using cobrc.exe.
4. Second part executes hello1 using rclrun. Here hello1 will invoke hello2.

