@echo off
call "%RCDIR%setenv_rccomp"
echo ## Find COB003D
cobrc ..\src\COB003A.cob 
echo ## Find COB003D + SQLCA
cobrc ..\src\COB003A.cob :IncludeSearchPath="%RCDIR%includes\cobol"
echo ## Find COB003D + SQLCA + COB003C
cobrc ..\src\COB003A.cob :IncludeSearchPath="%RCDIR%includes\cobol",..\src
echo ## Find COB003D + SQLCA + COB003C + COB003A + COB003B
cobrc ..\src\COB003A.cob :MaxMem=1G :IncludeSearchDir="%RCDIR%includes\cobol",..\src
pause


