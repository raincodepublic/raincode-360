$proc=Start-Process -PassThru -NoNewWindow -Wait -FilePath $env:RCDIR\bin\CobRc.exe -ArgumentList ":MaxMem=1G", "GOTOCBL.cob"
if($proc.ExitCode -ne 0){ 
	Write-Host "Compile failed with ExitCode:" $proc.ExitCode 
	exit $proc.ExitCode 
}
$proc=Start-Process -PassThru -NoNewWindow -Wait -FilePath $env:RCBIN\rclrun.exe -ArgumentList "GOTOCBL"
if($proc.ExitCode -ne 0){ 
	Write-Host "Run failed with ExitCode:" $proc.ExitCode
	exit $proc.ExitCode 
}