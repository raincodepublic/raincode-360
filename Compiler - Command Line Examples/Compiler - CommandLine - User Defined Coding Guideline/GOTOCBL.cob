       IDENTIFICATION DIVISION.
       PROGRAM-ID.   GOTOCBL.
       ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
       DATA DIVISION.
        WORKING-STORAGE SECTION.
         01 WS-X PIC 9(01).
        LINKAGE SECTION.
       PROCEDURE DIVISION.
       PARA-0.
             DISPLAY 'IN PARA-0'
             MOVE 1 TO WS-X
             GO TO PARA-1 PARA-2 DEPENDING ON WS-X
             DISPLAY 'IN PARA-0 AGAIN'
             STOP RUN.
       PARA-1.
             DISPLAY 'IN PARA-1'.
             GOBACK.
       PARA-2.
             DISPLAY 'IN PARA-2'.
             GOBACK.