
********************************************************
*                                                      *
*   Compiler Demo : User defined function to implement *
*                   a coding guideline                 *
*                                                      *
********************************************************

PURPOSE
  . By using the Raincode scripting language create a user defined function to implement a coding guideline.
  . This UDF adds extra validation to compiler so that it throws an error when "GO TO" statement is encountered in Cobol module.

HOW TO RUN
  Normal flow:
   . Right click on "run_normal_flow" and select "Run with PowerShell".
   . In case 'Execution Policy' content is displayed just opt for option "Y" and press enter.
   . This script triggers the build of module "GOTOCBL.cob" and build should be successful.
   . The execution will print "IN PARA-0" and "IN PARA-1" in consecutive lines.
   . Press any key to close the appication.
  Error flow:
   . Right click on "run_error_flow" and select "Run with PowerShell".
   . This script triggers the the build of module "GOTOCBL.cob".
   . In this case we have given the raincode script file (GotoUdf.rcs) as an input to the complier to error out "GO TO" statements from cobol modules.
   . Hence compilation fails and you see the error message "Goto statements are not allowed when working for XXXX Corporation!" in the Output tab.
