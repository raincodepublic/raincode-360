$proc=Start-Process -PassThru -NoNewWindow -Wait -FilePath $env:RCDIR\bin\CobRc.exe -ArgumentList ":MaxMem=1G", "GOTOCBL.cob",":UserDefinedValidationProc=GotoUdf.Validate"
if($proc.ExitCode -eq 26){ 
	Write-Host "Compile failed with ExitCode: " $proc.ExitCode	
}else {
	Write-Host "Compile didn't fail with expected ExitCode - 26, Actual ExitCode: " $proc.ExitCode 
	exit $proc.ExitCode 
}