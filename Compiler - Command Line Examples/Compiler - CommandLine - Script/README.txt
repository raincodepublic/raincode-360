cobrc :ExpandFNameProc=MScript.MyFunc

--> %RCDIR%scripts\MyScript.rcs

Expert Options
""""""""""""""
cobrc :?e 
Search for :*Proc options

EXAMPLE RewriteIfNull.rcs (automatically invoked)
"""""""""""""""""""""""""

MODULE RewriteIfNull;


PROCEDURE Rewrite (n)
VAR
    Fn;
BEGIN
  FOR IN n.SubNodes | X IS SqlFunctionExpression DO
    IF X.FuncName IS SqlIdentifier THEN
      Fn := X.FuncName.UnquotedId;
      IF Fn IS SqlAnonIdent THEN
        IF Fn.Image =@ "IFNULL" THEN
          Fn->Image := "VALUE";
        END;
      END;
    END;
  END;
  RESULT := n;
END;