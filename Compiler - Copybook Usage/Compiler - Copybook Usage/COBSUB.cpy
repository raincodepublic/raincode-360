      * PARAMETER SECTION
      * Level 1 group item
        01 W-STRUCT.
      *   Fixed length string                                Size: 20 
          10 W-MSG PIC X(20).
      *   Level 10 group item occurring 10 times
          10 W-ARRAY OCCURS 10 TIMES.
      *     Signed 32-bit binary integer                     Size:  4
            15 W-NUMBER1 PIC S9(8) USAGE IS BINARY.
      *     Signed 32-bit binary integer                     Size:  4
            15 W-SQUARE PIC S9(8) USAGE IS BINARY.
