       IDENTIFICATION DIVISION.
       PROGRAM-ID.     COBSQUARE.
      * Computes the square of numbers from 1 to 10
       ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
       DATA DIVISION.
        WORKING-STORAGE SECTION.
        COPY COBSUB.
        01 W-CNT PIC S9(8) USAGE IS BINARY.
        LINKAGE SECTION.
       PROCEDURE DIVISION.
           MOVE 1 TO W-CNT
           PERFORM UNTIL W-CNT > 10
               MOVE W-CNT TO W-NUMBER1(W-CNT)
               ADD 1 TO W-CNT
           END-PERFORM
           CALL "COBSUB" USING W-STRUCT
           MOVE 1 TO W-CNT
           PERFORM UNTIL W-CNT > 10
               DISPLAY W-SQUARE(W-CNT)
               ADD 1 TO W-CNT
           END-PERFORM           
           GOBACK.