
**************************************************************
*                                                            *
*   Compiler Demo : Custom Logger                            *
*                                                            *
**************************************************************

PURPOSE OF THIS DEMO
  . Demonstrate the use of loggers in a C# runner. 
    . Loggers are used to write formatted log messages to a particular target, such as the console or to a dedicated log file.
    . The LogLevel attribute af a logger can be set to show messages that particularlevel or higher.
  . Show how to create and register multiple loggers.
  . Show how set the loglevel and how to issue additional messages.

WHAT THE DEMO DOES
  . The main C# launcher creates and registers two loggers, 'ConsoleLogger' and 'CustomLogger'.
  . The LogLevel is set to 'DIAGNOSTIC', meaning that messages with this level or higher will be shown. 
  . The default level is 'SILENT', which surpresses all logging.
  . Both the registration and the setting of the LogLevel are needed to show any logging at all.
  . The ConsoleLogger writes messages to the console, while the CustomLogger writes messages to the file mylog.txt, which is included as a solution object.
  . A functional ConsoleLogger is already included in rccorlib, without the need for modification.
  . The behaviour of the CustomLogger is defined in CustomLogger.cs.

HOW TO RUN THE DEMO
  . Select 'Debug/Start Without Debugging' in the main menu
  . Observe what is displayed on the console
  . The solution object mylog.txt contains a copy of the message shown on the console

MANUAL REFERENCES
  . All Raincode manuals
    . https://www.raincode.com/docs/#_raincode_manuals
  . Specific links about logging
    . https://www.raincode.com/docs/Compiler-document/UserGuide/UserGuide.html#Logging
    . https://www.raincode.com/docs/Compiler-document/GettingStarted/GettingStarted.html#Logger