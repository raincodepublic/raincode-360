﻿using RainCode.Core.Logging;
using System;

namespace RainCode.Samples
{
    class CustomLogger : AbstractLogger
    {
        public System.IO.StreamWriter LogFile = new System.IO.StreamWriter(@"..\..\..\..\mylog.txt", false);
        protected override void LoggingFacilityLogged(LogEventArgs e)
        {
            LogFile.WriteLine("{0} {1} {2}", e.TimeStamp, e.Header, e.Message);
        }
    }
}
