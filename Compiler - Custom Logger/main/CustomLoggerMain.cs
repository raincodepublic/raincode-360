﻿using RainCode.Core.Logging;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions;
using RainCodeLegacyRuntime.Module;
using System;

namespace RainCode.Samples
{
    class CustomLoggerMain
    {
        private static LogSource LOG_SOURCE = new LogSource("Raincode.Samples.Logging");
        private static System.IO.StreamWriter LogFile;

        private static Executable LoadExecutable(string name)
        {
            var exec = ModuleDictionary.FindExecutable(name);
            ModuleDictionary.EnsureModuleIsLoaded(name);
            return exec;
        }

        private static void SetupLogger()
        {
            LogEventArgs.ShowTimeStamp = false;
            LogEventArgs.ShowThreadId = false;
            LogEventArgs.ShowLogSource = false;

            LogFile = Logger.CreateAndRegister<CustomLogger>().LogFile;
            Logger.CreateAndRegister<ConsoleLogger>();
            Logger.LogLevel = Level.DIAGNOSTIC;
            Logger.LogTrace(LOG_SOURCE, "Some totally relevant tracing");
        }

        static void Main(string[] args)
        {
            var exec = LoadExecutable("hello");
            SetupLogger();
            var ec = new ExecutionContext();
            ec.IOOperation.ConfigureSystemDatasets();
            try
            {
                using (LogFile)
                {
                    exec.Execute(ec);
                }
            }
            catch (StopRunUnitException)
            {
                Console.WriteLine("Program 'hello' ended");
            }

            Environment.Exit(ec.ReturnCode);
        }
    }
}
