********************************************************************
*                                                                  *
*   Batch - Legacy with Database Access                            *
*                                                                  *
********************************************************************

PURPOSE
  . Connect a COBOL program to the BANKDEMO database via IKJEFT01

HOW TO RUN
  . Start the Raincode Debugger
  . Examine the contents of the output files

