
Prerequisite:
Make sure ImSql Processing server is up and running the region "DEALER".

Note:
IMSQL_PROD_CONNSTRING - The connection string is read from the environmental variable. 

Demo Execution steps:
Use postman to send the HTTP requests.

Example with dealerOnline:
1. search a dealer
Url : https://localhost:7249/SendImsMsg/dsearch
post message:
{
  "DNUMBER": "DDDD00002 "
}

Expected result:
{
  "DNUMBER": "DDDD00002",
  "DNAME": "NAMX00002",
  "DPHONE": "004900002",
  "DSTREET": "STRE00002",
  "DZIP": "699900002",
  "DCITY": "FRAN00002",
  "DCOUNTR": "GERM00002 XXXX",
  "DSTATUS": "Succeded"
}

2. search for a model
Url: https://localhost:7249/SendImsMsg/msearch
post message:
{
  "MTYPE": "BMWV00001           "
}

Expected result:
{
  "MTYPE": "BMWV00001",
  "MBRAND": "BMW 00001 TEST2",
  "MDETAIL": "DETA00001  MORE DETAILS OF THIS CAR BMW",
  "MSTATUS": "Succeded"
}

3. search configured transactions

Url: https://localhost:7249/SendImsMsg/

get

Expected result:
["dsearch","msearch"]

4. search configured transaction input schemas
Url: https://localhost:7249/SendImsMsg/msearch/in

get

Expected result:
{
  "$schema": "https://json-schema.org/draft/2019-09/schema#",
  "$id": "https://schema.raincode.com/IMSql/SendMsg/MFSMessage",
  "title": "Raincode JSON schema for helper class: IMSql.SendMsg.MFSMessage",
  "type": "object",
  "properties": {
    "MTYPE": {
      "type": "string",
      "maxLength": 20
    }
  },
  "required": [
    "MTYPE"
  ]
}

5. search configured transaction output schemas
Url: https://localhost:7249/SendImsMsg/msearch/out

get

Expected result:
{
  "$schema": "https://json-schema.org/draft/2019-09/schema#",
  "$id": "https://schema.raincode.com/IMSql/SendMsg/MFSMessage",
  "title": "Raincode JSON schema for helper class: IMSql.SendMsg.MFSMessage",
  "type": "object",
  "properties": {
    "MTYPE": {
      "type": "string",
      "maxLength": 20
    },
    "MBRAND": {
      "type": "string",
      "maxLength": 40
    },
    "MDETAIL": {
      "type": "string",
      "maxLength": 60
    },
    "MSTATUS": {
      "type": "string",
      "maxLength": 30
    }
  },
  "required": [
    "MTYPE",
    "MBRAND",
    "MDETAIL",
    "MSTATUS"
  ]
}