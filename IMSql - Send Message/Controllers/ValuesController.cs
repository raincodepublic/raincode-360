﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IMSql.SendMsg.Controllers
{
    internal class MessageDescriptor { internal MFSMessage Mid { get; set; } internal MFSMessage Mod; };
    public class TransactionMapping { public string TransactionName { get; set; } public string InputMsg { get; set; } public string OutputMsg { get; set; } };

    [Route("/")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly string ServiceBrokerConnectionString;
        private readonly string Region;
        private readonly string Queue;
        private readonly string Service;

        private readonly Dictionary<string, string> Mid = new Dictionary<string, string>();
        private readonly Dictionary<string, string> Mod = new Dictionary<string, string>();
        private readonly Dictionary<string, MessageDescriptor> TransactionMapping = new Dictionary<string, MessageDescriptor>();

        private CountdownEvent Cde;

        public ValuesController(IConfiguration configuration)
        {
            ServiceBrokerConnectionString = Environment.GetEnvironmentVariable(configuration["ServiceBrokerConnectionString"]);
            Region = configuration["Region"];
            Queue = configuration["Queue"];
            Service = configuration["Service"];
            var MessageFormatPath = configuration["MessageFormatPath"];
            if (MessageFormatPath != null)
            {
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                var encoding = configuration["IMSEncoding"];
                var transactionmapping = configuration.GetSection("TransactionMapping");
                if (transactionmapping != null)
                {
                    var data = transactionmapping.Get<TransactionMapping[]>();
                    foreach (var tm in data)
                    {
                        Mid.Add(tm.InputMsg, tm.TransactionName);
                        Mod.Add(tm.OutputMsg, tm.TransactionName);
                        TransactionMapping.Add(tm.TransactionName, new MessageDescriptor());
                    }
                }
                foreach (var f in Directory.EnumerateFiles(MessageFormatPath))
                {
                    var ext = Path.GetExtension(f);
                    var name = Path.GetFileNameWithoutExtension(f);
                    if (ext == ".mid")
                    {
                        if (Mid.TryGetValue(name, out string val))
                        {
                            var mid = new MFSMessage(f);
                            mid.SetEncoding(encoding);
                            TransactionMapping[val].Mid = mid;
                        }
                    }
                    else if (ext == ".mod")
                    {
                        if (Mod.TryGetValue(name, out string val))
                        {
                            var mod = new MFSMessage(f);
                            mod.SetEncoding(encoding);
                            TransactionMapping[val].Mod = mod;
                        }
                    }
                }
            }
        }

        [HttpGet("SendImsMsg/")]
        public string GetTransactions() => System.Text.Json.JsonSerializer.Serialize(TransactionMapping.Keys);

        [HttpGet("SendImsMsg/{tranid?}/in")]
        public string GetInputSchema(string? tranid)
        {
            try
            {
                var trans = TransactionMapping[tranid];
                return trans.Mid.Schema();
            }
            catch (KeyNotFoundException ex)
            {
                throw new ArgumentException($"Unknown transaction {tranid} {ex}");
            }
        }

        [HttpGet("SendImsMsg/{tranid?}/out")]
        public string GetOutputSchema(string? tranid)
        {
            try
            {
                var trans = TransactionMapping[tranid];
                return trans.Mod.Schema();
            }
            catch (KeyNotFoundException ex)
            {
                throw new ArgumentException($"Unknown transaction {tranid} {ex}");
            }
        }

        [HttpPost]
        [Route("SendImsMsg/{tranid?}")]
        public async Task<string> ReadStringDataManual(string? tranid)
        {
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                string input = await reader.ReadToEndAsync();
                try
                {
                    var trans = TransactionMapping[tranid];
                    var IDesc = trans.Mid;
                    var ODesc = trans.Mod;
                    byte[] inputmsg = IDesc.EncodeFields(input);
                    SendMsg send = new SendMsg(ServiceBrokerConnectionString, Region, Queue, Service);
                    byte[] outputmsg = send.run(tranid, inputmsg);
                    return ODesc.DecodeFields(outputmsg);
                }
                catch (KeyNotFoundException ex)
                {
                    throw new ArgumentException($"Unknown transaction {tranid} {ex}");
                }
            }
        }

        [HttpPost]
        [Route("SendImsMsgStress")]
        public async Task<string> SendMsgStress()
        {
            using (StreamReader bodyReader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                string input = await bodyReader.ReadToEndAsync();
                return SendMsgStress(input);
            }
        }

        private string SendMsgStress(string input)
        {
            string outputMsg = "Stress ";
            var reader = new System.Text.Json.Utf8JsonReader(System.Text.Encoding.UTF8.GetBytes(input));
            int parallel = 1;
            int iter = 1;
            string propName = null;
            string TransCode = null;
            Cde = new CountdownEvent(1);
            List<LoopMsgParam> transactionParams = new List<LoopMsgParam>();

            List<Thread> threads = new List<Thread>();
            while (reader.Read())
            {
                switch (reader.TokenType)
                {
                    case System.Text.Json.JsonTokenType.String:
                        switch (propName)
                        {
                            case "parallel":
                                parallel = int.Parse(reader.GetString());
                                break;
                            case "iter":
                                iter = int.Parse(reader.GetString());
                                break;
                            default:
                                throw new ArgumentException($"Invalid message format: expected parallel, iter or msg. Actual {propName}");
                        }
                        break;
                    case System.Text.Json.JsonTokenType.StartObject:
                        if (propName != null)
                        {
                            TransCode = propName;
                            outputMsg = outputMsg + TransCode + ", ";
                            try
                            {
                                var trans = TransactionMapping[TransCode];
                                var IDesc = trans.Mid;
                                var ODesc = trans.Mod;
                                byte[] inputMsg = IDesc.EncodeFields(ref reader);
                                Cde.AddCount(parallel);
                                transactionParams.Add(new LoopMsgParam(TransCode, iter, inputMsg));
                            }
                            catch (KeyNotFoundException ex)
                            {
                                throw new ArgumentException($"Unknown transaction {TransCode} {ex}");
                            }
                        }
                        break;
                    case System.Text.Json.JsonTokenType.EndObject:
                        if (TransCode != null)
                        {
                            TransCode = null;
                        }
                        break;
                    case System.Text.Json.JsonTokenType.PropertyName:
                        propName = reader.GetString();
                        break;
                    default:
                        break;
                }
            }

            Cde.Signal();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            // create the threads
            for (int i = 0; i < parallel; i++)
            {
                foreach (var param in transactionParams)
                {
                    Thread t = new Thread(LoopMsg);
                    t.Start(param);
                    threads.Add(t);
                }
            }
            foreach (Thread t in threads)
            {
                t.Join();
            }
            stopwatch.Stop();
            return $"{outputMsg} (parallel: {parallel}, iter: {iter}) : {stopwatch.ElapsedMilliseconds}";
        }

        private void LoopMsg(object param)
        {
            LoopMsgParam loopMsg = ((LoopMsgParam)param);
            SendMsg send = new SendMsg(ServiceBrokerConnectionString, Region, Queue, Service);
            Console.WriteLine($"SendMsg created {loopMsg.TransCode} ({Thread.CurrentThread.ManagedThreadId })");
            Cde.Signal();
            Cde.Wait();
            Console.WriteLine($"SendMsg starting {loopMsg.TransCode} ({Thread.CurrentThread.ManagedThreadId }) : {DateTime.Now.ToFileTime()}");
            for (int i = 0; i < loopMsg.Iter; i++)
                send.run(loopMsg.TransCode, loopMsg.InputMsg);
            send.Close();
            Console.WriteLine($"SendMsg finished {loopMsg.TransCode} ({Thread.CurrentThread.ManagedThreadId }) : {DateTime.Now.ToFileTime()}");
        }
    }

    public class LoopMsgParam
    {
        public string TransCode {get;set;}
        public int Iter { get; set; }
        public byte[] InputMsg { get; set; }

        public LoopMsgParam(string transCode, int iter, byte[] inputMsg)
        {
            TransCode = transCode;
            Iter = iter;
            InputMsg = inputMsg;
        }
    }
}
