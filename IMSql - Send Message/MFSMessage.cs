﻿namespace IMSql.SendMsg
{
    using System.Collections.Generic;
    using System.Text.Json;
    using XmlBooster;

    public class Field
    {
        public int Length;
        public int Offset;
        public string Fill;
        public string DeviceFieldName;

    }

    public class MFSMessage
    {
        RainCode.IMSql.MfsParser.Generated.MFSMessage Desc;

        private RainCode.IMSql.MfsParser.Generated.MFSMessage Read(string path)
        {
            StreamReader reader = new StreamReader(path, System.Text.Encoding.UTF8);
            XmlBContext context = new XmlBStringContext(reader.ReadToEnd());
            reader.Close();
            if (RainCode.IMSql.MfsParser.Generated.acceptor.getFactory() == null)
            {
                RainCode.IMSql.MfsParser.Generated.acceptor.setFactory(new RainCode.IMSql.MfsParser.ParsingNodes.Factory());
            }
            return RainCode.IMSql.MfsParser.Generated.acceptor.acceptMFSMessage(context);
        }

        public string Name;
        public string Type;
        public string Source;
        public bool Ignore;
        public string Option;
        public string Next;
        public int Size;
        public List<Field> Fields = new List<Field>();
        private System.Text.Encoding SystemEncoding = System.Text.Encoding.GetEncoding(37);

        public void SetEncoding(string encoding)
        {
            if (encoding != null)
            {
                SystemEncoding = System.Text.Encoding.GetEncoding(encoding);
            }
        }
        public MFSMessage() { }

        public MFSMessage(string path)
        {
            Desc = Read(path);
            Name = Desc.getName();
            Source = Desc.getSource();
            Ignore = Desc.getIgnore();
            Option = Desc.getOption_AsString();
            Next = Desc.getNext();
            int ofs = 0;
            foreach (var f in Desc.getMessagePages(0).getSegment(0).allMessageFields())
            {
                var mf = f as RainCode.IMSql.MfsParser.Generated.MFSMessageField;
                int length = mf.getLength();
                string name = mf.getDeviceFieldName();
                Fields.Add(new Field
                {
                    Length = length,
                    Offset = name != null ? ofs : 0,
                    Fill = mf.getFill(),
                    DeviceFieldName = name,
                });
                if (name != null)
                {
                    ofs += length;
                }
            }
            Size = ofs;
        }

        static readonly string JSON_METASCHEMA = "https://json-schema.org/draft/2019-09/schema#";

        public string Schema()
        {
            var options = new System.Text.Json.JsonWriterOptions
            {
                Indented = true
            };
            using var stream = new MemoryStream();
            using var writer = new System.Text.Json.Utf8JsonWriter(stream, options);
            writer.WriteStartObject();
            writer.WriteString("$schema", JSON_METASCHEMA);
            writer.WriteString("$id", $"https://schema.raincode.com/{GetType().FullName.Replace(".", "/")}");
            writer.WriteString("title", $"Raincode JSON schema for helper class: {GetType().FullName}");
            writer.WriteString("type", "object");
            List<string> required = new List<string>();
            writer.WriteStartObject("properties");
            foreach (var f in Fields)
            {
                if (f.DeviceFieldName != null)
                {
                    required.Add(f.DeviceFieldName);
                    writer.WriteStartObject(f.DeviceFieldName);
                    writer.WriteString("type", "string");
                    writer.WriteNumber("maxLength", f.Length);
                    writer.WriteEndObject();
                }
            }
            writer.WriteEndObject();
            writer.WriteStartArray("required");
            foreach (string req in required)
            {
                writer.WriteStringValue(req);
            }
            writer.WriteEndArray();
            writer.WriteEndObject();
            writer.Flush();

            return System.Text.Encoding.UTF8.GetString(stream.ToArray());
        }

        public string DecodeFields(byte[] msg)
        {
            var options = new System.Text.Json.JsonWriterOptions
            {
                Indented = true
            }; 
            using var stream = new MemoryStream();
            using var writer = new System.Text.Json.Utf8JsonWriter(stream, options);
            writer.WriteStartObject();
            foreach (var f in Fields)
            {
                if (f.DeviceFieldName != null)
                {
                    writer.WriteString(f.DeviceFieldName, SystemEncoding.GetString(msg, f.Offset, f.Length).Trim());
                }
            }
            writer.WriteEndObject();
            writer.Flush();

            return System.Text.Encoding.UTF8.GetString(stream.ToArray());
        }

        public byte[] EncodeFields(string msg)
        { 
            string propName = null;
            Utf8JsonReader reader = new System.Text.Json.Utf8JsonReader(System.Text.Encoding.UTF8.GetBytes(msg));
            while (reader.Read())
            {
                switch (reader.TokenType)
                {
                    case System.Text.Json.JsonTokenType.StartObject:
                        if (propName == null)
                        {
                            return EncodeFields(ref reader);
                        }
                        else
                        {
                            throw new ArgumentException($"Invalid message format: object with a name {propName}");
                        }
                    case System.Text.Json.JsonTokenType.PropertyName:
                        propName = reader.GetString();
                        break;
                    default:
                        throw new ArgumentException($"Invalid message format: unexpected message element {reader.TokenType}");
                }
            }
            throw new ArgumentException($"Invalid message format: unexpected end of message");
        }

        public byte[] EncodeFields(ref Utf8JsonReader reader)
        {
            var result = new byte[Size];
            int i = 0;
            string propName = null;
            while (reader.Read())
            {
                switch (reader.TokenType)
                {
                    case System.Text.Json.JsonTokenType.String:
                        var f = Fields[i];
                        while (f.DeviceFieldName == null && i < Fields.Count)
                        {
                            f = Fields[++i];
                        }
                        if (propName == f.DeviceFieldName)
                        {
                            var b = SystemEncoding.GetBytes(reader.GetString());
                            if (b.Length < f.Length)
                            {
                                byte c;
                                if(f.Fill == null)
                                {
                                    c = 0x00;
                                } else
                                {
                                    c = SystemEncoding.GetBytes(f.Fill.Substring(0, 1))[0];
                                }
                                
                                for (int j = f.Offset + b.Length; j < f.Offset + f.Length; j++)
                                {
                                    result[j] = c;
                                }
                            }
                            System.Buffer.BlockCopy(b, 0, result, f.Offset, b.Length < f.Length ? b.Length : f.Length);
                            i++;
                        }
                        else
                        {
                            throw new ArgumentException($"Invalid message format: expected {f.DeviceFieldName} actual {propName}");
                        }
                        break;
                    case System.Text.Json.JsonTokenType.StartObject:
                        throw new ArgumentException($"Invalid message format: object into a transaction");
                    case System.Text.Json.JsonTokenType.EndObject:
                        return result;
                    case System.Text.Json.JsonTokenType.PropertyName:
                        propName = reader.GetString();
                        break;
                    default:
                        throw new ArgumentException($"Invalid message format: unexpected element");
                }
            }
            throw new ArgumentException($"Invalid message format: transaction without EndObject");
        }
    }
}
