﻿using RainCode.IMSql.Common.SQL.ServiceBroker;
using RainCode.IMSql.Common.SQL.ServiceBroker.Commands;
using RainCodeLegacyRuntime.IMSql.Messages;
using Microsoft.Data.SqlClient;
using System.Text;

namespace IMSql.SendMsg
{
    internal class SendMsg
    {
        private string RegionName;
        private string TerminalName;
        private SendOnConversationCommand _sendOnConversation;
        private BeginDialogCommand _beginConversation;
        private WaitForReceiveInConversationCommand _nextConversationMessage;
        private EndConversationCommand _endConversation;

        private SqlConnection Connection;

        public SendMsg(string connectionString, string regionName, string queueName, string serviceName)
        {
            Connection = new SqlConnection(connectionString);
            Connection.Open();
            TerminalName = "SendMsg";
            RegionName = regionName;
            
            string _serviceName = serviceName;
            string _queueName = queueName;
            _beginConversation = new BeginDialogCommand(Connection, _serviceName);
            _sendOnConversation = new SendOnConversationCommand(Connection, "BeginSession");
            _nextConversationMessage = new WaitForReceiveInConversationCommand(Connection, _queueName);
            _endConversation = new EndConversationCommand(Connection);
        }

        public byte[] run(string transactionCode, byte[] data)
        {
            var inputMessage = BuildInputMessage(transactionCode, data);

            if (inputMessage == null)
            {
                throw new Exception("No input message");
            }

            var svcUrl = $"imsql://{RegionName}/TRANS/{inputMessage.TranCode}";
            return HandleInputMessage(inputMessage, svcUrl);
        }

        public void Close()
        {
            Connection.Close();
        }

        private byte[] HandleInputMessage(InputMessage inputMessage, string serviceUrl)
        {
            Guid conversationId = StartConversation(serviceUrl);
            SendMessage(new ProcessingServerMessage
            {
                ConversationId = conversationId,
                Input = inputMessage,
                ModName = "test",
                Source = TerminalName,
                Type = ProcessingServiceMessageType.InputMessage
            });

            int i = 0;
            ProcessingServerMessage reply;
            while ((reply = NextMessage(conversationId)) == null)
            {
                Thread.Sleep(100);
                if (i > 100)
                {
                    throw new Exception("Processing server didn't response");
                }
                i++;
            }
            EndConversation(conversationId);
            if (reply.Type == ProcessingServiceMessageType.ErrorMessage)
            {
                throw new Exception($"Processing error: {Encoding.ASCII.GetString((reply.Data ?? (new byte[0])))}");
            }
            else
            {
                OutputMessage outMessage = reply.Output;
                String modName = outMessage.ModName.Trim();

                var segments = outMessage.SegmentsWithoutSPA;
                if (segments != null && segments.Count > 0)
                {
                    return segments.First().Data;
                }
                else
                {
                    throw new Exception("output message empty");
                }
            }
        }

        private void EndConversation(Guid id)
        {
            _endConversation.Handle = id;
            _endConversation.ExecuteNonQuery();
        }

        private ProcessingServerMessage NextMessage(Guid conversationId)
        {
            _nextConversationMessage.DialogId = conversationId;
            var r = _nextConversationMessage.ExecuteNonQuery();
            if (r == 0)
                return null;

            if (_nextConversationMessage.MessageType == "http://schemas.microsoft.com/SQL/ServiceBroker/Error")
            {
                throw new Exception(Encoding.Unicode.GetString(_nextConversationMessage.Data));
            }

            ProcessingServerMessage message = (ProcessingServerMessage)ProcessingServerMessage.Deserialize(_nextConversationMessage.Data);
            message.ConversationId = conversationId;
            return message;
        }

        private Guid StartConversation(string serviceUrl)
        {
            _beginConversation.ToService = serviceUrl;
            _beginConversation.OnContract = "ProcessingServiceContract";
            _beginConversation.ExecuteNonQuery();
            return _beginConversation.DialogId;
        }

        public int SendMessage(ProcessingServerMessage message)
        {
            _sendOnConversation.Data = message.Serialize();
            _sendOnConversation.SessionId = message.ConversationId;
            _sendOnConversation.MessageType = message.Type.ToString();
            return _sendOnConversation.ExecuteNonQuery();
        }

        public InputMessage BuildInputMessage(string transactionCode, byte[] data)
        {
            var inputMessage = new InputMessage();
            short zz = 2; //Option2;

            var inputSegment = new InputSegment
            {
                ZZ = zz,
                TransactionCode = transactionCode,
                Data = data
            };
            inputMessage.Enqueue(inputSegment);

            return inputMessage;
        }
    }
}
