********************************************************************
*                                                                  *
*   Batch - PLI with Database Access                            *
*                                                                  *
********************************************************************

PURPOSE
  . Connect a PL/1 program to the BANKDEMO database via IKJEFT01

HOW TO RUN
  . Start the Raincode Debugger
  . Examine the contents of the output files JCL003A.FILE01 and JCL003A.FILE02
    in the catalog.

