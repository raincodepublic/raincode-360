       IDENTIFICATION DIVISION.
       PROGRAM-ID.     GENREP.
       ENVIRONMENT DIVISION.
        INPUT-OUTPUT SECTION.
         FILE-CONTROL.
           SELECT CUS-FILE
               ASSIGN       TO  CUSTFILE
               ORGANIZATION IS  INDEXED
               RECORD KEY   IS  W-CUSTOMERID OF CUSTOMERS-RECORD
               ACCESS MODE  IS  DYNAMIC.
           SELECT ACCOUNT-FILE
               ASSIGN       TO  ACCFILE
               ORGANIZATION IS  INDEXED
               RECORD KEY   IS  W-ACCOUNT-ID OF ACCOUNT-RECORD
               ACCESS MODE  IS  SEQUENTIAL.
           SELECT OUTPUT-FILE
               ASSIGN       TO OUTFILE
               ORGANIZATION IS SEQUENTIAL.
       DATA DIVISION.
        FILE SECTION.
        FD CUS-FILE.
       COPY CUSTOMER.
        FD ACCOUNT-FILE.
       COPY ACCOUNT.
        FD OUTPUT-FILE.
        01 OUTPUT-REC.
           02 O-ACC-NUM  PIC X(19).
           02 FILLER     PIC X.
           02 O-AMOUNT   PIC +9(8).99.
           02 FILLER     PIC X.
           02 O-FNAME    PIC X(40).
           02 FILLER     PIC X.
           02 O-LNAME    PIC X(40).
           02 FILLER     PIC X.
           02 O-CITY     PIC X(40).
           02 FILLER     PIC X.
           02 O-ZIP      PIC X(4).
           02 FILLER     PIC X.
       WORKING-STORAGE SECTION.
       01 END-FILE PIC 9 VALUE 0.
       PROCEDURE DIVISION.
           OPEN I-O CUS-FILE.
           OPEN INPUT ACCOUNT-FILE.
           OPEN OUTPUT OUTPUT-FILE.

           READ ACCOUNT-FILE NEXT
              AT END MOVE 1 TO END-FILE
           END-READ.

           PERFORM WRITE-LINE UNTIL END-FILE = 1.

           CLOSE CUS-FILE
           CLOSE ACCOUNT-FILE
           CLOSE OUTPUT-FILE.
           GOBACK.

       WRITE-LINE.
           MOVE SPACE TO OUTPUT-REC
           MOVE W-ACCOUNT-ID TO O-ACC-NUM
           MOVE W-AMOUNT TO O-AMOUNT
           MOVE W-CUSTOMERID-ID TO W-CUSTOMERID
           READ CUS-FILE KEY IS W-CUSTOMERID
             INVALID KEY DISPLAY "CUSTOMER NOT FOUND"
           END-READ.
           MOVE W-FIRSTNAME TO O-FNAME
           MOVE W-LASTNAME TO O-LNAME
           MOVE W-ZIPCODE TO O-ZIP
           MOVE W-CITY TO O-CITY
           WRITE OUTPUT-REC
           READ ACCOUNT-FILE NEXT
              AT END MOVE 1 TO END-FILE
           END-READ.