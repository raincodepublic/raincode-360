       IDENTIFICATION DIVISION.
        PROGRAM-ID. COB001.
       ENVIRONMENT DIVISION.
        INPUT-OUTPUT SECTION.
         FILE-CONTROL.
           SELECT CUSTOMERS-FILE
               ASSIGN       TO  CUSTFILE
               ORGANIZATION IS  INDEXED
               RECORD KEY   IS  W-CUSTOMERID OF CUSTOMERS-RECORD
               ACCESS MODE  IS  SEQUENTIAL.
           SELECT ACCOUNT-FILE
               ASSIGN       TO  ACCFILE
               ORGANIZATION IS  INDEXED
               RECORD KEY   IS  W-ACCOUNT-ID OF ACCOUNT-RECORD
               ACCESS MODE  IS  SEQUENTIAL.
       DATA DIVISION.
        FILE SECTION.
        FD CUSTOMERS-FILE.
       COPY CUSTOMER.
        FD ACCOUNT-FILE.
       COPY ACCOUNT.
       WORKING-STORAGE SECTION.
        01 HV-AMOUNT           PIC S9(8)V99.
       COPY SQLCA.
   	   EXEC SQL
   	       DECLARE CUR01 CURSOR FOR
      	   SELECT 
               C.CUSTOMERID, 
               C.FIRSTNAME, 
               C.LASTNAME, 
               C.ZIPCODE, 
               Z.CITY
           FROM 
               DBO.CUSTOMER C,
               DBO.ZIPCODE Z
           WHERE 
               Z.ZIPCODE = C.ZIPCODE
   	   END-EXEC.
   	   EXEC SQL
   	       DECLARE ACC-CUR CURSOR FOR
      	   SELECT 
               ACCOUNTNUMBER,
               CUSTOMERID,
               AMOUNT
           FROM 
               DBO.ACCOUNT
   	   END-EXEC.
       PROCEDURE DIVISION.
      * IMPORT CUSTOMER FILE
           OPEN OUTPUT CUSTOMERS-FILE
           EXEC SQL
   	           OPEN CUR01
   	       END-EXEC
   	       PERFORM FETCH-CUR01
           PERFORM UNTIL SQLCODE > 0
               WRITE CUSTOMERS-RECORD
   	           PERFORM FETCH-CUR01
           END-PERFORM
           EXEC SQL
   	           CLOSE CUR01
   	       END-EXEC
           CLOSE CUSTOMERS-FILE.
      * IMPORT ACCOUNT FILE
           OPEN OUTPUT ACCOUNT-FILE
           EXEC SQL
   	           OPEN ACC-CUR
   	       END-EXEC
   	       PERFORM FETCH-ACC-CUR
           PERFORM UNTIL SQLCODE > 0
               WRITE ACCOUNT-RECORD
   	           PERFORM FETCH-ACC-CUR
           END-PERFORM
           EXEC SQL
   	           CLOSE ACC-CUR
   	       END-EXEC
           CLOSE ACCOUNT-FILE.

           GOBACK
           .
       FETCH-CUR01.
   	       EXEC SQL
   	           FETCH 
                   CUR01 
               INTO
                   :W-CUSTOMERID,
                   :W-FIRSTNAME,
                   :W-LASTNAME,
                   :W-ZIPCODE,
                   :W-CITY
   	       END-EXEC.

       FETCH-ACC-CUR.
   	       EXEC SQL
   	           FETCH 
                   ACC-CUR
               INTO
                   :W-ACCOUNT-ID,
                   :W-CUSTOMERID-ID,
                   :HV-AMOUNT
   	       END-EXEC.
           DISPLAY HV-AMOUNT
           MOVE HV-AMOUNT TO W-AMOUNT. 
