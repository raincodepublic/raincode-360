       IDENTIFICATION DIVISION.
        PROGRAM-ID. COB001.
       ENVIRONMENT DIVISION.
        INPUT-OUTPUT SECTION.
         FILE-CONTROL.
           SELECT RELATIVE-FILE
               ASSIGN       TO  CUSTFILE
               ORGANIZATION IS  RELATIVE
               RELATIVE KEY   IS  W-KEY
      *         ACCESS MODE  IS  SEQUENTIAL
               FILE STATUS  IS W-STAT.
       DATA DIVISION.
        FILE SECTION.
        FD RELATIVE-FILE.
        01 RELATIVE-RECORD.
           02 W-CUSTOMERID PIC 9(4). 
           02 W-FIRSTNAME PIC X(40).
           02 W-LASTNAME PIC X(40).
           02 W-ZIPCODE PIC X(4).
           02 W-CITY PIC X(40).
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       01 W-KEY PIC 9(9).
       01 W-STAT PIC XX.
   	   EXEC SQL
   	       DECLARE CUR01 CURSOR FOR
      	   SELECT 
               C.CUSTOMERID, 
               C.FIRSTNAME, 
               C.LASTNAME, 
               C.ZIPCODE, 
               Z.CITY
           FROM 
               DBO.CUSTOMER C,
               DBO.ZIPCODE Z
           WHERE 
               Z.ZIPCODE = C.ZIPCODE
   	   END-EXEC.
       PROCEDURE DIVISION.
           OPEN OUTPUT RELATIVE-FILE
           DISPLAY '**** OPEN' W-STAT
           EXEC SQL
   	           OPEN CUR01
   	       END-EXEC
   	       PERFORM FETCH-CUR01
      *     MOVE 1 TO W-KEY
           PERFORM UNTIL SQLCODE > 0
               WRITE RELATIVE-RECORD
               DISPLAY '******* WRITE' W-STAT
   	           PERFORM FETCH-CUR01
      *         ADD 1 TO W-KEY
           END-PERFORM
           EXEC SQL
   	           CLOSE CUR01
   	       END-EXEC
           CLOSE RELATIVE-FILE.
           GOBACK
           .
       FETCH-CUR01.
   	       EXEC SQL
   	           FETCH 
                   CUR01 
               INTO
                   :W-CUSTOMERID,
                   :W-FIRSTNAME,
                   :W-LASTNAME,
                   :W-ZIPCODE,
                   :W-CITY
   	       END-EXEC
           .
