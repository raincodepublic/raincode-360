CATALOG EXPLORER DEMO: INDEXED FILES

PURPOSE
. This demo creates an indexed sequential file that can be vieuwed in the Catalog Explorer

HOW TO RUN
. Start the Raincode Debugger in Visual Studio
. Launch the Catalog Explorer to look at the result

NOTES
. The JCL starts by deleting the output from the previous run (if any)
