
**************************************************************
*                                                            *
*   Compiler Demo : Dump Processor                           *
*                   rclrun with plugin implementation        *
*                                                            *
**************************************************************

PURPOSE
  . Install a dump processor based on the abstract BaseDumpProcessor class.
  . The Dump Processor will produce a formatted report when a program crashes.

HOW TO RUN
  . Start the Raincode debugger.
  . The execution will hit a breakpoint in other.cob caused by a DivideByZeroException.
  . Press continue.
  . Close the application window to exit the debugger.
  . Alternatively, use 'Run without debugging' to avoid the second and third step.

EXAMINING THE RESULT
  . Open the dump.xml solution object.
  . The xml contains the working storage of the three COBOL programs involved at the time of the crash.
  . Note that other0.cob is not on the stack at this point.

PROGRAMMING CONSIDERATIONS
  . Care must be taken when writing the output bevause valid XML can only contain printable characters.
  . In this particular example, we replace these characters by a decimal point ('.').
  . The TextValue attribute of a scalar value is transformed as follows :

      s = Regex.Replace(s.TextValue.Trim(), @"\p{C}", ".")

    wher \p{C} represents the non-printable characters in unicode.