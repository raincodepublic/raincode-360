       IDENTIFICATION DIVISION.
       PROGRAM-ID. crash.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-VARS.
         02 WS-P1 PIC s9(5) COMP-5.
         02 WS-P2 PIC s9(5) COMP-5.
         02 WS-TXT PIC X(8).
       01 W-LOW PIC X(10) VALUE LOW-VALUES.

       LINKAGE SECTION.
       PROCEDURE DIVISION.
           DISPLAY "BEGIN CRASH"
           PERFORM INIT
           PERFORM BODY
           DISPLAY "END"
           STOP RUN
           .
       BODY.
           DISPLAY "CALLING OTHER (NO CRASH)"
           CALL "OTHER" USING "F"
           CALL "OTHER0" USING "SOMETEXTWITHDATA"

           DISPLAY "CALLING OTHER (CRASH)"
           CALL "OTHER" USING "T"
           DISPLAY "END CALL OTHER"
           .
       INIT.
           MOVE 3 TO WS-P1
           MOVE 42 TO WS-P2
           MOVE 'CONTENT' TO WS-TXT
           .
