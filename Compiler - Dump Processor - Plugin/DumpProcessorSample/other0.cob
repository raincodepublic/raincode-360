       IDENTIFICATION DIVISION.
       PROGRAM-ID. OTHER0.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 SOME-TABLES.
         02 TBL OCCURS 2.
           03 TBL-0 PIC X(4).
           03 TBL-1 PIC S9(7) COMP-3.
         02 STTBL PIC X(16) OCCURS 4.
       77 R0 PIC S9(9) COMP-5.
       77 R1 PIC S9(9) COMP-5.
       77 R2 PIC S9(9) COMP-5.
       LINKAGE SECTION.
       01 PARM PIC X(16).

       PROCEDURE DIVISION USING PARM.
           PERFORM WITH TEST AFTER
               VARYING R0 FROM 1 BY 1 UNTIL R0 = 2
             COMPUTE R1 = (R0 - 1) * 4 + 1
             MOVE PARM(R1:4) TO TBL-0(R0)
             MOVE R1 TO TBL-1(R0)
           END-PERFORM

           PERFORM WITH TEST AFTER
               VARYING R0 FROM 1 BY 1 UNTIL R0 = 4
             COMPUTE R1 = (R0 - 1) * 4 + 1
             PERFORM WITH TEST BEFORE
                 VARYING R2 FROM 1 BY 4 UNTIL R2 = 17
               MOVE PARM(R1:4) TO STTBL(R0)(R2:4)
             END-PERFORM

           END-PERFORM
           DISPLAY STTBL

           GOBACK
           .