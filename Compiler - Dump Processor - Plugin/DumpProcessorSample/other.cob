       IDENTIFICATION DIVISION.
       PROGRAM-ID. OTHER.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-OPS.
          02 LHS PIC S9(9) COMP-5.
          02 RHS PIC S9(9) COMP-5.
          02 RES PIC S9(9) COMP-5.
       01 PREVIOUS-PARM PIC X(1).

       LINKAGE SECTION.
       01 PARM PIC X.
           88 DO-CRASH VALUE 'T'.

       PROCEDURE DIVISION USING PARM.
           MOVE 21 TO LHS
           IF DO-CRASH THEN
               MOVE 0 TO RHS
           ELSE
               MOVE 1 TO RHS
           END-IF
           COMPUTE RES = LHS / RHS
           DISPLAY RES
           GOBACK
           .