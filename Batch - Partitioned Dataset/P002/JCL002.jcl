//JCL002 JOB
//*******************************************************************
//*                                                                 *
//*  JCL002: Execute a program via a JCL procedure                  *
//*          Run JCL001 first to create the procedure               *
//*                                                                 *
//*******************************************************************
//LIB001   JCLLIB ORDER=PROCLIB.TEST.JCL
//*-----------------------------------------------------------------*
//*  STEP001: Call procedure MYPROC in PROCLIB.TEST.JCL             *
//*-----------------------------------------------------------------*
//STEP001  EXEC MYPROC,PROG=COB001
//