//JCL001 JOB
//*******************************************************************
//*                                                                 *
//*  JCL001: Create a PDSLIB and put a single member on it          *
//*                                                                 *
//*******************************************************************
//*-----------------------------------------------------------------*
//*  STEP001: Cleanup files from previous run                       *
//*-----------------------------------------------------------------*
//STEP001 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSOUT DD SYSOUT=*
//SYSIN DD *
 DELETE PROCLIB.*
      SET LASTCC=0
      SET MAXCC=0
/*
//*-----------------------------------------------------------------*
//*  STEP002: Create PDSLIB                                         *
//*-----------------------------------------------------------------*
//STEP002 EXEC PGM=IEFBR14
//DD1     DD DSN=PROCLIB.TEST.JCL,DSNTYPE=PDS,
//        DISP=(NEW,CATLG,DELETE),RECFM=LSEQ
//*-----------------------------------------------------------------*
//*  STEP003: Add member from file                                  *
//*-----------------------------------------------------------------*
//STEP003 EXEC PGM=IEFBR14
//DD1     DD DSN=PROCLIB.TEST.JCL(MYPROC),DISP=SHR,
//        PATH=('myproc.txt',COPY)
//
