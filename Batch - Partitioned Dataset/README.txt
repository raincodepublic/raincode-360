********************************************************************
*                                                                  *
*   Batch - Partitioned Dataset                                    *
*                                                                  *
********************************************************************

ILLUSTRATED FEATURES
  . Create a partitioned dataset (pdslib) via jcl.
  . Usage of IEBFR14 to create a file.
  . Usage of IEBFR14 with the PATH parameter. 

HOW TO RUN
  . Run JCL001 (Start Raincode Debugger)
  . Run JCL002 (right-click on P002 and select Debug/Start New Instance)

NOTES
  . IEBFR14 does literally nothing, but you can create files with it via the DISP parameter.
  . JCL001 creates the pdslib PROCLIB.TEST.JCL with a single member named MYPROC.
  . MYPROC is a jcl procedure that can be invoked from other jcls with parameters.
  . JCL002 has this statement that indicates wher the procedure can be found:

      //LIB001   JCLLIB ORDER=PROCLIB.TEST.JCL

    The default location for jcl procedures is SYS1.PROCLIB.

