       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBRETRIEVE.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 W-ZIPCODE PIC XXXX.
       PROCEDURE DIVISION.
           EXEC CICS 
               RETRIEVE INTO(W-ZIPCODE)
           END-EXEC
           CALL 'COBCOMMON' USING W-ZIPCODE
           EXEC CICS
               RETURN
           END-EXEC
           .