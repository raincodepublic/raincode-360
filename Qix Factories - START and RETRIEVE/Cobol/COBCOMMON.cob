       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBCOMMON.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       01 W-CITY PIC X(25).
       01 W-SQLCODE PIC +++++9.
       LINKAGE SECTION.
       01 W-ZIPCODE PIC XXXX.
       PROCEDURE DIVISION USING W-ZIPCODE.
           EXEC SQL
               SELECT City
               INTO :W-CITY
               FROM ZipCode
               WHERE ZipCode = :W-ZIPCODE
           END-EXEC
           IF SQLCODE = 0
               DISPLAY ' '
               DISPLAY W-CITY
           ELSE
               MOVE SQLCODE TO W-SQLCODE
               DISPLAY '++++ ZIPCODE:' W-ZIPCODE ' SQLCODE:' W-SQLCODE
           END-IF
           GOBACK
           .