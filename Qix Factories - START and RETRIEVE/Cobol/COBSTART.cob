       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBSTART.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 W-ZIPCODE PIC 9999. 
       PROCEDURE DIVISION.
           PERFORM VARYING W-ZIPCODE FROM 1000 BY 20 
                   UNTIL W-ZIPCODE > 1200
               EXEC CICS
                   START TRANSID('IN02') FROM(W-ZIPCODE)
               END-EXEC 
           END-PERFORM
           EXEC CICS
               RETURN
           END-EXEC
           .