using RainCodeLegacyRuntime.Core;
using RainCodeQixInterface.Constants;
using RainCodeQixRuntimeInterface.QixInstances;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Runner
{
    public class Start : StartInstance
    {
        public override void Execute()
        {
            switch (TRANSID.Trim().ToUpper())
            {
                case "IN02":
                    // Take a copy of FROM before it changes in the main task
                    var bytes = FROM.Bytes;
                    Task.Run(() => DoStart("COBRETRIEVE", bytes));
                    DfheiblkHelper.EIBRESP = Resp.NORMAL;
                    DfheiblkHelper.EIBRESP2 = 0;
                    break;
                default:
                    DfheiblkHelper.EIBRESP = Resp.TRANSIDERR;
                    DfheiblkHelper.EIBRESP2 = 0;
                    break;
            }
        }
        private void DoStart(string program, byte[] bytes)
        {
            using (var ec = DatabaseConfiguration.PreparedExecutionContext())
            {
                var startDataPointer = ec.QixAllocate(bytes.Length);
                startDataPointer.Write(0, bytes);
                ec.UserInfo["DATA"] = startDataPointer;
                try
                {
                    var link = ec.QixInterface.Factory.CreateLink();
                    link.Context = ec;
                    link.PROGRAM = program;
                    ec.QixInterface.Operation.Link(link);
                    if (link.DfheiblkHelper.EIBRESP == Resp.NORMAL)
                    {
                        var syncpoint = ec.QixInterface.Factory.CreateSyncPoint();
                        syncpoint.Context = ec;
                        ec.QixInterface.Operation.SyncPoint(syncpoint);
                    }
                }
                catch (Exception)
                {
                    DfheiblkHelper.EIBRESP = Resp.INVREQ;
                }
            }
        }
    }
}