﻿using System;
using System.Threading;
using RainCode.Core.AssemblyLoading;
using RainCode.Core.Logging;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Module;
using RainCodeLegacyRuntime.Sql.SqlServer;
using RainCodeQixInterface.Constants;
using ExecutionContext = RainCodeLegacyRuntime.Core.ExecutionContext;

namespace Runner
{
    class Runner
    {
        private static readonly LogSource LogSource = new LogSource("Runner");
        private static void Main(string[] args)
        {
            Logger.CreateAndRegister<ConsoleLogger>();
            Logger.LogLevel = Level.WARNING;
            AssemblyLoading.Loader.AddPath(ModuleDictionary.AssemblyCategory, @"..\..\..\..\Cobol\bin\Debug\net6.0");
            using (var ec = DatabaseConfiguration.PreparedExecutionContext())
            {
                var link = ec.QixInterface.Factory.CreateLink();
                link.Context = ec;
                link.PROGRAM = "COBSTART";
                ec.QixInterface.Operation.Link(link);
                if (link.DfheiblkHelper.EIBRESP != Resp.NORMAL)
                {
                    var syncPointRollback = ec.QixInterface.Factory.CreateSyncPointRollBack();
                    syncPointRollback.Context = ec;
                    ec.QixInterface.Operation.SyncPointRollBack(syncPointRollback);
                    Logger.LogError(LogSource,
                        $"Error during CICS LINK on {link.PROGRAM}. RESP: {link.DfheiblkHelper.EIBRESP}, RESP2: {link.DfheiblkHelper.EIBRESP2}");
                }
                else
                {
                    var syncPoint = ec.QixInterface.Factory.CreateSyncPoint();
                    syncPoint.Context = ec;
                    ec.QixInterface.Operation.SyncPoint(syncPoint);
                }
                // Make sure all the threads are finished
                Thread.Sleep(10000);
            }
        }
    }
}