using System;
using System.Threading;
using RainCodeQixInterface.Constants;
using RainCodeQixRuntimeInterface.QixInstances;
using RainCodeLegacyRuntime.Core;
using RainCodeQixRuntimeInterface;

namespace Runner
{
    public class Retrieve : RetrieveInstance
    {
        public override void Execute()
        {
            try
            {
                var ec = (RainCodeLegacyRuntime.Core.ExecutionContext)Context;
                var dataPointer = (IQixPointer)ec.UserInfo["DATA"];
                if (Variant == Variants.Into)
                {
                    DATA.Write(0, dataPointer.Bytes);
                }
                else
                {
                    var ptr = Context.QixReallocate(dataPointer.Size, this);
                    ptr.Write(0, dataPointer.Bytes);
                    DATA = ptr;
                }

                DfheiblkHelper.EIBRESP = Resp.NORMAL;
            }
            catch (Exception)
            {
                DfheiblkHelper.EIBRESP = Resp.INVREQ;
            }
        }
    }
}