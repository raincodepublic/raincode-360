using RainCodeLegacyRuntime.QIX.Interface;
using RainCodeQixRuntimeInterface.QixInstances;

namespace Runner
{
    public class QixFactory : Factory
    {
        public override StartInstance CreateStart()
        {
            return new Start();
        }

        public override RetrieveInstance CreateRetrieve()
        {
            return new Retrieve();
        }
    }
}