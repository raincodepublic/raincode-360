using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Sql;
using RainCodeLegacyRuntime.Sql.SqlServer;
using System;

namespace Runner
{
    public static class DatabaseConfiguration
    {
        private static readonly ExecutionContextArgs ExecutionContextArgs = new ExecutionContextArgs()
        {
            QixMode = true,
            QixFactory = new QixFactory()
        };
        private static string ConnectionString => GetConnectionString();
        public static ExecutionContext PreparedExecutionContext()
        {
            var ec = new ExecutionContext(ExecutionContextArgs);
            ec.SqlRuntime = new SqlServerRuntime(ec, ConnectionString)
            {
                ExecutionStrategy = SqlRuntime.ExecutionModeStrategy.DynamicOnly
            };
            ec.IOOperation.ConfigureSystemDatasets();
            return ec;
        }
      #region connectionstring
        private static string GetConnectionString()
        {
            string NightbuildConnectionString = "$SQLSERVERCONNECTIONSTRING$"; //Automated test framework substitution
            string Rc360ConnectionString = Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog=BankDemo;";
            return !NightbuildConnectionString.Contains("$SQLSERVERCONNECTIONSTRING") ? NightbuildConnectionString : Rc360ConnectionString;
        }
      #endregion connectionstring
   }
}