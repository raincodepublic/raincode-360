+------------------------------------------------------------------+
|                                                                  |
|                                                                  |
|   Qix - Asynchronous Processing with START / RETRIEVE            |
|                                                                  |
|                                                                  |
+------------------------------------------------------------------+

PURPOSE

  . Implement the CICS START and RECEIVE commands with Qix factories.

START AND RETRIEVE IN CICS

  . The START command implements a 'fire-and-forget' mechanism by starting a new asynchronous transaction as follows :

      START TRANSID('name') FROM(data)

    whereby data can be any part of working storage.
  . If CICS is not able to execute the new transaction immediately, it will be queued to start at a later
    point in time. The original transaction does not wait for the new transaction to be started. The return status
    of the command just indicates that the new transaction has been successfully queued.
  . The started transaction then receives the data with the following command :

      RETRIEVE INTO(data)

    whereby data now refers to the working storage of the started transaction.

PRACICAL APPLICATIONS OF START AND RECEIVE

  . The START and RECEIVE commands are often used to create a listener for incoming data on a queue.
  . When ever data is received, the listener will launch a seperate task to process the data.

RUNNING THE DEMO IN VISUAL STUDIO

  . Press ctrl-F5 to run without debugging.

EXPECTED OUTPUT

  . The program launches 10 seperate tasks to retrieve the cities associated with certain zipcodes.

    SINT-GILLIS              BRUSSEL

    Laken
    ETTERBEEK
    SINT-JANS-MOLENBEEK
    Postcheque
    Neder-Over-Heembeek
    EVERE
    UKKEL
    OUDERGEM

    The output can appear more or less out of order depending on which taks finish first.

SIMULATING THE TRANSACTION

  . The transaction 'IN02' is translated into an execution of COBRETRIEVE in Start.cs.

TASK INDEPENDENCE

  . Each task get its own database connection within a seperate execution context.
  . Common routines such as COBCOMMON get a separate copy of working storage per task.
  . Database events such as COMMIT and ROLLBACK only affect the task inwhich they occur.

MAKING SURE ALL THREADS ARE FINISHED BEFORE THE APPLICATION ENDS

  . Runner.cs has a built-in a delay at the end of the main method for this purpose.

      Thread.Sleep(2000);

CONNECTION POOLING

  . The initial connection to a database is a relatively expensive process. In this particular example, 
    the connection to the database is made 11 times.
  . I a more realistic example, finished tasks will return their connections to a connection pool where they can
    be used by subsequent tasks.
  . Connection pooling is supported by default in .NET.
  

