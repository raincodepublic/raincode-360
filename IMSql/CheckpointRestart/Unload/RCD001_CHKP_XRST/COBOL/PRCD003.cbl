       IDENTIFICATION DIVISION.                                         00020000
       PROGRAM-ID.  PRCD003.                                            00030003
      *                                                                 00040000
      ***************************************************************** 00050000
      *                                                                 00060000
      * APPLICATION  :  BMP DL/I PROGRAM INPUT FROM QSAM FILE           00070000
      * TRANSACTION  :  NONE (BMP/DLI)                                  00080000
      * PSB          :  DFSIVP64                                        00090000
      * DATABASE     :  RCD001                                          00100000
      * INPUT:                                                          00110000
      * SEGEMENT NAME A1111111 must be changed to RCADDR                00120000
      * now with CHKP / RESTART  CHKP/XRST                              00121001
      *                                                                 00130000
      *        TELEPHONE DIRECTORY SYSTEM                               00140000
      *        PROCESS CODE : CCCCCCCC                                  00150000
      *        LAST NAME    : XXXXXXXXXX  FOR TEST DNNNNNN              00160044
      *        FIRST NAME   : XXXXXXXXXX  FOR TEST ENNNNNN              00170044
      *        EXTENSION#   : N-NNN-NNNN  FOR TEST FNNNNNN              00180044
      *        INTERNAL ZIP : XXX/XXX NOT USED                          00190044
      *                                                                 00191032
      * BMP abends with U0102 because checkpoint data isn't found       00192032
      *     REASON : WRONG CHECKPOINT NAME                              00192144
      *            : JCLNAME DIFFERENT FROM ORIGINAL JCL                00192244
      * BMP abends with U0458 pcb is stopped because last exec failed   00193032
      *                                                                 00194032
      *                                                                 00200000
      * CCCCCCCC = COMMAND                                              00210000
      *        ABEND   = ABEND TASK to test CHKP/XRST                   00220027
      *                  in the case an abend occured the               00220131
      *                  PCB will be stopped no other job can run       00220231
      *                  until PCB is restarted                         00220331
      *                  COMMAND: /START PROGRAM pcbname                00220431
      *        ADD     = INSERT ENTRY IN DB                             00221027
      *        DELETE  = DELETE ENTRY FROM DB                           00230000
      *        UPDATE  = UPDATE ENTRY FROM DB                           00240000
      *        DISPLAY = DISPLAY ENTRY                                  00250000
      *        TADD    = SAME AS ADD, BUT WRITE TO OPERATOR             00260000
      *                                                                 00270000
       ENVIRONMENT DIVISION.                                            00280000
       CONFIGURATION SECTION.                                           00290000
       SOURCE-COMPUTER.  IBM-370.                                       00300000
       OBJECT-COMPUTER.  IBM-370.                                       00310000
      *                                                                 00320000
       INPUT-OUTPUT SECTION.                                            00320116
       FILE-CONTROL.                                                    00320218
           SELECT CKPTFREQ    ASSIGN TO CKPTFREQ.                       00320317
      *                                                                 00321016
       DATA DIVISION.                                                   00330000
       FILE SECTION.                                                    00331016
       FD  CKPTFREQ                                                     00332016
           LABEL RECORD STANDARD                                        00333017
           BLOCK CONTAINS 0 RECORDS.                                    00334016
       01  CKPTRFRE-REC    PIC X(80).                                   00335016
      *                                                                 00336016
       WORKING-STORAGE SECTION.                                         00340000
                                                                        00350000
      * DL/I FUNCTION CODES                                             00360000
                                                                        00370000
       77  GET-UNIQUE      PIC  X(4)  VALUE 'GU  '.                     00380000
       77  GET-HOLD-UNIQUE PIC  X(4)  VALUE 'GHU '.                     00390000
       77  GET-NEXT        PIC  X(4)  VALUE 'GN  '.                     00400000
       77  ISRT            PIC  X(4)  VALUE 'ISRT'.                     00410000
       77  DLET            PIC  X(4)  VALUE 'DLET'.                     00420000
       77  REPL            PIC  X(4)  VALUE 'REPL'.                     00430000
       77  CHKP            PIC  X(4)  VALUE 'CHKP'.                     00431001
       77  XRST            PIC  X(4)  VALUE 'XRST'.                     00432001
                                                                        00440000
      * DL/I CALL STATUS CODE                                           00450000
                                                                        00460000
       77  END-OF-DATABASE PIC  X(4)  VALUE 'GB'.                       00470000
       77  DB-GOOD         PIC  X(2)  VALUE '  '.                       00471001
                                                                        00480000
      * MESSAGES                                                        00490000
                                                                        00500000
       77  MDEL    PIC  X(40)                                           00510000
                   VALUE 'ENTRY WAS DELETED                       '.    00520000
       77  MADD    PIC  X(40)                                           00530000
                   VALUE 'ENTRY WAS ADDED                         '.    00540000
       77  MEND    PIC  X(40)                                           00550000
                   VALUE 'BMP/DLI PGM HAS ENDED                   '.    00560000
       77  MDIS    PIC  X(40)                                           00570000
                   VALUE 'ENTRY WAS DISPLAYED                     '.    00580000
       77  MUPD1   PIC  X(40)                                           00590000
                   VALUE 'ENTRY WAS UPDATED                       '.    00600000
       77  MTEST   PIC  X(40)                                           00610000
                   VALUE 'TEST REQUEST WAS ENDED                  '.    00620000
       77  MMORE   PIC  X(40)                                           00630000
                   VALUE 'DATA IS NOT ENOUGH                      '.    00640000
       77  MINV    PIC  X(40)                                           00650000
                   VALUE 'PROCESS CODE IS NOT VALID               '.    00660000
       77  MUPD0   PIC  X(40)                                           00670000
                   VALUE 'PLEASE UPDATE ENTRY                     '.    00680000
       77  MNODATA PIC  X(40)                                           00690000
                   VALUE 'NO DATA WAS ENTERED                     '.    00700000
       77  MNONAME PIC  X(40)                                           00710000
                   VALUE 'LAST NAME WAS NOT SPECIFIED             '.    00720000
       77  MNOENT  PIC  X(40)                                           00730000
                   VALUE 'SPECIFIED PERSON WAS NOT FOUND          '.    00740000
       77  MISRTE  PIC  X(40)                                           00750000
                   VALUE 'ADDITION OF ENTRY HAS FAILED            '.    00760000
       77  MDLETE  PIC  X(40)                                           00770000
                   VALUE 'DELETION OF ENTRY HAS FAILED            '.    00780000
       77  MREPLE  PIC  X(40)                                           00790000
                   VALUE 'UPDATE OF ENTRY HAS FAILED              '.    00800000
                                                                        00810000
      * VARIABLES                                                       00820000
                                                                        00830000
       77  TEMP-ONE   PICTURE X(8) VALUE SPACES.                        00840000
       77  TEMP-TWO   PICTURE X(8) VALUE SPACES.                        00850000
       77  REPLY      PICTURE X(16).                                    00860000
                                                                        00870000
      * CONSTANTS                                                       00880000
                                                                        00890000
       77  HEADER-BLOCK    PIC X(50)                                    00900000
           VALUE '**************************************************'.  00910000
       77  HEADER-NAME     PIC X(50)                                    00920000
           VALUE '* TEST FOR CHECKPOINT RESTART            '.           00930020
       77  CONSTANT1       PIC X(24)                                    00940000
           VALUE   'PROCESS  CODE  (*1) :   '.                          00950000
       77  CONSTANT2       PIC X(24)                                    00960000
           VALUE   'LAST  NAME          :   '.                          00970000
       77  CONSTANT3       PIC X(24)                                    00980000
           VALUE   'FIRST NAME          :   '.                          00990000
       77  CONSTANT4       PIC X(24)                                    01000000
           VALUE   'EXTENSION  NUMBER   :   '.                          01010000
       77  CONSTANT5       PIC X(24)                                    01020000
           VALUE   'INTERNAL  ZIP CODE  :   '.                          01030000
       77  CONSTANT6       PIC X(17)                                    01040000
           VALUE   '(*1) PROCESS CODE'.                                 01050000
       77  CONSTANT7       PIC X(7)                                     01060000
           VALUE   'ADD    '.                                           01070000
       77  CONSTANT8       PIC X(7)                                     01080000
           VALUE   'DELETE '.                                           01090000
       77  CONSTANT9       PIC X(7)                                     01100000
           VALUE   'UPDATE '.                                           01110000
       77  CONSTANT10      PIC X(7)                                     01120000
           VALUE   'DISPLAY'.                                           01130000
       77  CONSTANT11      PIC X(7)                                     01140000
           VALUE   'TADD   '.                                           01150000
      *77  SSA1            PIC X(9)  VALUE 'ADDR     '.                 01160000
       77  SSA1            PIC X(9)  VALUE 'RCADDR   '.                 01170000
       77  RESTARTED       PIC X(1)  VALUE SPACES.                      01170135
      *                          Y  = Job is restarted                  01170235
      *                                                                 01170328
      *    ABEND FIELDS TO SIMULATE AND PROGRAM ABEND 0C7               01170428
       01  ABEND-FIELD.                                                 01171028
           03 ABEND-PACKED PIC S9(5) COMP-3 VALUE ZEROS.                01171128
           03 ABEND-RED REDEFINES ABEND-PACKED.                         01171228
              05 ABEND-CHR PIC XXX.                                     01171328
           03 ABEND-WORK   PIC S9(5) COMP-3 VALUE ZEROS.                01171428
      *                                                                 01172107
       01  GICHKPPA.                                                    01172207
           03 CHKPT-COUNT              PIC 9(5) COMP-3  VALUE 0.        01172307
           03 CHKPT-AREA               PIC X(8)         VALUE SPACE.    01172407
           03 CHKPT-AREA-LENGTH        PIC S9(8) COMP   VALUE +8.       01172507
           03 CHKPT-XRST-AREA          PIC X(12)        VALUE SPACE.    01172607
           03 FILLER                   PIC X(02)        VALUE SPACE.    01172707
           03 CHKPT-FREQUENCY          PIC S9(8) COMP   VALUE ZERO.     01172807
           03 CHKPT-ID.                                                 01172907
              05 CHKPT-ID-PROG         PIC X(3)         VALUE SPACE.    01173007
              05 CHKPT-ID-TIME         PIC 9(5)         VALUE zeros.    01173134
                                                                        01173207
           COPY GWDISPL.                                                01173307
           COPY GIPCBIO.                                                01173409
      *                                                                 01173509
       01  CHKPT-PAR.                                                   01173609
           03  CHK-PAR-KEY.                                             01173709
               05  CHK-PAR-POLNR   PIC 9(10)  COMP-3.                   01173809
           03  CHK-DBKZ            PIC 999.                             01173909
           03  CHK-LES             PIC S9(7) COMP-3.                    01174009
           03  CHK-DISP            PIC S9(7) COMP-3.                    01174109
           03  CHK-VERARB          PIC S9(7) COMP-3.                    01174209
      *                                                                 01174309
       01  CHKPT-PAR-LEN           PIC S9(8) COMP  VALUE +21.           01174409
      *                                                                 01174509
      * FLAGS                                                           01174609
                                                                        01174709
       01 FLAGS.                                                        01175000
          02  SET-DATA-FLAG  PIC X VALUE '0'.                           01176000
             88  NO-SET-DATA       VALUE '1'.                           01177000
          02  TADD-FLAG      PIC X VALUE '0'.                           01178000
             88  PROCESS-TADD      VALUE '1'.                           01179000
                                                                        01180000
      * COUNTERS                                                        01190000
                                                                        01200000
       01 COUNTERS.                                                     01210000
          02  L-SPACE-CTR    PIC   9(2) COMP VALUE 0.                   01220000
                                                                        01230000
      * OUTLINE FORMAT                                                  01240000
                                                                        01250000
       01  BLANKLINE.                                                   01260000
           02  ANSI     PIC  X.                                         01270000
           02  HFILLER  PIC  X(04)  VALUE SPACES.                       01280044
           02  LEDGE    PIC  X(1)   VALUE '|'.                          01290000
           02  FILLER   PIC  X(80)  VALUE SPACES.                       01300000
           02  REDGE    PIC  X(1)   VALUE '|'.                          01310000
           02  FILLER   PIC  X(34)  VALUE SPACES.                       01320044
       01  OUTLINE1.                                                    01330000
           02  O-ANSI   PIC  X.                                         01340000
           02  HFILLER  PIC  X(04)  VALUE SPACES.                       01350044
           02  OUTLN1A  PIC  X(40)                                      01360000
               VALUE '*---------------------------------------'.        01370000
           02  OUTLN1B  PIC  X(42)                                      01380000
               VALUE '-----------------------------------------*'.      01390000
           02  FILLER   PIC  X(34)  VALUE SPACES.                       01400044
       01  OUTLINE2.                                                    01410000
           02  ANSI     PIC  X.                                         01420000
           02  HFILLER  PIC  X(04)  VALUE SPACES.                       01430044
           02  LEDGE    PIC  X(1)   VALUE '|'.                          01440000
           02  FILLER   PIC  X(15)  VALUE SPACES.                       01450000
           02  HDRLN    PIC  X(50)  VALUE SPACES.                       01460000
           02  FILLER   PIC  X(15)  VALUE SPACES.                       01470000
           02  REDGE    PIC  X(1)   VALUE '|'.                          01480000
           02  FILLER   PIC  X(34)  VALUE SPACES.                       01490044
       01  OUTLINE3.                                                    01500000
           02  ANSI     PIC  X.                                         01510000
           02  HFILLER  PIC  X(04)  VALUE SPACES.                       01520044
           02  LEDGE    PIC  X(1)   VALUE '|'.                          01530000
           02  FILLER   PIC  X(40)  VALUE SPACES.                       01540000
           02  D1LN     PIC  X(27)                                      01550000
               VALUE 'TRANSACTION TYPE : BMP/DLI '.                     01560000
           02  D2LN     PIC  X(10)  VALUE '(HIDAM DB)'.                 01570000
           02  FILLER   PIC  X(3)   VALUE SPACES.                       01580000
           02  REDGE    PIC  X(1)   VALUE '|'.                          01590000
           02  FILLER   PIC  X(34)  VALUE SPACES.                       01600044
       01  OUTLINE4.                                                    01610000
           02  ANSI     PIC  X.                                         01620000
           02  HFILLER  PIC  X(04)  VALUE SPACES.                       01630044
           02  LEDGE    PIC  X(1)   VALUE '|'.                          01640000
           02  FILLER   PIC  X(40)  VALUE SPACES.                       01650000
           02  D1CON    PIC  X(19)  VALUE 'DATE      :'.                01660000
           02  D1VAR    PIC  X(17)  VALUE '  /  /     :  :'.            01670000
           02  TEMP-DATE REDEFINES D1VAR.                               01680000
               04  MM          PIC  X(2).                               01690000
               04  DATE-FILL1  PIC  X.                                  01700000
               04  DD          PIC  X(2).                               01710000
               04  DATE-FILL2  PIC  X.                                  01720000
               04  YY          PIC  X(2).                               01730000
			   04  FILLER      PIC  X.
			   04  DHH         PIC  X(2).
			   04  FILLER      PIC  X.
			   04  DMM         PIC  X(2).
			   04  FILLER      PIC  X.
			   04  DSS         PIC  X(2).
           02  FILLER   PIC  X(4)  VALUE SPACES.                        01740000
           02  REDGE    PIC  X(1)   VALUE '|'.                          01750000
           02  FILLER   PIC  X(34)  VALUE SPACES.                       01760044
       01  OUTLINE5.                                                    01770000
           02  ANSI     PIC  X.                                         01780000
           02  HFILLER  PIC  X(04)  VALUE SPACES.                       01790044
           02  LEDGE    PIC  X(1)   VALUE '|'.                          01800000
           02  DFILL2   PIC  X(10)  VALUE SPACES.                       01810000
           02  D2CON1   PIC  X(24).                                     01820000
           02  D2VAR    PIC  X(10).                                     01830000
           02  DFILL2A  PIC  X(23)  VALUE SPACES.                       01840000
           02  D2CON2   PIC  X(7)   VALUE SPACES.                       01850000
           02  FILLER   PIC  X(6)   VALUE SPACES.                       01860000
           02  REDGE    PIC  X(1)   VALUE '|'.                          01870000
           02  FILLER   PIC  X(34)  VALUE SPACES.                       01880044
       01  OUTLINE6.                                                    01890000
           02  ANSI     PIC  X.                                         01900000
           02  HFILLER  PIC  X(04)  VALUE SPACES.                       01910044
           02  LEDGE    PIC  X(1)   VALUE '|'.                          01920000
           02  DFILL3   PIC  X(59)  VALUE SPACES.                       01930000
           02  D3CON    PIC  X(17).                                     01940000
           02  FILLER   PIC  X(4)   VALUE SPACES.                       01950000
           02  REDGE    PIC  X(1)   VALUE '|'.                          01960000
           02  FILLER   PIC  X(34)  VALUE SPACES.                       01970044
       01  OUTLINE7.                                                    01980000
           02  ANSI     PIC  X.                                         01990000
           02  HFILLER  PIC  X(04)  VALUE SPACES.                       02000044
           02  LEDGE    PIC  X(1)   VALUE '|'.                          02010000
           02  DFILL4   PIC  X(10)  VALUE SPACES.                       02020000
           02  D4VAR1   PIC  X(40).                                     02030000
           02  DFILL4A  PIC  X(10)  VALUE SPACES.                       02040000
           02  D4CON    PIC  X(12)  VALUE 'SEGMENT# :  '.               02050000
           02  D4VAR2   PIC  X(4).                                      02060000
           02  FILLER   PIC  X(4)   VALUE SPACES.                       02070000
           02  REDGE    PIC  X(1)   VALUE '|'.                          02080000
           02  FILLER   PIC  X(34)  VALUE SPACES.                       02090044
                                                                        02100000
      * DATA AREA FOR TERMINAL INPUT                                    02110000
                                                                        02120000
       01  INPUT-AREA.                                                  02130000
           02  IN-BLANK  PIC  X(80) VALUE SPACES.                       02140000
           02  IN-TEXT REDEFINES IN-BLANK.                              02150000
               03  IN-COMMAND    PIC  X(8).                             02160000
               03  TEMP-COMMAND REDEFINES IN-COMMAND.                   02170000
                   04  TEMP-IOCMD PIC  X(3).                            02180000
                   04  FILLER     PIC  X(5).                            02190000
               03  IN-LAST-NAME  PIC  X(10).                            02200000
               03  IN-FIRST-NAME PIC  X(10).                            02210000
               03  IN-EXTENSION  PIC  X(10).                            02220000
               03  IN-ZIP-CODE   PIC  X(7).                             02230000
               03  INFILL        PIC  X(35).                            02240000
                                                                        02250000
      * DATA AREA OUTPUT                                                02260000
                                                                        02270000
       01  OUTPUT-AREA.                                                 02280000
           02  OUT-BLANK  PIC  X(85) VALUE SPACES.                      02290000
           02  OUT-TEXT REDEFINES OUT-BLANK.                            02300000
               03  OUT-MESSAGE   PIC  X(40).                            02310000
               03  OUT-COMMAND   PIC  X(8).                             02320000
               03  OUT-DATA.                                            02330000
                   04  OUT-LAST-NAME   PIC  X(10).                      02340000
                   04  OUT-FIRST-NAME  PIC  X(10).                      02350000
                   04  OUT-EXTENSION   PIC  X(10).                      02360000
                   04  OUT-ZIP-CODE    PIC  X(7).                       02370000
           02  OUT-SEGMENT-NO    PIC  9(4).                             02380000
           02  OUT-FILL          PIC  X(32) VALUE SPACES.               02390000
                                                                        02400000
      * I/O AREA FOR DATACASE HANDLING                                  02410000
                                                                        02420000
       01  IOAREA.                                                      02430000
           02  IO-BLANK  PIC  X(37) VALUE SPACES.                       02440000
           02  IO-DATA REDEFINES IO-BLANK.                              02450000
               03  IO-LAST-NAME   PIC  X(10).                           02460000
               03  IO-FIRST-NAME  PIC  X(10).                           02470000
               03  IO-EXTENSION   PIC  X(10).                           02480000
               03  IO-ZIP-CODE    PIC  X(7).                            02490000
           02  IO-FILLER    PIC  X(3) VALUE SPACES.                     02500000
           02  IO-COMMAND   PIC  X(8) VALUE SPACES.                     02510000
                                                                        02520000
      * GSAM TEXT FOR ERROR CALL                                        02530000
                                                                        02540000
       01  GS-TEXT.                                                     02550000
           02  GS-TEXT1           PIC  X(7)   VALUE 'STATUS '.          02560000
           02  GS-ERROR-STATUS    PIC  X(2).                            02570000
           02  GS-TEXT2           PIC  X(12)  VALUE 'GSAM CALL = '.     02580000
           02  GS-ERROR-CALL      PIC  X(4).                            02590000
                                                                        02600000
      * DC TEXT FOR ERROR CALL                                          02610000
                                                                        02620000
       01 DC-TEXT.                                                      02630000
          02  DC-TEXT1         PIC  X(7) VALUE 'STATUS '.               02640000
          02  DC-ERROR-STATUS  PIC  X(2).                               02650000
          02  DC-TEXT2         PIC  X(12) VALUE 'DLI  CALL = '.         02660000
          02  DC-ERROR-CALL    PIC  X(4).                               02670000
                                                                        02680000
       01  TEMPDATE.                                                    02690000
           02  TYY      PIC  99.                                        02700000
           02  TMM      PIC  99.                                        02710000
           02  TDD      PIC  99.                                        02720000
	   01  TEMPTIME.
	       02  TIHH      PIC 99.
		   02  TIMM      PIC 99.
		   02  TISS      PIC 99.
		   02  TIHS      PIC 99.
      *                                                                 02721016
       01  WS-CKPTFREQ.                                                 02722016
           03 FREQ      PIC  9(6)  VALUE ZERO.                          02723016
           03 FILLER    PIC  X(74) VALUE SPACES.                        02724018
                                                                        02730000
      * SEGMENT SEARCH ARGUMENT                                         02740000
                                                                        02750000
       01 SSA.                                                          02760000
          02  SEGMENT-NAME  PIC X(8)  VALUE 'RCADDR  '.                 02770000
          02  SEG-KEY-NAME  PIC X(11) VALUE '(RCADDR   ='.              02780000
          02  SSA-KEY       PIC X(10).                                  02790000
          02  FILLER        PIC X VALUE ')'.                            02800000
                                                                        02810000
       LINKAGE SECTION.                                                 02820000
                                                                        02830000
       01  IOPCB.                                                       02840000
           02  LTERM-NAME      PIC  X(8).                               02850000
           02  IO-RESERVE-IMS  PIC  X(2).                               02860000
           02  IO-STATUS       PIC  X(2).                               02870000
           02  CURR-DATE       PIC  X(4).                               02880000
           02  CURR-TIME       PIC  X(4).                               02890000
           02  IN-MSN          PIC  X(4).                               02900000
           02  MODNAME         PIC  X(8).                               02910000
           02  USERID          PIC  X(8).                               02920000
       01  DBPCB.                                                       02930000
           02  DBD-NAME        PIC  X(8).                               02940000
           02  SEG-LEVEL       PIC  X(2).                               02950000
           02  DBSTATUS        PIC  X(2).                               02960000
           02  PROC-OPTIONS    PIC  X(4).                               02970000
           02  RESERVE-DLI     PIC  X(4).                               02980000
           02  SEG-NAME-FB     PIC  X(8).                               02990000
           02  LENGTH-FB-KEY   PIC  9(4).                               03000000
           02  NUMB-SENS-SEGS  PIC  9(4).                               03010000
           02  KEY-FB-AREA     PIC  X(17).                              03020000
       01  GIPCB.                                                       03030000
           02  DBD-NAME        PIC  X(8).                               03040000
           02  SEG-LEVEL       PIC  X(2).                               03050000
           02  GI-STATUS       PIC  X(2).                               03060000
           02  PROC-OPTIONS    PIC  X(4).                               03070000
           02  RESERVE-DLI     PIC  X(4).                               03080000
           02  SEG-NAME-FB     PIC  X(8).                               03090000
           02  LENGTH-FB-KEY   PIC  9(4).                               03100000
           02  NUMB-SENS-SEGS  PIC  9(4).                               03110000
           02  KEY-FB-AREA     PIC  X(17).                              03120000
       01  GOPCB.                                                       03130000
           02  DBD-NAME        PIC  X(8).                               03140000
           02  SEG-LEVEL       PIC  X(2).                               03150000
           02  GO-STATUS       PIC  X(2).                               03160000
           02  PROC-OPTIONS    PIC  X(4).                               03170000
           02  RESERVE-DLI     PIC  x(4).                               03180000
           02  SEG-NAME-FB     PIC  X(8).                               03190000
           02  LENGTH-FB-KEY   PIC  9(4).                               03200000
           02  NUMB-SENS-SEGS  PIC  9(4).                               03210000
           02  KEY-FB-AREA     PIC  X(17).                              03220000
      *                                                                 03232002
       PROCEDURE DIVISION USING IOPCB, DBPCB, GIPCB, GOPCB.             03240000
                                                                        03250000
      * ON ENTRY IMS PASSES ADDRESSES FOR IOPCB, DBPCB, GIPCB AND GOPCB 03260000
                                                                        03270000
       MAIN-RTN.                                                        03280000
                                                                        03281013
           DISPLAY 'WE ARE IN PRCD003'                                  03290001
           MOVE IOPCB TO IO-PCB.                                        03291013
           MOVE 0     TO SET-DATA-FLAG.                                 03300044
           MOVE 0     TO TADD-FLAG.                                     03310044
           MOVE 0     TO CHKPT-COUNT.                                   03310144
           INITIALIZE CHKPT-PAR.                                        03310244
           DISPLAY    'FOR   XRST'                                      03311044
           PERFORM    CHKPT-XRST.                                       03312044
           DISPLAY    'AFTER XRST STATUS:' GI-STATUS                    03330144
           DISPLAY    'IOPCB: ' IOPCB                                   03330244
           DISPLAY    'FOR   READ-CHKPT-FREQ'                           03330344
           PERFORM    READ-CHKPT-FREQ.                                  03330444
           DISPLAY    'AFTER READ-CHKPT-FREQ:' FREQ                     03330544
      *                                                                 03330601
           MOVE GET-NEXT TO GS-ERROR-CALL.                              03331001
           CALL 'CBLTDLI' USING GET-NEXT, GIPCB, INPUT-AREA.            03332001
       READ-INPUT.                                                      03340000
           IF GI-STATUS = END-OF-DATABASE GOBACK.                       03350000
           IF GI-STATUS NOT EQUAL SPACES                                03360000
              PERFORM GSAM-ERROR                                        03370000
           ELSE                                                         03380000
              PERFORM PROCESS-INPUT THRU PROCESS-INPUT-END.             03390000
           ADD 1 TO CHKPT-COUNT.                                        03391025
           PERFORM CHECKPOINT.                                          03392025
           MOVE GET-NEXT TO GS-ERROR-CALL.                              03400000
           CALL 'CBLTDLI' USING GET-NEXT, GIPCB, INPUT-AREA.            03410000
           GO TO READ-INPUT.                                            03420000
                                                                        03430000
      * PROCEDURE PROCESS-INPUT                                         03440000
                                                                        03450000
       PROCESS-INPUT.                                                   03460000
                                                                        03470000
           MOVE SPACES TO OUT-BLANK.                                    03480000
           MOVE SPACES TO IO-BLANK.                                     03490000
           DISPLAY '**P-INPUT:' IN-TEXT.                                03491027
                                                                        03500000
      *    CHECK THE LEADING SPACE IN INPUT COMMAND AND TRIM IT OFF     03510000
                                                                        03520000
           INSPECT IN-COMMAND TALLYING L-SPACE-CTR FOR LEADING SPACE    03530000
             REPLACING LEADING SPACE BY '*'.                            03540000
           IF L-SPACE-CTR > 0                                           03550000
             UNSTRING IN-COMMAND DELIMITED BY ALL '*' INTO TEMP-ONE     03560000
               TEMP-TWO                                                 03570000
             MOVE TEMP-TWO TO IN-COMMAND                                03580000
             MOVE 0 TO L-SPACE-CTR                                      03590000
             MOVE SPACES TO TEMP-TWO.                                   03600000
                                                                        03610000
      *    CHECK THE LEADING SPACE IN INPUT LAST NAME AND TRIM IT OFF   03620000
                                                                        03630000
           INSPECT IN-LAST-NAME TALLYING L-SPACE-CTR FOR LEADING        03640000
             SPACE REPLACING LEADING SPACE BY '*'.                      03650000
           IF L-SPACE-CTR > 0                                           03660000
             UNSTRING IN-LAST-NAME DELIMITED BY ALL '*' INTO TEMP-ONE   03670000
               TEMP-TWO                                                 03680000
             MOVE TEMP-TWO TO IN-LAST-NAME                              03690000
             MOVE 0 TO L-SPACE-CTR                                      03700000
             MOVE SPACES TO TEMP-TWO.                                   03710000
                                                                        03720000
      *    CHECK THE LEADING SPACE IN INPUT FIRST NAME AND TRIM IT OFF  03730000
                                                                        03740000
           INSPECT IN-FIRST-NAME TALLYING L-SPACE-CTR FOR LEADING       03750000
             SPACE REPLACING LEADING SPACE BY '*'.                      03760000
           IF L-SPACE-CTR > 0                                           03770000
             UNSTRING IN-FIRST-NAME DELIMITED BY ALL '*' INTO TEMP-ONE  03780000
               TEMP-TWO                                                 03790000
             MOVE TEMP-TWO TO IN-FIRST-NAME                             03800000
             MOVE 0 TO L-SPACE-CTR                                      03810000
             MOVE SPACES TO TEMP-TWO.                                   03820000
                                                                        03830000
      *    CHECK THE LEADING SPACE IN INPUT EXTENSION AND TRIM IT OFF   03840000
                                                                        03850000
           INSPECT IN-EXTENSION TALLYING L-SPACE-CTR FOR LEADING        03860000
             SPACE REPLACING LEADING SPACE BY '*'.                      03870000
           IF L-SPACE-CTR > 0                                           03880000
             UNSTRING IN-EXTENSION DELIMITED BY ALL '*' INTO TEMP-ONE   03890000
               TEMP-TWO                                                 03900000
             MOVE TEMP-TWO TO IN-EXTENSION                              03910000
             MOVE 0 TO L-SPACE-CTR                                      03920000
             MOVE SPACES TO TEMP-TWO.                                   03930000
                                                                        03940000
      *    CHECK THE LEADING SPACE IN INPUT ZIP CODE AND TRIM IT OFF    03950000
                                                                        03960000
           INSPECT IN-ZIP-CODE TALLYING L-SPACE-CTR FOR LEADING SPACE   03970000
             REPLACING LEADING SPACE BY '*'.                            03980000
           IF L-SPACE-CTR > 0                                           03990000
             UNSTRING IN-ZIP-CODE DELIMITED BY ALL '*' INTO TEMP-ONE    04000000
               TEMP-TWO                                                 04010000
             MOVE TEMP-TWO TO IN-ZIP-CODE                               04020000
             MOVE 0 TO L-SPACE-CTR                                      04030000
             MOVE SPACES TO TEMP-TWO.                                   04040000
      *                                                                 04050000
           MOVE IN-LAST-NAME TO IO-LAST-NAME.                           04060000
           MOVE IN-COMMAND TO IO-COMMAND.                               04070000
                                                                        04080000
           IF TEMP-IOCMD   EQUAL 'ABE' THEN                             04081030
                PERFORM ABEND-IO THRU ABEND-IO-EX                       04082027
           END-IF                                                       04083027
           IF IO-COMMAND EQUAL SPACES                                   04090000
           THEN MOVE MINV TO OUT-MESSAGE                                04100000
                PERFORM ISRT-IO THRU ISRT-IO-END                        04110000
           ELSE IF IO-LAST-NAME EQUAL SPACES                            04120000
                THEN MOVE MNONAME TO OUT-MESSAGE                        04130000
                     PERFORM ISRT-IO THRU ISRT-IO-END                   04140044
           ELSE IF TEMP-IOCMD EQUAL 'ADD'                               04150000
                THEN PERFORM TO-ADD THRU TO-ADD-END                     04160000
           ELSE IF TEMP-IOCMD EQUAL 'TAD'                               04170000
                THEN MOVE 1 TO TADD-FLAG                                04180000
                     PERFORM TO-ADD THRU TO-ADD-END                     04190044
           ELSE IF TEMP-IOCMD EQUAL 'UPD'                               04200000
                THEN PERFORM TO-UPD THRU TO-UPD-END                     04210000
           ELSE IF TEMP-IOCMD EQUAL 'DEL'                               04220000
                THEN PERFORM TO-DEL THRU TO-DEL-END                     04230000
           ELSE IF TEMP-IOCMD EQUAL 'DIS'                               04240000
                THEN PERFORM TO-DIS THRU TO-DIS-END                     04250000
           ELSE                                                         04260000
               MOVE IN-COMMAND   TO OUT-COMMAND                         04270044
               MOVE IN-LAST-NAME TO OUT-LAST-NAME                       04280000
               MOVE MINV         TO OUT-MESSAGE                         04290044
               PERFORM ISRT-IO THRU ISRT-IO-END.                        04300000
       PROCESS-INPUT-END.                                               04310000
           EXIT.                                                        04320000
                                                                        04330000
      * PROCEDURE GSAM-ERROR                                            04340000
                                                                        04350000
       GSAM-ERROR.                                                      04360000
           MOVE GI-STATUS TO GS-ERROR-STATUS.                           04370000
           DISPLAY GS-TEXT1, GS-ERROR-STATUS, GS-TEXT2,                 04380000
                   GS-ERROR-CALL UPON CONSOLE                           04390000
           GOBACK.                                                      04400000
                                                                        04410000
      * PROCEDURE TO-ADD : ADDITION REQUEST HANDLER                     04420000
                                                                        04430000
       TO-ADD.                                                          04440000
           MOVE IN-FIRST-NAME TO IO-FIRST-NAME.                         04450000
           MOVE IN-EXTENSION  TO IO-EXTENSION.                          04460000
           MOVE IN-ZIP-CODE   TO IO-ZIP-CODE.                           04470000
           MOVE IO-DATA       TO OUT-DATA.                              04480000
           MOVE IO-COMMAND    TO OUT-COMMAND.                           04490000
           IF IN-FIRST-NAME EQUAL SPACES OR                             04500000
              IN-EXTENSION  EQUAL SPACES OR                             04510044
              IN-ZIP-CODE   EQUAL SPACES                                04520044
           THEN                                                         04530000
              MOVE MMORE TO OUT-MESSAGE                                 04540000
              PERFORM ISRT-IO THRU ISRT-IO-END                          04550000
           ELSE                                                         04560000
              PERFORM ISRT-DB THRU ISRT-DB-END.                         04570000
       TO-ADD-END.                                                      04580000
           EXIT.                                                        04590000
                                                                        04600000
      * PROCEDURE TO-UPD : UPDATE REQUEST HANDLER                       04610000
                                                                        04620000
       TO-UPD.                                                          04630000
           MOVE 0            TO SET-DATA-FLAG.                          04640044
           MOVE IN-COMMAND   TO OUT-COMMAND.                            04650044
           MOVE IN-LAST-NAME TO OUT-LAST-NAME.                          04660000
           MOVE IO-LAST-NAME TO SSA-KEY.                                04670000
           PERFORM GET-HOLD-UNIQUE-DB THRU GET-HOLD-UNIQUE-DB-END.      04680000
           IF DBSTATUS = SPACES                                         04690000
           THEN                                                         04700000
             IF IN-FIRST-NAME NOT = SPACES                              04710000
               MOVE 1             TO SET-DATA-FLAG                      04720044
               MOVE IN-FIRST-NAME TO IO-FIRST-NAME                      04730000
             END-IF                                                     04740000
             IF IN-EXTENSION  NOT = SPACES                              04750000
               MOVE 1             TO SET-DATA-FLAG                      04760044
               MOVE IN-EXTENSION  TO IO-EXTENSION                       04770000
             END-IF                                                     04780000
             IF IN-ZIP-CODE   NOT = SPACES                              04790000
               MOVE 1             TO SET-DATA-FLAG                      04800044
               MOVE IN-ZIP-CODE   TO IO-ZIP-CODE                        04810000
             END-IF                                                     04820000
             MOVE IO-DATA         TO OUT-DATA.                          04830044
             MOVE IO-COMMAND      TO OUT-COMMAND.                       04840044
             IF NO-SET-DATA                                             04850000
             THEN                                                       04860000
               PERFORM REPL-DB THRU REPL-DB-END                         04870000
             ELSE                                                       04880000
               MOVE MNODATA       TO OUT-MESSAGE                        04890044
               PERFORM ISRT-IO THRU ISRT-IO-END.                        04900000
       TO-UPD-END.                                                      04910000
           EXIT.                                                        04920000
                                                                        04930000
      * PROCEDURE TO-DEL : DELETE REQUEST HANDLER                       04940000
                                                                        04950000
       TO-DEL.                                                          04960000
           MOVE IO-LAST-NAME TO SSA-KEY.                                04970000
           PERFORM GET-HOLD-UNIQUE-DB THRU GET-HOLD-UNIQUE-DB-END.      04980000
           IF DBSTATUS = SPACES                                         04990000
           THEN                                                         05000000
              MOVE IO-DATA    TO OUT-DATA                               05010044
              MOVE IO-COMMAND TO OUT-COMMAND                            05020000
              PERFORM DLET-DB THRU DLET-DB-END.                         05030000
       TO-DEL-END.                                                      05040000
           EXIT.                                                        05050000
                                                                        05060000
      * PROCEDURE TO-DIS : DISPLAY REQUEST HANDLER                      05070000
                                                                        05080000
       TO-DIS.                                                          05090000
           MOVE IN-COMMAND   TO OUT-COMMAND.                            05100044
           MOVE IN-LAST-NAME TO OUT-LAST-NAME.                          05110000
           MOVE IO-LAST-NAME TO SSA-KEY.                                05120000
           PERFORM GET-UNIQUE-DB THRU GET-UNIQUE-DB-END.                05130000
           IF DBSTATUS = SPACES                                         05140000
           THEN                                                         05150000
              MOVE IO-DATA    TO OUT-DATA                               05160044
              MOVE IO-COMMAND TO OUT-COMMAND                            05170000
              MOVE MDIS       TO OUT-MESSAGE                            05180044
              PERFORM ISRT-IO THRU ISRT-IO-END.                         05190000
       TO-DIS-END.                                                      05200000
           EXIT.                                                        05210000
                                                                        05220000
      * PROCEDURE ISRT-DB : DATA BASE SEGMENT INSERT REQUEST HANDLER    05230000
                                                                        05240000
       ISRT-DB.                                                         05250000
           MOVE ISRT TO DC-ERROR-CALL.                                  05260000
           CALL 'CBLTDLI' USING ISRT, DBPCB, IOAREA, SSA1               05270000
           IF DBSTATUS   = SPACES                                       05280000
           THEN                                                         05290000
              IF PROCESS-TADD                                           05300000
                 DISPLAY 'INSERT IS DONE, no REPLY nec' UPON CONSOLE    05310023
      *          ACCEPT REPLY FROM CONSOLE                              05320023
                 MOVE 0 TO TADD-FLAG                                    05330000
              END-IF                                                    05340000
              MOVE MADD TO OUT-MESSAGE                                  05350000
              PERFORM ISRT-IO THRU ISRT-IO-END                          05360000
           ELSE                                                         05370000
              DISPLAY "EXISTING SEGMENT " DBSTATUS
              MOVE MISRTE   TO OUT-MESSAGE                              05380044
              MOVE DBSTATUS TO DC-ERROR-STATUS                          05390000
              PERFORM ISRT-IO THRU ISRT-IO-END.                         05400000
       ISRT-DB-END.                                                     05410000
           EXIT.                                                        05420000
                                                                        05430000
      * PROCEDURE GET-UNIQUE-DB                                         05440000
      *    DATA BASE SEGMENT GET-UNIQUE-DB REQUEST HANDLER              05450000
                                                                        05460000
       GET-UNIQUE-DB.                                                   05470000
           MOVE GET-UNIQUE TO DC-ERROR-CALL.                            05480000
           CALL 'CBLTDLI' USING GET-UNIQUE, DBPCB, IOAREA, SSA.         05490000
           IF DBSTATUS NOT = SPACES                                     05500000
           THEN                                                         05510000
              MOVE MNOENT   TO OUT-MESSAGE                              05520044
              MOVE DBSTATUS TO DC-ERROR-STATUS                          05530000
              PERFORM ISRT-IO THRU ISRT-IO-END.                         05540000
       GET-UNIQUE-DB-END.                                               05550000
           EXIT.                                                        05560000
                                                                        05570000
      * PROCEDURE GET-HOLD-UNIQUE-DB                                    05580000
      *    DATA BASE SEGMENT GET-HOLD-UNIQUE-DB REQUEST HANDLER         05590000
                                                                        05600000
       GET-HOLD-UNIQUE-DB.                                              05610000
           MOVE GET-HOLD-UNIQUE TO DC-ERROR-CALL.                       05620000
           CALL 'CBLTDLI' USING GET-HOLD-UNIQUE, DBPCB, IOAREA, SSA.    05630000
           IF DBSTATUS NOT = SPACES                                     05640000
           THEN                                                         05650000
              MOVE MNOENT   TO OUT-MESSAGE                              05660044
              MOVE DBSTATUS TO DC-ERROR-STATUS                          05670000
              PERFORM ISRT-IO THRU ISRT-IO-END.                         05680000
       GET-HOLD-UNIQUE-DB-END.                                          05690000
           EXIT.                                                        05700000
                                                                        05710000
      * PROCEDURE REPL-DB : DATA BASE SEGMENT REPLACE REQUEST HANDLER   05720000
                                                                        05730000
       REPL-DB.                                                         05740000
           MOVE REPL TO DC-ERROR-CALL.                                  05750000
           CALL 'CBLTDLI' USING REPL, DBPCB, IOAREA.                    05760000
           IF DBSTATUS = SPACES                                         05770000
           THEN                                                         05780000
              MOVE MUPD1 TO OUT-MESSAGE                                 05790000
              PERFORM ISRT-IO THRU ISRT-IO-END                          05800000
           ELSE                                                         05810000
              MOVE MREPLE   TO OUT-MESSAGE                              05820044
              MOVE DBSTATUS TO DC-ERROR-STATUS                          05830000
              PERFORM ISRT-IO THRU ISRT-IO-END.                         05840000
       REPL-DB-END.                                                     05850000
           EXIT.                                                        05860000
                                                                        05870000
      * PROCEDURE DLET-DB : DATA BASE SEGMENT DELETE REQUEST HANDLER    05880000
                                                                        05890000
       DLET-DB.                                                         05900000
           MOVE DLET TO DC-ERROR-CALL.                                  05910000
           CALL 'CBLTDLI' USING DLET, DBPCB, IOAREA.                    05920000
           IF DBSTATUS = SPACES                                         05930000
           THEN                                                         05940000
              MOVE MDEL TO OUT-MESSAGE                                  05950000
              PERFORM ISRT-IO THRU ISRT-IO-END                          05960000
           ELSE                                                         05970000
              MOVE MDLETE   TO OUT-MESSAGE                              05980044
              MOVE DBSTATUS TO DC-ERROR-STATUS                          05990000
              PERFORM ISRT-IO THRU ISRT-IO-END.                         06000000
       DLET-DB-END.                                                     06010000
           EXIT.                                                        06020000
                                                                        06030000
                                                                        06040000
      * PROCEDURE ISRT-IO : FORMAT AND PRINT OUTPUT PAGE                06050000
                                                                        06060000
       ISRT-IO.                                                         06070000
           MOVE ISRT  TO GS-ERROR-CALL.                                 06080000
           ADD +1     TO OUT-SEGMENT-NO.                                06090044
           ACCEPT TEMPDATE FROM DATE.                                   06100000
		   ACCEPT TEMPTIME FROM TIME.
           PERFORM SETDATE.                                             06110000
                                                                        06120000
           MOVE 1 TO O-ANSI.                                            06130000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE1.                  06140000
           IF GO-STATUS NOT EQUAL SPACES                                06150000
              PERFORM GSAM-ERROR.                                       06160000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE2.                  06170000
           IF GO-STATUS NOT EQUAL SPACES                                06180000
              PERFORM GSAM-ERROR.                                       06190000
                                                                        06200000
           MOVE HEADER-BLOCK TO HDRLN.                                  06210000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE2.                  06220000
           IF GO-STATUS NOT EQUAL SPACES                                06230000
              PERFORM GSAM-ERROR.                                       06240000
           MOVE SPACES TO HDRLN.                                        06250000
                                                                        06260000
           MOVE HEADER-NAME TO HDRLN.                                   06270000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE2.                  06280000
           IF GO-STATUS NOT EQUAL SPACES                                06290000
              PERFORM GSAM-ERROR.                                       06300000
           MOVE SPACES TO HDRLN.                                        06310000
                                                                        06320000
           MOVE HEADER-BLOCK TO HDRLN.                                  06330000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE2.                  06340000
           IF GO-STATUS NOT EQUAL SPACES                                06350000
              PERFORM GSAM-ERROR.                                       06360000
           MOVE SPACES TO HDRLN.                                        06370000
                                                                        06380000
      *    CALL 'CBLTDLI' USING ISRT, GOPCB, BLANKLINE.                 06390044
      *    IF GO-STATUS NOT EQUAL SPACES                                06400044
      *       PERFORM GSAM-ERROR.                                       06410044
                                                                        06420000
      *    CALL 'CBLTDLI' USING ISRT, GOPCB, BLANKLINE.                 06430044
      *    IF GO-STATUS NOT EQUAL SPACES                                06440044
      *       PERFORM GSAM-ERROR.                                       06450044
                                                                        06460000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE3.                  06470000
           IF GO-STATUS NOT EQUAL SPACES                                06480000
              PERFORM GSAM-ERROR.                                       06490000
                                                                        06500000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE4.                  06510000
           IF GO-STATUS NOT EQUAL SPACES                                06520000
              PERFORM GSAM-ERROR.                                       06530000
                                                                        06540000
      *    CALL 'CBLTDLI' USING ISRT, GOPCB, BLANKLINE.                 06550044
      *    IF GO-STATUS NOT EQUAL SPACES                                06560044
      *       PERFORM GSAM-ERROR.                                       06570044
           MOVE CONSTANT1 TO D2CON1.                                    06580000
           MOVE OUT-COMMAND TO D2VAR.                                   06590000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE5.                  06600000
           IF GO-STATUS NOT EQUAL SPACES                                06610000
              PERFORM GSAM-ERROR.                                       06620000
           MOVE SPACES TO D2CON1.                                       06630000
           MOVE SPACES TO D2VAR.                                        06640000
                                                                        06650000
           MOVE CONSTANT6 TO D3CON.                                     06660000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE6.                  06670000
           IF GO-STATUS NOT EQUAL SPACES                                06680000
              PERFORM GSAM-ERROR.                                       06690000
                                                                        06700000
           MOVE CONSTANT2     TO D2CON1.                                06710044
           MOVE OUT-LAST-NAME TO D2VAR.                                 06720000
           MOVE CONSTANT7     TO D2CON2.                                06730044
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE5.                  06740000
           IF GO-STATUS NOT EQUAL SPACES                                06750000
              PERFORM GSAM-ERROR.                                       06760000
           MOVE SPACES TO D2CON1.                                       06770000
           MOVE SPACES TO D2VAR.                                        06780000
           MOVE SPACES TO D2CON2.                                       06790000
                                                                        06800000
           MOVE CONSTANT8 TO D2CON2.                                    06810000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE5.                  06820000
           IF GO-STATUS NOT EQUAL SPACES                                06830000
              PERFORM GSAM-ERROR.                                       06840000
           MOVE SPACES TO D2CON2.                                       06850000
                                                                        06860000
           MOVE CONSTANT3      TO D2CON1.                               06870044
           MOVE OUT-FIRST-NAME TO D2VAR.                                06880000
           MOVE CONSTANT9      TO D2CON2.                               06890044
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE5.                  06900000
           IF GO-STATUS NOT EQUAL SPACES                                06910000
              PERFORM GSAM-ERROR.                                       06920000
           MOVE SPACES TO D2CON1.                                       06930000
           MOVE SPACES TO D2VAR.                                        06940000
           MOVE SPACES TO D2CON2.                                       06950000
                                                                        06960000
           MOVE CONSTANT10 TO D2CON2.                                   06970000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE5.                  06980000
           IF GO-STATUS NOT EQUAL SPACES                                06990000
              PERFORM GSAM-ERROR.                                       07000000
           MOVE SPACES TO D2CON2.                                       07010000
                                                                        07020000
           MOVE CONSTANT4     TO D2CON1.                                07030044
           MOVE OUT-EXTENSION TO D2VAR.                                 07040000
           MOVE CONSTANT11    TO D2CON2.                                07050044
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE5.                  07060000
           IF GO-STATUS NOT EQUAL SPACES                                07070000
              PERFORM GSAM-ERROR.                                       07080000
           MOVE SPACES TO D2CON1.                                       07090000
           MOVE SPACES TO D2VAR.                                        07100000
           MOVE SPACES TO D2CON2.                                       07110000
                                                                        07120000
      *    CALL 'CBLTDLI' USING ISRT, GOPCB, BLANKLINE.                 07130045
      *    IF GO-STATUS NOT EQUAL SPACES                                07140045
      *       PERFORM GSAM-ERROR.                                       07150045
                                                                        07160000
           MOVE CONSTANT5    TO D2CON1.                                 07170044
           MOVE OUT-ZIP-CODE TO D2VAR.                                  07180000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE5.                  07190000
           IF GO-STATUS NOT EQUAL SPACES                                07200000
              PERFORM GSAM-ERROR.                                       07210000
           MOVE SPACES TO D2CON1.                                       07220000
           MOVE SPACES TO D2VAR.                                        07230000
                                                                        07240000
     *     CALL 'CBLTDLI' USING ISRT, GOPCB, BLANKLINE.                 07250045
     *     IF GO-STATUS NOT EQUAL SPACES                                07260045
     *        PERFORM GSAM-ERROR.                                       07270045
                                                                        07280000
     *     CALL 'CBLTDLI' USING ISRT, GOPCB, BLANKLINE.                 07290045
     *     IF GO-STATUS NOT EQUAL SPACES                                07300045
     *        PERFORM GSAM-ERROR.                                       07310045
                                                                        07320000
           MOVE OUT-MESSAGE    TO D4VAR1.                               07330044
           MOVE OUT-SEGMENT-NO TO D4VAR2.                               07340000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE7.                  07350000
           IF GO-STATUS NOT EQUAL SPACES                                07360000
              PERFORM GSAM-ERROR.                                       07370000
                                                                        07380000
      *    CALL 'CBLTDLI' USING ISRT, GOPCB, BLANKLINE.                 07390044
      *    IF GO-STATUS NOT EQUAL SPACES                                07400044
      *       PERFORM GSAM-ERROR.                                       07410044
                                                                        07420000
      *    CALL 'CBLTDLI' USING ISRT, GOPCB, BLANKLINE.                 07430044
      *    IF GO-STATUS NOT EQUAL SPACES                                07440044
      *       PERFORM GSAM-ERROR.                                       07450044
                                                                        07460000
      *    CALL 'CBLTDLI' USING ISRT, GOPCB, BLANKLINE.                 07470045
      *    IF GO-STATUS NOT EQUAL SPACES                                07480045
      *       PERFORM GSAM-ERROR.                                       07490045
                                                                        07500000
           MOVE 0 TO O-ANSI.                                            07510000
           CALL 'CBLTDLI' USING ISRT, GOPCB, OUTLINE1.                  07520000
           IF GO-STATUS NOT EQUAL SPACES                                07530000
              PERFORM GSAM-ERROR.                                       07540000
       ISRT-IO-END.                                                     07550000
           EXIT.                                                        07560000
                                                                        07570000
      * PROCEDURE SETDATE : SET THE DATE                                07580000
                                                                        07590000
       SETDATE.                                                         07600000
           MOVE TYY TO YY.                                              07610000
           MOVE TMM TO MM.                                              07620000
           MOVE TDD TO DD.                                              07630000
           MOVE TIHH TO DHH.
           MOVE TIMM TO DMM.
           MOVE TISS TO DSS.
      *     EXIT.                                                       07640000
      *                                                                 07641023
       ABEND-IO.                                                        07641227
           DISPLAY 'ABEND-IO NOW WE ABEND THIS JOB'                     07641327
           DISPLAY 'TO TEST THE CHKP RESTARTED:' RESTARTED              07641436
      *    NO WE TRY WITH A ZERO DIVIDE                                 07641528
           IF RESTARTED NOT = 'Y' THEN                                  07641643
              MOVE 0 TO ABEND-PACKED                                    07641736
              DIVIDE 5 BY ABEND-PACKED GIVING ABEND-PACKED              07641836
              DISPLAY 'AFTER ZERO DIVIDE WE SHOULD NEVER GET HERE'      07641936
      *       NO WE TRY WITH A 0C7 ABEND                                07642036
              MOVE LOW-VALUES TO ABEND-CHR                              07642136
              COMPUTE ABEND-PACKED = ABEND-WORK * -1                    07642236
              DISPLAY 'AFTER WORKING WITH PACKED FIELD.'                07642337
           ELSE                                                         07642536
              DISPLAY 'NO ABEND BECAUSE WE ARE IN RESTART'              07642636
           END-IF.                                                      07642737
       ABEND-IO-EX.                                                     07642827
           EXIT.                                                        07642929
      *                                                                 07643027
       CHECKPOINT.                                                      07643123
           DISPLAY 'CHECKPOINT FREQ:' FREQ ':CHKPT-FREQUENCY:'          07643224
                   CHKPT-FREQUENCY ':COUNT:' CHKPT-COUNT.               07643325
           IF  CHKPT-COUNT > CHKPT-FREQUENCY                            07643423
               MOVE ZERO TO CHKPT-COUNT                                 07643523
               MOVE 1    TO CHK-PAR-POLNR                               07643623
               MOVE 2    TO CHK-LES                                     07643723
               MOVE 3    TO CHK-DISP                                    07643823
               MOVE 4    TO CHK-VERARB                                  07643923
               PERFORM CHKPT-CHKP.                                      07644023
           DISPLAY 'CHECKPOINT CHKP-AREA:' CHKPT-AREA                   07644123
               ':COUNTER:' CHKPT-COUNT.                                 07644223
       CHECKPOINT-EX.                                                   07644323
           EXIT.                                                        07645023
                                                                        07650000
      *CHKPT-XRST SECTION.                                              07651009
       CHKPT-XRST.                                                      07652011
           MOVE        SPACE     TO CHKPT-XRST-AREA.                    07653004
           MOVE        +4096     TO CHKPT-AREA-LENGTH.                  07654004
           MOVE        +21       TO CHKPT-PAR-LEN.                      07655004
           DISPLAY 'FOR CALL XRST:' IOPCB                               07655133
           CALL        'CBLTDLI'  USING XRST                            07656004
                                        IOPCB                           07657013
                                        CHKPT-AREA-LENGTH               07658004
                                        CHKPT-XRST-AREA                 07659004
                                        CHKPT-PAR-LEN                   07659104
                                        CHKPT-PAR.                      07659204
           DISPLAY 'AFT CALL XRST:' IOPCB ':IO-STATCD:' IO-STATCD       07659341
           DISPLAY 'CHKPT-XRST-AREA:' CHKPT-XRST-AREA                   07659441
           MOVE IOPCB TO IO-PCB.                                        07659641
           IF          IO-STATCD NOT = SPACE                            07659741
              MOVE     'CHECKPOINT-XRST-ERROR: ' TO DISPL-TEXT          07659841
              MOVE     IO-STATCD                 TO DISPL-WERT          07660004
              PERFORM  GPDISPL-UP THRU GPDISPL-UP-EX                    07670004
              DISPLAY  'HELL'                                           07671012
      *       CALL     'HELL'.                                          07680012
              GOBACK.                                                   07690012
           IF CHKPT-XRST-AREA NOT = SPACE                               07690138
              MOVE CHKPT-XRST-AREA TO CHKPT-ID                          07690246
              MOVE 'Y' TO RESTARTED.                                    07690341
           DISPLAY 'CHKPT-XRST-AREA:' CHKPT-XRST-AREA                   07691038
              ':RESTARTED:' RESTARTED.                                  07692036
       CHKPT-XRST-EX.                                                   07700004
           EXIT.                                                        07710004
      *                                                                 07711009
       CHKPT-CHKP.                                                      07711123
           MOVE        +4096     TO CHKPT-AREA-LENGTH                   07711333
           MOVE        +21       TO CHKPT-PAR-LEN                       07711433
           MOVE        'D01'     TO CHKPT-ID-PROG                       07711533
           ADD         2         TO CHKPT-ID-TIME                       07711633
           MOVE        CHKPT-ID  TO CHKPT-AREA                          07711733
           CALL        'CBLTDLI'  USING CHKP                            07711823
                                        IOPCB                           07711923
                                        CHKPT-AREA-LENGTH               07712023
                                        CHKPT-AREA                      07712123
                                        CHKPT-PAR-LEN                   07712223
                                        CHKPT-PAR.                      07712323
           MOVE IOPCB TO IO-PCB.                                        07712423
           IF          IO-STATCD NOT = SPACE                            07712523
              MOVE     'CHECKPOINT-CHKP-ERROR: ' TO DISPL-TEXT          07712623
              MOVE     IO-STATCD                 TO DISPL-WERT          07712723
              PERFORM  GPDISPL-UP THRU GPDISPL-UP-EX                    07712823
              DISPLAY  'HELL'                                           07712923
      *       CALL     'HELL'.                                          07713023
              GOBACK.                                                   07713123
           DISPLAY 'CHKP-AREA:' CHKPT-AREA.                             07713239
      *     DISPLAY 'CHKP-PAR:' CHKPT-PAR.                              07713326
       CHKPT-CHKP-EX.                                                   07713426
      *                                                                 07713526
       GPDISPL-UP.                                                      07714010
           COPY GPDISPL.                                                07720009
       GPDISPL-UP-EX.                                                   07731010
           EXIT.                                                        07740009
      *                                                                 07741016
       READ-CHKPT-FREQ.                                                 07742016
      *    READ CHECKPOINT FREQ FILE . DEFAULT IS 2                     07742116
           OPEN        INPUT CKPTFREQ.                                  07742216
           READ        CKPTFREQ INTO WS-CKPTFREQ                        07742316
              AT END   DISPLAY 'RECORD NOT FOUND FREQ'                  07742418
                       MOVE 2 TO FREQ.                                  07742525
           MOVE        FREQ TO CHKPT-FREQUENCY.                         07742716
           CLOSE       CKPTFREQ.                                        07743016
           DISPLAY 'FREQ:' FREQ ':CHKPT-FREQUENCY:'                     07743124
                   CHKPT-FREQUENCY.                                     07743224
       READ-CHKPT-FREQ-EX.                                              07743316
           EXIT.                                                        07744016
