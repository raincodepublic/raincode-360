      *****************************************************************�
      * DIVERSER COPY    GWDISPL                                       *
      ******************************************************************
      *                                                                *
      * KURZBESCHR.  : DISPLAY-ZEILEN for JOBKONTROLLEN                *
      *                                                                *
      * INHALT       : KOPFZEILE,EINZELZEILE (FoR DIVERSE AUSGABEN)    *
      *                                                                *
      ******************************************************************
       01  GWDISPL.
           03 DISPL-KOPF                             VALUE SPACE.
              05 DISPL-PROG.
                 07 PROGNAME.
                    09 PROGNAME-MDT     PIC X.
                    09 PROGNAME-REST    PIC X(7).
                 07 PROGLMDT            PIC X(4).
              05 FILLER                 PIC X(3).
              05 DISPL-MESG.
                 07 KURZTEXT            PIC X(80).
              05 FILLER                 PIC X.
              05 DISPL-DATZEIT-KOPF     PIC X(19).
              05 FILLER                 PIC X(5).
      *
           03 DISPL-EINZEL                           VALUE SPACE.
              05 DISPL-WERT-X.
                 07 DISPL-WERT          PIC Z(17)9-.
              05 DISPL-WERT-R           REDEFINES
                 DISPL-WERT-X           PIC Z(14)9,99-.
              05 DISPL-WERT-PKT         REDEFINES
                 DISPL-WERT-X           PIC Z(17)9..
              05 FILLER                 PIC X.
              05 DISPL-TEXT             PIC X(75).
              05 FILLER                 PIC X.
              05 DISPL-DATZEIT-EINZEL   PIC X(19).
              05 FILLER                 PIC X.
              05 DISPL-NUMMER-X.
                 07 DISPL-NUMMER        PIC 9(4).
      *
           03 DISPL-STRICH              PIC X(120)   VALUE ALL '='.
           03 DISPL-LEER                PIC X(120)   VALUE SPACE.
      *
      *............... Hilfs-Felder ....................................
      *
           03 FILLER.
              05 DISPL-DAT              PIC 9(8)     VALUE ZEROS.
              05 DISPL-DAT-X            REDEFINES    DISPL-DAT.
                 07 DISPL-JH            PIC 99.
                 07 DISPL-JJ            PIC 99.
                 07 DISPL-MM            PIC 99.
                 07 DISPL-TT            PIC 99.
              05 DISPL-ZEIT-X.
                 07 DISPL-ST            PIC 99.
                 07 DISPL-MI            PIC 99.
                 07 DISPL-SE            PIC 99.
              05 DISPL-DATZEIT.
                 07 DISPL-JH            PIC 99.
                 07 DISPL-JJ            PIC 99.
                 07 FILLER              PIC X        VALUE '-'.
                 07 DISPL-MM            PIC 99.
                 07 FILLER              PIC X        VALUE '-'.
                 07 DISPL-TT            PIC 99.
                 07 FILLER              PIC X        VALUE '/'.
                 07 DISPL-ST            PIC 99.
                 07 FILLER              PIC X        VALUE ':'.
                 07 DISPL-MI            PIC 99.
                 07 FILLER              PIC X        VALUE ':'.
                 07 DISPL-SE            PIC 99.
              05 DISPL-DB2DATUM.
                 07 DISPL-JHJJ          PIC 9(4).
                 07 FILLER              PIC X(6).
      *
              05 DISPL-SCHALTER         PIC 9        VALUE ZEROS.
                 88 DISPL-ENDE        VALUE 9.
