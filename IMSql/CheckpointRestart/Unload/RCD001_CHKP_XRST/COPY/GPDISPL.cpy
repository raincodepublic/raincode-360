      *****************************************************************�
      *                                                                *
      * KURZBESCHR.  : AUSGABE DER ZEILEN AUS GWDISPL                  *
      *                                                                *
      * INHALT       : PROGNAME und KURZTEXT m�ssen vorbelegt werden.  *
      *                Die nachfolgenden Einzelzeilen k�nnen           *
      *                individuell gestaltet werden.                   *
      *                Beim ersten Aufruf (DISPL-SCHALTER = 0)         *
      *                wird eine Kopfzeile geschrieben, danach         *
      *                erfolgt eine Umschaltung auf Einzelzeilen.      *
      *                                                                *
      ******************************************************************
           IF          DISPL-DAT           <= ZEROS
                 OR    DISPL-ENDE
              ACCEPT   DISPL-DAT         FROM DATE YYYYMMDD
              ACCEPT   DISPL-ZEIT-X      FROM TIME
              IF       DISPL-ZEIT-X (1:4)  =  ZEROS
                       ACCEPT DISPL-DAT  FROM DATE YYYYMMDD
              END-IF
              MOVE     CORR
                       DISPL-DAT-X         TO DISPL-DATZEIT
              MOVE     CORR
                       DISPL-ZEIT-X        TO DISPL-DATZEIT
              MOVE     DISPL-DATZEIT       TO DISPL-DATZEIT-KOPF
                                              DISPL-DATZEIT-EINZEL
              MOVE     RETURN-CODE         TO DISPL-JHJJ
      *       CALL    'DATUMSIM'        USING DISPL-DATZEIT
              MOVE     DISPL-JHJJ          TO RETURN-CODE
              IF       DISPL-DATZEIT  NOT  =  DISPL-DATZEIT-EINZEL
                 MOVE  DISPL-DATZEIT  (1:) TO DISPL-DAT-X    (1:)
                 MOVE  DISPL-DATZEIT  (6:) TO DISPL-DAT-X    (5:)
                 MOVE  DISPL-DATZEIT  (9:) TO DISPL-DAT-X    (7:)
                 MOVE  DISPL-DATZEIT (12:) TO DISPL-ZEIT-X   (1:)
                 MOVE  DISPL-DATZEIT (15:) TO DISPL-ZEIT-X   (3:)
                 MOVE  DISPL-DATZEIT (18:) TO DISPL-ZEIT-X   (5:)
              END-IF
              MOVE     DISPL-DATZEIT       TO DISPL-DB2DATUM
           END-IF.
      *
           EVALUATE    TRUE
            WHEN       0                   =  DISPL-SCHALTER
              DISPLAY  DISPL-LEER
              DISPLAY  DISPL-KOPF
            WHEN       DISPL-ENDE
              DISPLAY  DISPL-STRICH
              MOVE     SPACE               TO DISPL-EINZEL(1:96)
              MOVE     SPACE               TO DISPL-EINZEL(116:)
              MOVE    'PROGRAMM BEENDET:'  TO DISPL-TEXT
              DISPLAY  DISPL-EINZEL
            WHEN       OTHER
              DISPLAY  DISPL-EINZEL
           END-EVALUATE.
           IF         '20'                 =  DISPL-DATZEIT-EINZEL(1:2)
                 AND  (DISPL-DATZEIT  NOT  =  DISPL-DATZEIT-EINZEL)
              MOVE     DISPL-DATZEIT       TO DISPL-DATZEIT-EINZEL
                                              DISPL-DATZEIT-KOPF
              MOVE     SPACE               TO DISPL-WERT-X
                                              DISPL-TEXT
              MOVE    'simuliert:'         TO DISPL-TEXT (66:)
              DISPLAY  DISPL-EINZEL
           END-IF.
           IF          0                   =  DISPL-SCHALTER
              MOVE     1                   TO DISPL-SCHALTER
              DISPLAY  DISPL-STRICH
           END-IF.
      *
           MOVE        SPACE               TO DISPL-EINZEL(1:116).
