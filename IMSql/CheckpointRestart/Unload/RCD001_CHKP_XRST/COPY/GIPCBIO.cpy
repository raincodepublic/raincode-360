      *****************************************************************�
      *                                                                *
      * KURZBESCHR.  : MASKE F�R IMS-IO-PCB                            *
      *                                                                *
      ******************************************************************
       01  IO-PCB.
           03 IO-TERM.
              05 IO-TERM-1         PIC X(4).
              05 IO-TERM-NR        PIC X(4).
           03 IO-RESERVED          PIC XX.
           03 IO-STATCD            PIC XX.
              88 IO-SUCCESSFUL         VALUE '  '.
           03 IO-DATE              PIC 9(7)    COMP-3.
           03 IO-TIME              PIC 9(7)    COMP-3.
           03 IO-MSGNR             PIC S9(8)   COMP.
           03 IO-MODNAME           PIC X(8).
           03 IO-USERID            PIC X(8).
      *
