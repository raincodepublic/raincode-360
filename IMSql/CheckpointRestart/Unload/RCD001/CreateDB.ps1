#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline_Config"
)

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

$ErrorActionPreference = "Stop"

$MasterConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog "master"

$DbConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog $DbName

$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output"
$OutputBinDir = Join-Path -Path $PSScriptRoot -ChildPath "Output/bin"

$proc = Start-ImsProcess -Program "IMSql.DbGenerator" -ArgumentList `
    ":LogLevel=TRACE", ":DbName=$DbName", `
    ":Files=$(Join-Path -Path $PSScriptRoot -ChildPath "RCD001.DBD")", `
    ":OutputDir=$OutputDir", "-SqlDatabase=""$DbConnectionString"""
if ($proc.ExitCode -ne 0) {
    Write-Error "IMSql.DbGenerator failed for RCD001.DBD with code $($proc.ExitCode)."
}
#Generate DBD Xml in a seperate call since we need this in bin directory for execution
$proc = Start-ImsProcess -Program "IMSql.DbGenerator" -ArgumentList `
    ":LogLevel=TRACE", ":DbName=RCD001G", `
    ":Files=$(Join-Path -Path $PSScriptRoot -ChildPath "RCD001G.DBD")", `
    ":OutputDir=$OutputBinDir", "-SqlDatabase=""$DbConnectionString"""
if ($proc.ExitCode -ne 0) {
    Write-Error "IMSql.DbGenerator failed for RCD001G.DBD with code $($proc.ExitCode)."
}
Write-Host "   *** Finished Creating DATA Sql script."

Invoke-RobustSqlcmd -ConnectionString $MasterConnectionString `
    -InputFile (Join-Path -Path "Output" -ChildPath "$DbName.sql")
Write-Host "   *** Finished creating DB : $DbName."
