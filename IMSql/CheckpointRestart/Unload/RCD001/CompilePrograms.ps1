#!/usr/bin/env pwsh

$ErrorActionPreference = "Stop"

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output/bin"

if (!(Test-Path -Path $OutputDir )) {
     New-Item -ItemType "Directory" -Path $OutputDir
}

$DemoDir = Join-Path -Path $PSScriptRoot -ChildPath "../RCD001_CHKP_XRST/COBOL"
$IncludeDir = Join-Path -Path $PSScriptRoot -ChildPath "../RCD001_CHKP_XRST/COPY"
Get-ChildItem $DemoDir -Recurse -Filter "*.cbl"  | ForEach-Object {
     Write-Output $_.FullName
     $proc = Start-RcdirProcess -Program "CobRc" -ArgumentList ":MaxMem=1G", ":Target=NetCore", ":OutputDir=`"$OutputDir`"", ":MaxMem=1G", ":IncludeSearchDir=`"$IncludeDir`"", $_.FullName
     if ($proc.ExitCode -ne 0) {
          Write-Error "CobRc failed with code $($proc.ExitCode)."
     }
}
