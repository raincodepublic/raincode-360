#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline"
)

$ErrorActionPreference = "Stop"

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output/bin"

if (!(Test-Path -Path $OutputDir)) {
    New-Item -ItemType "Directory" -Path $OutputDir
}

$ConfigConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog "IMSQL_DealerOnline_Config"

$PSBDir = $PSScriptRoot
Get-ChildItem $PSBDir -Filter "*.psb" | ForEach-Object {
    Write-Output $_.FullName
    $proc = Start-ImsProcess -Program "IMSql.Psb" -ArgumentList "-OutputDirectory=`"$OutputDir`"", "-SqlDatabase=""$ConfigConnectionString""", $_.FullName
    if ($proc.ExitCode -ne 0) {
        Write-Error "IMSql.Psb failed with code $($proc.ExitCode)."
    }
}
