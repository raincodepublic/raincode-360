#!/usr/bin/env pwsh

[System.Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingPlainTextForPassword', '')]
Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline_Config"
)

$ErrorActionPreference = "Stop"

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

$env:RCLRUNARGS = ""

$env:RC_DB_FILE = Join-Path -Path $PSScriptRoot -ChildPath "RcDbConnections.csv"
Set-RcDbFile -Path $env:RC_DB_FILE -PlanName "IVP1" -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog $DbName -MultipleActiveResultSets
Set-RcDbFile -Path $env:RC_DB_FILE -PlanName "VSAMSQL" -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog "VsamSql" -Append

$JclDir = Join-Path -Path $PSScriptRoot -ChildPath "../RCD001_CHKP_XRST"

$proc = Start-RcBatchdirProcess -Program "Submit" -ArgumentList "-File=`"$PSScriptRoot/CREATE-PROCLIB.JCL`"", "-LogToConsole=True"
if ($proc.ExitCode -ne 0) {
    Write-Error "Submit failed with code: $($proc.ExitCode)."
}
#clean the DB before
sqlcmd -S "$env:DB_SERVER" -U "$env:DB_USER" -P "$env:DB_PASSWORD" -Q "SET QUOTED_IDENTIFIER ON;delete FROM [IMSQL_DealerOnline_Config].[dbo].[RCD001_RCADDR]"

#submit the JCL
Push-Location (Join-Path -Path $PSScriptRoot -ChildPath "Output/bin")
$proc = Start-RcBatchdirProcess -Program "Submit" -ArgumentList "-File=`"$JclDir/RCD001.EPRCD003.JCL`"", "-LogToConsole=FALSE","-LogLevel=ERROR"
if ($proc.ExitCode -ne 0) {
    Write-Error "Submit failed with code: $($proc.ExitCode)."
}
Pop-Location
