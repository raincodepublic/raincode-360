
*****************************************************************************************
*                                                                                       *
*   IMSql Demo : Checkpoint and Restart of IMSql Batch                                  *
*                                                                                       *
*****************************************************************************************

PURPOSE OF THIS DEMO
  . To showcase the checkpoint and restart functionalities of IMSql batch.

HOW TO RUN THE DEMO
  . Open powershell and navigate to the directory 'C:\Raincode360\Repositories\Demos\IMSql\CheckpointRestart'
  . Type command 'make demo' to run the first part of the demo
  . Job RCD001.EPRCD003.JCL executes and it fails with return code of 9000, which is expected
  . Type the command 'make restart' to run the second part of the demo
  . This time RCD001.EPRCDR03.JCL executes and finishes with return code of 0.

REFERENCES
  . Refer the pages with heading 'IMSql – Demo – Checkpoint/Restart' in Raincode 360 Manual
