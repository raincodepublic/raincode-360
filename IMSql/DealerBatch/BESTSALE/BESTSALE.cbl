       IDENTIFICATION DIVISION.
       PROGRAM-ID.  BESTSALE.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  DLI-FUNCTIONS.
           05 DLI-GN               PIC X(4)   VALUE 'GN'.
       01  WRK-SSA.
           05 WRK-SSA-BEGIN PIC X(9) VALUE 'SALES'.
       01  WRK-IOAREA     PIC X(240).
       01  WRK-IOAREA-DEALER REDEFINES WRK-IOAREA.
           05 IO-DLRNO    PIC X(10) .
           05 IO-DLRNAME  PIC X(60).
           05 IO-PHONE    PIC X(20).
           05 IO-STREET   PIC X(40).
           05 IO-ZIPCODE  PIC X(10).
           05 IO-CITY     PIC X(40).
           05 IO-COUNTRY  PIC X(40).
       01  WRK-IOAREA-CATALOG REDEFINES WRK-IOAREA.
           05 IO-MODTYPE PIC X(20).
           05 IO-COMMENT PIC X(200).
       01  WRK-IOAREA-ORDERS REDEFINES WRK-IOAREA.
           05 IO-SEGDATA1 PIC X(20).
           05 IO-ORDNBR   PIC X(20).
           05 IO-DETAILS  PIC X(200).
       01  WRK-IOAREA-SALES  REDEFINES WRK-IOAREA.
           05 IO-SALDATE  PIC X(8).
           05 IO-DETAILS  PIC X(200).
       01  NBR-REC        PIC 9(5).
       01  CUR-SALES-CONCAT-KEY.
           05 C-DEALER-KEY                 PIC X(10).
           05 C-CATALOG-KEY                PIC X(20).
           05 C-SALES-KEY                  PIC X(8).
       01  CUR-NBR-SALES                   PIC 9(5).
       01  PREV-SALES-CONCAT-KEY.
           05 P-DEALER-KEY                 PIC X(10).
           05 P-CATALOG-KEY                PIC X(20).
           05 P-SALES-KEY                  PIC X(8).
       01  BEST-SALES.
           05 BEST-DEALER                  PIC X(10).
           05 BEST-NBR-SALES               PIC 9(5).
       01  WS-CURRENT-DATE-DATA.
           05  WS-CURRENT-DATE.
               10  WS-CURRENT-YEAR         PIC 9(04).
               10  WS-CURRENT-MONTH        PIC 9(02).
               10  WS-CURRENT-DAY          PIC 9(02).
           05  WS-CURRENT-TIME.
               10  WS-CURRENT-HOURS        PIC 9(02).
               10  WS-CURRENT-MINUTE       PIC 9(02).
               10  WS-CURRENT-SECOND       PIC 9(02).
               10  WS-CURRENT-MILLISECONDS PIC 9(02).
       LINKAGE SECTION.
       01 IO-PCB-MASK.
           05 IO-PCB-LOGICAL-TERMINAL PIC X(8).
           05 FILLER                  PIC XX.
           05 IO-PCB-STATUS           PIC XX.
           05 IO-PCB-DATE             PIC X(4).
           05 IO-PCB-TIME             PIC X(4).
           05 IO-PCB-MSG-SEQ-NUMBER   PIC X(4).
           05 IO-PCB-MOD-NAME         PIC X(8).
       01 DB-PCB.
           05 DB-DBNAME PIC X(8).
           05 DB-LEVEL  PIC 99.
           05 DB-STAT   PIC XX.
           05 DB-PROC   PIC XXXX.
           05 DB-RESERVED PIC S9(5) COMP.
           05 DB-SEGMENT PIC X(8).
           05 DB-KFBL    PIC S9(5) COMP.
           05 DB-NSSG    PIC S9(5) COMP.
           05 DB-KFBA    PIC X(60).

       PROCEDURE DIVISION USING IO-PCB-MASK
                                DB-PCB.
           ENTRY 'DLITCBL'.

              DISPLAY "INITIAL PCB STATE: "
              PERFORM DISP-DBPCB.
              PERFORM DOGNCALL.

           GOBACK.

       DISP-DBPCB.
           DISPLAY "PCB STATE : "
           DISPLAY "    DB-LEVEL  : " DB-LEVEL
           DISPLAY "    DB-KFBA   : " DB-KFBL " -> " DB-KFBA(1:DB-KFBL)
           DISPLAY "    DB-SEGMENT: " DB-SEGMENT
           DISPLAY "    DB-STAT   : " DB-STAT
           DISPLAY "    DB-PROC   : " DB-PROC.

       DOGNCALL.
           MOVE FUNCTION CURRENT-DATE to WS-CURRENT-DATE-DATA
           DISPLAY "START TIME " WS-CURRENT-YEAR "/" WS-CURRENT-MONTH
                   "/" WS-CURRENT-DAY
                   " " WS-CURRENT-HOURS ":" WS-CURRENT-MINUTE
                   ":" WS-CURRENT-SECOND ":" WS-CURRENT-MILLISECONDS
           DISPLAY "EXTRACT"
           CALL 'CBLTDLI' USING DLI-GN
                                DB-PCB
                                WRK-IOAREA
                                WRK-SSA.
           PERFORM COUNT-SALES UNTIL (DB-STAT NOT = SPACE
                                      AND DB-STAT NOT = "GA"
                                      AND DB-STAT NOT = "GK")
      *                               OR NBR-REC > 5000
           IF DB-STAT = SPACE
               PERFORM UPDATE-SALES
           END-IF
           DISPLAY "NBR RECORD  : " NBR-REC.
           DISPLAY "BEST DEALER : " BEST-DEALER " / " BEST-NBR-SALES

           MOVE FUNCTION CURRENT-DATE to WS-CURRENT-DATE-DATA
           DISPLAY "END TIME " WS-CURRENT-YEAR "/" WS-CURRENT-MONTH
                   "/" WS-CURRENT-DAY
                   " " WS-CURRENT-HOURS ":" WS-CURRENT-MINUTE
                   ":" WS-CURRENT-SECOND ":" WS-CURRENT-MILLISECONDS

           DISPLAY "final PCB STATE: "
           PERFORM DISP-DBPCB.

       COUNT-SALES.
           ADD 1 TO NBR-REC
           MOVE DB-KFBA TO CUR-SALES-CONCAT-KEY
      *    DISPLAY NBR-REC " " C-DEALER-KEY
           IF C-DEALER-KEY NOT = P-DEALER-KEY
               PERFORM UPDATE-SALES
               MOVE 1 TO CUR-NBR-SALES
           ELSE
               ADD 1 TO CUR-NBR-SALES
           END-IF.
           MOVE CUR-SALES-CONCAT-KEY TO PREV-SALES-CONCAT-KEY
           CALL 'CBLTDLI' USING DLI-GN
                                DB-PCB
                                WRK-IOAREA
                                WRK-SSA.

       UPDATE-SALES.
               IF CUR-NBR-SALES > BEST-NBR-SALES
                   MOVE CUR-NBR-SALES TO BEST-NBR-SALES
                   MOVE C-DEALER-KEY TO BEST-DEALER
               END-IF.
