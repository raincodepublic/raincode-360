#!/usr/bin/env pwsh

Param (
    [Parameter(Mandatory)]
    [string]
    $ProjectDir,

    [Parameter(Mandatory)]
    [string]
    $Configuration
)

$ErrorActionPreference = 'Stop'

$rcpsb = Join-Path -Path $env:RCBIN -ChildPath "IMSql.Psb"
$DbContextDir = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "DealerOnline/output"
$OutputDir = Join-Path -Path $ProjectDir -ChildPath "bin/$Configuration/net8.0"

Get-ChildItem $DbContextDir -Filter "DEALER*.csproj" | ForEach-Object {
    Write-Host "Compiling the DB Context for the DBD: " $_.FullName
    & dotnet build $_.FullName -o $OutputDir -nodeReuse:false -p:UseSharedCompilation=false
    if ($LASTEXITCODE -ne 0) {
        Write-Error "Build failed with code $LASTEXITCODE."
    }
}

Get-ChildItem $ProjectDir -Filter "*.psb" | ForEach-Object {
    Write-Host "Translating the PSB : " $_.FullName
    $proc = Start-Process -PassThru -WindowStyle Hidden -Wait -FilePath $rcpsb  -ArgumentList "-OutputDirectory=`"$OutputDir`"", $_.FullName
    if ($proc.ExitCode -ne 0) {
       Write-Error "IMSql.Psb build failed with code $($proc.ExitCode)."
    }
}
