
*****************************************************************************************
*                                                                                       *
*   IMSql Demo : Dealer Batch                                                           *
*                                                                                       *
*****************************************************************************************

PURPOSE OF THESE DEMOS
  . To access the data through PSBs and DBDs of IMSql batch

HOW TO RUN THE DEMOS

Prerequisite :

  . Make 'ADDPROC' as the startup project by right clicking on the project and selecting 'Set as Startup Project'
  . Start Raincode Debugger
  . This creates the PROC IMSBATCH in the catalog, which is one time task

DEMO-1:
  . Make 'BESTSALE' as the startup project by right clicking on the project and selecting 'Set as Startup Project'
  . Start Raincode Debugger
  . This demo should execute job BESTSALE.JCL and finish successfully with result on the console.

DEMO-2:
  . Make 'EXTRACT-STAT' as the startup project by right clicking on the project and selecting 'Set as Startup Project'
  . Start Raincode Debugger
  . This demo should execute the job EXTRACT-STAT.JCL successfully and produce result on the console

REFERENCES
  . Refer the pages with heading 'IMSql – Demo – Batch' in Raincode 360 Manual
