$ErrorActionPreference = "Stop"

# Robustness settings
$maxRetries = 4
$retryWait = 10

function Install-PrerequisiteModule {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Name
    )

    Process {
        if ((Get-InstalledModule -Name $Name).Count -eq 0) {
            Install-Module -Name $Name -AllowClobber -Scope "CurrentUser"
        }
    }
}

function Install-Prerequisites {
    Install-PrerequisiteModule -Name "SqlServer"
}

function Get-BinSuffix {
    $BinSuffix = ""
    if ($IsWindows) {
        $BinSuffix = ".exe"
    }
    $BinSuffix
}

function Get-DefaultTargetFramework {
    $TargetFramework = "net6.0"
    if ($IsLinux) {
        $TargetFramework = "net6.0"
    }
    $TargetFramework
}

function Get-ImsProgramPath {
    [CmdletBinding()]
    Param (
        [Parameter()]
        [string]
        $Program,

        [Parameter()]
        [string]
        $TargetFramework
    )

    Process {
        if (!(Test-Path -Path env:RCBIN)) {
            $RootDir = Join-Path -Path $env:YAFL_ROOT -ChildPath "dotnet/runtime/ImsBin"
            if (!$TargetFramework) {
                $TargetFramework = Get-DefaultTargetFramework
            }
            $Result = Join-Path -Path $RootDir -ChildPath $TargetFramework
            if ($Program) {
                $ProgramFile = "${Program}$(Get-BinSuffix)"
                $Result = Join-Path -Path $Result -ChildPath $ProgramFile
            }
        }
        else {
            $Result = $env:RCBIN
            if ($Program) {
                $ProgramFile = "${Program}$(Get-BinSuffix)"
                $Result = Join-Path -Path $Result -ChildPath $ProgramFile
            }
        }
        $Result
    }
}

function Get-RcdirProgramPath {
    [CmdletBinding()]
    Param (
        [Parameter()]
        [string]
        $Program
    )

    Process {
        $BinDir = "bin"
        if ($IsLinux) {
            $BinDir = "bin.core"
        }
        $Result = Join-Path -Path $env:RCDIR -ChildPath $BinDir
        if ($Program) {
            $ProgramFile = "${Program}$(Get-BinSuffix)"
            $Result = Join-Path -Path $Result -ChildPath $ProgramFile
        }
        $Result
    }
}

function Get-RcBatchdirProgramPath {
    [CmdletBinding()]
    Param (
        [Parameter()]
        [string]
        $Program
    )

    Process {
        if (!(Test-Path -Path env:RCBATCHDIR)) {
            $RootDir = Join-Path -Path $env:RC_BATCH_REPO -ChildPath "raincodebatch/Raincode.Batch/Build"
            if (!$TargetFramework) {
                $TargetFramework = Get-DefaultTargetFramework
            }
            $Result = Join-Path -Path $RootDir -ChildPath "$TargetFramework/Release"
            if ($Program) {
                $ProgramFile = "${Program}$(Get-BinSuffix)"
                $Result = Join-Path -Path $Result -ChildPath $ProgramFile
            }
        }
        else {
            $Result = $env:RCBATCHDIR
            if ($Program) {
                $ProgramFile = "${Program}$(Get-BinSuffix)"
                $Result = Join-Path -Path $Result -ChildPath $ProgramFile
            }
            $Result
        }
    }
}

function Start-ImsProcess {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Program,

        [Parameter()]
        [string[]]
        $ArgumentList,

        [Parameter()]
        [string]
        $TargetFramework
    )

    Process {
        $ProgramPath = Get-ImsProgramPath -Program $Program -TargetFramework $TargetFramework
        Start-Process -PassThru -NoNewWindow -Wait -Verbose -FilePath $ProgramPath -ArgumentList $ArgumentList
    }
}

function Start-RcdirProcess {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Program,

        [Parameter()]
        [string[]]
        $ArgumentList
    )

    Process {
        $ProgramPath = Get-RcdirProgramPath -Program $Program
        Start-Process -PassThru -NoNewWindow -Wait -Verbose -FilePath $ProgramPath -ArgumentList $ArgumentList
    }
}

function Start-RcBatchdirProcess {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Program,

        [Parameter()]
        [string[]]
        $ArgumentList
    )

    Process {
        $ProgramPath = Get-RcBatchdirProgramPath -Program $Program
        Start-Process -PassThru -NoNewWindow -Wait -Verbose -FilePath $ProgramPath -ArgumentList $ArgumentList
    }
}

function Invoke-RobustSqlcmd {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory, ParameterSetName = 'File')]
        [Parameter(Mandatory, ParameterSetName = 'Query')]
        [string]
        $ConnectionString,

        [Parameter(Mandatory, ParameterSetName = 'File')]
        [string]
        $InputFile,

        [Parameter(Mandatory, ParameterSetName = 'Query')]
        [string]
        $Query,

        [Parameter()]
        [switch]
        $AllowFailure
    )

    Process {
        $retries = 0
        $OldErrorActionPreference = $ErrorActionPreference
        while ($true) {
            try {
                $ErrorActionPreference = "Continue"
                if ($PSBoundParameters.ContainsKey('InputFile')) {
                    Invoke-Sqlcmd -AbortOnError:(!$AllowFailure) -ConnectionString $ConnectionString -InputFile $InputFile
                }
                else {
                    Invoke-Sqlcmd -AbortOnError:(!$AllowFailure) -ConnectionString $ConnectionString -Query $Query
                }
                break
            }
            catch [System.Data.SqlClient.SqlException], [System.InvalidOperationException] {
                Write-Host $_.Exception
                if ($retries -lt $maxRetries) {
                    Write-Host "SQL command failed. $($maxRetries - $retries) retries left..."
                    $retries += 1
                    Start-Sleep -Seconds $retryWait
                }
                else {
                    Write-Host "SQL command failed too many times. Giving up!"
                    throw "SQL command failed too many times."
                }
            }
            finally {
                $ErrorActionPreference = $OldErrorActionPreference
            }
        }
    }
}

function Get-SqlConnectionString {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingPlainTextForPassword', '')]
    Param(
        [Parameter(Mandatory)]
        [string]
        $DbServerString,

        [Parameter()]
        [string]
        $DbUser = "sa",

        [Parameter(Mandatory)]
        [string]
        $DbPassword,

        [Parameter()]
        [string]
        $InitialCatalog,

        [Parameter()]
        [switch]
        $MultipleActiveResultSets,

        [Parameter()]
        [switch]
        $Encrypt,

        [Parameter()]
        [switch]
        $PersistSecurityInfo
    )

    Process {
        $cs = "Server=$DbServerString;"

        # If we hit Azure SQL, we force encryption.
        if ($DbServerString.Contains("database.windows.net")) {
            $Encrypt = $true
        }

        if ($DbServerString.StartsWith("tcp:")) {
            $cs += "User ID=$DbUser;"
            $cs += "Password=$DbPassword;"
            if ($PersistSecurityInfo) {
                $cs += "Persist Security Info=True;"
            }
            else {
                $cs += "Persist Security Info=False;"
            }
            if ($Encrypt) {
                $cs += "Encrypt=True;"
                $cs += "TrustServerCertificate=False;"
            } else {
                $cs += "Encrypt=False;"
                $cs += "TrustServerCertificate=True;"
            }
        }
        else {
            # We're running against a local SQL Server, use Windows authentication.
            $cs += "Integrated Security=SSPI;"
			if ($Encrypt) {
                $cs += "Encrypt=True;"
                $cs += "TrustServerCertificate=False;"
            } else {
                $cs += "Encrypt=False;"
                $cs += "TrustServerCertificate=True;"
            }
        }
        $cs += "Connection Timeout=30;"
        if ($PSBoundParameters.ContainsKey('InitialCatalog')) {
            $cs += "Initial Catalog=$InitialCatalog;"
        }
        if ($PSBoundParameters.ContainsKey('MultipleActiveResultSets')) {
            $cs += "MultipleActiveResultSets=$MultipleActiveResultSets;"
        }
        $cs
    }
}

function Set-RcDbFile {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingPlainTextForPassword', '')]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Path,

        [Parameter(Mandatory)]
        [string]
        $PlanName,

        [Parameter(Mandatory)]
        [string]
        $DbServerString,

        [Parameter()]
        [string]
        $DbUser = "sa",

        [Parameter(Mandatory)]
        [string]
        $DbPassword,

        [Parameter()]
        [string]
        $InitialCatalog,

        [Parameter()]
        [switch]
        $MultipleActiveResultSets,

        [Parameter()]
        [switch]
        $Encrypt,

        [Parameter()]
        [switch]
        $PersistSecurityInfo,

        [Parameter()]
        [switch]
        $Append
    )

    Process {
        $ConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog $InitialCatalog -MultipleActiveResultSets:$MultipleActiveResultSets -Encrypt:$Encrypt -PersistSecurityInfo:$PersistSecurityInfo
        "$PlanName,SqlServer,$ConnectionString" | Out-File -FilePath $Path -Encoding "utf-8" -Append:$Append
    }
}

function Get-GenerateViewsDir {
    if (Test-Path -Path env:RCBIN) {
        Join-Path -Path $env:RCBIN -ChildPath "sql"
    }
    else {
        Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "../GenerateViews/sql"
    }
}
