       IDENTIFICATION DIVISION.
       PROGRAM-ID.  TERMLOGGER.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01 DLI-FUNCTIONS.
         05 DLI-GU                 PIC X(4)   VALUE 'GU  '.
         
       01 IN-MSG-AREA
         05 IN-MSG-LL              PIC S9(4) COMP.
         05 IN-MSG-ZZ              PIC S9(4) COMP.
         05 IN-MSG-TRAN            PIC X(8).
         05 IN-MSG-TEXT            PIC X(20).

       LINKAGE SECTION.

       01 IO-PCB.
         05 IO-PCB-LOGICAL-TERMINAL PIC X(8).
            05 FILLER               PIC XX.
            05 IO-PCB-STATUS        PIC XX.
            05 IO-PCB-DATE          PIC X(4).
            05 IO-PCB-TIME          PIC X(4).
            05 IO-PCB-MSG-SEQ-NUMBER  PIC X(4).
            05 IO-PCB-MOD-NAME      PIC X(8).    

       PROCEDURE DIVISION USING IO-PCB .
       
            CALL 'CBLTDLI' USING DLI-GU, IO-PCB, IN-MSG-AREA.

      * If there is an error, just abandon
         IF IO-PCB-STATUS IS NOT EQUALS TO SPACES
            GO TO END-OF-PROGRAM.

        DISPLAY ' ====== Terminal Message Incoming ======= '.
        DISPLAY IN-MSG-TEXT.

        END-OF-PROGRAM.
         GOBACK.