       IDENTIFICATION DIVISION.
       PROGRAM-ID.  DINSERT.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 INPUT-MSG.
           05 INPUT-MSG-LL			PIC S9(4) COMP.
       	05 INPUT-MSG-ZZ			PIC S9(4) COMP.
       	05 INPUT-TRANCOD        PIC X(8).
       	05 INPUT-NUMBER         PIC X(10).
       	05 INPUT-NAME           PIC X(60).
       	05 INPUT-PHONE          PIC X(20). 
       	05 INPUT-STREET         PIC X(40).
       	05 INPUT-ZIPCODE        PIC X(10).
       	05 INPUT-CITY           PIC X(40).
       	05 INPUT-COUNTRY        PIC X(40).
       01 OUT-MSG.
           05 OUT-MSG-LL		    PIC S9(4) COMP VALUE 254.
       	05 OUT-MSG-ZZ		    PIC S9(4) COMP.
       	05 OUT-MSG-NUMBER       PIC X(10).
       	05 OUT-MSG-NAME      	PIC X(60).
       	05 OUT-MSG-PHONE        PIC X(20).
       	05 OUT-MSG-STREET       PIC X(40).
       	05 OUT-MSG-ZIPCODE      PIC X(10).
       	05 OUT-MSG-CITY         PIC X(40).
       	05 OUT-MSG-COUNTRY      PIC X(40).
       	05 OUT-MSG-STATUS       PIC X(30).
       01  DLI-FUNCTIONS.
       	05 DLI-GU               PIC X(4)   VALUE 'GU  '.
       	05 DLI-GHU              PIC X(4)   VALUE 'GHU '.
       	05 DLI-ISRT             PIC X(4)   VALUE 'ISRT'.
       	05 DLI-PURG             PIC X(4)   VALUE 'PURG'.
       	05 DLI-CHNG            PIC X(4)    VALUE 'CHNG'.
       01  WRK-SSA-ISRT.
           05 WRK-SSA-ISRT-SEG    PIC X(9) VALUE 'DEALER   '.
       01  WRK-SSA.
       	05 WRK-SSA-BEGIN PIC X(19) VALUE "DEALER  (DLRNO    =".
           05 WRK-SSA-VALUE PIC X(10).
           05 WRK-SSA-END   PIC X VALUE ")".
       01  WRK-IOAREA.
       	05 IO-DLRNO    PIC X(10).
       	05 IO-DLRNAME  PIC X(60).
       	05 IO-PHONE    PIC X(20).
       	05 IO-STREET   PIC X(40).
       	05 IO-ZIPCODE  PIC X(10).
       	05 IO-CITY     PIC X(40).
       	05 IO-COUNTRY  PIC X(40).	
       
       LINKAGE SECTION.
       
       01 IO-PCB-MASK.
       	05 IO-PCB-LOGICAL-TERMINAL PIC X(8).
       	05 FILLER                  PIC XX.
       	05 IO-PCB-STATUS           PIC XX.
       	05 IO-PCB-DATE             PIC X(4).
       	05 IO-PCB-TIME             PIC X(4).
       	05 IO-PCB-MSG-SEQ-NUMBER   PIC X(4).
       	05 IO-PCB-MOD-NAME         PIC X(8).	
       01 DB-PCB.
           05 DB-DBNAME PIC X(8).
           05 DB-LEVEL  PIC 99.
           05 DB-STAT   PIC XX.
           05 DB-PROC   PIC XXXX.
           05 DB-RESERVED PIC S9(5) COMP.
           05 DB-SEGMENT PIC X(8).
           05 DB-KFBL    PIC S9(5) COMP.
           05 DB-NSSG    PIC S9(5) COMP.
           05 DB-KFBA    PIC X(60).
       	
       PROCEDURE DIVISION USING IO-PCB-MASK
                                DB-PCB.						 
       	ENTRY 'DLITCBL'.
       	CALL 'CBLTDLI' USING DLI-GU
       						 IO-PCB-MASK
       						 INPUT-MSG.
       	MOVE INPUT-NUMBER  TO IO-DLRNO.
       	MOVE INPUT-NAME  TO IO-DLRNAME.
       	MOVE INPUT-PHONE TO IO-PHONE.
       	MOVE INPUT-STREET TO IO-STREET.
       	MOVE INPUT-ZIPCODE TO IO-ZIPCODE.
       	MOVE INPUT-CITY TO IO-CITY.
       	MOVE INPUT-COUNTRY TO IO-COUNTRY.
      *****************************************************************
       	MOVE 'DEALER   ' TO WRK-SSA-ISRT.
           CALL 'CBLTDLI' USING DLI-ISRT
                                DB-PCB	
       						 WRK-IOAREA
       						 WRK-SSA-ISRT.
           IF (DB-STAT = SPACE OR DB-STAT = LOW-VALUE) THEN						 
       	    MOVE IO-DLRNO TO WRK-SSA-VALUE
               CALL 'CBLTDLI' USING DLI-GU                     
                                    DB-PCB
                                    WRK-IOAREA
                                    WRK-SSA
       	END-IF
                                
       	IF (DB-STAT = SPACE OR DB-STAT = LOW-VALUE) THEN	
       		MOVE 'Successfully Inserted' TO OUT-MSG-STATUS
           ELSE 
       	    STRING "Failed, Status Code:" DB-STAT INTO OUT-MSG-STATUS
           END-IF
           MOVE IO-DLRNO TO OUT-MSG-NUMBER.
       	MOVE IO-DLRNAME TO OUT-MSG-NAME.
       	MOVE IO-PHONE TO OUT-MSG-PHONE.  
       	MOVE IO-STREET TO OUT-MSG-STREET.
       	MOVE IO-ZIPCODE TO OUT-MSG-ZIPCODE.
       	MOVE IO-CITY TO OUT-MSG-CITY.
       	MOVE IO-COUNTRY TO OUT-MSG-COUNTRY.
      ****************************************************************
       	CALL 'CBLTDLI' USING DLI-ISRT
       	                     IO-PCB-MASK
       						 OUT-MSG.
       	CALL 'CBLTDLI' USING DLI-PURG
       	                     IO-PCB-MASK.	
           GOBACK.
       
