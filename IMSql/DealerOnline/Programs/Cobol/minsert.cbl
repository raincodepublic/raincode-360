       IDENTIFICATION DIVISION.
       PROGRAM-ID.  MINSERT.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 INPUT-MSG.
           05 INPUT-MSG-LL         PIC S9(4) COMP.
           05 INPUT-MSG-ZZ         PIC S9(4) COMP.
           05 INPUT-TRANCOD        PIC X(8).
           05 INPUT-MODTYPE        PIC X(20).
           05 INPUT-BRAND          PIC X(40).
           05 INPUT-DETAILS        PIC X(60). 
       01 OUT-MSG.
           05 OUT-MSG-LL           PIC S9(4) COMP VALUE 154.
           05 OUT-MSG-ZZ           PIC S9(4) COMP.
           05 OUT-MSG-MODTYPE      PIC X(20).
           05 OUT-MSG-BRAND        PIC X(40).
           05 OUT-MSG-DETAILS      PIC X(60).
           05 OUT-MSG-STATUS       PIC X(30).     
       01  DLI-FUNCTIONS.
           05 DLI-GU               PIC X(4)   VALUE 'GU  '.
           05 DLI-GHU              PIC X(4)   VALUE 'GHU '.
           05 DLI-ISRT             PIC X(4)   VALUE 'ISRT'.
           05 DLI-PURG             PIC X(4)   VALUE 'PURG'.
           05 DLI-CHNG             PIC X(4)    VALUE 'CHNG'.
       01  WRK-SSA-ISRT.
           05 WRK-SSA-ISRT-SEG     PIC X(9) VALUE 'MODEL    '.
       01  WRK-IOAREA.
           05 IO-MODTYPE           PIC X(20).
           05 IO-BRAND             PIC X(40).
           05 IO-DETAILS           PIC X(60).
       
       LINKAGE SECTION.
       
       01 IO-PCB-MASK.
           05 IO-PCB-LOGICAL-TERMINAL PIC X(8).
           05 FILLER                  PIC XX.
           05 IO-PCB-STATUS           PIC XX.
           05 IO-PCB-DATE             PIC X(4).
           05 IO-PCB-TIME             PIC X(4).
           05 IO-PCB-MSG-SEQ-NUMBER   PIC X(4).
           05 IO-PCB-MOD-NAME         PIC X(8).    
       01 DB-PCB.
           05 DB-DBNAME PIC X(8).
           05 DB-LEVEL  PIC 99.
           05 DB-STAT   PIC XX.
           05 DB-PROC   PIC XXXX.
           05 DB-RESERVED PIC S9(5) COMP.
           05 DB-SEGMENT PIC X(8).
           05 DB-KFBL    PIC S9(5) COMP.
           05 DB-NSSG    PIC S9(5) COMP.
           05 DB-KFBA    PIC X(60).
           
       PROCEDURE DIVISION USING IO-PCB-MASK
                                DB-PCB.                         
           ENTRY 'DLITCBL'.
           CALL 'CBLTDLI' USING DLI-GU
                                IO-PCB-MASK
                                INPUT-MSG.
           MOVE INPUT-MODTYPE  TO IO-MODTYPE.
           MOVE INPUT-BRAND  TO IO-BRAND.
           MOVE INPUT-DETAILS TO IO-DETAILS.
       *****************************************************************
           MOVE 'MODEL    ' TO WRK-SSA-ISRT.
           CALL 'CBLTDLI' USING DLI-ISRT
                                DB-PCB    
                                WRK-IOAREA
                                WRK-SSA-ISRT.
           IF DB-STAT = "  " THEN    
               MOVE 'Successfully Inserted' TO OUT-MSG-STATUS
           ELSE 
               STRING "Failed, Status Code:" DB-STAT INTO OUT-MSG-STATUS
           END-IF
           MOVE IO-MODTYPE TO OUT-MSG-MODTYPE.
           MOVE IO-BRAND TO OUT-MSG-BRAND.
           MOVE IO-DETAILS TO OUT-MSG-DETAILS.     
       **************************************************************** 
           CALL 'CBLTDLI' USING DLI-ISRT
                                IO-PCB-MASK
                                OUT-MSG.
           CALL 'CBLTDLI' USING DLI-PURG
                                IO-PCB-MASK.    
           GOBACK.
       
