       IDENTIFICATION DIVISION.
       PROGRAM-ID.  MIRLOG.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      * The DLI functions we will use later
       01 DLI-FUNCTIONS.
         05 DLI-GU                 PIC X(4)   VALUE 'GU  '.
         05 DLI-ISRT               PIC X(4)   VALUE 'ISRT'.
         05 DLI-PURG               PIC X(4)   VALUE 'PURG'.
         
       01 IN-MSG-AREA
      * The first 2 elements are reserved for IMS 
         05 IN-MSG-LL              PIC S9(4) COMP.
         05 IN-MSG-ZZ              PIC S9(4) COMP.
      * Transaction code
         05 IN-MSG-TRAN            PIC X(8).
      * Actual data
         05 IN-MSG-DEST            PIC 9.
         05 IN-MSG-TEXT            PIC X(20).

       01 OUT-MSG-AREA
      * The first 2 elements are reserved for IMS 
         05 OUT-MSG-LL             PIC S9(4) COMP VALUE 24.
         05 OUT-MSG-ZZ             PIC S9(4) COMP.
      * Actual data
         05 OUT-MSG-TEXT           PIC X(20).

       01 ALT-DEST                 PIC X(8) VALUE 'LOGGERTM'.

       LINKAGE SECTION.

       01 IO-PCB.
         05 IO-PCB-LOGICAL-TERMINAL PIC X(8).
            05 FILLER              PIC XX.
            05 IO-PCB-STATUS       PIC XX.
            05 IO-PCB-DATE         PIC X(4).
            05 IO-PCB-TIME         PIC X(4).
            05 IO-PCB-MSG-SEQ-NUMBER PIC X(4).
            05 IO-PCB-MOD-NAME     PIC X(8).    
      * This PCB is longer but we ignore the rest here

       01 ALT-PCB-1.
         05 ALT-PCB-1-LOGICAL-TERMINAL PIC X(8).
            05 FILLER              PIC XX.
            05 ALT-PCB-1-STATUS    PIC XX.

       01 ALT-PCB-2.
         05 ALT-PCB-2-LOGICAL-TERMINAL PIC X(8).
            05 FILLER              PIC XX.
            05 ALT-PCB-2-STATUS    PIC XX.

       01 ALT-PCB-3.
         05 ALT-PCB-3-LOGICAL-TERMINAL PIC X(8).
            05 FILLER              PIC XX.
            05 ALT-PCB-3-STATUS    PIC XX.

       PROCEDURE DIVISION USING IO-PCB,ALT-PCB-1,ALT-PCB-2,ALT-PCB-3.
       
            CALL 'CBLTDLI' USING DLI-GU, IO-PCB, IN-MSG-AREA.
         IF IO-PCB-STATUS IS NOT EQUALS TO SPACES
            GO TO END-OF-PROGRAM.
           
         MOVE FUNCTION REVERSE(IN-MSG-TEXT) TO OUT-MSG-TEXT.

         IF IN-MSG-DEST = 1 PERFORM SEND-1
         ELSE IF IN-MSG-DEST = 2 PERFORM SEND-2
         ELSE IF IN-MSG-DEST = 3 PERFORM SEND-3
         END-IF.
                    

            CALL 'CBLTDLI' USING DLI-ISRT, IO-PCB, OUT-MSG-AREA.
         IF IO-PCB-STATUS IS NOT EQUALS TO SPACES
            GO TO END-OF-PROGRAM.

         CALL 'CBLTDLI' USING DLI-PURG, IO-PCB.
         GO TO END-OF-PROGRAM.

       SEND-1.
         CALL 'CBLTDLI' USING DLI-ISRT, ALT-PCB-1, OUT-MSG-AREA
         IF ALT-PCB-1-STATUS IS NOT EQUALS TO SPACES
            GO TO END-OF-PROGRAM.
            
         CALL 'CBLTDLI' USING DLI-PURG, ALT-PCB-1.
         IF IO-PCB-STATUS IS NOT EQUALS TO SPACES
            GO TO END-OF-PROGRAM.

       SEND-2.
         CALL 'CBLTDLI' USING DLI-ISRT, ALT-PCB-2, OUT-MSG-AREA
         IF ALT-PCB-2-STATUS IS NOT EQUALS TO SPACES
            GO TO END-OF-PROGRAM.
            
         CALL 'CBLTDLI' USING DLI-PURG, ALT-PCB-2.
         IF IO-PCB-STATUS IS NOT EQUALS TO SPACES
            GO TO END-OF-PROGRAM.

       SEND-3.
         CALL 'CBLTDLI' USING DLI-ISRT, ALT-PCB-3, OUT-MSG-AREA
         IF ALT-PCB-3-STATUS IS NOT EQUALS TO SPACES
            GO TO END-OF-PROGRAM.
            
         CALL 'CBLTDLI' USING DLI-PURG, ALT-PCB-2.
         IF IO-PCB-STATUS IS NOT EQUALS TO SPACES
            GO TO END-OF-PROGRAM.


       END-OF-PROGRAM.
         GOBACK.