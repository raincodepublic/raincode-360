       IDENTIFICATION DIVISION.
       PROGRAM-ID.  CATALOGS.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 INPUT-MSG.
           05 INPUT-MSG-LL         PIC S9(4) COMP.
           05 INPUT-MSG-ZZ         PIC S9(4) COMP.
           05 INPUT-TRANCOD        PIC X(8).
           05 INPUT-NUMBER         PIC X(10).
       01 OUT-DEALER.
           05 OUT-DEALER-LL        PIC S9(4) COMP.
           05 OUT-DEALER-ZZ        PIC S9(4) COMP.
           05 OUT-DEALER-NUMBER    PIC X(10).
           05 OUT-DEALER-NAME      PIC X(60).
           05 OUT-DEALER-PHONE     PIC X(20).
      *01 OUT-CAT1.
      *    05 OUT-CAT1-LL           PIC S9(4) COMP.
      *    05 OUT-CAT1-ZZ           PIC S9(4) COMP.
           05 OUT-CAT1-CATTYPE     PIC X(20).
           05 OUT-CAT1-COMMENT     PIC X(40).
      *01 OUT-CAT2.
      *    05 OUT-CAT2-LL           PIC S9(4) COMP.
      *    05 OUT-CAT2-ZZ           PIC S9(4) COMP.
           05 OUT-CAT2-CATTYPE     PIC X(20).
           05 OUT-CAT2-COMMENT     PIC X(40).
      *01 OUT-CAT3.
      *    05 OUT-CAT3-LL             PIC S9(4) COMP.
      *    05 OUT-CAT3-ZZ              PIC S9(4) COMP.
           05 OUT-CAT3-CATTYPE     PIC X(20).
           05 OUT-CAT3-COMMENT     PIC X(40).
       01  DLI-FUNCTIONS.
           05 DLI-GU               PIC X(4)   VALUE 'GU  '.
           05 DLI-GHU              PIC X(4)   VALUE 'GHU '.
           05 DLI-GN               PIC X(4)   VALUE 'GN  '.
           05 DLI-ISRT             PIC X(4)   VALUE 'ISRT'.
           05 DLI-PURG             PIC X(4)   VALUE 'PURG'.
           05 DLI-CHNG             PIC X(4)   VALUE 'CHNG'.
       01  DEALER-SSA.
           05 DEALER-SSA-BEGIN PIC X(19)      VALUE "DEALER  (DLRNO    =".
           05 DEALER-SSA-VALUE PIC X(10).
           05 DEALER-SSA-END   PIC X          VALUE ")".
       01  CATALOG-SSA1.
           05 CATALOG-SSA1-BEGIN PIC X(19) VALUE "CATALOG (MODTYPE  =".
           05 CATALOG-SSA1-VALUE PIC X(20).
           05 CATALOG-SSA1-END   PIC X VALUE ")".
       01  CATALOG-SSA2.
           05 CATALOG-SSA2-BEGIN PIC X(19) VALUE "CATALOG (MODTYPE  >".
           05 CATALOG-SSA2-VALUE PIC X(20).
           05 CATALOG-SSA2-END   PIC X VALUE ")".
       01  SSA-CAT.
           05 SSA-CAT-VALUE      PIC X(19) VALUE "CATALOG  ".
       01  WRK-IOAREA.
           05 IO-DLRNO    PIC X(10).
           05 IO-DLRNAME  PIC X(60).
           05 IO-PHONE    PIC X(20).
           05 IO-STREET   PIC X(40).
           05 IO-ZIPCODE  PIC X(10).
           05 IO-CITY     PIC X(40).
           05 IO-COUNTRY  PIC X(40).
       01  WRK-IOAREA-CAT.
           05 IO-CATTYPE  PIC X(20).
           05 IO-COMMENT  PIC X(200).
       01  CATMOD-IOAREA.
              02 CATALOG-IOAREA.
                  05 CIO-CATTYPE     PIC X(20).
                  05 CIO-COMMENT     PIC X(200).
              02 MODEL-IOAREA.
                  05 CIO-MODTYPE     PIC X(20).
                  05 CIO-BRAND       PIC X(40).
                  05 CIO-DESCR       PIC X(40).
       
       01  SAVEMODETYPE PIC X(20).
       
       LINKAGE SECTION.
       
       01 IO-PCB-MASK.
           05 IO-PCB-LOGICAL-TERMINAL PIC X(8).
           05 FILLER                  PIC XX.
           05 IO-PCB-STATUS           PIC XX.
           05 IO-PCB-DATE             PIC X(4).
           05 IO-PCB-TIME             PIC X(4).
           05 IO-PCB-MSG-SEQ-NUMBER   PIC X(4).
           05 IO-PCB-CAT-NAME         PIC X(8).
       01 DB-PCB.
           05 DB-DBNAME PIC X(8).
           05 DB-LEVEL  PIC 99.
           05 DB-STAT   PIC XX.
           05 DB-PROC   PIC XXXX.
           05 DB-RESERVED PIC S9(5) COMP.
           05 DB-SEGMENT PIC X(8).
           05 DB-KFBL    PIC S9(5) COMP.
           05 DB-NSSG    PIC S9(5) COMP.
           05 DB-KFBA    PIC X(60).
           
       PROCEDURE DIVISION USING IO-PCB-MASK
                                DB-PCB.                         
           ENTRY 'DLITCBL'.
           CALL 'CBLTDLI' USING DLI-GU
                                IO-PCB-MASK
                                INPUT-MSG.
           DISPLAY INPUT-TRANCOD 
           DISPLAY  INPUT-NUMBER 
       *****************************************************************
           MOVE INPUT-NUMBER TO DEALER-SSA-VALUE.
           CALL 'CBLTDLI' USING DLI-GU                     
                                DB-PCB
                                WRK-IOAREA
                                DEALER-SSA. 
           MOVE IO-DLRNO TO OUT-DEALER-NUMBER.
           MOVE IO-DLRNAME TO OUT-DEALER-NAME.
           MOVE IO-PHONE TO OUT-DEALER-PHONE.
           DISPLAY "GU DEALER (" DB-STAT ") " IO-DLRNO
           MOVE 286 TO  OUT-DEALER-LL.    
           CALL 'CBLTDLI' USING DLI-GU                     
                                DB-PCB
                                CATMOD-IOAREA
                                DEALER-SSA
                                SSA-CAT
           DISPLAY "GU CATALOG (" DB-STAT ") " CIO-CATTYPE
      *   MOVE 64 TO OUT-CAT1-LL.    
           MOVE CIO-CATTYPE TO OUT-CAT1-CATTYPE
           MOVE CIO-COMMENT TO OUT-CAT1-COMMENT
       
            CALL 'CBLTDLI' USING DLI-GN                     
                                DB-PCB
                                CATMOD-IOAREA
                                DEALER-SSA
                                SSA-CAT
                   
      *    MOVE 64 TO OUT-CAT2-LL.
           MOVE CIO-CATTYPE TO OUT-CAT2-CATTYPE
           MOVE CIO-COMMENT TO OUT-CAT2-COMMENT
           CALL 'CBLTDLI' USING DLI-GN                     
                                DB-PCB
                                CATMOD-IOAREA
                                DEALER-SSA
                                SSA-CAT
      *    MOVE 64 TO OUT-CAT3-LL.
           MOVE CIO-CATTYPE TO OUT-CAT3-CATTYPE
           MOVE CIO-COMMENT TO OUT-CAT3-COMMENT
           
           
       ****************************************************************
           CALL 'CBLTDLI' USING DLI-ISRT
                                IO-PCB-MASK
                                OUT-DEALER.                        
      *    CALL 'CBLTDLI' USING DLI-ISRT
      *                         IO-PCB-MASK
      *                         OUT-CAT1.
      *    CALL 'CBLTDLI' USING DLI-ISRT
      *                         IO-PCB-MASK
      *                         OUT-CAT2.
      *    CALL 'CBLTDLI' USING DLI-ISRT
      *                         IO-PCB-MASK
      *                         OUT-CAT3.
       
          CALL 'CBLTDLI' USING DLI-PURG
                                IO-PCB-MASK.
           GOBACK.
       
