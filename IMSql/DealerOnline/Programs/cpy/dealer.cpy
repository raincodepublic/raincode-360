        01 CPY-DEALER.
           05 CPY-DLRNO      PIC X(10).
           05 CPY-DLRNAME    PIC X(60).
           05 CPY-PHONE      PIC X(20).
           05 CPY-STREET     PIC X(40).
           05 CPY-ZIPCODE    PIC X(10).
           05 CPY-CITY       PIC X(40).
           05 CPY-COUNTRY    PIC X(40).