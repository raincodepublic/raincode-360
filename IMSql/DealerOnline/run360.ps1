$ErrorActionPreference = 'Stop'

$wc3270 = Join-Path -Path $([Environment]::GetFolderPath('ProgramFiles')) -ChildPath 'wc3270/wc3270.exe'

$runner = Start-Process -PassThru -NoNewWindow -FilePath "make" -ArgumentList "run"
Start-Sleep 15
Start-Process -FilePath $wc3270 -ArgumentList "+S", "-model", "4", "-title", "`"IMSql DealerOnline`"", "127.0.0.1:32023"

$runner.Handle
$runner.WaitForExit()
