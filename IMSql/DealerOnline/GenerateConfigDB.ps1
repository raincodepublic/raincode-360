#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline"
)

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

# the directory where all the scripts are generated
$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output"

# Generation of the config database creation script
$proc = Start-ImsProcess -Program "IMSql.DbGenerator" -ArgumentList `
    ":LogLevel=TRACE", ":Online=true", ":Data=false", ":OnlineDbName=${DbName}_Config", `
    ":OutputDir=$OutputDir"
if ($proc.ExitCode -ne 0) {
    Write-Error "IMSql.DbGenerator failed with code $($proc.ExitCode)."
}
Write-Host "   *** Finished Creating Online Sql script."
