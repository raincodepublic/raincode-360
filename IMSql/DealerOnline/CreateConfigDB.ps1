#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline"
)

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

# the output directory
$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output"

$MasterConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog "master"
$ConfigConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog "${DbName}_Config"

# drop config DB if it exists
Invoke-RobustSqlcmd -ConnectionString $MasterConnectionString `
    -Query "ALTER DATABASE [${DbName}_Config] SET single_user WITH ROLLBACK IMMEDIATE;DROP DATABASE [${DbName}_Config]" `
    -AllowFailure
Write-Host "   *** Dropping existing ConfigDB : ${DbName}_Config."

# create the service broker components
Invoke-RobustSqlcmd -ConnectionString $MasterConnectionString `
    -InputFile (Join-Path -Path "$OutputDir" -ChildPath "${DbName}_Config_ServiceBroker.sql")
Write-Host "   *** Finished Creating DB : ${DbName}_Config_ServiceBroker."

# create the config tables
Invoke-RobustSqlcmd -ConnectionString $ConfigConnectionString `
    -InputFile (Join-Path -Path "$OutputDir" -ChildPath "${DbName}_Config_Tables.sql")
Write-Host "   *** Finished Creating Config Tables : ${DbName}_Config."
