#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline"
)

# DBD's directory
$DbdDir = Join-Path -Path $PSScriptRoot -ChildPath "DBDs"
# Data directory
$DataDir = Join-Path -Path $PSScriptRoot -ChildPath "Data"

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

# Connection string to the data DB
$LoadConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog ${DbName} -MultipleActiveResultSets

if ($DbUser) {
    if (!$DbPassword) {
        $DbPassword = Read-Host -Prompt "Password" -AsSecureString
        $DbPassword = ConvertFrom-SecureString -SecureString $DbPassword -AsPlainText
    }
}

# Load MODELDB data
$proc = Start-ImsProcess -Program "IMSql.Load" `
    -ArgumentList ":LogLevel=TRACE", ":DbdFile=`"$(Join-Path -Path $DbdDir -ChildPath "MODELDB.dbd")`"", ":UnloadFile=$(Join-Path -Path $DataDir -ChildPath "UMODELD.UNLOAD")", ":ConnectionString=`"$LoadConnectionString`""
if ($proc.ExitCode -ne 0) {
    Write-Error "MODELDB load failed with code: $($proc.ExitCode)"
}
Write-Host "   *** Finished loading MODELDB tables."

# Load DEALERDB data
$proc = Start-ImsProcess -Program "IMSql.Load" `
    -ArgumentList ":LogLevel=TRACE", ":DbdFile=`"$(Join-Path -Path $DbdDir -ChildPath "DEALERDB.dbd")`"", ":UnloadFile=$(Join-Path -Path $DataDir -ChildPath "UDEALERD.UNLOAD")", ":ConnectionString=`"$LoadConnectionString`""
if ($proc.ExitCode -ne 0) {
    Write-Error "DEALERDB load failed with code: $($proc.ExitCode)"
}
Write-Host "   *** Finished loading DEALERDB tables."

Write-Host "   *** Finished loading Database."
