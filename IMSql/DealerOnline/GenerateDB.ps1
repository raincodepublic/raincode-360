#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline"
)

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

# the directory where all the scripts are generated
$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output"
# the directory with the DBD's
$DBDDir = Join-Path -Path $PSScriptRoot -ChildPath "DBDs"
# connection string to the configuration DB
$ConfigConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog "${DbName}_Config"

# Generation of the database creation script
$proc = Start-ImsProcess -Program "IMSql.DbGenerator" -ArgumentList `
    ":LogLevel=TRACE", ":DbName=$DbName", `
    ":Files=$DBDDir", `
    ":OutputDir=$OutputDir", "-SqlDatabase=""$ConfigConnectionString"""
if ($proc.ExitCode -ne 0) {
    Write-Error "IMSql.DbGenerator failed with code $($proc.ExitCode)."
}
Write-Host "   *** Finished Creating DATA Sql script."
