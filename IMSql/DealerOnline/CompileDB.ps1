#!/usr/bin/env pwsh

$ErrorActionPreference = "Stop"
$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output/bin"
$CsprojDir = Join-Path -Path $PSScriptRoot -ChildPath "Output"
& dotnet build $(Join-Path -Path $CsprojDir -ChildPath "DEALERDB.csproj") -o $OutputDir -nodeReuse:false -p:UseSharedCompilation=false
if ($LASTEXITCODE -ne 0) {
    Write-Error "Build failed with code $LASTEXITCODE."
}
Write-Host "   *** Finished Compiling DEALERDB."
& dotnet build $(Join-Path -Path $CsprojDir -ChildPath "MODELDB.csproj") -o $OutputDir -nodeReuse:false -p:UseSharedCompilation=false
if ($LASTEXITCODE -ne 0) {
    Write-Error "Build failed with code $LASTEXITCODE."
}
Write-Host "   *** Finished Compiling MODELDB."

& dotnet build $(Join-Path -Path $CsprojDir -ChildPath "LOGGERC.csproj") -o $OutputDir -nodeReuse:false -p:UseSharedCompilation=false
if ($LASTEXITCODE -ne 0) {
    Write-Error "Build failed with code $LASTEXITCODE."
}
Write-Host "   *** Finished Compiling LOGGERC."

del "$OutputDir\IMSql.*"
