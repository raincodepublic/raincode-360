#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline"
)

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

# Output directory
$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output"

# Connection string to the master DB
$MasterConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog "master"
# Connection string to the data DB
$DbConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog $DbName
# Directory that contains the copybooks
$copyDir= Join-Path -Path $PSScriptRoot -ChildPath "Programs/cpy"
# Directory where to generate the views creation scripts
$DbdViewScriptDir = Join-Path -Path $PSScriptRoot -ChildPath "DbdViewScripts"

# Drop the data DB if it exists
Invoke-RobustSqlcmd -ConnectionString $MasterConnectionString `
    -Query "ALTER DATABASE [${DbName}] SET single_user WITH ROLLBACK IMMEDIATE; DROP DATABASE [$DbName]" `
    -AllowFailure
Write-Host "   *** Dropping existing DB : $DbName."

# Create the database (not necessary - done in the script)
Invoke-RobustSqlcmd -ConnectionString $MasterConnectionString `
    -Query "CREATE DATABASE [${DbName}]"
Write-Host "   *** Creating DB : $DbName."

# Function to create one view
function GenerateView($copyName, $tableName)
{
	$proc = Start-Process -NoNewWindow -Wait -FilePath "cobrc" `
		-ArgumentList ":FirstUsableColumn=7", ":LastUsableColumn=72", ":MaxMem=1G",
		":DeclDescriptors=$outputDir/$copyName.xml", ":OutputDir=$outputDir",  "$copyDir/$copyName.cpy"
    $proc = Start-Process -PassThru -NoNewWindow -Wait -FilePath copybookViewGenerator `
        -ArgumentList "-conn=""$DbConnectionString""", "-OnlyTech", "-table=$tableName", "-output=$DbdViewScriptDir/${copyName}_view.sql", "-xml=$outputDir/$copyName.xml"
    if ($proc.ExitCode -ne 0) {
		Write-Error "copybookViewGenerator failed for  with code $($proc.ExitCode)."
	}
}

# Create conversion functions needs for the views
Invoke-RobustSqlcmd -ConnectionString $DbConnectionString `
    -InputFile (Join-Path -Path (Get-GenerateViewsDir) -ChildPath "Functions.sql")

Invoke-RobustSqlcmd -ConnectionString $DbConnectionString `
    -InputFile (Join-Path -Path (Get-GenerateViewsDir) -ChildPath "EbcdicFuncs.sql")

Write-Host "   *** Finished creating Ebcdic-ascii function."

# Create the data DB
Invoke-RobustSqlcmd -ConnectionString $MasterConnectionString `
    -InputFile (Join-Path -Path "Output" -ChildPath "$DbName.sql")
Write-Host "   *** Finished creating DB : $DbName."

# Can't generate view in GenerateDB.ps1 since physical existence of tables required
if(!(Test-Path -Path $DbdViewScriptDir)){
    New-Item -Path $DbdViewScriptDir -ItemType Directory
}

GenerateView "catalog" "DEALERDB_CATALOG"
GenerateView "dealer" "DEALERDB_DEALER"
GenerateView "model" "MODELDB_MODEL"
GenerateView "orders" "DEALERDB_ORDERS"
GenerateView "sales" "DEALERDB_SALES"
GenerateView "stock" "MODELDB_STOCK"
Write-Host "   *** Finished Generating Views Sql script."

# Create the views
$DbdViewScriptDir = Join-Path -Path $PSScriptRoot -ChildPath "DbdViewScripts"
Get-ChildItem $DbdViewScriptDir -Filter "*.sql" | ForEach-Object {
    Write-Output $_.FullName
    Invoke-RobustSqlcmd -ConnectionString $DbConnectionString -InputFile $_.FullName
}
Write-Host "   *** Finished creating DBD views for ${DbName} "
