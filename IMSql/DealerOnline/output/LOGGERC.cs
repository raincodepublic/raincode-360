﻿using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Module;
using RainCodeLegacyRuntime.Module.Attribute;
using System;
using Org.BouncyCastle.Crypto.Engines;
using System.IO;

namespace RainCode.Samples
{
    /// <summary>
    /// Add the RainCodeExport (with the optional module name) and inherit from BaseModule to make the class callable from a COBOL program
    /// </summary>
    [RainCodeExport]
    public class LOGGERC : BaseModule<LOGGERC>
    {
        private static readonly int MsgPrefix = 12;
        /// <summary>
        /// This method will be called automatically by the Raincode legacy runtime whenever a COBOL CALL to this program is issued.
        /// The only parameter we receive is the IO-PCB
        /// </summary>
        /// <param name="ec">The execution context of the current run</param>
        /// <param name="workMem">The static memory of the program</param>
        /// <param name="parms">The arguments to the program (COBOL USING clause)</param>
        protected override void Run(ExecutionContext ec, MemoryArea workMem, CallParameters parms)
        {
            string str = GetMessage(ec, parms);
            Console.WriteLine($" ====== C# Log Recieved {str} ======= ");
            File.AppendAllText("C:/temp/LTermLog.txt", str + "\n");
        }

        /// <summary>
        /// Retrieve the message. To do this it calls CBLTDLI.
        /// </summary>
        /// <param name="ec"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        private static string GetMessage(ExecutionContext ec, CallParameters parms)
        {
            // extract the first (and only) parameter: The PCB
            MemoryArea pcbMem = parms.GetParameterAddress(ec, 0);

            var exec = LoadExecutable("CBLTDLI");

            var callParms = CallParameters.AllocateParameterList(ec, 3);
            // GU
            var gu = ec.Allocate(4);
            RainCodeLegacyRuntime.Types.Convert.move_string_to_char_string(ec, "GU  ", gu);
            callParms.SetParameterAddress(ec, 0, gu);
            //PCB
            callParms.SetParameterAddress(ec, 1, pcbMem);
            // IO-AREA
            var ioArea = ec.Allocate(64);
            callParms.SetParameterAddress(ec, 2, ioArea);
            // execute CBLTDLI
            exec.Execute(ec, callParms);

            // get the message length
            int ll = ioArea.LoadMainframeInt16();
            //Console.WriteLine($"Message length: {ll}");

            // get status code
            var statusMem = pcbMem.Substr(10, 2);
            string status = RainCodeLegacyRuntime.Types.Convert.cnv_char_string_to_string(ec, statusMem);
            //Console.WriteLine($"Status: -{status}-");

            // the message start at 12 in ioArea
            var msg = ioArea.Substr(MsgPrefix, ll - MsgPrefix);
            string str = RainCodeLegacyRuntime.Types.Convert.cnv_char_string_to_string(ec, msg);

            return str;
        }

        private static Executable LoadExecutable(string name)
        {
            var exec = ModuleDictionary.FindExecutable(name);
            ModuleDictionary.EnsureModuleIsLoaded(name);
            return exec;
        }
    }
}
