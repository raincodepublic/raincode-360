
*****************************************************************************************
*                                                                                       *
*   IMSql Demo : Dealer Online                                                          *
*                                                                                       *
*****************************************************************************************

PURPOSE OF THESE DEMO
  . To showcase IMS Transaction Management (Online) with Sqlserver as database

HOW TO RUN THE DEMO

  . Double-click on “Start IMSql 3270 demo” shortcut placed on the desktop
  . IMSql should start up (Processing server & Terminal server windows) and would prompt a 3270 terminal
  . Enter username as 'demo' and password as 'demo' to proceed further
  . Enter the command '/FOR OMMENU' to open first screen of this online demo
  . Use function keys (F3/F7/F8...etc) to navigate through the screens

REFERENCES
  . Refer the pages with heading 'IMSql – Demo – Online' in Raincode 360 Manual
