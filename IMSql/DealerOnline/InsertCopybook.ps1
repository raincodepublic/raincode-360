Function GetInsert($dbd, $segment, $copy){
	$cpy=Get-Content $copy -raw
	"insert into Segment ([DbdName], [SegmentName], [Copybook]) values('$dbd', '$segment', N'$cpy');"
	"GO"
}

GetInsert "DEALERDB" "DEALER" "Programs\cpy\dealer.cpy"
GetInsert "DEALERDB" "CATALOG" "Programs\cpy\catalog.cpy"
GetInsert "DEALERDB" "ORDERS" "Programs\cpy\orders.cpy"
GetInsert "DEALERDB" "SALES" "Programs\cpy\sales.cpy"

GetInsert "MODELDB" "MODEL" "Programs\cpy\model.cpy"
GetInsert "MODELDB" "STOCK" "Programs\cpy\stock.cpy"