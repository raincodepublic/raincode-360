Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline",

    [Parameter()]
    [string]
    $OnlineDbName = "IMSQL_DealerOnline_Config"
)

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

$ErrorActionPreference = "Stop"

$ConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -InitialCatalog $OnlineDbName -MultipleActiveResultSets -DbUser $DbUser -DbPassword $DbPassword -PersistSecurityInfo
$binDir = Join-Path -Path $PSScriptRoot -ChildPath "Output/bin"
$binDirProcessing = $binDir
$binDirTerminal = $binDir

$processing = Start-Process -PassThru -FilePath $(Get-ImsProgramPath "IMSql.ProcessingServer") -WorkingDirectory $binDirProcessing `
    -ArgumentList ":LogLevel=TRACE", ":ConnectionString=`"$ConnectionString`"", ":RegionId=DEALER"

$terminal = Start-Process -PassThru -FilePath $(Get-ImsProgramPath "IMSql.TerminalServer") -WorkingDirectory $binDirTerminal `
    -ArgumentList ":LogLevel=TRACE", ":ConnectionString=`"$ConnectionString`"", ":RegionId=DEALER"

Write-Host "   *** Finished Starting Servers."

$processing.WaitForExit()
$terminal.WaitForExit()
