#!/usr/bin/env pwsh

$ErrorActionPreference = "Stop"

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output/bin"

if (!(Test-Path -Path $OutputDir )) {
     New-Item -ItemType "Directory" -Path $OutputDir
}

$DemoDir = Join-Path -Path "Programs" -ChildPath "PLI"
Get-ChildItem $DemoDir -Recurse -Filter "*.pli" | ForEach-Object {
     Write-Output $_.FullName
     $proc = Start-RcdirProcess -Program "PliRc" -ArgumentList "-Debug", ":MaxMem=1G", ":Target=NetCore", ":Warnings=`"-10010,-10020,-10030`"", ":RemoveFileVerifyFailed=FALSE", ":DLL", "-Debug", ":OutputDir=`"$OutputDir`"", $_.FullName
     if ($proc.ExitCode -ne 0) {
          Write-Error "PliRc failed with code $($proc.ExitCode)."
      }
}

$DemoDir = Join-Path -Path "Programs" -ChildPath "Cobol"
Get-ChildItem $DemoDir -Recurse -Filter "*.cbl" | ForEach-Object {
     Write-Output $_.FullName
     $proc = Start-RcdirProcess -Program "CobRc" -ArgumentList "-Debug", ":MaxMem=1G", ":Target=NetCore", ":OutputDir=`"$OutputDir`"", $_.FullName
     if ($proc.ExitCode -ne 0) {
          Write-Error "CobRc failed with code $($proc.ExitCode)."
      }
}
