#!/usr/bin/env pwsh

Param (
    [Parameter()]
    [string]
    $DbServerString = $env:DB_SERVER,

    [Parameter()]
    [string]
    $DbUser = $env:DB_USER,

    [Parameter()]
    [string]
    $DbPassword = $env:DB_PASSWORD,

    [Parameter()]
    [string]
    $DbName = "IMSQL_DealerOnline"
)

# Output directory
$OutputDir = Join-Path -Path $PSScriptRoot -ChildPath "Output"

$CommonModulePath = Join-Path -Path $env:IMSQL_DEMO_ROOT -ChildPath "IMSqlCommon.psm1"
Import-Module -Name $CommonModulePath

# Connection string to the config DB
$ConfigConnectionString = Get-SqlConnectionString -DbServerString $DbServerString -DbUser $DbUser -DbPassword $DbPassword -InitialCatalog "${DbName}_Config"

# Populate the config DB with DealerOnline mapping
Invoke-RobustSqlcmd -ConnectionString $ConfigConnectionString `
    -InputFile "InsertDealerMappings.sql"
Write-Host "   *** Finished populating DB : ${DbName}_Config."

# Populate the config DB with DealerOnline copybooks
$InsertSql = Join-Path -Path $OutputDir -ChildPath "InsertCopybook.sql"
 ./InsertCopybook.ps1 > "$InsertSql"

Invoke-RobustSqlcmd -ConnectionString $ConfigConnectionString `
    -InputFile "$InsertSql"
Write-Host "   *** Finished populating copybook."