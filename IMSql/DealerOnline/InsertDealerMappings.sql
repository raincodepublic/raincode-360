INSERT INTO [dbo].[Systems]
           ([Name])
     VALUES
           ('IMS')
INSERT INTO [dbo].[Regions]
           ([Name]
           ,[LocalEncoding]
           ,[TerminalEncoding]
           ,[SystemName]
		   ,[MaxThreads]
		   ,[Enabled])
     VALUES
           ('DEALER'
           ,'ibm037'
           ,'ibm037'
           ,'IMS'
           ,8
		   ,1)

INSERT INTO [dbo].[Programs]
           ([ProgramName]
           ,[IsBMP]
           ,[Language]
           ,[ShedulingType])
     VALUES
           ('DINSERT' ,0 ,'COBOL' ,2),
           ('DSEARCH' ,0 ,'PL1' ,2),
		   ('CATALOGS' ,0 ,'COBOL' ,2),
		   ('DDELETE' ,0 ,'PL1' ,2),
		   ('MDELETE' ,0 ,'PL1' ,2),
		   ('MINSERT' ,0 ,'COBOL' ,2),
		   ('MREPLACE' ,0 ,'PL1' ,2),
		   ('MSEARCH' ,0 ,'PL1' ,2),
		   ('RCTE02' ,0 ,'COBOL' ,2),
		   ('SALORD' ,0 ,'COBOL' ,2)

INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     VALUES
           ('DEALER'
           ,'DSEARCH'
           ,'DSEARCH'
           ,'DSEARCH')
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     VALUES
           ('DEALER'
           ,'DINSERT'
           ,'DINSERT'
           ,'DINSERT')
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     VALUES
           ('DEALER'
           ,'MINSERT'
           ,'MINSERT'
           ,'MINSERT')

INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     VALUES ('DEALER', 'MSEARCH', 'MSEARCH', 'MSEARCH')
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     values ('DEALER', 'CATALOGS', 'CATALOGS', 'CATALOGS');
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName]
		   ,[SpaSize])
     values ('DEALER', 'SALORD', 'SALORD', 'SALORD', 152);
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     values ('DEALER', 'DDELETE', 'DDELETE', 'DDELETE');
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     values ('DEALER', 'MDELETE', 'MDELETE', 'MDELETE');
INSERT INTO [dbo].TransactionMappings
           ([RegionId]
           ,[TransactionCode]
           ,[ExecutableName]
           ,[PsbName])
     values ('DEALER', 'MREPLACE', 'MREPLACE', 'MREPLACE');

-- Transaction mapping for alt pcb demo
  insert into [TransactionMappings] (regionId, TransactionCode, ExecutableName, PsbName, SpaSize, IsLterm)
  values ('DEALER', 'LOGTM002', 'termlogger', NULL,0, 1),
('DEALER', 'LOGTM003', 'loggerc', NULL, 0, 1),
('DEALER', 'LOGTX001', 'logger', 'LOGGER00',0, 0),
('DEALER', 'ALTDEMO1', 'mirlog', 'MIRLOG00',0, 0)