********************************************************************
*                                                                  *
*   DEMO : File Access via FileConfig                              *
*                                                                  *
*                                                                  *
********************************************************************

PURPOSE
  . Use a FileConfig to write to a file.

CONCEPTS
  . When not running in the contect of a JCL, legacy programs need the help of a FileConfig to locate external files.
  . A FileConfig is an xml file that links logical file names to actual file names on disk.
  . A FileConfig can contain many attributes that depend on the file type. The logical and physical name are sufficient for this example.

ABOUT THE DEMO
  . The main program reads a table in the database and writes this information to an output file.
  . The output file is defined as a Solution Item (OutputFile.txt)

RUNNING THE DEMO IN VISUAL STUDIO
  . Press F5 or Ctrl-F5 to start the demo

