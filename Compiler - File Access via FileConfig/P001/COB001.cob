﻿       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB001.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT FILE01 ASSIGN TO FILEDD
           ORGANIZATION IS LINE SEQUENTIAL
           ACCESS MODE IS SEQUENTIAL
           FILE STATUS IS W-FS.
       CONFIGURATION SECTION.
       DATA DIVISION.
       FILE SECTION.
       FD FILE01.
       01 REC01.
           10 REC01-NM-ENG PIC X(20).
           10 REC01-QTY PIC 999.
           10 REC01-PRICE PIC Z(7)9.99.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       01 ITEM.
           10 ITEM-NM-ENG PIC X(50).
           10 ITEM-QTY PIC S9(4) COMP.
           10 ITEM-PRICE PIC S9(7)V99.
       01 W-FS PIC XX.
   	   EXEC SQL
   	       DECLARE CUR01 CURSOR FOR
      	   SELECT 
               ITEM_NM_ENG,
               ITEM_QTY,
               ITEM_PRICE
           FROM 
               DBO.OFFICESUPPLIES
   	   END-EXEC.
       PROCEDURE DIVISION.
           OPEN OUTPUT FILE01
           EXEC SQL
   	           OPEN CUR01
   	       END-EXEC
           DISPLAY 'open  ' W-FS
   	       PERFORM FETCH-CUR01
           PERFORM UNTIL SQLCODE NOT = 0
               MOVE ITEM-NM-ENG TO REC01-NM-ENG
               MOVE ITEM-QTY TO REC01-QTY
               MOVE ITEM-PRICE TO REC01-PRICE
               WRITE REC01
               DISPLAY 'write ' W-FS
   	           PERFORM FETCH-CUR01
           END-PERFORM
           EXEC SQL
   	           CLOSE CUR01
   	       END-EXEC
           CLOSE FILE01
           DISPLAY 'close ' W-FS
           GOBACK
           .
       FETCH-CUR01.
   	       EXEC SQL
   	           FETCH CUR01 INTO
                   :ITEM-NM-ENG,
                   :ITEM-QTY,
                   :ITEM-PRICE
   	       END-EXEC
           .
