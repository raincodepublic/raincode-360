********************************************************************
*                                                                  *
*   DEMO : AUTOMATIC RESTART                                       *
*                                                                  *
*          . File based processing                                 *
*          . Automatic checkpointing                               *
*                                                                  *
********************************************************************

PURPOSE
  . Demonstrate the use of the Raincode Automatic Restart Extension with the above mentioned features.

CONCEPTS
  . File based processing
    . The main unit of work starts with reading a row from an input file
  . Automatic checkpointing
    . Eliminates the need to code an explicit checkpoint statement
    . The checkpoint frequency is controlled by the 'triggercount' parameter in configuration.xml

ABOUT THE DEMO
  . This demo runs by executing the script run.bat from the developer prompt
  . The script consists of a sequence of 7 submit commands, each having their own switches and environment variables.
  . The output of these jobs can be found in the default sysout location. 

      ----------------------------------------------------------------------------------
      Job  JCL      Program     Description           Files (prefixed ARC03.*)
      ----------------------------------------------------------------------------------
       1   JCL001   DBSETUP     initial setup         create ACCTLST
                                                      create empty OUTFILE
       2   SNAP01   DBDUMP      create snapshot #1    create SNAP01.DBDUMP
                                                      copy OUTFILE to SNAP01.OUTFILE
       3   JCL002   COB001      run COB001 and abend  read ACCTLST
                                                      update OUTFILE
       4   SNAP02   DBDUMP      create snapshot #2    create SNAP02.DBDUMP
                                                      copy OUTFILE to SNAP02.OUTFILE
       5   JCL002   COB001      restart               read ACCTLST
                                                      update OUTFILE
       6   SNAP03   DBDUMP      create snapshot #3    create SNAP03.DBDUMP
                                                      copy OUTFILE to SNAP03.OUTFILE
       7   JCL003   DBCLEANUP   clean up testdata     -

1: INITIAL SETUP 
  . The program DBSETUP creates 20 test accounts in the Account table of the Bankdemo database. 
  . The program ACCTLST creates a list of the test accounts and their current saldo, which is zero.
  . The file ARC03.OUTFILE is created empty.

2: SNAPSHOT #1
  . The program DBDUMP writes a list of the test accounts and their respective balances to the file ARC03.SNAP01.DBDUMP.
  . ARC03.OUTFILE is copied to ARC03.SNAP01.OUTFILE, which will be effectively empty.
  . Contents of ARC03.SNAP01.DBDUMP:

      CONTENTS OF DBO.AMOUNT                                                          
      --------------------------------                                                
      Accountnumber             Amount                                                
      --------------------------------                                                
      ARC03-01                    0,00                                                
      ARC03-02                    0,00      
      ARC03-03                    0,00      
      ..
      ARC03-20                    0,00                                                

3: START
  . The program COB001 does the following
    . read the numbers of the accounts to be updated from the file ARC01.ACCTLST.
    . Increment the balance of each account with 0.10 Euro.
    . Create a report in ARC01.OUTFILE.
  . Contents of ARC01.OUTFILE:
  
    COB001 UPDATES                                                                  
    ---------------------------------------------                                   
    Accountnumber        Amnt Before   Amnt After                                   
    ---------------------------------------------                                   
    ARC03-01                    0,00         0,10                                   
    ARC03-02                    0,00         0,10                                   
    ..
    ARC03-20                    0,00         0,10 

  . Comments
    . Automatic checkponts have been enabled per 5 rows that are processed (TriggerCount="5" in configuration.xml). 
    . An abend is forced just before the 4th commit (TRMBEFORCKP=3 in the ARCSYSIN dataset of JCL002)
      . This is the point where the changes to account 16 thru 20 would be committed to the database
      . The abend will roll back all changes made to account 16 thru 20 in the database, putting the back to zero
      . The balaces of accounts 1 thru 15 remain updated

4: SNAPSHOT #2
  . Contents of ARC03.SNAP01.DBDUMP:

      CONTENTS OF DBO.AMOUNT                                                          
      --------------------------------                                                
      Accountnumber             Amount                                                
      --------------------------------                                                
      ARC03-01                    0,10                                                
      ARC03-02                    0,10                                                
      ..
      ARC03-14                    0,10                                                
      ARC03-15                    0,10   <-- rollback                                                
      ARC03-16                    0,00                                                
      ARC03-17                    0,00                                                
      ARC03-18                    0,00                                                
      ARC03-19                    0,00                                                
      ARC03-20                    0,00   <-- abend                            

  . Contents of ARC03.SNAP02.OUTFILE:    
    
      COB001 UPDATES                                                                  
      ---------------------------------------------                                   
      Accountnumber        Amnt Before   Amnt After                                   
      ---------------------------------------------                                   
      ARC03-01                    0,00         0,10                                   
      ARC03-02                    0,00         0,10                                   
      ..
      ARC03-15                    0,00         0,10                                  

  . Comments
    . Note that ARC03.SNAP02.OUTFILE does not contain accounts 16 thru 20, even though they were written by COB001 before the abend took place.
    . The TRMBEFORCKP feature truncates the output when it is activated.
    . In case of a 'hard' program-induced abend, such as caused by a division by zero, there would be no truncation
    . In either case the contents of the output file after restart will be the same

5: RESTART
  . The restart job runs JCL002 again in restart mode
  . This run should end with RC=0

6: SNAPSHOT #3
  . Contents of ARC03.SNAP03.DBDUMP:

      CONTENTS OF DBO.AMOUNT                                                          
      --------------------------------                                                
      Accountnumber             Amount                                                
      --------------------------------                                                
      ARC03-01                    0,10                                                
      ARC03-02                    0,10                                                
      ..
      ARC03-19                    0,10                                                
      ARC03-20                    0,10   <-- final commit             
    
  . Contents of ARC03.SNAP03.OUTFILE:

      COB001 UPDATES                                                                  
      ---------------------------------------------                                   
      Accountnumber        Amnt Before   Amnt After                                   
      ---------------------------------------------                                   
      ARC03-01                    0,00         0,10                                   
      ARC03-02                    0,00         0,10                                   
      ARC03-03                    0,00         0,10                                   
      ..
      ARC03-19                    0,00         0,10                                  
      ARC03-20                    0,00         0,10                                  
