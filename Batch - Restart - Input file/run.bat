@ECHO OFF
CLS
echo ********************************************************************
echo *                                                                  *
echo *   DEMO : AUTOMATIC RESTART                                       *
echo *                                                                  *
echo *          . File based processing                                 *
echo *          . Automatic checkpointing                               *
echo *                                                                  *
echo ********************************************************************
SET SOLUTIONDIR=%CD%
SET PROJECTDIR=%SOLUTIONDIR%\Batch
SET DEBUGDIR=%PROJECTDIR%\bin\Debug\net8.0
SET PATH=%RCBIN%;%PATH%;
SET RC_RESTART_CONFIG=%SOLUTIONDIR%\configuration.xml
SET RC_DB_TYPE=Sqlserver
SET RC_DB_CONNECTION=%RC360_CONNSTRING%;Initial Catalog=BankDemo;
SET RC_LegacyRunner=
CD "%DEBUGDIR%"
IF %ERRORLEVEL% NEQ 0 (
   echo ** %DEBUGDIR% not found
   goto nok
)
echo --------------------------------------------------------------------
echo    JCL001: Initial setup
echo --------------------------------------------------------------------
"%RCBATCHDIR%\submit.exe" -File="%PROJECTDIR%\JCL001.jcl" -LogLevel=WARNING -LogToConsole=false
IF %ERRORLEVEL% NEQ 0 (
   echo ** Error while executing JCL001
   goto nok
)
echo --------------------------------------------------------------------
echo    SNAP01: Create snapshot 01
echo --------------------------------------------------------------------
SET CTM_NN=01
"%RCBATCHDIR%\submit.exe" -File="%PROJECTDIR%\SNAP.jcl" -LogLevel=WARNING -LogToConsole=false
IF %ERRORLEVEL% NEQ 0 (
   echo ** Error while executing SNAP%CTM_NN%
   goto nok
)
echo --------------------------------------------------------------------
echo    JCL002: Start and invoke abend (RC=9202)
echo --------------------------------------------------------------------
del jobid.txt
SET RCLRUNARGS=-Plugin=RainCodeRestart
"%RCBATCHDIR%\submit.exe" -File="%PROJECTDIR%\JCL002.jcl" -SaveJobID=jobid.txt -DllLogLevel=INFO -LogLevel=INFO -LogToConsole=false
IF %ERRORLEVEL% NEQ 9202 (
   echo ** Expected RC=9202, got RC=%ERRORLEVEL%
   goto nok
)
set /p JOBID=<jobid.txt
SET RCLRUNARGS=
echo --------------------------------------------------------------------
echo    SNAP02: Create snapshot 02
echo --------------------------------------------------------------------
SET CTM_NN=02
"%RCBATCHDIR%\submit.exe" -File="%PROJECTDIR%\SNAP.jcl" -LogLevel=WARNING -LogToConsole=false
IF %ERRORLEVEL% NEQ 0 (
   echo ** Error while executing SNAP%CTM_NN%
   goto nok
)
echo --------------------------------------------------------------------
echo    JCL002: Restart
echo --------------------------------------------------------------------
SET RCLRUNARGS=-Plugin=RainCodeRestart
"%RCBATCHDIR%\submit.exe" -File="%PROJECTDIR%\JCL002.jcl" -Restart=STEP001 -RestartJobId=%JOBID% -DllLogLevel=INFO -LogLevel=INFO -LogToConsole=false
IF %ERRORLEVEL% NEQ 0 (
   echo ** Error while restarting JCL002
   goto nok
)
SET RCLRUNARGS=
echo --------------------------------------------------------------------
echo    SNAP03: Create snapshot 03
echo --------------------------------------------------------------------
SET CTM_NN=03
"%RCBATCHDIR%\submit.exe" -File="%PROJECTDIR%\SNAP.jcl" -LogLevel=WARNING -LogToConsole=false
IF %ERRORLEVEL% NEQ 0 (
   echo ** Error while executing SNAP%CTM_NN%
   goto nok
)
echo --------------------------------------------------------------------
echo    JCL003: Cleanup
echo --------------------------------------------------------------------
"%RCBATCHDIR%\submit.exe" -File="%PROJECTDIR%\JCL003.jcl" -LogLevel=WARNING -LogToConsole=false
IF %ERRORLEVEL% NEQ 0 (
   echo ** Error while executing JCL003
   goto nok
)
echo ========== Script succeeded ==========
goto end
:nok
echo ========== Script failed - check job output ==========
:end
cd %SOLUTIONDIR%
