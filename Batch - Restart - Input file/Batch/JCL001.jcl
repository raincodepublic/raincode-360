//*********************************************************************
//*   
//*   JCL001 : Initial setup 
//* 
//*********************************************************************
//JCL001 JOB COND=(4,LT)
//*--------------------------------------------------------------------
//*   File cleanup
//*--------------------------------------------------------------------
//STEP001   EXEC PGM=IDCAMS
//SYSPRINT    DD SYSOUT=*
//SYSOUT      DD SYSOUT=*
//SYSIN       DD *
 DELETE ARC03.*
      SET LASTCC=0
      SET MAXCC=0
/*
//*--------------------------------------------------------------------
//*   DBSETUP : Create test accounts
//*--------------------------------------------------------------------
//STEP002   EXEC PGM=DBSETUP
//SYSOUT      DD SYSOUT=*
//SYSPRINT    DD SYSOUT=*
//SYSTSPRT    DD SYSOUT=*
//*--------------------------------------------------------------------
//*   Create output file
//*--------------------------------------------------------------------
//STEP003   EXEC PGM=IEFBR14
//OUTPUT      DD DSN=ARC03.OUTFILE,
//               DISP=(MOD,CATLG,CATLG),AVGREC=K,
//               SPACE=(TRK,(1,5)),
//               DCB=(RECFM=FB,LRECL=80,BLKSIZE=800)
//*--------------------------------------------------------------------
//*   ACCTLST: Create account list
//*--------------------------------------------------------------------
//STEP004   EXEC PGM=ACCTLST,COND=(4,LT)
//SYSOUT      DD SYSOUT=*
//SYSPRINT    DD SYSOUT=*
//SYSTSPRT    DD SYSOUT=*
//ACCTLST     DD DSN=ARC03.ACCTLST,
//               DISP=(NEW,CATLG,DELETE),RECFM=LSEQ
//