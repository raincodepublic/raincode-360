       IDENTIFICATION DIVISION.
       PROGRAM-ID. DBDUMP.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT DBDUMP ASSIGN TO DBDUMP
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS W-DBDUMP-STATUS.
       DATA DIVISION.
       FILE SECTION.
       FD DBDUMP
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS R-DBDUMP.
       01 R-DBDUMP PIC X(80).
       WORKING-STORAGE SECTION.
      *********************
      *     VARIABLES     *
      *********************
       01 W-REC.
          10 W-ACCOUNTNUMBER PIC X(19).
          10 W-CUSTOMERID PIC 9(4). 
          10 W-AMOUNT PIC S9(8)V9(2) COMP-3. 
          10 W-AMOUNT-X PIC ---------9,99.
          10 W-DBDUMP-STATUS PIC XX.
       01 FILLER PIC X(30) VALUE '** CHKP AREA END FOR AR/CTL **' 
          VOLATILE.
      *********************
      *     COPYBOOKS     *
      *********************
       COPY SQLCA.
      *********************
      *     CURSORS       *
      *********************
   	   EXEC SQL
   	      DECLARE CUR01 CURSOR FOR
      	  SELECT 
             AccountNumber, 
             Amount
          FROM 
             dbo.Account
          WHERE
             AccountNumber LIKE 'ARC03%'
          ORDER BY
             AccountNumber
   	   END-EXEC.
      *********************
      *   PROGRAM         *
      *********************
       PROCEDURE DIVISION.
          PERFORM OPEN-DBDUMP
          PERFORM PUT-HEADER
          PERFORM OPEN-CUR01
   	      PERFORM FETCH-CUR01
          PERFORM UNTIL SQLCODE > 0
             MOVE SPACES TO R-DBDUMP
             MOVE W-ACCOUNTNUMBER TO R-DBDUMP
             MOVE W-AMOUNT TO W-AMOUNT-X
             MOVE W-AMOUNT-X TO R-DBDUMP(20:)
             WRITE R-DBDUMP
             DISPLAY R-DBDUMP
   	         PERFORM FETCH-CUR01
          END-PERFORM
          PERFORM CLOSE-CUR01
          PERFORM CLOSE-DBDUMP
          STOP RUN
          .
       PUT-HEADER.
          MOVE 'CONTENTS OF DBO.AMOUNT' 
             TO R-DBDUMP
          PERFORM WRITE-DBDUMP
          MOVE '--------------------------------' 
             TO R-DBDUMP
          PERFORM WRITE-DBDUMP
          MOVE 'Accountnumber             Amount' 
             TO R-DBDUMP
          PERFORM WRITE-DBDUMP
          MOVE '--------------------------------' 
             TO R-DBDUMP
          PERFORM WRITE-DBDUMP
          .
       OPEN-CUR01.
   	      EXEC SQL
   	         OPEN CUR01
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
             CONTINUE
          WHEN OTHER
             DISPLAY '** DBDUMP: OPEN CUR01: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       FETCH-CUR01.
   	      EXEC SQL
   	         FETCH CUR01 INTO
                :W-ACCOUNTNUMBER,
                :W-AMOUNT
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
          WHEN 100
             CONTINUE
          WHEN OTHER
             DISPLAY '** DBDUMP: FETCH CUR01: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       CLOSE-CUR01.
          EXEC SQL
   	         CLOSE CUR01
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
             CONTINUE
          WHEN OTHER
             DISPLAY '** DBDUMP: CLOSE CUR01: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       OPEN-DBDUMP.
          OPEN OUTPUT DBDUMP
          IF W-DBDUMP-STATUS IS NOT EQUAL '00'
             DISPLAY '** DBDUMP: OPEN DBDUMP: STATUS: ' 
                     W-DBDUMP-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       WRITE-DBDUMP.
          WRITE R-DBDUMP
          IF W-DBDUMP-STATUS IS NOT EQUAL '00'
             DISPLAY '** DBDUMP: WRITE DBDUMP: STATUS: ' 
                     W-DBDUMP-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       CLOSE-DBDUMP.
          CLOSE DBDUMP
          IF W-DBDUMP-STATUS IS NOT EQUAL '00'
             DISPLAY '** DBDUMP: CLOSE DBDUMP: STATUS: ' 
                     W-DBDUMP-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
