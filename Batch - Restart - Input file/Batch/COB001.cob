       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB001.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT ACCTLST ASSIGN TO ACCTLST
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS W-ACCTLST-STATUS
           .
           SELECT OUTFILE ASSIGN TO OUTFILE
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE IS SEQUENTIAL
           FILE STATUS IS W-OUTFILE-STATUS
           .
       DATA DIVISION.
       FILE SECTION.
       FD OUTFILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS R-OUTFILE.
       01 R-OUTFILE PIC X(80).
       FD ACCTLST
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS R-ACCTLST.
       01 R-ACCTLST.
          10 R-ACCOUNTNUMBER PIC X(19).
          10 R-AMOUNT PIC 9(8)V9(2).
       WORKING-STORAGE SECTION.
      *********************
      *     VARIABLES     *
      *********************
       01 W-ACCOUNTNUMBER PIC X(19).
       01 W-AMOUNT PIC S9(8)V9(2) COMP-3. 
       01 W-AMOUNT-X PIC ---------9,99.
       01 W-OUTFILE-STATUS PIC XX.
       01 W-ACCTLST-STATUS PIC XX.
       01 W-RESTART PIC X VALUE 'N'.
       01 W-EOF PIC X.
       01 FILLER PIC X(30) VALUE '** CHKP AREA END FOR AR/CTL **' 
          VOLATILE.
      *********************
      *     COPYBOOKS     *
      *********************
       COPY SQLCA.
      *********************
      *   PROGRAM         *
      *********************
       PROCEDURE DIVISION.
          PERFORM OPEN-OUTFILE
          IF W-RESTART = 'N'
             PERFORM PUT-HEADER
             MOVE 'Y' TO W-RESTART
          END-IF
          PERFORM OPEN-ACCTLST
   	      PERFORM READ-ACCTLST
          PERFORM UNTIL W-EOF = 'Y'
             MOVE SPACES TO R-OUTFILE
             MOVE R-ACCOUNTNUMBER TO R-OUTFILE
             MOVE R-AMOUNT TO W-AMOUNT-X
             MOVE W-AMOUNT-X TO R-OUTFILE(20:)
             MOVE R-AMOUNT TO W-AMOUNT
             ADD 10 TO W-AMOUNT
             PERFORM UPDATE-AMOUNT
             MOVE W-AMOUNT TO W-AMOUNT-X
             MOVE W-AMOUNT-X TO R-OUTFILE(33:)
             PERFORM WRITE-OUTFILE
   	         PERFORM READ-ACCTLST
          END-PERFORM
          PERFORM CLOSE-ACCTLST
          PERFORM CLOSE-OUTFILE
          STOP RUN
          .
       PUT-HEADER.
          MOVE 'COB001 UPDATES' 
             TO R-OUTFILE
          PERFORM WRITE-OUTFILE
          MOVE '---------------------------------------------' 
             TO R-OUTFILE
          PERFORM WRITE-OUTFILE
          MOVE 'Accountnumber        Amnt Before   Amnt After' 
             TO R-OUTFILE
          PERFORM WRITE-OUTFILE
          MOVE '---------------------------------------------' 
             TO R-OUTFILE
          PERFORM WRITE-OUTFILE
          .
       OPEN-ACCTLST.
          OPEN INPUT ACCTLST
          IF W-ACCTLST-STATUS IS NOT EQUAL '00'
             DISPLAY '** ACCTLST: OPEN ACCTLST: STATUS: ' 
                     W-ACCTLST-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       READ-ACCTLST.
          READ ACCTLST
          EVALUATE W-ACCTLST-STATUS
          WHEN '00'
             CONTINUE
          WHEN '10'
             MOVE 'Y' TO W-EOF
          WHEN OTHER
             DISPLAY '** ACCTLST: READ ACCTLST: STATUS: ' 
                     W-ACCTLST-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       CLOSE-ACCTLST.
          CLOSE ACCTLST
          IF W-ACCTLST-STATUS IS NOT EQUAL '00'
             DISPLAY '** ACCTLST: CLOSE ACCTLST: STATUS: ' 
                     W-ACCTLST-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       UPDATE-AMOUNT.
          EXEC SQL
   	         UPDATE
                dbo.Account
             SET
                Amount = :W-AMOUNT
             WHERE 
                AccountNumber = :R-ACCOUNTNUMBER
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
             CONTINUE
          WHEN OTHER
             DISPLAY '** COB001: UPDATE ACCOUNT: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             PERFORM CLOSE-OUTFILE
             STOP RUN
          END-EVALUATE
          .
       OPEN-OUTFILE.
          OPEN OUTPUT OUTFILE
          IF W-OUTFILE-STATUS IS NOT EQUAL '00'
             DISPLAY '** COB001: OPEN OUTFILE: STATUS: ' 
                     W-OUTFILE-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       WRITE-OUTFILE.
          WRITE R-OUTFILE
          IF W-OUTFILE-STATUS IS NOT EQUAL '00'
             DISPLAY '** COB001: WRITE OUTFILE: STATUS: ' 
                     W-OUTFILE-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       CLOSE-OUTFILE.
          CLOSE OUTFILE
          IF W-OUTFILE-STATUS IS NOT EQUAL '00'
             DISPLAY '** COB001: CLOSE OUTFILE: STATUS: ' 
                     W-OUTFILE-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
