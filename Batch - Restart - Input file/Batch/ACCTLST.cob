       IDENTIFICATION DIVISION.
       PROGRAM-ID. ACCTLST.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT ACCTLST ASSIGN TO ACCTLST
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS W-ACCTLST-STATUS.
       DATA DIVISION.
       FILE SECTION.
       FD ACCTLST
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS R-ACCTLST.
       01 R-ACCTLST.
          10 R-ACCOUNTNUMBER PIC X(19).
          10 R-AMOUNT PIC 9(8)V9(2).
       WORKING-STORAGE SECTION.
      *********************
      *     VARIABLES     *
      *********************
       01 W-REC.
          10 W-ACCOUNTNUMBER PIC X(19).
          10 W-CUSTOMERID PIC 9(4). 
          10 W-AMOUNT PIC S9(8)V9(2) COMP-3. 
          10 W-AMOUNT-X PIC ---------9,99.
          10 W-ACCTLST-STATUS PIC XX.
          10 W-SQLCODE PIC -(7)9.
       01 FILLER PIC X(30) VALUE '** CHKP AREA END FOR AR/CTL **' 
          VOLATILE.
      *********************
      *     COPYBOOKS     *
      *********************
       COPY SQLCA.
      *********************
      *     CURSORS       *
      *********************
   	   EXEC SQL
   	      DECLARE CUR01 CURSOR FOR
      	  SELECT 
             AccountNumber,
             Amount
          FROM 
             dbo.Account
          WHERE
             AccountNumber LIKE 'ARC03%'
          ORDER BY
             AccountNumber
   	   END-EXEC.
      *********************
      *   PROGRAM         *
      *********************
       PROCEDURE DIVISION.
          PERFORM OPEN-ACCTLST
          PERFORM OPEN-CUR01
   	      PERFORM FETCH-CUR01
          PERFORM UNTIL SQLCODE > 0
             MOVE W-ACCOUNTNUMBER TO R-ACCOUNTNUMBER
             MOVE W-AMOUNT TO R-AMOUNT
             PERFORM WRITE-ACCTLST
   	         PERFORM FETCH-CUR01
          END-PERFORM
          PERFORM CLOSE-CUR01
          PERFORM CLOSE-ACCTLST
          STOP RUN
          .
       OPEN-CUR01.
   	      EXEC SQL
   	         OPEN CUR01
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
             CONTINUE
          WHEN OTHER
             DISPLAY '** ACCTLST: OPEN CUR01: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       FETCH-CUR01.
   	      EXEC SQL
   	         FETCH CUR01 INTO
                :W-ACCOUNTNUMBER,
                :W-AMOUNT
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
          WHEN 100
             CONTINUE
          WHEN OTHER
             MOVE SQLCODE TO W-SQLCODE
             DISPLAY '** ACCTLST: FETCH CUR01: SQLCODE: ' W-SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       CLOSE-CUR01.
          EXEC SQL
   	         CLOSE CUR01
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
             CONTINUE
          WHEN OTHER
             DISPLAY '** ACCTLST: CLOSE CUR01: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       OPEN-ACCTLST.
          OPEN OUTPUT ACCTLST
          IF W-ACCTLST-STATUS IS NOT EQUAL '00'
             DISPLAY '** ACCTLST: OPEN ACCTLST: STATUS: ' 
                     W-ACCTLST-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       WRITE-ACCTLST.
          WRITE R-ACCTLST
          IF W-ACCTLST-STATUS IS NOT EQUAL '00'
             DISPLAY '** ACCTLST: WRITE ACCTLST: STATUS: ' 
                     W-ACCTLST-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
       CLOSE-ACCTLST.
          CLOSE ACCTLST
          IF W-ACCTLST-STATUS IS NOT EQUAL '00'
             DISPLAY '** ACCTLST: CLOSE ACCTLST: STATUS: ' 
                     W-ACCTLST-STATUS
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-IF
          .
