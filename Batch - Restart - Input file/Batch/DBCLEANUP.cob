       IDENTIFICATION DIVISION.
       PROGRAM-ID. DBCLEANUP.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *********************
      *     VARIABLES     *
      *********************
       01 W-ACCOUNTNUMBER PIC X(19).
       01 W-NBR PIC 99. 
      *********************
      *     COPYBOOKS     *
      *********************
       COPY SQLCA.
      *********************
      *   PROGRAM         *
      *********************
       PROCEDURE DIVISION.
          PERFORM ACCOUNT-DELETE
          STOP RUN
          .
       ACCOUNT-DELETE.
   	      EXEC SQL
             DELETE  
             FROM 
                dbo.Account
             WHERE
                AccountNumber LIKE 'ARC03%'
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
          WHEN 100
             CONTINUE
          WHEN OTHER
             DISPLAY '** DBCLEANUP: DELETE: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
