      ***********************************************************
      *                                                         *
      *   CUSTOMER LIST                                         *
      *                                                         *
      ***********************************************************
       01 COB004A.
           10 COB004A-IN.
   1           20 COB004A-DIRECTION PIC X(01).
                   88 COB004A-DOWN VALUE 'D'.
                   88 COB004A-UP VALUE 'U'.
   2           20 COB004A-CUSTOMERID-FIRST pic S9(4) COMP. 
   2           20 COB004A-CUSTOMERID-LAST pic S9(4) COMP.
  15           20 COB004A-FIRSTNAME-FIRST PIC X(15).
  15           20 COB004A-FIRSTNAME-LAST PIC X(15).
  15           20 COB004A-LASTNAME-FIRST PIC X(15).
  15           20 COB004A-LASTNAME-LAST PIC X(15).
   6           20 COB004A-CUSTOMERID-FILTER PIC 999999.
  15           20 COB004A-LASTNAME-FILTER PIC X(15).
  15           20 COB004A-FIRSTNAME-FILTER PIC X(15).
   4           20 COB004A-ZIPCODE-FILTER PIC X(4).
  10           20 COB004A-CITY-FILTER PIC X(10).
  10           20 COB004A-BIRTHDATE-FILTER PIC X(10).
   2           20 COB004A-LINECOUNT PIC S9(4) COMP.
   1           20 COB004A-SORT PIC X.
                   88 COB004A-SORT-CUSTOMERID VALUE 'C'.
                   88 COB004A-SORT-FIRSTNAME VALUE 'F'.
                   88 COB004A-SORT-LASTNAME VALUE 'L'.
           10 COB004A-OUT.
   4           20 COB004A-SQLCODE PIC S9(8) COMP-5.
   2           20 COB004A-FETCHED PIC S9(4) COMP.
  50           20 COB004A-CUSTOMERID PIC S9(4) COMP OCCURS 25.
 500           20 COB004A-LASTNAME PIC X(20) OCCURS 25.
 500           20 COB004A-FIRSTNAME PIC X(20) OCCURS 25.
 100           20 COB004A-ZIPCODE PIC X(4) OCCURS 25.
 500           20 COB004A-CITY PIC X(20) OCCURS 25.
 250           20 COB004A-BIRTHDATE PIC X(10) OCCURS 25.
----  
2034  

