using Microsoft.AspNetCore.Mvc;
using RainCode.Core.Logging;
using RainCode.Samples;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions.QIX;
using RainCodeLegacyRuntime.QIX.Interface;
using RainCodeLegacyRuntime.Sql.SqlServer;
using RainCodeLegacyRuntimeUtils;
using System.Net;
using System.Text.Json;


namespace API.Demo.RCMainframeAPI
{
    [ApiController]
    [Route("/Api")]
    public class APIController : ControllerBase
    {
        private readonly ILogger<APIController> _logger;
        private string PLI001 => "PLI001";
        private string COB004A => "COB004A";

        private JsonSerializerOptions options = new JsonSerializerOptions
        {
            WriteIndented = true
        };
        public APIController(ILogger<APIController> logger)
        {
           _logger = logger;
        }
        [HttpGet]
        [Route("/COB004A")]
        public IActionResult GetJson([FromQuery]COB004AInput cOB004AInput,Level RcLogLevel)
        {
            _logger.Log(LogLevel.Information, "Executing the GetJson...");
            Logger.CreateAndRegister<ConsoleLogger>(); //Creating console logger and setting the loglevel received
            Logger.LogLevel = RcLogLevel;
            RainCodeLegacyRuntime.Core.ExecutionContext ec = CreateSetExecutionContext();
            var link = ec.QixInterface.Factory.CreateLink();
            link.Context = ec;
            link.PROGRAM = COB004A;
            var ca = ec.Allocate(COB004AHelper.StructureSize);
            //Set Input values
            COB004AHelper cob004AHelper = new COB004AHelper(ec, ca);
            cob004AHelper.COB004A_DIRECTION = cOB004AInput.DIRECTION;
            cob004AHelper.COB004A_SORT = cOB004AInput.SORT;
            cob004AHelper.COB004A_LINECOUNT = cOB004AInput.LINECOUNT;
#pragma warning disable CS8601 
            cob004AHelper.COB004A_CUSTOMERID_FILTER = cOB004AInput.CUSTOMERID_FILTER;
            cob004AHelper.COB004A_BIRTHDATE_FILTER  = HandleNull(cOB004AInput.BIRTHDATE_FILTER);
            cob004AHelper.COB004A_FIRSTNAME_FILTER = HandleNull(cOB004AInput.FIRSTNAME_FILTER);

            cob004AHelper.COB004A_LASTNAME_FILTER = HandleNull(cOB004AInput.LASTNAME_FILTER);
            cob004AHelper.COB004A_CITY_FILTER     = HandleNull(cOB004AInput.CITY_FILTER);
            cob004AHelper.COB004A_ZIPCODE_FILTER  = HandleNull(cOB004AInput.ZIPCODE_FILTER);

            cob004AHelper.COB004A_CUSTOMERID_FIRST = cOB004AInput.CUSTOMERID_FIRST;
            cob004AHelper.COB004A_CUSTOMERID_LAST  = cOB004AInput.CUSTOMERID_LAST;
            cob004AHelper.COB004A_FIRSTNAME_FIRST = HandleNull(cOB004AInput.FIRSTNAME_FIRST);
            cob004AHelper.COB004A_FIRSTNAME_LAST = HandleNull(cOB004AInput.FIRSTNAME_LAST);
            cob004AHelper.COB004A_LASTNAME_FIRST = HandleNull(cOB004AInput.LASTNAME_FIRST);
            cob004AHelper.COB004A_LASTNAME_LAST    = HandleNull(cOB004AInput.LASTNAME_LAST);
#pragma warning restore CS8601 

            link.COMMAREA = new QixPointer(ca);

            try
            {
                ec.QixInterface.Operation.Link(link);
                ec.SqlRuntime.commit();
            }
            catch (AbendException e)
            {
                _logger.LogError("Abend {0} detected", e.Message);
                return Response(HttpStatusCode.InternalServerError, JsonSerializer.Serialize<COB004AInput>(cOB004AInput, options));
            }
            //Set Output values
            COB004AOutput cOB004AOutput = new COB004AOutput();
            cOB004AOutput.COB004A_SQLCODE = cob004AHelper.COB004A_SQLCODE;
            cOB004AOutput.COB004A_FETCHED = cob004AHelper.COB004A_FETCHED;
            cOB004AOutput.COB004A_LASTNAME = cob004AHelper.COB004A_OUT.COB004A_LASTNAME.ToList().Where(q => !q.Contains("\0")); //Don't copy null entries
            cOB004AOutput.COB004A_FIRSTNAME = cob004AHelper.COB004A_OUT.COB004A_FIRSTNAME.ToList().Where(q => !q.Contains("\0"));
            cOB004AOutput.COB004A_ZIPCODE = cob004AHelper.COB004A_OUT.COB004A_ZIPCODE.ToList().Where(q => !q.Contains("\0"));
            cOB004AOutput.COB004A_CITY = cob004AHelper.COB004A_OUT.COB004A_CITY.ToList().Where(q => !q.Contains("\0"));
            cOB004AOutput.COB004A_BIRTHDATE = cob004AHelper.COB004A_OUT.COB004A_BIRTHDATE.ToList().Where(q => !q.Contains("\0"));
            string ResponseBody = JsonSerializer.Serialize<COB004AOutput>(cOB004AOutput, options);
            //string ResponseBody = cob004AHelper.COB004A_OUT.ToJSON()
            if (cOB004AOutput.COB004A_SQLCODE != 0 || cOB004AOutput.COB004A_FETCHED == 0)
            {
                return Response(HttpStatusCode.NotFound, ResponseBody);
            }
            return Response(HttpStatusCode.OK, ResponseBody);
        }

        [HttpPost]
        [Route("/PLI001")]
        public IActionResult PostJson([FromBody] PLI001InputOutput pLI001InputOutput,Level RcLogLevel)
        {
            _logger.Log(LogLevel.Information, "Executing the GetJson...");
            Logger.CreateAndRegister<ConsoleLogger>();
            Logger.LogLevel = RcLogLevel;
            RainCodeLegacyRuntime.Core.ExecutionContext ec = CreateSetExecutionContext();
            var link = ec.QixInterface.Factory.CreateLink();
            link.Context = ec;
            link.PROGRAM = PLI001;
            var ca = ec.Allocate(PLI001Helper.StructureSize);
            PLI001Helper pLI001Helper = new PLI001Helper(ec, ca);

            pLI001Helper.P001_ACCOUNTNUMBER_FROM = pLI001InputOutput.P001_ACCOUNTNUMBER_FROM;
            pLI001Helper.P001_ACCOUNTNUMBER_TO = pLI001InputOutput.P001_ACCOUNTNUMBER_TO;
            pLI001Helper.P001_AMOUNT = pLI001InputOutput.P001_AMOUNT;
            pLI001Helper.P001_DESCRIPTIONTEXT = pLI001InputOutput.P001_DESCRIPTIONTEXT;

            link.COMMAREA = new QixPointer(ca);

            try
            {
                ec.QixInterface.Operation.Link(link);
                ec.SqlRuntime.commit();
            }
            catch (AbendException e)
            {
                Console.WriteLine("Abend {0} detected", e.Message);
                Response(HttpStatusCode.InternalServerError, JsonSerializer.Serialize<PLI001InputOutput>(pLI001InputOutput, options).Replace("\\u0000", ""));
            }
            pLI001InputOutput.P001_SQLCODE = pLI001Helper.P001_SQLCODE;
            pLI001InputOutput.P001_ERRTXT = pLI001Helper.P001_ERRTXT;
            //string ResponseBody = pLI001Helper.ToJSON().Replace("\\u0000", ""); //Latest version of compiler
            string ResponseBody = JsonSerializer.Serialize<PLI001InputOutput>(pLI001InputOutput, options).Replace("\\u0000", "");
            if (pLI001Helper.P001_SQLCODE != 0)
            {
                return Response(HttpStatusCode.Conflict, ResponseBody);
            }
            return Response(HttpStatusCode.Created, ResponseBody);
        }
        private string HandleNull(string value)
        {
            if (string.IsNullOrEmpty(value))
                return "";
            return value;
        }
        private RainCodeLegacyRuntime.Core.ExecutionContext CreateSetExecutionContext()
        {
            var ecargs = new ExecutionContextArgs()
            {
                QixMode = true,
            };
            var ec = new RainCodeLegacyRuntime.Core.ExecutionContext(ecargs);
            ec.SqlRuntime = new SqlServerRuntime(ec, Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog = BankDemo;");
            ec.SqlRuntime.ExecutionStrategy = RainCodeLegacyRuntime.Sql.SqlRuntime.ExecutionModeStrategy.DynamicOnly;
            ec.IOOperation.ConfigureSystemDatasets();
            return ec;
        }
        private ObjectResult Response(HttpStatusCode httpStatusCode, string responseBody)
        {
            _logger.Log(LogLevel.Information, "Returning the response, the httpStatusCode: " + (int)httpStatusCode);
            return new ObjectResult(responseBody)
            {
                StatusCode = (int)httpStatusCode
            };
        }
    }
    public class COB004AInput
    {
        public string DIRECTION { get; set; }
        public string SORT { get; set; }

        public int CUSTOMERID_FIRST { get; set; }
        public int CUSTOMERID_LAST { get; set; }
        public string? FIRSTNAME_FIRST { get; set; }
        public string? FIRSTNAME_LAST { get; set; }
        public string? LASTNAME_FIRST { get; set; }
        public string? LASTNAME_LAST { get; set; }

        public int CUSTOMERID_FILTER { get; set; }
        public string? LASTNAME_FILTER { get; set; }
        public string? FIRSTNAME_FILTER { get; set; }
        public string? ZIPCODE_FILTER { get; set; }
        public string? CITY_FILTER { get; set; }
        public string? BIRTHDATE_FILTER { get; set; }

        public int LINECOUNT { get; set; }
    }
    public class COB004AOutput
    {
        public int COB004A_SQLCODE { get; set; }
        public int COB004A_FETCHED { get; set; }
        public IEnumerable<string> COB004A_LASTNAME { get; set; }
        public IEnumerable<string> COB004A_FIRSTNAME { get; set; }
        public IEnumerable<string> COB004A_ZIPCODE { get; set; }
        public IEnumerable<string> COB004A_CITY { get; set; }
        public IEnumerable<string> COB004A_BIRTHDATE { get; set; }

    }
    public class PLI001InputOutput
    {
        public string P001_ACCOUNTNUMBER_FROM { get; set; }
        public string P001_ACCOUNTNUMBER_TO { get; set; }
        public decimal P001_AMOUNT { get; set; }
        public string P001_DESCRIPTIONTEXT { get; set; }
        public int P001_SQLCODE { get; set; }
        public string? P001_ERRTXT { get; set; }
    }
}
