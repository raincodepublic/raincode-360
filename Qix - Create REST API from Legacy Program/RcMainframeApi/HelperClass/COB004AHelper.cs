namespace RainCode.Samples
{
    using RainCodeLegacyRuntime.Core;
    using RainCodeLegacyRuntime.Descriptor;
    using RainCodeLegacyRuntime.Types;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Generated on 02/12/2024 at 17:49:01
    /// by Raincode COBOL compiler version 5.0.3.0 (Build nb_v5.0.2.0-7-g91a6eb07eda)
    /// with arguments -HelperClassName=RainCode.Samples.COB004AHelper -SQL=sqlserver -HelperStructure=COB004A -IncludeSearchPath=.\Legacy -HelperClassOutputDir=.\RcMainframeApi\HelperClass .\Legacy\COB004A.cpy
    ///
    /// Input program path: 'C:\Repositories\raincode360\Qix - Create REST API from Legacy Program\Legacy\COB004A.cpy'
    /// Structure was originally in files:
    ///   COB004A.cpy
    /// </summary>
    public partial class COB004AHelper : BaseHelper, System.IDisposable
    {
        public override string Name => "COB004A";
        public const int StructureSize = 2034;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public COB004AHelper(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public COB004AHelper(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static COB004AHelper StackAllocate(ExecutionContext ctxt)
        {
            return new COB004AHelper(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static COB004AHelper Allocate(ExecutionContext ctxt)
        {
            COB004AHelper result = new COB004AHelper(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea COB004A_IN => theMemoryArea.Substr(0, 128);
            public MemoryArea COB004A_OUT => theMemoryArea.Substr(128, 1906);
        }

        /// <summary>
        /// name:   COB004A_IN
        /// type:   COB004A_COB004A_IN
        /// offset: 0
        /// size:   128
        /// </summary> 
        private COB004A_COB004A_IN theCOB004A_IN_0;
        public COB004A_COB004A_IN COB004A_IN
        {
            get
            {
                if (theCOB004A_IN_0 == null)
                {
                    theCOB004A_IN_0 = new COB004A_COB004A_IN(theContext, Raw.COB004A_IN);
                }
                return theCOB004A_IN_0;
            }
        }

        private TypedLValue theCachedCOB004A_IN_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_IN_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_IN_TypedLValue.IsNull)
                {
                    theCachedCOB004A_IN_TypedLValue = new TypedLValue(Raw.COB004A_IN, COB004A_COB004A_IN.GetStaticDescriptor());
                }
                return theCachedCOB004A_IN_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_OUT
        /// type:   COB004A_COB004A_OUT
        /// offset: 128
        /// size:   1906
        /// </summary> 
        private COB004A_COB004A_OUT theCOB004A_OUT_16;
        public COB004A_COB004A_OUT COB004A_OUT
        {
            get
            {
                if (theCOB004A_OUT_16 == null)
                {
                    theCOB004A_OUT_16 = new COB004A_COB004A_OUT(theContext, Raw.COB004A_OUT);
                }
                return theCOB004A_OUT_16;
            }
        }

        private TypedLValue theCachedCOB004A_OUT_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_OUT_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_OUT_TypedLValue.IsNull)
                {
                    theCachedCOB004A_OUT_TypedLValue = new TypedLValue(Raw.COB004A_OUT, COB004A_COB004A_OUT.GetStaticDescriptor());
                }
                return theCachedCOB004A_OUT_TypedLValue;
            }
        }

        public string COB004A_BIRTHDATE_FILTER
        {
            get => COB004A_IN.COB004A_BIRTHDATE_FILTER;
            set => COB004A_IN.COB004A_BIRTHDATE_FILTER = value;
        }

        public string COB004A_CITY_FILTER
        {
            get => COB004A_IN.COB004A_CITY_FILTER;
            set => COB004A_IN.COB004A_CITY_FILTER = value;
        }

        public int COB004A_CUSTOMERID_FILTER
        {
            get => COB004A_IN.COB004A_CUSTOMERID_FILTER;
            set => COB004A_IN.COB004A_CUSTOMERID_FILTER = value;
        }

        public int COB004A_CUSTOMERID_FIRST
        {
            get => COB004A_IN.COB004A_CUSTOMERID_FIRST;
            set => COB004A_IN.COB004A_CUSTOMERID_FIRST = value;
        }

        public int COB004A_CUSTOMERID_LAST
        {
            get => COB004A_IN.COB004A_CUSTOMERID_LAST;
            set => COB004A_IN.COB004A_CUSTOMERID_LAST = value;
        }

        public string COB004A_DIRECTION
        {
            get => COB004A_IN.COB004A_DIRECTION;
            set => COB004A_IN.COB004A_DIRECTION = value;
        }

        public bool COB004A_DOWN => COB004A_IN.COB004A_DOWN;

        public int COB004A_FETCHED
        {
            get => COB004A_OUT.COB004A_FETCHED;
            set => COB004A_OUT.COB004A_FETCHED = value;
        }

        public string COB004A_FIRSTNAME_FILTER
        {
            get => COB004A_IN.COB004A_FIRSTNAME_FILTER;
            set => COB004A_IN.COB004A_FIRSTNAME_FILTER = value;
        }

        public string COB004A_FIRSTNAME_FIRST
        {
            get => COB004A_IN.COB004A_FIRSTNAME_FIRST;
            set => COB004A_IN.COB004A_FIRSTNAME_FIRST = value;
        }

        public string COB004A_FIRSTNAME_LAST
        {
            get => COB004A_IN.COB004A_FIRSTNAME_LAST;
            set => COB004A_IN.COB004A_FIRSTNAME_LAST = value;
        }

        public string COB004A_LASTNAME_FILTER
        {
            get => COB004A_IN.COB004A_LASTNAME_FILTER;
            set => COB004A_IN.COB004A_LASTNAME_FILTER = value;
        }

        public string COB004A_LASTNAME_FIRST
        {
            get => COB004A_IN.COB004A_LASTNAME_FIRST;
            set => COB004A_IN.COB004A_LASTNAME_FIRST = value;
        }

        public string COB004A_LASTNAME_LAST
        {
            get => COB004A_IN.COB004A_LASTNAME_LAST;
            set => COB004A_IN.COB004A_LASTNAME_LAST = value;
        }

        public int COB004A_LINECOUNT
        {
            get => COB004A_IN.COB004A_LINECOUNT;
            set => COB004A_IN.COB004A_LINECOUNT = value;
        }

        public string COB004A_SORT
        {
            get => COB004A_IN.COB004A_SORT;
            set => COB004A_IN.COB004A_SORT = value;
        }

        public bool COB004A_SORT_CUSTOMERID => COB004A_IN.COB004A_SORT_CUSTOMERID;

        public bool COB004A_SORT_FIRSTNAME => COB004A_IN.COB004A_SORT_FIRSTNAME;

        public bool COB004A_SORT_LASTNAME => COB004A_IN.COB004A_SORT_LASTNAME;

        public int COB004A_SQLCODE
        {
            get => COB004A_OUT.COB004A_SQLCODE;
            set => COB004A_OUT.COB004A_SQLCODE = value;
        }

        public bool COB004A_UP => COB004A_IN.COB004A_UP;

        public string COB004A_ZIPCODE_FILTER
        {
            get => COB004A_IN.COB004A_ZIPCODE_FILTER;
            set => COB004A_IN.COB004A_ZIPCODE_FILTER = value;
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {
              var temp_0 = FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP());
              var temp_1 = CharacterStringType.Get(20,0,false);
              var temp_2 = CharacterStringType.Get(20,0,false);
              var temp_3 = CharacterStringType.Get(4,0,false);
              var temp_4 = CharacterStringType.Get(20,0,false);
              var temp_5 = CharacterStringType.Get(10,0,false);

                theCachedDesc =
                    StructTypeDescriptor.Get(new FieldDescriptor[] {
                            new FieldDescriptor ("COB004A-IN",0,StructTypeDescriptor.Get(new FieldDescriptor[] {
                                    new FieldDescriptor ("COB004A-DIRECTION",0,CharacterStringType.Get(1,0,false)),
                                    new FieldDescriptor ("COB004A-CUSTOMERID-FIRST",1,FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP())),
                                    new FieldDescriptor ("COB004A-CUSTOMERID-LAST",3,FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP())),
                                    new FieldDescriptor ("COB004A-FIRSTNAME-FIRST",5,CharacterStringType.Get(15,0,false)),
                                    new FieldDescriptor ("COB004A-FIRSTNAME-LAST",20,CharacterStringType.Get(15,0,false)),
                                    new FieldDescriptor ("COB004A-LASTNAME-FIRST",35,CharacterStringType.Get(15,0,false)),
                                    new FieldDescriptor ("COB004A-LASTNAME-LAST",50,CharacterStringType.Get(15,0,false)),
                                    new FieldDescriptor ("COB004A-CUSTOMERID-FILTER",65,FixedDecimalDescriptor.Get(6,0,FixedDecimalDescriptor.Storage_DISPLAY_UNSIGNED())),
                                    new FieldDescriptor ("COB004A-LASTNAME-FILTER",71,CharacterStringType.Get(15,0,false)),
                                    new FieldDescriptor ("COB004A-FIRSTNAME-FILTER",86,CharacterStringType.Get(15,0,false)),
                                    new FieldDescriptor ("COB004A-ZIPCODE-FILTER",101,CharacterStringType.Get(4,0,false)),
                                    new FieldDescriptor ("COB004A-CITY-FILTER",105,CharacterStringType.Get(10,0,false)),
                                    new FieldDescriptor ("COB004A-BIRTHDATE-FILTER",115,CharacterStringType.Get(10,0,false)),
                                    new FieldDescriptor ("COB004A-LINECOUNT",125,FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP())),
                                    new FieldDescriptor ("COB004A-SORT",127,CharacterStringType.Get(1,0,false))
                            })
                            ),
                            new FieldDescriptor ("COB004A-OUT",128,StructTypeDescriptor.Get(new FieldDescriptor[] {
                                    new FieldDescriptor ("COB004A-SQLCODE",0,FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP5())),
                                    new FieldDescriptor ("COB004A-FETCHED",4,FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP())),
                                    new FieldDescriptor ("COB004A-CUSTOMERID",6,temp_0.GetArray(1,25,2,false)),
                                    new FieldDescriptor ("COB004A-LASTNAME",56,temp_1.GetArray(1,25,20,false)),
                                    new FieldDescriptor ("COB004A-FIRSTNAME",556,temp_2.GetArray(1,25,20,false)),
                                    new FieldDescriptor ("COB004A-ZIPCODE",1056,temp_3.GetArray(1,25,4,false)),
                                    new FieldDescriptor ("COB004A-CITY",1156,temp_4.GetArray(1,25,20,false)),
                                    new FieldDescriptor ("COB004A-BIRTHDATE",1656,temp_5.GetArray(1,25,10,false))
                            })
                            )
                    });
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class COB004A_COB004A_IN : BaseHelper, System.IDisposable
    {
        public const int StructureSize = 128;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public COB004A_COB004A_IN(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public COB004A_COB004A_IN(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static COB004A_COB004A_IN StackAllocate(ExecutionContext ctxt)
        {
            return new COB004A_COB004A_IN(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static COB004A_COB004A_IN Allocate(ExecutionContext ctxt)
        {
            COB004A_COB004A_IN result = new COB004A_COB004A_IN(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea COB004A_DIRECTION => theMemoryArea.Substr(0, 1);
            public MemoryArea COB004A_CUSTOMERID_FIRST => theMemoryArea.Substr(1, 2);
            public MemoryArea COB004A_CUSTOMERID_LAST => theMemoryArea.Substr(3, 2);
            public MemoryArea COB004A_FIRSTNAME_FIRST => theMemoryArea.Substr(5, 15);
            public MemoryArea COB004A_FIRSTNAME_LAST => theMemoryArea.Substr(20, 15);
            public MemoryArea COB004A_LASTNAME_FIRST => theMemoryArea.Substr(35, 15);
            public MemoryArea COB004A_LASTNAME_LAST => theMemoryArea.Substr(50, 15);
            public MemoryArea COB004A_CUSTOMERID_FILTER => theMemoryArea.Substr(65, 6);
            public MemoryArea COB004A_LASTNAME_FILTER => theMemoryArea.Substr(71, 15);
            public MemoryArea COB004A_FIRSTNAME_FILTER => theMemoryArea.Substr(86, 15);
            public MemoryArea COB004A_ZIPCODE_FILTER => theMemoryArea.Substr(101, 4);
            public MemoryArea COB004A_CITY_FILTER => theMemoryArea.Substr(105, 10);
            public MemoryArea COB004A_BIRTHDATE_FILTER => theMemoryArea.Substr(115, 10);
            public MemoryArea COB004A_LINECOUNT => theMemoryArea.Substr(125, 2);
            public MemoryArea COB004A_SORT => theMemoryArea.Substr(127, 1);
        }

        /// <summary>
        /// name:   COB004A_DIRECTION
        /// type:   string
        /// offset: 0
        /// size:   1
        /// </summary> 
        public string COB004A_DIRECTION
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_DIRECTION);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_DIRECTION);
        }

        private TypedLValue theCachedCOB004A_DIRECTION_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_DIRECTION_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_DIRECTION_TypedLValue.IsNull)
                {
                    theCachedCOB004A_DIRECTION_TypedLValue = new TypedLValue(Raw.COB004A_DIRECTION, CharacterStringType.Get(1,0,false));
                }
                return theCachedCOB004A_DIRECTION_TypedLValue;
            }
        }

        public bool COB004A_DOWN
        {
            get => StringOperations.Compare2(theContext, Raw.COB004A_DIRECTION.Asp.Mem, Raw.COB004A_DIRECTION.Ofs, Raw.COB004A_DIRECTION.Size, theContext.Encoding.GetBytes("D"), 0, 1) == 0;
            set { if (value) Convert.move_string_to_char_string(theContext, "D", Raw.COB004A_DIRECTION); else throw new System.ArgumentException("Cannot set COB004A_DOWN to false"); }
        }

        public bool COB004A_UP
        {
            get => StringOperations.Compare2(theContext, Raw.COB004A_DIRECTION.Asp.Mem, Raw.COB004A_DIRECTION.Ofs, Raw.COB004A_DIRECTION.Size, theContext.Encoding.GetBytes("U"), 0, 1) == 0;
            set { if (value) Convert.move_string_to_char_string(theContext, "U", Raw.COB004A_DIRECTION); else throw new System.ArgumentException("Cannot set COB004A_UP to false"); }
        }

        /// <summary>
        /// name:   COB004A_CUSTOMERID_FIRST
        /// type:   int
        /// offset: 1
        /// size:   2
        /// </summary> 
        public int COB004A_CUSTOMERID_FIRST
        {
            get => Convert.cnv_fixed_dec_to_int32(theContext, Raw.COB004A_CUSTOMERID_FIRST, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
            set => Convert.move_int32_to_fixed_dec(theContext, value, Raw.COB004A_CUSTOMERID_FIRST, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
        }

        private TypedLValue theCachedCOB004A_CUSTOMERID_FIRST_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_CUSTOMERID_FIRST_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_CUSTOMERID_FIRST_TypedLValue.IsNull)
                {
                    theCachedCOB004A_CUSTOMERID_FIRST_TypedLValue = new TypedLValue(Raw.COB004A_CUSTOMERID_FIRST, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
                }
                return theCachedCOB004A_CUSTOMERID_FIRST_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_CUSTOMERID_LAST
        /// type:   int
        /// offset: 3
        /// size:   2
        /// </summary> 
        public int COB004A_CUSTOMERID_LAST
        {
            get => Convert.cnv_fixed_dec_to_int32(theContext, Raw.COB004A_CUSTOMERID_LAST, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
            set => Convert.move_int32_to_fixed_dec(theContext, value, Raw.COB004A_CUSTOMERID_LAST, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
        }

        private TypedLValue theCachedCOB004A_CUSTOMERID_LAST_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_CUSTOMERID_LAST_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_CUSTOMERID_LAST_TypedLValue.IsNull)
                {
                    theCachedCOB004A_CUSTOMERID_LAST_TypedLValue = new TypedLValue(Raw.COB004A_CUSTOMERID_LAST, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
                }
                return theCachedCOB004A_CUSTOMERID_LAST_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_FIRSTNAME_FIRST
        /// type:   string
        /// offset: 5
        /// size:   15
        /// </summary> 
        public string COB004A_FIRSTNAME_FIRST
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_FIRSTNAME_FIRST);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_FIRSTNAME_FIRST);
        }

        private TypedLValue theCachedCOB004A_FIRSTNAME_FIRST_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_FIRSTNAME_FIRST_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_FIRSTNAME_FIRST_TypedLValue.IsNull)
                {
                    theCachedCOB004A_FIRSTNAME_FIRST_TypedLValue = new TypedLValue(Raw.COB004A_FIRSTNAME_FIRST, CharacterStringType.Get(15,0,false));
                }
                return theCachedCOB004A_FIRSTNAME_FIRST_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_FIRSTNAME_LAST
        /// type:   string
        /// offset: 20
        /// size:   15
        /// </summary> 
        public string COB004A_FIRSTNAME_LAST
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_FIRSTNAME_LAST);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_FIRSTNAME_LAST);
        }

        private TypedLValue theCachedCOB004A_FIRSTNAME_LAST_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_FIRSTNAME_LAST_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_FIRSTNAME_LAST_TypedLValue.IsNull)
                {
                    theCachedCOB004A_FIRSTNAME_LAST_TypedLValue = new TypedLValue(Raw.COB004A_FIRSTNAME_LAST, CharacterStringType.Get(15,0,false));
                }
                return theCachedCOB004A_FIRSTNAME_LAST_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_LASTNAME_FIRST
        /// type:   string
        /// offset: 35
        /// size:   15
        /// </summary> 
        public string COB004A_LASTNAME_FIRST
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_LASTNAME_FIRST);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_LASTNAME_FIRST);
        }

        private TypedLValue theCachedCOB004A_LASTNAME_FIRST_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_LASTNAME_FIRST_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_LASTNAME_FIRST_TypedLValue.IsNull)
                {
                    theCachedCOB004A_LASTNAME_FIRST_TypedLValue = new TypedLValue(Raw.COB004A_LASTNAME_FIRST, CharacterStringType.Get(15,0,false));
                }
                return theCachedCOB004A_LASTNAME_FIRST_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_LASTNAME_LAST
        /// type:   string
        /// offset: 50
        /// size:   15
        /// </summary> 
        public string COB004A_LASTNAME_LAST
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_LASTNAME_LAST);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_LASTNAME_LAST);
        }

        private TypedLValue theCachedCOB004A_LASTNAME_LAST_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_LASTNAME_LAST_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_LASTNAME_LAST_TypedLValue.IsNull)
                {
                    theCachedCOB004A_LASTNAME_LAST_TypedLValue = new TypedLValue(Raw.COB004A_LASTNAME_LAST, CharacterStringType.Get(15,0,false));
                }
                return theCachedCOB004A_LASTNAME_LAST_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_CUSTOMERID_FILTER
        /// type:   int
        /// offset: 65
        /// size:   6
        /// </summary> 
        public int COB004A_CUSTOMERID_FILTER
        {
            get => Convert.cnv_fixed_dec_to_int32(theContext, Raw.COB004A_CUSTOMERID_FILTER, FixedDecimalDescriptor.Get(6,0,FixedDecimalDescriptor.Storage_DISPLAY_UNSIGNED()));
            set => Convert.move_int32_to_fixed_dec(theContext, value, Raw.COB004A_CUSTOMERID_FILTER, FixedDecimalDescriptor.Get(6,0,FixedDecimalDescriptor.Storage_DISPLAY_UNSIGNED()));
        }

        private TypedLValue theCachedCOB004A_CUSTOMERID_FILTER_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_CUSTOMERID_FILTER_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_CUSTOMERID_FILTER_TypedLValue.IsNull)
                {
                    theCachedCOB004A_CUSTOMERID_FILTER_TypedLValue = new TypedLValue(Raw.COB004A_CUSTOMERID_FILTER, FixedDecimalDescriptor.Get(6,0,FixedDecimalDescriptor.Storage_DISPLAY_UNSIGNED()));
                }
                return theCachedCOB004A_CUSTOMERID_FILTER_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_LASTNAME_FILTER
        /// type:   string
        /// offset: 71
        /// size:   15
        /// </summary> 
        public string COB004A_LASTNAME_FILTER
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_LASTNAME_FILTER);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_LASTNAME_FILTER);
        }

        private TypedLValue theCachedCOB004A_LASTNAME_FILTER_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_LASTNAME_FILTER_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_LASTNAME_FILTER_TypedLValue.IsNull)
                {
                    theCachedCOB004A_LASTNAME_FILTER_TypedLValue = new TypedLValue(Raw.COB004A_LASTNAME_FILTER, CharacterStringType.Get(15,0,false));
                }
                return theCachedCOB004A_LASTNAME_FILTER_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_FIRSTNAME_FILTER
        /// type:   string
        /// offset: 86
        /// size:   15
        /// </summary> 
        public string COB004A_FIRSTNAME_FILTER
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_FIRSTNAME_FILTER);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_FIRSTNAME_FILTER);
        }

        private TypedLValue theCachedCOB004A_FIRSTNAME_FILTER_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_FIRSTNAME_FILTER_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_FIRSTNAME_FILTER_TypedLValue.IsNull)
                {
                    theCachedCOB004A_FIRSTNAME_FILTER_TypedLValue = new TypedLValue(Raw.COB004A_FIRSTNAME_FILTER, CharacterStringType.Get(15,0,false));
                }
                return theCachedCOB004A_FIRSTNAME_FILTER_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_ZIPCODE_FILTER
        /// type:   string
        /// offset: 101
        /// size:   4
        /// </summary> 
        public string COB004A_ZIPCODE_FILTER
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_ZIPCODE_FILTER);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_ZIPCODE_FILTER);
        }

        private TypedLValue theCachedCOB004A_ZIPCODE_FILTER_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_ZIPCODE_FILTER_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_ZIPCODE_FILTER_TypedLValue.IsNull)
                {
                    theCachedCOB004A_ZIPCODE_FILTER_TypedLValue = new TypedLValue(Raw.COB004A_ZIPCODE_FILTER, CharacterStringType.Get(4,0,false));
                }
                return theCachedCOB004A_ZIPCODE_FILTER_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_CITY_FILTER
        /// type:   string
        /// offset: 105
        /// size:   10
        /// </summary> 
        public string COB004A_CITY_FILTER
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_CITY_FILTER);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_CITY_FILTER);
        }

        private TypedLValue theCachedCOB004A_CITY_FILTER_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_CITY_FILTER_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_CITY_FILTER_TypedLValue.IsNull)
                {
                    theCachedCOB004A_CITY_FILTER_TypedLValue = new TypedLValue(Raw.COB004A_CITY_FILTER, CharacterStringType.Get(10,0,false));
                }
                return theCachedCOB004A_CITY_FILTER_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_BIRTHDATE_FILTER
        /// type:   string
        /// offset: 115
        /// size:   10
        /// </summary> 
        public string COB004A_BIRTHDATE_FILTER
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_BIRTHDATE_FILTER);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_BIRTHDATE_FILTER);
        }

        private TypedLValue theCachedCOB004A_BIRTHDATE_FILTER_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_BIRTHDATE_FILTER_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_BIRTHDATE_FILTER_TypedLValue.IsNull)
                {
                    theCachedCOB004A_BIRTHDATE_FILTER_TypedLValue = new TypedLValue(Raw.COB004A_BIRTHDATE_FILTER, CharacterStringType.Get(10,0,false));
                }
                return theCachedCOB004A_BIRTHDATE_FILTER_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_LINECOUNT
        /// type:   int
        /// offset: 125
        /// size:   2
        /// </summary> 
        public int COB004A_LINECOUNT
        {
            get => Convert.cnv_fixed_dec_to_int32(theContext, Raw.COB004A_LINECOUNT, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
            set => Convert.move_int32_to_fixed_dec(theContext, value, Raw.COB004A_LINECOUNT, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
        }

        private TypedLValue theCachedCOB004A_LINECOUNT_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_LINECOUNT_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_LINECOUNT_TypedLValue.IsNull)
                {
                    theCachedCOB004A_LINECOUNT_TypedLValue = new TypedLValue(Raw.COB004A_LINECOUNT, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
                }
                return theCachedCOB004A_LINECOUNT_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_SORT
        /// type:   string
        /// offset: 127
        /// size:   1
        /// </summary> 
        public string COB004A_SORT
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.COB004A_SORT);
            set => Convert.move_string_to_char_string(theContext, value, Raw.COB004A_SORT);
        }

        private TypedLValue theCachedCOB004A_SORT_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_SORT_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_SORT_TypedLValue.IsNull)
                {
                    theCachedCOB004A_SORT_TypedLValue = new TypedLValue(Raw.COB004A_SORT, CharacterStringType.Get(1,0,false));
                }
                return theCachedCOB004A_SORT_TypedLValue;
            }
        }

        public bool COB004A_SORT_CUSTOMERID
        {
            get => StringOperations.Compare2(theContext, Raw.COB004A_SORT.Asp.Mem, Raw.COB004A_SORT.Ofs, Raw.COB004A_SORT.Size, theContext.Encoding.GetBytes("C"), 0, 1) == 0;
            set { if (value) Convert.move_string_to_char_string(theContext, "C", Raw.COB004A_SORT); else throw new System.ArgumentException("Cannot set COB004A_SORT_CUSTOMERID to false"); }
        }

        public bool COB004A_SORT_FIRSTNAME
        {
            get => StringOperations.Compare2(theContext, Raw.COB004A_SORT.Asp.Mem, Raw.COB004A_SORT.Ofs, Raw.COB004A_SORT.Size, theContext.Encoding.GetBytes("F"), 0, 1) == 0;
            set { if (value) Convert.move_string_to_char_string(theContext, "F", Raw.COB004A_SORT); else throw new System.ArgumentException("Cannot set COB004A_SORT_FIRSTNAME to false"); }
        }

        public bool COB004A_SORT_LASTNAME
        {
            get => StringOperations.Compare2(theContext, Raw.COB004A_SORT.Asp.Mem, Raw.COB004A_SORT.Ofs, Raw.COB004A_SORT.Size, theContext.Encoding.GetBytes("L"), 0, 1) == 0;
            set { if (value) Convert.move_string_to_char_string(theContext, "L", Raw.COB004A_SORT); else throw new System.ArgumentException("Cannot set COB004A_SORT_LASTNAME to false"); }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    StructTypeDescriptor.Get(new FieldDescriptor[] {
                            new FieldDescriptor ("COB004A-DIRECTION",0,CharacterStringType.Get(1,0,false)),
                            new FieldDescriptor ("COB004A-CUSTOMERID-FIRST",1,FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP())),
                            new FieldDescriptor ("COB004A-CUSTOMERID-LAST",3,FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP())),
                            new FieldDescriptor ("COB004A-FIRSTNAME-FIRST",5,CharacterStringType.Get(15,0,false)),
                            new FieldDescriptor ("COB004A-FIRSTNAME-LAST",20,CharacterStringType.Get(15,0,false)),
                            new FieldDescriptor ("COB004A-LASTNAME-FIRST",35,CharacterStringType.Get(15,0,false)),
                            new FieldDescriptor ("COB004A-LASTNAME-LAST",50,CharacterStringType.Get(15,0,false)),
                            new FieldDescriptor ("COB004A-CUSTOMERID-FILTER",65,FixedDecimalDescriptor.Get(6,0,FixedDecimalDescriptor.Storage_DISPLAY_UNSIGNED())),
                            new FieldDescriptor ("COB004A-LASTNAME-FILTER",71,CharacterStringType.Get(15,0,false)),
                            new FieldDescriptor ("COB004A-FIRSTNAME-FILTER",86,CharacterStringType.Get(15,0,false)),
                            new FieldDescriptor ("COB004A-ZIPCODE-FILTER",101,CharacterStringType.Get(4,0,false)),
                            new FieldDescriptor ("COB004A-CITY-FILTER",105,CharacterStringType.Get(10,0,false)),
                            new FieldDescriptor ("COB004A-BIRTHDATE-FILTER",115,CharacterStringType.Get(10,0,false)),
                            new FieldDescriptor ("COB004A-LINECOUNT",125,FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP())),
                            new FieldDescriptor ("COB004A-SORT",127,CharacterStringType.Get(1,0,false))
                    });
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY : BaseHelper, System.IDisposable, IEnumerable<int>
    {
        public const int StructureSize = 50;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY StackAllocate(ExecutionContext ctxt)
        {
            return new COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY Allocate(ExecutionContext ctxt)
        {
            COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY result = new COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData: IEnumerable<MemoryArea>
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            internal const int Count0 = 25;
            internal int Count => Count0;

            public MemoryArea this[int idx] => theMemoryArea.Substr(idx * 2, 2);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public IEnumerator<MemoryArea> GetEnumerator()
            {
                for (int idx = 0; idx < Count; ++idx)
                {
                    yield return this[idx];
                }
            }

        }

        public const int Count0 = RawData.Count0;
        public int Count => Raw.Count;

        public int this[int idx]
        {
            get => Convert.cnv_fixed_dec_to_int32(theContext, Raw[idx], FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
            set => Convert.move_int32_to_fixed_dec(theContext, value, Raw[idx], FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<int> GetEnumerator()
        {
            for (int idx = 0; idx < Count; ++idx)
            {
                yield return Convert.cnv_fixed_dec_to_int32(theContext, Raw[idx], FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()).GetArray(1,25,2,false);
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY : BaseHelper, System.IDisposable, IEnumerable<string>
    {
        public const int StructureSize = 500;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY StackAllocate(ExecutionContext ctxt)
        {
            return new COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY Allocate(ExecutionContext ctxt)
        {
            COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY result = new COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData: IEnumerable<MemoryArea>
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            internal const int Count0 = 25;
            internal int Count => Count0;

            public MemoryArea this[int idx] => theMemoryArea.Substr(idx * 20, 20);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public IEnumerator<MemoryArea> GetEnumerator()
            {
                for (int idx = 0; idx < Count; ++idx)
                {
                    yield return this[idx];
                }
            }

        }

        public const int Count0 = RawData.Count0;
        public int Count => Raw.Count;

        public string this[int idx]
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            set => Convert.move_string_to_char_string(theContext, value, Raw[idx]);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<string> GetEnumerator()
        {
            for (int idx = 0; idx < Count; ++idx)
            {
                yield return Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    CharacterStringType.Get(20,0,false).GetArray(1,25,20,false);
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY : BaseHelper, System.IDisposable, IEnumerable<string>
    {
        public const int StructureSize = 500;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY StackAllocate(ExecutionContext ctxt)
        {
            return new COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY Allocate(ExecutionContext ctxt)
        {
            COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY result = new COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData: IEnumerable<MemoryArea>
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            internal const int Count0 = 25;
            internal int Count => Count0;

            public MemoryArea this[int idx] => theMemoryArea.Substr(idx * 20, 20);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public IEnumerator<MemoryArea> GetEnumerator()
            {
                for (int idx = 0; idx < Count; ++idx)
                {
                    yield return this[idx];
                }
            }

        }

        public const int Count0 = RawData.Count0;
        public int Count => Raw.Count;

        public string this[int idx]
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            set => Convert.move_string_to_char_string(theContext, value, Raw[idx]);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<string> GetEnumerator()
        {
            for (int idx = 0; idx < Count; ++idx)
            {
                yield return Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    CharacterStringType.Get(20,0,false).GetArray(1,25,20,false);
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY : BaseHelper, System.IDisposable, IEnumerable<string>
    {
        public const int StructureSize = 100;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY StackAllocate(ExecutionContext ctxt)
        {
            return new COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY Allocate(ExecutionContext ctxt)
        {
            COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY result = new COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData: IEnumerable<MemoryArea>
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            internal const int Count0 = 25;
            internal int Count => Count0;

            public MemoryArea this[int idx] => theMemoryArea.Substr(idx * 4, 4);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public IEnumerator<MemoryArea> GetEnumerator()
            {
                for (int idx = 0; idx < Count; ++idx)
                {
                    yield return this[idx];
                }
            }

        }

        public const int Count0 = RawData.Count0;
        public int Count => Raw.Count;

        public string this[int idx]
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            set => Convert.move_string_to_char_string(theContext, value, Raw[idx]);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<string> GetEnumerator()
        {
            for (int idx = 0; idx < Count; ++idx)
            {
                yield return Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    CharacterStringType.Get(4,0,false).GetArray(1,25,4,false);
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class COB004A_COB004A_OUT_COB004A_CITY_ARRAY : BaseHelper, System.IDisposable, IEnumerable<string>
    {
        public const int StructureSize = 500;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public COB004A_COB004A_OUT_COB004A_CITY_ARRAY(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public COB004A_COB004A_OUT_COB004A_CITY_ARRAY(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static COB004A_COB004A_OUT_COB004A_CITY_ARRAY StackAllocate(ExecutionContext ctxt)
        {
            return new COB004A_COB004A_OUT_COB004A_CITY_ARRAY(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static COB004A_COB004A_OUT_COB004A_CITY_ARRAY Allocate(ExecutionContext ctxt)
        {
            COB004A_COB004A_OUT_COB004A_CITY_ARRAY result = new COB004A_COB004A_OUT_COB004A_CITY_ARRAY(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData: IEnumerable<MemoryArea>
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            internal const int Count0 = 25;
            internal int Count => Count0;

            public MemoryArea this[int idx] => theMemoryArea.Substr(idx * 20, 20);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public IEnumerator<MemoryArea> GetEnumerator()
            {
                for (int idx = 0; idx < Count; ++idx)
                {
                    yield return this[idx];
                }
            }

        }

        public const int Count0 = RawData.Count0;
        public int Count => Raw.Count;

        public string this[int idx]
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            set => Convert.move_string_to_char_string(theContext, value, Raw[idx]);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<string> GetEnumerator()
        {
            for (int idx = 0; idx < Count; ++idx)
            {
                yield return Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    CharacterStringType.Get(20,0,false).GetArray(1,25,20,false);
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY : BaseHelper, System.IDisposable, IEnumerable<string>
    {
        public const int StructureSize = 250;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY StackAllocate(ExecutionContext ctxt)
        {
            return new COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY Allocate(ExecutionContext ctxt)
        {
            COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY result = new COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData: IEnumerable<MemoryArea>
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            internal const int Count0 = 25;
            internal int Count => Count0;

            public MemoryArea this[int idx] => theMemoryArea.Substr(idx * 10, 10);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public IEnumerator<MemoryArea> GetEnumerator()
            {
                for (int idx = 0; idx < Count; ++idx)
                {
                    yield return this[idx];
                }
            }

        }

        public const int Count0 = RawData.Count0;
        public int Count => Raw.Count;

        public string this[int idx]
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            set => Convert.move_string_to_char_string(theContext, value, Raw[idx]);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<string> GetEnumerator()
        {
            for (int idx = 0; idx < Count; ++idx)
            {
                yield return Convert.cnv_char_string_to_string(theContext, Raw[idx]);
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    CharacterStringType.Get(10,0,false).GetArray(1,25,10,false);
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

    public partial class COB004A_COB004A_OUT : BaseHelper, System.IDisposable
    {
        public const int StructureSize = 1906;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public COB004A_COB004A_OUT(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public COB004A_COB004A_OUT(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static COB004A_COB004A_OUT StackAllocate(ExecutionContext ctxt)
        {
            return new COB004A_COB004A_OUT(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static COB004A_COB004A_OUT Allocate(ExecutionContext ctxt)
        {
            COB004A_COB004A_OUT result = new COB004A_COB004A_OUT(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea COB004A_SQLCODE => theMemoryArea.Substr(0, 4);
            public MemoryArea COB004A_FETCHED => theMemoryArea.Substr(4, 2);
            public MemoryArea COB004A_CUSTOMERID => theMemoryArea.Substr(6, 50);
            public MemoryArea COB004A_LASTNAME => theMemoryArea.Substr(56, 500);
            public MemoryArea COB004A_FIRSTNAME => theMemoryArea.Substr(556, 500);
            public MemoryArea COB004A_ZIPCODE => theMemoryArea.Substr(1056, 100);
            public MemoryArea COB004A_CITY => theMemoryArea.Substr(1156, 500);
            public MemoryArea COB004A_BIRTHDATE => theMemoryArea.Substr(1656, 250);
        }

        /// <summary>
        /// name:   COB004A_SQLCODE
        /// type:   int
        /// offset: 0
        /// size:   4
        /// </summary> 
        public int COB004A_SQLCODE
        {
            get => Convert.cnv_fixed_dec_to_int32(theContext, Raw.COB004A_SQLCODE, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP5()));
            set => Convert.move_int32_to_fixed_dec(theContext, value, Raw.COB004A_SQLCODE, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP5()));
        }

        private TypedLValue theCachedCOB004A_SQLCODE_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_SQLCODE_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_SQLCODE_TypedLValue.IsNull)
                {
                    theCachedCOB004A_SQLCODE_TypedLValue = new TypedLValue(Raw.COB004A_SQLCODE, FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP5()));
                }
                return theCachedCOB004A_SQLCODE_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_FETCHED
        /// type:   int
        /// offset: 4
        /// size:   2
        /// </summary> 
        public int COB004A_FETCHED
        {
            get => Convert.cnv_fixed_dec_to_int32(theContext, Raw.COB004A_FETCHED, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
            set => Convert.move_int32_to_fixed_dec(theContext, value, Raw.COB004A_FETCHED, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
        }

        private TypedLValue theCachedCOB004A_FETCHED_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_FETCHED_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_FETCHED_TypedLValue.IsNull)
                {
                    theCachedCOB004A_FETCHED_TypedLValue = new TypedLValue(Raw.COB004A_FETCHED, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()));
                }
                return theCachedCOB004A_FETCHED_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_CUSTOMERID
        /// type:   COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY
        /// offset: 6
        /// size:   50
        /// </summary> 
        private COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY theCOB004A_CUSTOMERID_19;
        public COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY COB004A_CUSTOMERID
        {
            get
            {
                if (theCOB004A_CUSTOMERID_19 == null)
                {
                    theCOB004A_CUSTOMERID_19 = new COB004A_COB004A_OUT_COB004A_CUSTOMERID_ARRAY(theContext, Raw.COB004A_CUSTOMERID);
                }
                return theCOB004A_CUSTOMERID_19;
            }
        }

        public readonly int COB004A_CUSTOMERID_Size = 25;
        private TypedLValue theCachedCOB004A_CUSTOMERID_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_CUSTOMERID_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_CUSTOMERID_TypedLValue.IsNull)
                {
                    theCachedCOB004A_CUSTOMERID_TypedLValue = new TypedLValue(Raw.COB004A_CUSTOMERID, FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP()).GetArray(1,25,2,false));
                }
                return theCachedCOB004A_CUSTOMERID_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_LASTNAME
        /// type:   COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY
        /// offset: 56
        /// size:   500
        /// </summary> 
        private COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY theCOB004A_LASTNAME_20;
        public COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY COB004A_LASTNAME
        {
            get
            {
                if (theCOB004A_LASTNAME_20 == null)
                {
                    theCOB004A_LASTNAME_20 = new COB004A_COB004A_OUT_COB004A_LASTNAME_ARRAY(theContext, Raw.COB004A_LASTNAME);
                }
                return theCOB004A_LASTNAME_20;
            }
        }

        public readonly int COB004A_LASTNAME_Size = 25;
        private TypedLValue theCachedCOB004A_LASTNAME_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_LASTNAME_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_LASTNAME_TypedLValue.IsNull)
                {
                    theCachedCOB004A_LASTNAME_TypedLValue = new TypedLValue(Raw.COB004A_LASTNAME, CharacterStringType.Get(20,0,false).GetArray(1,25,20,false));
                }
                return theCachedCOB004A_LASTNAME_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_FIRSTNAME
        /// type:   COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY
        /// offset: 556
        /// size:   500
        /// </summary> 
        private COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY theCOB004A_FIRSTNAME_21;
        public COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY COB004A_FIRSTNAME
        {
            get
            {
                if (theCOB004A_FIRSTNAME_21 == null)
                {
                    theCOB004A_FIRSTNAME_21 = new COB004A_COB004A_OUT_COB004A_FIRSTNAME_ARRAY(theContext, Raw.COB004A_FIRSTNAME);
                }
                return theCOB004A_FIRSTNAME_21;
            }
        }

        public readonly int COB004A_FIRSTNAME_Size = 25;
        private TypedLValue theCachedCOB004A_FIRSTNAME_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_FIRSTNAME_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_FIRSTNAME_TypedLValue.IsNull)
                {
                    theCachedCOB004A_FIRSTNAME_TypedLValue = new TypedLValue(Raw.COB004A_FIRSTNAME, CharacterStringType.Get(20,0,false).GetArray(1,25,20,false));
                }
                return theCachedCOB004A_FIRSTNAME_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_ZIPCODE
        /// type:   COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY
        /// offset: 1056
        /// size:   100
        /// </summary> 
        private COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY theCOB004A_ZIPCODE_22;
        public COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY COB004A_ZIPCODE
        {
            get
            {
                if (theCOB004A_ZIPCODE_22 == null)
                {
                    theCOB004A_ZIPCODE_22 = new COB004A_COB004A_OUT_COB004A_ZIPCODE_ARRAY(theContext, Raw.COB004A_ZIPCODE);
                }
                return theCOB004A_ZIPCODE_22;
            }
        }

        public readonly int COB004A_ZIPCODE_Size = 25;
        private TypedLValue theCachedCOB004A_ZIPCODE_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_ZIPCODE_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_ZIPCODE_TypedLValue.IsNull)
                {
                    theCachedCOB004A_ZIPCODE_TypedLValue = new TypedLValue(Raw.COB004A_ZIPCODE, CharacterStringType.Get(4,0,false).GetArray(1,25,4,false));
                }
                return theCachedCOB004A_ZIPCODE_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_CITY
        /// type:   COB004A_COB004A_OUT_COB004A_CITY_ARRAY
        /// offset: 1156
        /// size:   500
        /// </summary> 
        private COB004A_COB004A_OUT_COB004A_CITY_ARRAY theCOB004A_CITY_23;
        public COB004A_COB004A_OUT_COB004A_CITY_ARRAY COB004A_CITY
        {
            get
            {
                if (theCOB004A_CITY_23 == null)
                {
                    theCOB004A_CITY_23 = new COB004A_COB004A_OUT_COB004A_CITY_ARRAY(theContext, Raw.COB004A_CITY);
                }
                return theCOB004A_CITY_23;
            }
        }

        public readonly int COB004A_CITY_Size = 25;
        private TypedLValue theCachedCOB004A_CITY_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_CITY_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_CITY_TypedLValue.IsNull)
                {
                    theCachedCOB004A_CITY_TypedLValue = new TypedLValue(Raw.COB004A_CITY, CharacterStringType.Get(20,0,false).GetArray(1,25,20,false));
                }
                return theCachedCOB004A_CITY_TypedLValue;
            }
        }

        /// <summary>
        /// name:   COB004A_BIRTHDATE
        /// type:   COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY
        /// offset: 1656
        /// size:   250
        /// </summary> 
        private COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY theCOB004A_BIRTHDATE_24;
        public COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY COB004A_BIRTHDATE
        {
            get
            {
                if (theCOB004A_BIRTHDATE_24 == null)
                {
                    theCOB004A_BIRTHDATE_24 = new COB004A_COB004A_OUT_COB004A_BIRTHDATE_ARRAY(theContext, Raw.COB004A_BIRTHDATE);
                }
                return theCOB004A_BIRTHDATE_24;
            }
        }

        public readonly int COB004A_BIRTHDATE_Size = 25;
        private TypedLValue theCachedCOB004A_BIRTHDATE_TypedLValue = TypedLValue.Null;
        public TypedLValue COB004A_BIRTHDATE_TypedLValue
        {
            get
            {
                if (theCachedCOB004A_BIRTHDATE_TypedLValue.IsNull)
                {
                    theCachedCOB004A_BIRTHDATE_TypedLValue = new TypedLValue(Raw.COB004A_BIRTHDATE, CharacterStringType.Get(10,0,false).GetArray(1,25,10,false));
                }
                return theCachedCOB004A_BIRTHDATE_TypedLValue;
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {
              var temp_0 = FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP());
              var temp_1 = CharacterStringType.Get(20,0,false);
              var temp_2 = CharacterStringType.Get(20,0,false);
              var temp_3 = CharacterStringType.Get(4,0,false);
              var temp_4 = CharacterStringType.Get(20,0,false);
              var temp_5 = CharacterStringType.Get(10,0,false);

                theCachedDesc =
                    StructTypeDescriptor.Get(new FieldDescriptor[] {
                            new FieldDescriptor ("COB004A-SQLCODE",0,FixedDecimalDescriptor.Get(8,0,FixedDecimalDescriptor.Storage_COMP5())),
                            new FieldDescriptor ("COB004A-FETCHED",4,FixedDecimalDescriptor.Get(4,0,FixedDecimalDescriptor.Storage_COMP())),
                            new FieldDescriptor ("COB004A-CUSTOMERID",6,temp_0.GetArray(1,25,2,false)),
                            new FieldDescriptor ("COB004A-LASTNAME",56,temp_1.GetArray(1,25,20,false)),
                            new FieldDescriptor ("COB004A-FIRSTNAME",556,temp_2.GetArray(1,25,20,false)),
                            new FieldDescriptor ("COB004A-ZIPCODE",1056,temp_3.GetArray(1,25,4,false)),
                            new FieldDescriptor ("COB004A-CITY",1156,temp_4.GetArray(1,25,20,false)),
                            new FieldDescriptor ("COB004A-BIRTHDATE",1656,temp_5.GetArray(1,25,10,false))
                    });
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

}
