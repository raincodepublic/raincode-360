namespace RainCode.Samples
{
    using RainCodeLegacyRuntime.Core;
    using RainCodeLegacyRuntime.Descriptor;
    using RainCodeLegacyRuntime.Types;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Generated on 02/12/2024 at 17:49:00
    /// by Raincode PL/I compiler version 5.0.3.0 (Build nb_v5.0.2.0-7-g91a6eb07eda)
    /// with arguments -HelperClassName=RainCode.Samples.PLI001Helper -SQL=sqlserver -HelperStructure=ca -IncludeSearchPath=.\Legacy -HelperClassOutputDir=.\RcMainframeApi\HelperClass .\Legacy\Pli001.pli
    ///
    /// Input program path: 'C:\Repositories\raincode360\Qix - Create REST API from Legacy Program\Legacy\Pli001.pli'
    /// Structure was originally in files:
    ///   Pli001.pli
    ///   pli001.plinc
    /// </summary>
    public partial class PLI001Helper : BaseHelper, System.IDisposable
    {
        public override string Name => "ca";
        public const int StructureSize = 129;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public PLI001Helper(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public PLI001Helper(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static PLI001Helper StackAllocate(ExecutionContext ctxt)
        {
            return new PLI001Helper(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static PLI001Helper Allocate(ExecutionContext ctxt)
        {
            PLI001Helper result = new PLI001Helper(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea P001_ACCOUNTNUMBER_FROM => theMemoryArea.Substr(0, 20);
            public MemoryArea P001_ACCOUNTNUMBER_TO => theMemoryArea.Substr(20, 20);
            public MemoryArea P001_AMOUNT => theMemoryArea.Substr(40, 5);
            public MemoryArea P001_DESCRIPTIONTEXT => theMemoryArea.Substr(45, 40);
            public MemoryArea P001_SQLCODE => theMemoryArea.Substr(85, 4);
            public MemoryArea P001_ERRTXT => theMemoryArea.Substr(89, 40);
        }

        /// <summary>
        /// name:   P001_ACCOUNTNUMBER_FROM
        /// type:   string
        /// offset: 0
        /// size:   20
        /// </summary> 
        public string P001_ACCOUNTNUMBER_FROM
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.P001_ACCOUNTNUMBER_FROM);
            set => Convert.move_string_to_char_string(theContext, value, Raw.P001_ACCOUNTNUMBER_FROM);
        }

        private TypedLValue theCachedP001_ACCOUNTNUMBER_FROM_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_ACCOUNTNUMBER_FROM_TypedLValue
        {
            get
            {
                if (theCachedP001_ACCOUNTNUMBER_FROM_TypedLValue.IsNull)
                {
                    theCachedP001_ACCOUNTNUMBER_FROM_TypedLValue = new TypedLValue(Raw.P001_ACCOUNTNUMBER_FROM, CharacterStringType.Get(20,0,false));
                }
                return theCachedP001_ACCOUNTNUMBER_FROM_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_ACCOUNTNUMBER_TO
        /// type:   string
        /// offset: 20
        /// size:   20
        /// </summary> 
        public string P001_ACCOUNTNUMBER_TO
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.P001_ACCOUNTNUMBER_TO);
            set => Convert.move_string_to_char_string(theContext, value, Raw.P001_ACCOUNTNUMBER_TO);
        }

        private TypedLValue theCachedP001_ACCOUNTNUMBER_TO_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_ACCOUNTNUMBER_TO_TypedLValue
        {
            get
            {
                if (theCachedP001_ACCOUNTNUMBER_TO_TypedLValue.IsNull)
                {
                    theCachedP001_ACCOUNTNUMBER_TO_TypedLValue = new TypedLValue(Raw.P001_ACCOUNTNUMBER_TO, CharacterStringType.Get(20,0,false));
                }
                return theCachedP001_ACCOUNTNUMBER_TO_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_AMOUNT
        /// type:   System.Decimal
        /// offset: 40
        /// size:   5
        /// </summary> 
        public System.Decimal P001_AMOUNT
        {
            get => Convert.cnv_fixed_dec_to_decimal(theContext, Raw.P001_AMOUNT, FixedDecimalDescriptor.Get(9,2,FixedDecimalDescriptor.Storage_BCD_PLI()));
            set => Convert.move_decimal_to_fixed_dec(theContext, value, Raw.P001_AMOUNT, FixedDecimalDescriptor.Get(9,2,FixedDecimalDescriptor.Storage_BCD_PLI()));
        }

        private TypedLValue theCachedP001_AMOUNT_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_AMOUNT_TypedLValue
        {
            get
            {
                if (theCachedP001_AMOUNT_TypedLValue.IsNull)
                {
                    theCachedP001_AMOUNT_TypedLValue = new TypedLValue(Raw.P001_AMOUNT, FixedDecimalDescriptor.Get(9,2,FixedDecimalDescriptor.Storage_BCD_PLI()));
                }
                return theCachedP001_AMOUNT_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_DESCRIPTIONTEXT
        /// type:   string
        /// offset: 45
        /// size:   40
        /// </summary> 
        public string P001_DESCRIPTIONTEXT
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.P001_DESCRIPTIONTEXT);
            set => Convert.move_string_to_char_string(theContext, value, Raw.P001_DESCRIPTIONTEXT);
        }

        private TypedLValue theCachedP001_DESCRIPTIONTEXT_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_DESCRIPTIONTEXT_TypedLValue
        {
            get
            {
                if (theCachedP001_DESCRIPTIONTEXT_TypedLValue.IsNull)
                {
                    theCachedP001_DESCRIPTIONTEXT_TypedLValue = new TypedLValue(Raw.P001_DESCRIPTIONTEXT, CharacterStringType.Get(40,0,false));
                }
                return theCachedP001_DESCRIPTIONTEXT_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_SQLCODE
        /// type:   int
        /// offset: 85
        /// size:   4
        /// </summary> 
        public int P001_SQLCODE
        {
            get => Convert.cnv_fixed_bin_to_int32(theContext, Raw.P001_SQLCODE, FixedBinaryDescriptor.Get(31,0,true));
            set => Convert.move_int32_to_fixed_bin(theContext, value, Raw.P001_SQLCODE, FixedBinaryDescriptor.Get(31,0,true));
        }

        private TypedLValue theCachedP001_SQLCODE_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_SQLCODE_TypedLValue
        {
            get
            {
                if (theCachedP001_SQLCODE_TypedLValue.IsNull)
                {
                    theCachedP001_SQLCODE_TypedLValue = new TypedLValue(Raw.P001_SQLCODE, FixedBinaryDescriptor.Get(31,0,true));
                }
                return theCachedP001_SQLCODE_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_ERRTXT
        /// type:   string
        /// offset: 89
        /// size:   40
        /// </summary> 
        public string P001_ERRTXT
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.P001_ERRTXT);
            set => Convert.move_string_to_char_string(theContext, value, Raw.P001_ERRTXT);
        }

        private TypedLValue theCachedP001_ERRTXT_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_ERRTXT_TypedLValue
        {
            get
            {
                if (theCachedP001_ERRTXT_TypedLValue.IsNull)
                {
                    theCachedP001_ERRTXT_TypedLValue = new TypedLValue(Raw.P001_ERRTXT, CharacterStringType.Get(40,0,false));
                }
                return theCachedP001_ERRTXT_TypedLValue;
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    StructTypeDescriptor.Get(new FieldDescriptor[] {
                            new FieldDescriptor ("p001_accountnumber_from",0,CharacterStringType.Get(20,0,false)),
                            new FieldDescriptor ("p001_accountnumber_to",20,CharacterStringType.Get(20,0,false)),
                            new FieldDescriptor ("p001_amount",40,FixedDecimalDescriptor.Get(9,2,FixedDecimalDescriptor.Storage_BCD_PLI())),
                            new FieldDescriptor ("p001_descriptiontext",45,CharacterStringType.Get(40,0,false)),
                            new FieldDescriptor ("p001_sqlcode",85,FixedBinaryDescriptor.Get(31,0,true)),
                            new FieldDescriptor ("p001_errtxt",89,CharacterStringType.Get(40,0,false))
                    });
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

}
