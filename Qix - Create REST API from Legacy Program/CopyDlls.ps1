[CmdletBinding()]
Param (
    [Parameter(Mandatory)]
    [string]
    $Configuration
)
$ErrorActionPreference = "Stop"
$LegacyDllsPath= Join-path -Path $PSScriptRoot -ChildPath "Legacy\bin\$Configuration\net8.0"
$DllDestinationPath = Join-path -Path $PSScriptRoot -ChildPath "RcMainframeApi\LegacyDll"

if(!(Test-Path -Path $DllDestinationPath)){
   New-Item -Path $DllDestinationPath -ItemType Directory
}
Get-ChildItem $DllDestinationPath | Remove-Item -Recurse -Force -ErrorAction SilentlyContinue

Write-Output "Removed Old dlls from $DllDestinationPath"
Get-ChildItem $LegacyDllsPath  -Filter *.dll | `
Foreach-Object {
    Copy-Item $_.FullName -Destination $DllDestinationPath
    Write-Host "Copied Dll :" $_.FullName
}

