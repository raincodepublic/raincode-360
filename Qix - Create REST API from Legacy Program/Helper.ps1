$ErrorActionPreference = "Stop"
$HelperDestinationPath = Join-path -Path $PSScriptRoot -ChildPath "RcMainframeApi\HelperClass"

Get-ChildItem $HelperDestinationPath | Remove-Item -Recurse -Force -ErrorAction SilentlyContinue

$proc=Start-Process -PassThru -NoNewWindow -Wait -FilePath $env:RCBIN\plirc.exe -ArgumentList "-HelperClassName=RainCode.Samples.PLI001Helper", "-SQL=sqlserver", "-HelperStructure=ca", "-IncludeSearchPath=.\Legacy", "-HelperClassOutputDir=.\RcMainframeApi\HelperClass", ".\Legacy\Pli001.pli" 
if($proc.ExitCode -ne 0){ 
	Write-Host "Helperclass generation failed for PLI001 with ExitCode:" $proc.ExitCode 
	exit $proc.ExitCode 
}

Write-Host "Helperclass generated successfully for PLI001"

$proc=Start-Process -PassThru -NoNewWindow -Wait -FilePath $env:RCBIN\CobRc.exe -ArgumentList "-HelperClassName=RainCode.Samples.COB004AHelper", "-SQL=sqlserver", "-HelperStructure=COB004A", "-IncludeSearchPath=.\Legacy", "-HelperClassOutputDir=.\RcMainframeApi\HelperClass", ".\Legacy\COB004A.cpy" 
if($proc.ExitCode -ne 0){ 
	Write-Host "Helperclass generation failed for COB004A with ExitCode:" $proc.ExitCode 
	exit $proc.ExitCode 
}

Write-Host "Helperclass generated successfully for COB004A"