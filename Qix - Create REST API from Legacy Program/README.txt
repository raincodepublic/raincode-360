*---------------------------------------------------------------------------------------*
*                                                                                       *
*   QIX Demo : Expose CICS module as a REST API                                         *
*                                                                                       *
*---------------------------------------------------------------------------------------*

PURPOSE
This demo shows how to create and invoke a REST API on top of CICS module

PREREQUISITE:
1. Right click on 'Legacy' project and select 'Build', this does the following
	a. Compiles the PLI001 and COB004A modules using Raincode PLI and Cobol compiler respectively
	b. Executes the script 'CopyDlls' which copies the generated dlls to the LegacyDll folder of RCMainframeAPI project
	c. This step to be performed when there is a change in the PLI001 or COB004A modules
2. Run the 'Helper.ps1' script using Powershell 
	a. This script generates the helper classes for PLI001 and COB004A modules using Raincode PLI and Cobol compilers respectively
	b. Also copies the generated helperclasses to the HelperClass folder of RCMainframeAPI project
	c. This script to be rerun when there is a change in the copybook layout of PLI001 or COB004A modules

STEPS:
1. Make sure 'RCMainframeAPI' project is the startup project
2. Run the project by pressing Ctrl + F5 or navigating to the 'Debug' menu and selecting 'Start Without Debugging'
3. This opens up the browser with Swagger UI for 'GET /COB004A' and 'POST /PL1001' methods
4. GET /COB004A :
	a. Try it out this method by providing inputs as : DIRECTION - D , SORT - C, FIRSTNAME_FILTER - DE, LINECOUNT - 15
	b. The request should be successfull and you would see 15 customer details in the response and their first name starts with 'DE'
5. POST /PL1001:
	a. Try it out this method by providing request body as below
		{
		  "p001_ACCOUNTNUMBER_FROM": "BE00-1000-0000-1438",
		  "p001_ACCOUNTNUMBER_TO": "BE00-1000-0000-2643",
		  "p001_AMOUNT": 10.5,
		  "p001_DESCRIPTIONTEXT": "RCMainframeAPI_Swagger"
		}
	b. The request should be accepted and you would see that result with SQLCODE as 0