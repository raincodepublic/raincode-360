***********************************************************************
*        Assembler routine that can be called from COBOL              *
*        Calculates the difference between dates                      *
***********************************************************************
DATAB    DSECT ,
DT1      DS    CL8         IN:  lower date in YYYYMMDD format (ASCII)
DT2      DS    CL8         IN:  upper date in YYYYMMDD format (ASCII)
AGE      DS    CL8         OUT: difference in whole years     (ASCII)
ASM001A  CSECT
         SAVE  (14,12)       
         BALR  9,0           
         USING *,9
         USING DATAB,4       
         L     4,0(1)        
         ST    13,SAVEB+4    
         LR    12,13
         LA    13,SAVEB
         ST    13,8(12)      
*****************************
         MVC   WK1,DT1
         MVZ   WK1,=X'F0F0F0F0F0F0F0F0'
         PACK  PK1,WK1
         MVC   WK2,DT2
         MVZ   WK2,=X'F0F0F0F0F0F0F0F0'
         PACK  PK2,WK2
         SP    PK2,PK1
         UNPK  WK1,PK2
         MVC   AGE,WK1
         MVZ   AGE,=X'3030303030303030'
*****************************
         L     13,SAVEB+4  
         LA    15,0              CLEAR RETURNCODE
         RETURN (14,12),RC=(15) 
*****************************
         LTORG
SAVEB    DS    18F
WK1      DS    CL8
WK2      DS    CL8
WK3      DS    CL8
PK1      DS    PL5
PK2      DS    PL5
         END   ASM001A