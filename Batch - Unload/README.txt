============================
UNLOADING DATA WITH DSNUTILB
============================

PURPOSE
  . Copy the contents of a database table into a sequential file with the DSNUTILB utility

HOW TO RUN
  . Start the Raincode Debugger
  . Examine the contents of the output files

DATABASE CONNECTION
  . The database connection relies on the following elements:
    . Planname
      . defined in Raincode.Catalog.xml
           <programDBConnectionDefinitions>
               <dbConnection JobId=".*" >DEFAULTPLAN</dbConnection >
           </programDBConnectionDefinitions>
    . Connection string
      . defined in RcDbConnections.csv
          .*,SqlServer,Data Source=<datasource>;Uid=<user>;Pwd=<password>;Initial Catalog=BankDemo;

NOTES
  . The utility does not automatically set the record length of the output file to the appropriate value
  . The record required length of the output is 57 in this particular case.
  . Any other length to any other value leads to the last record being written being incomplete, redering the file unusable for further processing.
  . Varchar fields are preceded by two bytes containing their length in binary format.
  . The same functionality can be implemented with DSNTIAUL (see inline SQL demo)
