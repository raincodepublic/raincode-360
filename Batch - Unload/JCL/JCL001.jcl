//*********************************************************
//*                                                       *
//*   UNLOAD EXAMPLE                                      *
//*                                                       *
//*********************************************************
//JCL001   JOB CLASS=A,MSGCLASS=C
//*********************************************************
//* CLEAN UP FILES FROM PREVIOUS RUN                      *
//*********************************************************
//STEP001  EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
 DELETE UNLOAD.ZIP.*
 SET MAXCC = 0
/*
//*********************************************************
//* SELECTIVE UNLOAD OF ZIPCODE TABLE                     *
//*********************************************************
//STEP002  EXEC PGM=DSNUTILB
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//UTPRINT  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSIN    DD *
  UNLOAD TABLESPACE PRIMARY FROM TABLE DBO.ZIPCODE LIMIT 25
  WHEN (ZIPCODE > 2000 AND ZIPCODE < 3000)
//SYSREC DD DSN=UNLOAD.ZIP.SEQ,
//         DISP=(NEW,CATLG,DELETE),LRECL=68
//*********************************************************
//* REMOVE BINARY CHARACTERS                              *
//*********************************************************
//STEP003  EXEC PGM=SORT                                  
//SYSOUT   DD SYSOUT=*
//SORTIN   DD DSNAME=UNLOAD.ZIP.SEQ,DISP=SHR
//SORTOUT  DD DSN=UNLOAD.ZIP.SORT,
//        DISP=(NEW,CATLG,DELETE),LRECL=66
//SYSIN    DD *
  SORT FIELDS=COPY
  OUTREC FIELDS=(1,4,7,62)
/*
//*********************************************************
//* REFORMAT TO LINE SEQUENTIAL                           *
//*********************************************************
//STEP004  EXEC PGM=IEBGENER
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  DUMMY
//SYSUT1   DD  DSNAME=UNLOAD.ZIP.SORT,DISP=SHR
//SYSUT2   DD  DSN=UNLOAD.ZIP.LSEQ,
//        DISP=(NEW,CATLG,DELETE),RECFM=LSEQ
//