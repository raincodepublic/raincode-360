﻿********************************************************************
*                                                                  *
*   DEMO : Displaying National Characters on the Console           *
*                                                                  *
*                                                                  *
********************************************************************

PURPOSE
  . Show how national characters can be written to files.
  . Demonstrate how the Record Editor can be used to view these files.

CONCEPTS
  . National character string are declared as PIC N(n) USAGE NATIONAL.
  . Each character is represented by two or four bytes.
  . Numeric edited variables can also be declared with USAGE NATIONAL.
  . Supported operations are computations, output to the console, viewing values in the debugger, and reading and writing to a database.
  . To compile programs with embedded national characters, as in 
          MOVE N'世界！' TO REC
    The compilation command line must contain the following option:
        :StringSourceFileEncoding=utf-8
   . National characters are encoded in UTF-16 big endian format.
   . The console is not able to display unicode characters. Change the language to 'Japanese' in
        [configuration/time and language/language/administrative language settings/change system locale]
     to make the the Japanese characters visible. This requires rebooting your machine.

RUNNING THE DEMO IN VISUAL STUDIO
  . Place a breakpoint and start the debugger.

EXPECTED OUTPUT (non-Japanese console)

ÝŒÉƒŒƒxƒ‹
----------
‰”•M                                     —Ê: 110
ƒ{[ƒ‹ƒyƒ“                               —Ê: 100
Á‚µƒSƒ€                                 —Ê: 005
ƒXƒe[ƒvƒ‹                               —Ê: 250
ƒvƒŠƒ“ƒ^[—pŽ†                           —Ê: 090
ƒgƒi[ƒJ[ƒgƒŠƒbƒW                       —Ê: 008
ŒuŒõƒyƒ“                                 —Ê: 060
ƒm[ƒgƒpƒ\ƒRƒ“—p‚ÌƒoƒbƒO                 —Ê: 010


‰½‚©ƒL[‚ð‰Ÿ‚·‚Æ‘±s‚µ‚Ü‚· ...