﻿       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB001.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       01 X PIC X.
       01 ITEM.
           10 ITEM-NM-ENG PIC X(50).
           10 ITEM-NM-JPN PIC N(50) USAGE NATIONAL.
           10 ITEM-QTY PIC S9(4) COMP.
           10 ITEM-PRICE PIC S9(7)V99 COMP-3.
   	   EXEC SQL
   	       DECLARE CUR01 CURSOR FOR
      	   SELECT 
               ITEM_NM_ENG,
               ITEM_NM_JPN,
               ITEM_QTY,
               ITEM_PRICE
           FROM 
               DBO.OFFICESUPPLIES
   	   END-EXEC.
       PROCEDURE DIVISION.
           DISPLAY N'在庫レベル'
           DISPLAY '----------'
           EXEC SQL
   	           OPEN CUR01
   	       END-EXEC
   	       PERFORM FETCH-CUR01
           PERFORM UNTIL SQLCODE NOT = 0
               DISPLAY ITEM-NM-JPN 
   	           PERFORM FETCH-CUR01
           END-PERFORM
           EXEC SQL
   	           CLOSE CUR01
   	       END-EXEC
           DISPLAY ' '
           DISPLAY ' '
           DISPLAY N'何かキーを押すと続行します ...'
           GOBACK
           .
       FETCH-CUR01.
   	       EXEC SQL
   	           FETCH CUR01 INTO
                   :ITEM-NM-ENG,
                   :ITEM-NM-JPN,
                   :ITEM-QTY,
                   :ITEM-PRICE
   	       END-EXEC
           .
