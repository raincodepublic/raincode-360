
**************************************************************
*                                                            *
*   Compiler Demo : Qix Factories                            *
*                   Redefining the Behaviour of LINK         *
*                                                            *
**************************************************************

PURPOSE 
  . Demonstrate how the behaviour of CICS commands can be overwritten using Qix factories.

WHAT IT DOES
  . The new link command writes a message plus a hex dump of the commarea before and after the actual link.
  . The original link mechanism is invoked by base.Execute() in CustomLink.cs.

HOW TO RUN THE DEMO
  . Select 'Start Without Debugging' in the Debug menu.
  . This will avoid automatic breakpoints and will also keep the console window open after the program ends.
  . Check the messages in the console window.
  . Close the console window.

LINKS
  . All Raincode manuals
    . https://www.raincode.com/docs/#_raincode_manuals
  . Qix Factories
    . https://www.raincode.com/docs/Compiler-document/UserGuide/StackUserGuide.html#Qix-Factory
    . https://www.raincode.com/docs/Compiler-document/GettingStarted/GettingStarted.html#qix_factory