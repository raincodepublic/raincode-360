       IDENTIFICATION DIVISION.
       PROGRAM-ID.     OTHERMAIN.
       DATA DIVISION.
        LINKAGE SECTION.
        01 DFHCOMMAREA.
          05 WS-MESSAGE PIC X(30).
          05 WS-NUM PIC S9(4) COMP.
       PROCEDURE DIVISION.
           MOVE "message written by othercobol" to WS-MESSAGE
           MOVE 16 TO WS-NUM
           EXEC CICS RETURN END-EXEC.