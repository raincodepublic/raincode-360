﻿using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions;
using RainCodeLegacyRuntime.Exceptions.QIX;
using System;

namespace RainCode.Samples
{
    class main
    {
        private static void Link(ExecutionContext ec, string program)
        {
            // Setup the LINK
            var link = ec.QixInterface.Factory.CreateLink();
            link.Context = ec;
            link.PROGRAM = program;
            //ec.QixInterface.Operation.Link(link);
            try
            {
                ec.QixInterface.Operation.Link(link);
            }
            catch (AbendException e)
            {
                Console.WriteLine("Abend {0} detected", e.Message);
            }
            catch (ReturnException)
            {
                Console.WriteLine("Program 'COBMAIN' ended");
            }

            // Check that everything went fine
            if (ec.RaincodeQixContext.DfheiblkHelper.EIBRESP != RainCodeQixInterface.Constants.Resp.NORMAL)
            {
                throw new Exception(string.Format("Program exited with RESP {0}", ec.RaincodeQixContext.DfheiblkHelper.EIBRESP));
            }
        }

        private static ExecutionContext SetupQix()
        {

            // Create mandatory ExecutionContext
            ExecutionContextArgs ecArgs = new ExecutionContextArgs()
            {
                // Enable QIX
                QixMode = true,
                QixFactory = new CustomQixFactory()
            };
            ExecutionContext ec = new ExecutionContext(ecArgs);

            // Configure IO runtime (because of DISPLAY statements)
            ec.IOOperation.ConfigureSystemDatasets(); 

            // Set the custom factory, so the custom RECEIVE implementation is used
            //ec.QixInterface.Factory = new CustomQixFactory();

            return ec;
        }

        static void Main(string[] args)
        {
            var ec = SetupQix();
            Link(ec, "COBMAIN");
        }
    }
}
