﻿using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions.QIX;
using RainCodeQixRuntimeInterface.QixInstances;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RainCode.Samples
{
    class CustomLink : RainCodeLegacyRuntime.QIX.Interface.Statements.Link
    {
        public CustomLink()
        {
        }

        public static string HexDump(byte[] bytes, int bytesPerLine = 16)
        {
            StringBuilder sb = new StringBuilder();
            for (int line = 0; line < bytes.Length; line += bytesPerLine)
            {
                byte[] lineBytes = bytes.Skip(line).Take(bytesPerLine).ToArray();
                sb.AppendFormat("{0:x8} ", line);
                sb.Append(string.Join(" ", lineBytes.Select(b => b.ToString("x2"))
                       .ToArray()).PadRight(bytesPerLine * 3));
                sb.Append(" ");
                sb.Append(new string(lineBytes.Select(b => b < 32 ? '.' : (char)b)
                       .ToArray()));
                sb.Append("\n");
            }
            return sb.ToString();
        }
        public void DumpCA()
        {
            if (COMMAREA != null)
            {
                var mem = new MemoryArea(((ExecutionContext)Context).AddressSpace, COMMAREA.Offset, COMMAREA.Size);
                var message = RainCodeLegacyRuntime.Types.Convert.cnv_char_string_to_string((ExecutionContext)Context, mem);
                Console.WriteLine(HexDump(Encoding.ASCII.GetBytes(message)));
            }
        }
        public override void Execute()
        {
            Console.WriteLine($"Linking to {PROGRAM}");
            DumpCA();
            base.Execute();
            Console.WriteLine($"Returning from link");
            DumpCA();
        }
    }
}
