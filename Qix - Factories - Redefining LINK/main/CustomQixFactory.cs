﻿
namespace RainCode.Samples
{
    /// <summary>
    /// Inheriting from RainCodeLegacyRuntime.QIX.Interface.Factory allows to reuse all QIX commands
    /// already implemented in the RainCode Legacy Runtime
    /// </summary>
    class CustomQixFactory : RainCodeLegacyRuntime.QIX.Interface.Factory
    {
        /// <summary>
        /// Override the CreateReceive method to provide your own implementation of ReceiveInstance
        /// </summary>
        /// <returns></returns>
        public override RainCodeQixRuntimeInterface.QixInstances.LinkInstance CreateLink()
        {
            return new CustomLink();
        }
    }
}
