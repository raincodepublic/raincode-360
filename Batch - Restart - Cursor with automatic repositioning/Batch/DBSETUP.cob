       IDENTIFICATION DIVISION.
       PROGRAM-ID. DBSETUP.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *********************
      *     VARIABLES     *
      *********************
       01 W-ACCOUNTNUMBER PIC X(19).
       01 W-NBR PIC 99. 
      *********************
      *     COPYBOOKS     *
      *********************
       COPY SQLCA.
      *********************
      *   PROGRAM         *
      *********************
       PROCEDURE DIVISION.
          PERFORM ACCOUNT-DELETE
          PERFORM ACCOUNT-INSERT
          STOP RUN
          .
       ACCOUNT-DELETE.
   	      EXEC SQL
             DELETE  
             FROM 
                dbo.Account
             WHERE
                AccountNumber LIKE 'ARC01%'
   	      END-EXEC
          EVALUATE SQLCODE
          WHEN 0
          WHEN 100
             CONTINUE
          WHEN OTHER
             DISPLAY '** DBSETUP: DELETE: SQLCODE: ' 
                     SQLCODE
             MOVE -1 TO RETURN-CODE
             STOP RUN
          END-EVALUATE
          .
       ACCOUNT-INSERT.
          MOVE 'ARC01-' TO W-ACCOUNTNUMBER
          PERFORM VARYING W-NBR FROM 1 BY 1 UNTIL W-NBR > 20
             MOVE W-NBR TO W-ACCOUNTNUMBER(7:)
   	         EXEC SQL
                INSERT INTO 
                   dbo.Account (AccountNumber, CustomerId, Amount) 
                VALUES 
                   ( :W-ACCOUNTNUMBER , 0 , 0 )
   	         END-EXEC
             EVALUATE SQLCODE
               WHEN 0
                  CONTINUE
               WHEN OTHER
                 DISPLAY '** DBSETUP: INSERT: SQLCODE: ' 
                         SQLCODE
                 MOVE -1 TO RETURN-CODE
                 STOP RUN
             END-EVALUATE
          END-PERFORM
          .
