*---------------------------------------------------------------------------------------*
*                                                                                       *
*   Batch Demo : EBCDIC mode                                                            *
*                                                                                       *
*---------------------------------------------------------------------------------------*

PURPOSE
  . Produce file output in EBCDIC

HOW TO RUN
  . Start the Raincode Debugger
  . Examine these files un the catalog
    . EBCDIC.EXTRACT
    . EBCDIC.SORTED

ABOUT THE DEMO
  . This is a modified version of the demo 'Batch - Legacy with Database Access'
  . Differences
    . The encoding is EBCDIC rather than ASCII
    . The files are FB rather than LSEQ (LSEQ makes no sense in EBCDIC)   
  . Use HxD (preinstalled on the VM) or Notepad++ (not preinstalled) to read inspect the files
  . The sort utility is installed in ASCII mode, so the character literal C'   ' (three spaces) in the ASCII version was replaced by X'404040' to produce readable EBCDIC.
  . To make COB003A produce EBCDIC file output, it needs to receive the parameter '-StringRuntimeEncoding=IBM037' via rclrun. This is handled as follows.
    . The solution object 'rclrun.args' holds the parameter.
    . The pre-build event copies 'rclrun.args' from the solution directory to the bin directory
    . rclrun automatically scans the current directory for a parameter when it is executed