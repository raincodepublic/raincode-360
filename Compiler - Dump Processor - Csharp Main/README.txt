
**************************************************************
*                                                            *
*   Compiler Demo : Dump Processor                           *
*                   csharp runner implementation             *
*                                                            *
**************************************************************

PURPOSE
  . Install a dump processor based on the abstract BaseDumpProcessor class.
  . The Dump Processor will produce a formatted report when a program crashes.

HOW TO RUN
  . Start the Raincode debugger.
  . The execution will hit a breakpoint in COB001.cob on the CICS ABEND statement.
  . Press continue.

EXAMINING THE RESULT
  . Open the dump.xml solution object.
  . The file contains the working storage of COB001.cob time of the crash plus the variables of the Cics Exec Interface Block (EIB).
