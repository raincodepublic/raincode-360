       IDENTIFICATION DIVISION.
       PROGRAM-ID.     COB001.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 SOME-TABLES.
         02 TBL OCCURS 2.
           03 TBL-0 PIC X(4).
           03 TBL-1 PIC S9(7) COMP-3.
         02 STTBL PIC X(16) OCCURS 4.
       77 R0 PIC S9(9) COMP-5.
       77 R1 PIC S9(9) COMP-5.
       77 R2 PIC S9(9) COMP-5.
       PROCEDURE DIVISION.
           EXEC CICS
               ABEND ABCODE('MYAB')
           END-EXEC
           EXEC CICS
               RETURN
           END-EXEC
           .