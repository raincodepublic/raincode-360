﻿using RainCode.Core.Logging;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Debug;
using RainCodeLegacyRuntime.Module;
using System;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using RainCodeLegacyRuntime.Exceptions;
using RainCodeLegacyRuntime.Exceptions.QIX;

namespace RainCode.Test
{
    public class Runner
    {
        public static void Main(string[] args)
        {
            Logger.CreateAndRegister<ConsoleLogger>();
            Logger.LogLevel = Level.ERROR;
            // Register the custom DumpProcessor constructor here
            BaseDumpProcessor.ConstructorPlugin.Provide(10, (_ec) => new MyDumpProcessor());
            var ec = new ExecutionContext(new ExecutionContextArgs()
            {
                QixMode = true,
            });
            var link = ec.QixInterface.Factory.CreateLink();
            link.Context = ec;
            link.PROGRAM = "COB001";
            link.TRANSID = "ABCD";
            try
            {
                ec.QixInterface.Operation.Link(link);
            }
            catch (AbendException e)
            {
                Console.WriteLine("Abend {0} detected", e.Message);
            }
            Environment.Exit(ec.ReturnCode);
        }
    }

    internal class MyDumpProcessor : BaseDumpProcessor
    {
        const string FILENAME = @"..\..\..\dump.xml";
        XDocument doc;
        Stack<XElement> elems;
        public override void Start()
        {
            doc = new XDocument();
            elems = new Stack<XElement>();
            doc.Add(new XElement("dump"));
            elems.Push(doc.Root);
        }
        public override void End()
        {
            doc.Save(FILENAME);
        }
        public override void Exception(Exception e)
        {
            doc.Root.Add(new XElement("exception", e.Message));
        }
        public override void CurrentStatement(string position)
        {
            doc.Root.Add(new XElement("position", position));
        }
        public override void StartCallStack(int stackSize)
        {
            var cs = new XElement("callstack");
            cs.Add(new XAttribute("depth", stackSize));
            PushElem(cs);
        }
        public override void CallStackEntry(Module module, string position)
        {
            var entry = new XElement("entry");
            entry.Add(new XAttribute("module", module.Name));
            entry.Add(new XAttribute("position", position));
            elems.Peek().Add(entry);
        }
        public override void EndCallStack() { elems.Pop(); }
        public override void StartRunUnit(int moduleCount)
        {
            var ru = new XElement("rununit");
            ru.Add(new XAttribute("module_count", moduleCount));
            PushElem(ru);
        }
        public override void EndRunUnit() { elems.Pop(); }
        public override void StartModule(Module module)
        {
            var mi = new XElement("module");
            mi.Add(new XAttribute("name", module.Name));
            mi.Add(new XAttribute("lang", module.Language));
            mi.Add(new XAttribute("file", module.FileName));
            mi.Add(new XAttribute("assembly", module.GetType().Assembly.FullName));
            PushElem(mi);
        }
        public override void EndModule() { elems.Pop(); }
        public override void InvalidValue(InvalidValueData iv)
        {
            var v = new XElement("var_failed_eval");
            v.Add(new XAttribute("name", iv.Name));
            v.Add(new XElement("exception", iv.Exception.Message));
            elems.Peek().Add(v);
        }
        public override void Scalar(ScalarData s)
        {
            var v = new XElement("var");
            v.Add(new XAttribute("name", s.Name));
            v.Add(new XAttribute("type", s.Type));
            // TexValue of certain EIB variables contains nonprintable characters that are not valid in XML
            //v.Add(new XElement("textvalue", s.TextValue.Trim()));
            //v.Add(new XElement("textvalue", Regex.Replace(s.TextValue.Trim(), @"\p{C}+", string.Empty)));
            v.Add(new XElement("textvalue", Regex.Replace(s.TextValue.Trim(), @"\p{C}+", "?")));
            v.Add(new XElement("hexvalue", s.HexValue));
            elems.Peek().Add(v);
        }
        public override void Level88(ScalarData s)
        {
            var v = new XElement("var88");
            v.Add(new XAttribute("name", s.Name));
            v.Add(new XAttribute("type", s.Type));
            v.Add(s.TextValue);
            elems.Peek().Add(v);
        }
        public override void StartArray(ArrayData array)
        {
            var s = new XElement("array");
            s.Add(new XAttribute("name", array.Name));
            s.Add(new XAttribute("type", array.Type));
            PushElem(s);
        }
        public override void EndArray() { elems.Pop(); }
        public override void StartStructure(StructureData structure)
        {
            var s = new XElement("struct");
            s.Add(new XAttribute("name", structure.Name));
            s.Add(new XAttribute("subfields", structure.SubfieldCount));
            PushElem(s);
        }
        public override void EndStructure() { elems.Pop(); }

        private void PushElem(XElement elem)
        {
            elems.Peek().Add(elem);
            elems.Push(elem);
        }
    }
}
