==========================
INLINE QUERY WITH DSNTIAUL
==========================

PURPOSE
  . Illustrate the use of DSNTIAUL in a JCL

HOW TO RUN
  . Start the Raincode Debugger 
  . Examine the contents of the output files

DATABASE CONNECTION
  . The database connection relies on the following elements:
    . Planname
      . defined by the PLAN parameter of IKJEFT01
   . Connection string
     . defined in RcDbConnections.csv
          .*,SqlServer,Data Source=<datasource>;Uid=<user>;Pwd=<password>;Initial Catalog=BankDemo;

NOTES
  . The utility does not automatically set the record length of the output file to the appropriate value
  . The record required length of the output is 57 in this particular case.
  . Any other length to any other value leads to the last record being written being incomplete, redering the file unusable for further processing.
  . Varchar fields are preceded by two bytes containing their length in binary format.
  . The cast statement avoids the binary length being printed but adds an extra space to the end of the field.
  . The final step in the JCL reformats the output to line sequential for readability
  . The same functionality can be implemented with DSNUTILB (see unload demo)

