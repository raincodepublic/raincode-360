//*********************************************************
//*                                                       *
//*   INLINE QUERY EXAMPLE                                *
//*                                                       *
//*********************************************************
//JCL004A   JOB  JOB ACCT,'NAME',COND=(4,LT)
//*********************************************************
//* CLEAN UP FILES FROM PREVIOUS RUN                      *
//*********************************************************
//STEP001 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSOUT DD SYSOUT=*
//SYSIN DD *
 DELETE INLINE.QUERY.*
      SET LASTCC=0
      SET MAXCC=0
/*
//*********************************************************
//* PERFORM INLINE QUERY                                  *
//*********************************************************
//STEP002   EXEC PGM=IKJEFT01
//SYSTSPRT  DD SYSOUT=*
//SYSTSIN   DD *  
 RUN PROGRAM(DSNTIAUL) PLAN(DSNTIAUL_PLAN) PARM('SQL') 
 END
//SYSPRINT  DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//SYSREC00  DD DSN=INLINE.QUERY.SEQ,
//        DISP=(NEW,CATLG,DELETE),RECFM=FB,VOL=SER=DEFAULT,LRECL=57
//SYSPUNCH  DD SYSOUT=*                                           
//SYSIN     DD *
 SELECT ZIPCODE, CAST(CITY AS CHAR(50)), COUNTRY from dbo.ZipCode;
/*
//*********************************************************
//* REFORMAT OUPUT FILE TO LINE SEQUENTIAL                *
//*********************************************************
//STEP003  EXEC PGM=IEBGENER
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  DUMMY
//SYSUT1   DD  DSNAME=INLINE.QUERY.SEQ,DISP=SHR
//SYSUT2   DD  DSN=INLINE.QUERY.LSEQ,
//        DISP=(NEW,CATLG,DELETE),RECFM=LSEQ
//