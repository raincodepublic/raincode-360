Set-Location $PSScriptRoot
$proc=Start-Process -PassThru -NoNewWindow -Wait -FilePath $env:RCBIN\Submit.exe -ArgumentList "-File=CATGVSAM.jcl", "-CatalogConfiguration=Raincode.Catalog.xml"
if($proc.ExitCode -ne 0){ 
	Write-Host "Submit failed with ExitCode:" $proc.ExitCode
	exit $proc.ExitCode 
}