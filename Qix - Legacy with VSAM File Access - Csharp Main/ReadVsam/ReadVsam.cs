﻿using RainCode.Core.Logging;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions.QIX;
using RainCodeLegacyRuntime.QIX.Interface;
using RainCodeLegacyRuntime.Sql.SqlServer;
using System;
using System.IO;

namespace RainCode.Samples
{
   class ReadVsam
   {
      public static void Main(string[] args)
      {
         Logger.CreateAndRegister<ConsoleLogger>();
         Logger.LogLevel = Level.INFO;
         var ecargs = new ExecutionContextArgs()
         {
            QixMode = true,
         };
         var ec = new ExecutionContext(ecargs);
         ec.SqlRuntime = new SqlServerRuntime(ec, GetConnectionString());
         ec.SqlRuntime.ExecutionStrategy = RainCodeLegacyRuntime.Sql.SqlRuntime.ExecutionModeStrategy.DynamicOnly;
         ec.Encoding = new RainCodeLegacyRuntimeUtils.RuntimeEncoding("500"); // Read VSAM in EBCDIC
         ec.IOOperation.ConfigureSystemDatasets();
         ReadAndRegisterFileConfig(ec);
         var link = ec.QixInterface.Factory.CreateLink();
         link.Context = ec;
         link.PROGRAM = "PLIREAD";
         var ca = ec.Allocate(PLIREADHelper.StructureSize);

         PLIREADHelper pLIREAD = new PLIREADHelper(ec, ca);
         pLIREAD.ACCOUNT_ID = "BE00-1000-0000-9297";
         link.COMMAREA = new QixPointer(ca);
         try
         {
            ec.QixInterface.Operation.Link(link);
         }
         catch (AbendException e)
         {
            Console.WriteLine("\nAbend {0} detected", e.Message);
         }
         Console.WriteLine("\nACCOUNT_ID: {0}", pLIREAD.ACCOUNT_ID);
         Console.WriteLine("\nCUSTOMERID: {0}", pLIREAD.CUSTOMERID);
         Console.WriteLine("\nAMOUNT: {0}", pLIREAD.AMOUNT);
         Environment.Exit(ec.ReturnCode);
      }
      private static void ReadAndRegisterFileConfig(ExecutionContext ec)
      {
         string fileConfigurationStr = string.Empty;
         if (File.Exists("FileConfigNightbuild.xml"))
            fileConfigurationStr = File.ReadAllText("FileConfigNightbuild.xml"); //When running from the nightbuild regtest
         else
            fileConfigurationStr = File.ReadAllText("FileConfig.xml"); //When running from the solution
         ec.IOOperation.LoadAndRegisterConfigurationFromData("fileConfig.xml", fileConfigurationStr);
      }
      #region connectionstring
      private static string GetConnectionString()
      {
         string NightbuildConnectionString = "$SQLSERVERCONNECTIONSTRING$"; //Automated test framework substitution
         string Rc360ConnectionString = Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog=BankDemo;";
         return !NightbuildConnectionString.Contains("$SQLSERVERCONNECTIONSTRING") ? NightbuildConnectionString : Rc360ConnectionString;
      }
      #endregion connectionstring
   }
}
