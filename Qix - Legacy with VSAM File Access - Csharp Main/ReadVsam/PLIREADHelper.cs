namespace RainCode.Samples
{
    using RainCodeLegacyRuntime.Core;
    using RainCodeLegacyRuntime.Descriptor;
    using RainCodeLegacyRuntime.Types;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Generated on 06/12/2024 at 19:28:36
    /// by Raincode PL/I compiler version 5.0.3.0 (Build nb_v5.0.2.0-7-g91a6eb07eda)
    /// with arguments -HelperClassName=RainCode.Samples.PLIREADHelper -SQL=sqlserver -HelperStructure=ca -IncludeSearchPath=.\Legacy -HelperClassOutputDir=.\ReadVsam .\Legacy\PlIREAD.pli
    ///
    /// Input program path: 'C:\Repositories\raincode360\Qix - Legacy with VSAM File Access - Csharp Main\Legacy\PlIREAD.pli'
    /// Structure was originally in files:
    ///   PlIREAD.pli
    /// </summary>
    public partial class PLIREADHelper : BaseHelper, System.IDisposable
    {
        public override string Name => "ca";
        public const int StructureSize = 34;
        public override int Size => StructureSize;

        public readonly RawData Raw;
        public PLIREADHelper(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
            Raw =  new RawData(mem);
        }

        public PLIREADHelper(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
            Raw =  new RawData(lv.BitString.MemoryArea);
        }

        public static PLIREADHelper StackAllocate(ExecutionContext ctxt)
        {
            return new PLIREADHelper(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static PLIREADHelper Allocate(ExecutionContext ctxt)
        {
            PLIREADHelper result = new PLIREADHelper(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        public struct RawData
        {
            private readonly MemoryArea theMemoryArea;
            internal RawData(MemoryArea mem)
            {
                theMemoryArea = mem;
            }

            public MemoryArea ACCOUNT_ID => theMemoryArea.Substr(0, 19);
            public MemoryArea CUSTOMERID => theMemoryArea.Substr(19, 5);
            public MemoryArea AMOUNT => theMemoryArea.Substr(24, 10);
        }

        /// <summary>
        /// name:   ACCOUNT_ID
        /// type:   string
        /// offset: 0
        /// size:   19
        /// </summary> 
        public string ACCOUNT_ID
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.ACCOUNT_ID);
            set => Convert.move_string_to_char_string(theContext, value, Raw.ACCOUNT_ID);
        }

        private TypedLValue theCachedACCOUNT_ID_TypedLValue = TypedLValue.Null;
        public TypedLValue ACCOUNT_ID_TypedLValue
        {
            get
            {
                if (theCachedACCOUNT_ID_TypedLValue.IsNull)
                {
                    theCachedACCOUNT_ID_TypedLValue = new TypedLValue(Raw.ACCOUNT_ID, CharacterStringType.Get(19,0,false));
                }
                return theCachedACCOUNT_ID_TypedLValue;
            }
        }

        /// <summary>
        /// name:   CUSTOMERID
        /// type:   string
        /// offset: 19
        /// size:   5
        /// </summary> 
        public string CUSTOMERID
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.CUSTOMERID);
            set => Convert.move_string_to_char_string(theContext, value, Raw.CUSTOMERID);
        }

        private TypedLValue theCachedCUSTOMERID_TypedLValue = TypedLValue.Null;
        public TypedLValue CUSTOMERID_TypedLValue
        {
            get
            {
                if (theCachedCUSTOMERID_TypedLValue.IsNull)
                {
                    theCachedCUSTOMERID_TypedLValue = new TypedLValue(Raw.CUSTOMERID, CharacterStringType.Get(5,0,false));
                }
                return theCachedCUSTOMERID_TypedLValue;
            }
        }

        /// <summary>
        /// name:   AMOUNT
        /// type:   string
        /// offset: 24
        /// size:   10
        /// </summary> 
        public string AMOUNT
        {
            get => Convert.cnv_char_string_to_string(theContext, Raw.AMOUNT);
            set => Convert.move_string_to_char_string(theContext, value, Raw.AMOUNT);
        }

        private TypedLValue theCachedAMOUNT_TypedLValue = TypedLValue.Null;
        public TypedLValue AMOUNT_TypedLValue
        {
            get
            {
                if (theCachedAMOUNT_TypedLValue.IsNull)
                {
                    theCachedAMOUNT_TypedLValue = new TypedLValue(Raw.AMOUNT, CharacterStringType.Get(10,0,false));
                }
                return theCachedAMOUNT_TypedLValue;
            }
        }

        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    StructTypeDescriptor.Get(new FieldDescriptor[] {
                            new FieldDescriptor ("ACCOUNT_ID",0,CharacterStringType.Get(19,0,false)),
                            new FieldDescriptor ("CUSTOMERID",19,CharacterStringType.Get(5,0,false)),
                            new FieldDescriptor ("AMOUNT",24,CharacterStringType.Get(10,0,false))
                    });
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor => GetStaticDescriptor();

    }

}
