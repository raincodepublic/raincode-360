       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBWRITE.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY SQLCA.
       COPY ACCOUNT.
   	   EXEC SQL
   	    DECLARE ACC-CUR CURSOR FOR
      	   SELECT 
               ACCOUNTNUMBER,
               CUSTOMERID,
               AMOUNT
           FROM 
               DBO.ACCOUNT
   	   END-EXEC.
       PROCEDURE DIVISION.
         EXEC SQL
   	      OPEN ACC-CUR
   	   END-EXEC
   	   PERFORM FETCH-ACC-CUR
         PERFORM UNTIL SQLCODE > 0
            EXEC CICS WRITE
              FILE('ACCNT')
              FROM(ACCOUNT-RECORD)
              RIDFLD(W-ACCOUNT-ID)
              KEYLENGTH(19)
              LENGTH(34)
            END-EXEC
   	      PERFORM FETCH-ACC-CUR
         END-PERFORM
         EXEC SQL
   	         CLOSE ACC-CUR
   	   END-EXEC

         EXEC CICS
            RETURN
         END-EXEC
          .
       FETCH-ACC-CUR.
   	   EXEC SQL
   	      FETCH 
            ACC-CUR
            INTO
            :W-ACCOUNT-ID,
            :W-CUSTOMERID-ID,
            :W-AMOUNT
   	   END-EXEC.