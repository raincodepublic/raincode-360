
*****************************************************************************************
*                                                                                       *
*   Qix VSAM Demo : C# calling legacy with VSAM access using CICS LINK                   *
*                                                                                       *
*****************************************************************************************

PURPOSE OF THIS DEMO
  . Demonstrates how to Write and Read VSAM files from CICS modules

WHAT THE DEMO DOES
  . The legacy program COBWRITE fetches account information from the database and writes to VSAM file.
  . The legacy program PLIREAD reads the VSAM file for particular record and displays on the console.

HOW TO RUN THE DEMO
  . Run the script CatalogVsam.ps1 from pwsh console(Tools --> Command line --> Developer Powershell), this catalogs the empty VSAM file
  . Rebuild the solution by navigating to Build from tool bar and clicking the Rebuild Solution.
  . Select 'WriteVsam' project and choose 'Set as Startup Project'.
  . Run the project by pressing Ctrl + F5 or navigating to the 'Debug' menu and selecting 'Start Without Debugging', this must write the VSAM file with the records fetched from the database.
  . Select 'ReadVsam' project and choose 'Set as Startup Project'.
  . Run the project by pressing Ctrl + F5 or navigating to the 'Debug' menu and selecting 'Start Without Debugging', this must read the VSAM file with the key "BE00-1000-0000-9297" and should display the file content on console.
  . Before re-running the WriteVsam, make sure to run the script CatalogVsam.ps1, otherwise you will encounter 'duplicate record QIX exception'.
  . Ignore the 'RainCodeLegacyRuntime.Exceptions.QIX.ReturnException' if you encounter during debug, this is not a real exception.

REFERENCES
  . All Raincode manuals
    . https://www.raincode.com/docs/#_raincode_manuals
  . Read/write VSAM file using FileConfig 
    . https://www.raincode.com/docs/Compiler-document/UserGuide/StackUserGuide.html#File-parameters-configuration-module