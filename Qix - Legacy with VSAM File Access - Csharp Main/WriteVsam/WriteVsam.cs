﻿using RainCode.Core.Logging;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions.QIX;
using RainCodeLegacyRuntime.Sql.SqlServer;
using System;
using System.IO;

namespace RainCode.Samples
{
    class WriteVsam
    {
        public static void Main(string[] args)
        {
            Logger.CreateAndRegister<ConsoleLogger>();
            Logger.LogLevel = Level.INFO;
            var ecargs = new ExecutionContextArgs()
            {
                QixMode = true,
            };
            var ec = new ExecutionContext(ecargs);
            ec.SqlRuntime = new SqlServerRuntime(ec, GetConnectionString());
            ec.SqlRuntime.ExecutionStrategy = RainCodeLegacyRuntime.Sql.SqlRuntime.ExecutionModeStrategy.DynamicOnly;
            ec.Encoding = new RainCodeLegacyRuntimeUtils.RuntimeEncoding("500"); // Write VSAM in EBCDIC
            ec.IOOperation.ConfigureSystemDatasets();
            ReadAndRegisterFileConfig(ec);
            var link = ec.QixInterface.Factory.CreateLink();
            link.Context = ec;
            link.PROGRAM = "COBWRITE";
            try
            {
                ec.QixInterface.Operation.Link(link);
            }
            catch (AbendException e)
            {
                Console.WriteLine("Abend {0} detected", e.Message);
            }
            Environment.Exit(ec.ReturnCode);
        }

        private static void ReadAndRegisterFileConfig(ExecutionContext ec)
        {
            string fileConfigurationStr = string.Empty;
            if (File.Exists("FileConfigNightbuild.xml"))
               fileConfigurationStr = File.ReadAllText("FileConfigNightbuild.xml"); //When running from the nightbuild regtest
            else
               fileConfigurationStr = File.ReadAllText("FileConfig.xml"); //When running from the solution
            ec.IOOperation.LoadAndRegisterConfigurationFromData("fileConfig.xml", fileConfigurationStr);
        }
        #region connectionstring
        private static string GetConnectionString()
        {
            string NightbuildConnectionString = "$SQLSERVERCONNECTIONSTRING$"; //Automated test framework substitution
            string Rc360ConnectionString = Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog=BankDemo;";
            return !NightbuildConnectionString.Contains("$SQLSERVERCONNECTIONSTRING") ? NightbuildConnectionString : Rc360ConnectionString;
        }
        #endregion connectionstring
      }
}
