$ErrorActionPreference = "Stop"
$HelperDestinationPath = Join-path -Path $PSScriptRoot -ChildPath "ReadVsam\PLIREADHelper.cs"

Remove-Item $HelperDestinationPath -Recurse -Force -ErrorAction SilentlyContinue

$proc=Start-Process -PassThru -NoNewWindow -Wait -FilePath $env:RCBIN\plirc.exe -ArgumentList "-HelperClassName=RainCode.Samples.PLIREADHelper", "-SQL=sqlserver", "-HelperStructure=ca", "-IncludeSearchPath=.\Legacy", "-HelperClassOutputDir=.\ReadVsam", ".\Legacy\PlIREAD.pli" 

if($proc.ExitCode -ne 0){ 
	Write-Host "Helperclass generation failed for PLI001 with ExitCode:" $proc.ExitCode 
	exit $proc.ExitCode 
}

Write-Host "Helperclass generated successfully for COB004A"