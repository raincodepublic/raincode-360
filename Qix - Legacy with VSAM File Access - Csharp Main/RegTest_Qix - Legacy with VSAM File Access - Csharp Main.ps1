& ".\CatalogVsam.ps1"
$proc = Start-Process -PassThru -NoNewWindow -Wait -FilePath "dotnet" -ArgumentList "WriteVsam.dll"
if($proc.ExitCode -ne 0){ exit $proc.ExitCode }
$proc = Start-Process -PassThru -NoNewWindow -Wait -FilePath "dotnet" -ArgumentList "ReadVsam.dll"
if($proc.ExitCode -ne 0){ exit $proc.ExitCode }