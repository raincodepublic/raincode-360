//SETUP001 JOB
//*******************************************************************
//*                                                                 *
//*  Add MYPROC to the default library SYS1.PROCLIB                 *
//*  (DLM method)                                                   *
//*                                                                 *
//*******************************************************************
//*-----------------------------------------------------------------*
//*   Delete member MYPROC                                          *
//*-----------------------------------------------------------------*
//STEP001 EXEC PGM=IDCAMS
//SYSOUT  DD SYSOUT=*
//SYSIN   DD *
 DELETE SYS1.PROCLIB(MYPROC) FORCE PURGE 
 SET MAXCC=0
/*
//*-----------------------------------------------------------------*
//*  Add member MYPROC                                              *
//*-----------------------------------------------------------------*
//STEP002  EXEC PGM=SORT
//SYSOUT   DD SYSOUT=*
//SORTIN   DD DATA,DLM=XX
//MYPROC   PROC PR=IEFBR14,HLQ=XXX
//P001     EXEC PGM=&PR
//FILEA    DD DSN=&HLQ..FILEA,
//         DISP=(NEW,CATLG,DELETE),RECFM=LSEQ
//FILEB    DD DSN=&HLQ..FILEB,
//         DISP=(NEW,CATLG,DELETE),RECFM=LSEQ
//*
XX
//SORTOUT  DD DSN=SYS1.PROCLIB(MYPROC),DISP=(SHR,CATLG,DELETE),
//            RECFM=LSEQ
//SYSIN    DD   *
  OPTION COPY
/*
//