--Compiler Errors and Crashes
--"""""""""""""""""""""""""""
SELECT 
    rc_name "Program",
    rc_lang "Language",
    rc_ret_code "rc", 
    case when rc_ret_code > 900 
    then "Compiler Crash"
    else "Error" 
    end "Issue"
FROM 
    RC_PROGRAM P
WHERE 
    P.RC_RET_CODE <> 0
ORDER BY 4, 1, 2, 3


--Missing COBOL Call Targets
--""""""""""""""""""""""""""
with CTE as (
    select rc_id
	     , COB_START_LINE
         , cob_to
	     , (case substr(cob_to,1,1) when '>' then substr(cob_to,2) else cob_to end) target
      from cob_call
)
select PFROM.RC_NAME
       , PFROM.RC_LANG
       , CTE.COB_START_LINE
       , CTE.TARGET
  from CTE
     , RC_PROGRAM PFROM
 where CTE.RC_ID = PFROM.RC_ID
   AND CTE.target not in ('CBLTDLI', '<Dynamic>')
   AND not exists (select 1 from RC_PROGRAM PTO where PTO.RC_NAME = CTE.target)
order by 1, 4


--CICS Commands
--"""""""""""""
select
    rc_command,
	count(*)
from
    RC_CICS
group by
    1
order by 
    2 desc


--Most Common Batch Programs
--""""""""""""""""""""""""""
select 
    s.rc_prog_name,
    p.rc_lang,
    count(*)
from 
    RC_JCL_PGM_STEP s left outer join RC_PROGRAM p 
    on p.rc_name = s.rc_prog_name
group by
    1, 2
order by 
    3 desc


--JCL Errors
--""""""""""
select 
    j.rc_job_name,
    substr(e.rc_err_msg, 1, 30),
    pgm.rc_step_name,
    pgm.rc_prog_name
from 
    RC_JCL_ERR e
    join RC_JCL_JOB j on j.rc_id = e.rc_id
    left join rc_jcl_pgm_step pgm on pgm.rc_id = j.rc_id 
where
    rc_prog_name not in ('IKJEFT01', 'DSNTIAUL', 'SORT', 'IDCAMS', 'DSNUTILB', 'IEBGENER', 'FTP', 'IEFBR14')
group by
    1, 2, 3, 4


--Unresolved Program References in JCL
--""""""""""""""""""""""""""""""""""""
SELECT J.RC_JOB_NAME,
       S.RC_STEP_NAME,
       S.RC_PROG_NAME
  FROM RC_JCL_PGM_STEP S,
       RC_JCL_JOB J
 WHERE J.RC_ID = S.RC_ID AND
       S.RC_PROG_NAME NOT IN ('IKJEFT01', 'DSNTIAUL', 'SORT', 'IDCAMS', 'DSNUTILB', 'IEBGENER', 'FTP', 'IEFBR14') AND
       NOT EXISTS (
           SELECT 1 
		     FROM RC_PROGRAM P 
			WHERE P.RC_NAME = S.RC_PROG_NAME
	   )
