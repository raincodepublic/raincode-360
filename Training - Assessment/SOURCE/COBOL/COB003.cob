       IDENTIFICATION DIVISION.
       PROGRAM-ID. COB003.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 MYPGM PIC X(8).
       01 PARMS usage DISPLAY.
           10 DT1 PIC 9(8).
           10 DT2 PIC 9(8).
           10 AGE PIC 9(8).
       PROCEDURE DIVISION.
           MOVE 19651124 TO DT1
           MOVE 20201124 TO DT2
           DISPLAY 'BEFORE: ' DT1 ' ' DT2
           CALL 'ASM001A' USING PARMS
           MOVE 'COB002' TO MYPGM
           CALL MYPGM
           MOVE 'NOTTHERE' TO MYPGM
           CALL MYPGM
           DISPLAY 'AFTER : ' DT1 ' ' DT2 ' ' AGE
           DISPLAY RETURN-CODE
           GOBACK
           .
