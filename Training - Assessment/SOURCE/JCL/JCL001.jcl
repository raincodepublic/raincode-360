//JCL001 JOB
//*******************************************************************
//*                                                                 *
//*  JOBLIB and STEPLIB example                                     *
//*                                                                 *
//*******************************************************************
//JOBLIB  DD   DSNAME=JOB.LIB,DISP=SHR
//STEP001      EXEC PGM=COB002
//STEPLIB DD   DSNAME=STEP.LIB,DISP=SHR
//