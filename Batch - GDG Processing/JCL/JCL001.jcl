//JCL001   JOB CLASS=A,MSGCLASS=C
//*********************************************************
//* DELETE GDG DEFINITION AND PREVIOUSLY CREATED FILES    *
//*********************************************************
//STEP001  EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
 DELETE   (CUSTOMER.DATA.GDG) GDG FORCE
 SET       MAXCC = 0
/*
//*********************************************************
//* DEFINE GDG WITH 3 GENERATIONS                         *
//*********************************************************
//STEP002 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN DD *
 DEFINE GDG(NAME(CUSTOMER.DATA.GDG) -
 LIMIT(3) -
 NOEMPTY -
 SCRATCH)
/*
//*********************************************************
//* CREATE GENERATION 1                                   *
//*********************************************************
//STEP003 EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSIN DD DUMMY
//SYSUT1 DD *
GENERATION 1
/*
//SYSUT2 DD DSN=CUSTOMER.DATA.GDG(+1),
//       DISP=(NEW,CATLG,DELETE)
//*********************************************************
//* CREATE GENERATION 2                                   *
//*********************************************************
//STEP004 EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSIN DD DUMMY
//SYSUT1 DD *
GENERATION 2
/*
//SYSUT2 DD DSN=CUSTOMER.DATA.GDG(+2),
//       DISP=(NEW,CATLG,DELETE)
//*********************************************************
//* CREATE GENERATION 3                                   *
//*********************************************************
//STEP005 EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSIN DD DUMMY
//SYSUT1 DD *
GENERATION 3
/*
//SYSUT2 DD DSN=CUSTOMER.DATA.GDG(+3),
//       DISP=(NEW,CATLG,DELETE)
//*********************************************************
//* CREATE GENERATION 4 AND DELETE GENERATION 1           *
//*********************************************************
//STEP006 EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSIN DD DUMMY
//SYSUT1 DD *
GENERATION 4
/*
//SYSUT2 DD DSN=CUSTOMER.DATA.GDG(+4),
//       DISP=(NEW,CATLG,DELETE)
