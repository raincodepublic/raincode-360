BATCH DEMO : GDG PROCESSING

PURPOSE
  . Create multiple generations of a GDG

HOW TO RUN
  . Start the Raincode Debugger in Visual Studio
  . Use the Catalog Explorer to look at the result (CUSTOMER.DATA.GDG)

NOTES
. The JCL starts by deleting the output from the previous run (if any)
