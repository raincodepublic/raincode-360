*---------------------------------------------------------------------------------------*
*                                                                                       *
*   Batch Demo : CommandLine -Submit                                                       *
*                                                                                       *
*---------------------------------------------------------------------------------------*

PURPOSE
This demo shows how to execute submit on the command line prompt

STEPS
1. Right click on the script RunCommandLineDemo_Batch-CommandLine-Submit.ps1
2. Select Run with Powershell. 
3. Script will generate dlls for all the sample programs in the same folder.
4. Further, it will execute a JCL file (JCL001) using Raincode Submit.

