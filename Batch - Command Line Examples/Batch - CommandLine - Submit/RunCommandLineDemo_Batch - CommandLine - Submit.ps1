$proc=Start-Process -PassThru -NoNewWindow -Wait -FilePath $env:RCBIN\CobRc.exe -ArgumentList ":MaxMem=1G", "HELLO1.cob", "HELLO2.cob"
if($proc.ExitCode -ne 0){ 
	Write-Host "Compile failed with ExitCode:" $proc.ExitCode 
	exit $proc.ExitCode 
}else{
}
$proc=Start-Process -PassThru -NoNewWindow -Wait -FilePath $env:RCBIN\Submit.exe -ArgumentList "-File=JCL001.jcl"
if($proc.ExitCode -ne 0){ 
	Write-Host "Submit failed with ExitCode:" $proc.ExitCode
	exit $proc.ExitCode 
}
