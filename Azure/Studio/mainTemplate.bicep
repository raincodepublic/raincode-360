@description('Default location for deployed resources')
param location string = resourceGroup().location

@description('Name of the virtual machine hosting Raincode Studio')
param vmName string = 'rcstudio-vm'

@description('Size of the virtual machine hosting Raincode Studio')
param vmSize string = 'Standard_D2s_v3'

@description('Administrator username of the virtual machine hosting Raincode Studio')
param vmAdminUsername string = 'rcstudio'

@description('Administrator password of the virtual machine hosting Raincode Studio')
@secure()
param vmAdminPassword string

@description('Deploy Raincode Studio VM in Azure Bastion. Will otherwise use a public IP.')
@allowed([
  'public'
  'bastion'
])
param vmAccessPolicy string = 'public'

@description('Tags to be set by resource type')
param tagsByResource object = {}

var enableAzureBastion = (vmAccessPolicy == 'bastion')
var nsgName = '${vmName}-nsg'
var publicIpAddressName = '${vmName}-ip'
var vnetName = '${resourceGroup().name}-vnet'
var baseSubnetName = 'BaseSubnet'
var bastionSubnetName = 'AzureBastionSubnet'
var nicName = '${resourceGroup().name}-nic'
var bastionNicName = '${resourceGroup().name}-bastion-nic'
var bastionHostName = '${resourceGroup().name}-bastion'
var vmImage = {
  publisher: 'raincode'
  offer: 'raincode_studio'
  sku: 'raincode_studio_gen2'
  version: 'latest'
}

resource nic 'Microsoft.Network/networkInterfaces@2022-01-01' = if (!enableAzureBastion) {
  name: nicName
  location: location
  properties: {
    ipConfigurations: [
      {
        name: 'ipconfig1'
        properties: {
          subnet: {
            id: baseSubnet.id
          }
          privateIPAllocationMethod: 'Dynamic'
          publicIPAddress: {
            id: publicIpAddress.id
          }
        }
      }
    ]
    networkSecurityGroup: {
      id: nsg.id
    }
  }
}

resource bastionNic 'Microsoft.Network/networkInterfaces@2022-01-01' = if (enableAzureBastion) {
  name: bastionNicName
  location: location
  properties: {
    ipConfigurations: [
      {
        name: 'ipconfig1'
        properties: {
          subnet: {
            id: baseSubnet.id
          }
          privateIPAllocationMethod: 'Dynamic'
        }
      }
    ]
  }
}

resource nsg 'Microsoft.Network/networkSecurityGroups@2022-01-01' = if (!enableAzureBastion) {
  name: nsgName
  location: location
  properties: {
    securityRules: [
      {
        name: 'RDP'
        properties: {
          priority: 300
          protocol: 'TCP'
          access: 'Allow'
          direction: 'Inbound'
          sourceAddressPrefix: '*'
          sourcePortRange: '*'
          destinationAddressPrefix: '*'
          destinationPortRange: '3389'
        }
      }
    ]
  }
}

resource baseSubnet 'Microsoft.Network/virtualNetworks/subnets@2022-01-01' = {
  name: baseSubnetName
  parent: vnet
  properties: {
    addressPrefix: '172.16.1.0/26'
  }
}

resource bastionSubnet 'Microsoft.Network/virtualNetworks/subnets@2022-01-01' = if (enableAzureBastion) {
  name: bastionSubnetName
  parent: vnet
  properties: {
    addressPrefix: '172.16.1.192/26'
  }
  dependsOn: [
    baseSubnet
  ]
}

resource vnet 'Microsoft.Network/virtualNetworks@2022-01-01' = {
  name: vnetName
  location: location
  properties: {
    addressSpace: {
      addressPrefixes: [
        '172.16.1.0/24'
      ]
    }
  }
}

resource publicIpAddress 'Microsoft.Network/publicIPAddresses@2022-01-01' = {
  name: publicIpAddressName
  location: location
  sku: {
    name: enableAzureBastion ? 'Standard' : 'Basic'
  }
  properties: {
    publicIPAllocationMethod: enableAzureBastion ? 'Static' : 'Dynamic'
  }
}

resource bastionHost 'Microsoft.Network/bastionHosts@2022-01-01' = if (enableAzureBastion) {
  name: bastionHostName
  location: location
  tags: contains(tagsByResource, 'Microsoft.Network/bastionHosts') ? tagsByResource['Microsoft.Network/bastionHosts'] : {}
  properties: {
    ipConfigurations: [
      {
        name: 'IpConf'
        properties: {
          subnet: {
            id: bastionSubnet.id
          }
          publicIPAddress: {
            id: publicIpAddress.id
          }
        }
      }
    ]
  }
}

resource vm 'Microsoft.Compute/virtualMachines@2022-03-01' = {
  name: vmName
  location: location
  identity: {
    type: 'SystemAssigned'
  }
  tags: contains(tagsByResource, 'Microsoft.Compute/virtualMachines') ? tagsByResource['Microsoft.Compute/virtualMachines'] : {}
  properties: {
    hardwareProfile: {
      vmSize: vmSize
    }
    storageProfile: {
      osDisk: {
        createOption: 'fromImage'
        managedDisk: {
          storageAccountType: 'Premium_LRS'
        }
      }
      imageReference: vmImage
    }
    networkProfile: {
      networkInterfaces: [
        {
          id: enableAzureBastion ? bastionNic.id : nic.id
        }
      ]
    }
    osProfile: {
      computerName: vmName
      adminUsername: vmAdminUsername
      adminPassword: vmAdminPassword
      windowsConfiguration: {
        enableAutomaticUpdates: true
        provisionVMAgent: true
      }
    }
  }
  plan: {
    publisher: 'raincode'
    product: 'raincode_studio'
    name: 'raincode_studio_gen2'
  }
}
