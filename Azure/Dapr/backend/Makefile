.PHONY: all build boxes images push

PREFIX?=raincode360
GIT_VERSION=$(shell git describe --dirty || echo edge)
SEM_VERSION=$(shell cat VERSION || echo edge)
REPO_ROOT=$(shell readlink -f ../../..)
SRC_DIR=$(REPO_ROOT)/Qix - Emulator - Bank Application/P005
OUT_DIR=$(shell readlink -f boxes)

RCFLAGS=:MaxMem=1G
RCFLAGS+=:OutputDir="$(SRC_DIR)"
RCFLAGS+=:IncludeSearchPath="$(SRC_DIR)"
RCFLAGS+=:QIX :SQL=SqlServer
RCFLAGS+=:SQLServerSchemaCacheFile="$(SRC_DIR)/schemacache.xml"
RCFLAGS+=:IncludeCaseInsensitive
RCFLAGS+=:Warnings=-220
RCFLAGS+=:Silent=TRUE

RAINBOX=RainBox -ConfigurationFile=rainbox.xml -AssemblyPath="$(SRC_DIR)" -SourcePath="$(SRC_DIR)" -LogLevel=TRACE -OutputDir=boxes -AppNamespace=Raincode360.Demos.Dapr

all:
	$(MAKE) build
	$(MAKE) images

build:
	cobrc $(RCFLAGS) "$(SRC_DIR)/COB004A.cob"
	cobrc $(RCFLAGS) "$(SRC_DIR)/COB010A.cob"
	cobrc $(RCFLAGS) "$(SRC_DIR)/COB011.cob"
	plirc $(RCFLAGS) "$(SRC_DIR)/PLI001.pli"

boxes:
	rm -rf "$(OUT_DIR)"
	$(RAINBOX) -Language=COBOL -ServiceName=customers COB004A
	$(RAINBOX) -Language=COBOL -ServiceName=accounts COB010A
	$(RAINBOX) -Language=COBOL -ServiceName=balance COB011
	$(RAINBOX) -Language=PLI -ServiceName=payment PLI001

images: boxes
	docker build -t $(PREFIX)/bankdemo_customers:$(SEM_VERSION) boxes/COB004A
	docker build -t $(PREFIX)/bankdemo_accounts:$(SEM_VERSION) boxes/COB010A
	docker build -t $(PREFIX)/bankdemo_balance:$(SEM_VERSION) boxes/COB011
	docker build -t $(PREFIX)/bankdemo_payment:$(SEM_VERSION) boxes/PLI001

push:
	docker push $(PREFIX)/bankdemo_accounts:$(SEM_VERSION)
	docker push $(PREFIX)/bankdemo_balance:$(SEM_VERSION)
	docker push $(PREFIX)/bankdemo_customers:$(SEM_VERSION)
	docker push $(PREFIX)/bankdemo_payment:$(SEM_VERSION)
