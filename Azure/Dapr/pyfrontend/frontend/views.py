import json
import os
import requests

from datetime import datetime
from decimal import Decimal
from django.shortcuts import render

DAPR_PORT = 3500
ACCOUNTS_APPID = 'accounts'
BALANCE_APPID = 'balance'
PAYMENTS_APPID = 'payment'
CUSTOMERS_APPID = 'customers'
ACCOUNTS_URL = f'http://localhost:{DAPR_PORT}/v1.0/invoke/{ACCOUNTS_APPID}/method/'
BALANCE_URL = f'http://localhost:{DAPR_PORT}/v1.0/invoke/{BALANCE_APPID}/method/'
PAYMENTS_URL = f'http://localhost:{DAPR_PORT}/v1.0/invoke/{PAYMENTS_APPID}/method/'
CUSTOMERS_URL = f'http://localhost:{DAPR_PORT}/v1.0/invoke/{CUSTOMERS_APPID}/method/'


class DecimalJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return float(o)
        return super().default(o)


def post(url, data):
    headers = {'Content-Type': 'application/json'}
    return requests.post(url, data=json.dumps(data, cls=DecimalJSONEncoder), headers=headers)


def home(request):
    template_values = {'accounts': fetch_accounts(
        request), 'insightsUrl': os.getenv('APPINSIGHTS_URL')}
    if 'account' in request.GET:
        template_values['balance'] = fetch_balance(request.GET['account'])
    elif 'amount' in request.POST:
        account_to = request.POST['account_to']
        account_from = request.POST['account_from']
        # Fetch balances before payment
        before = balances([account_to, account_from])
        before = [before[account_from], before[account_to]]
        # Execute payment
        do_payment(request)
        # Fetch updated balances
        after = balances([account_to, account_from])
        after = [after[account_from], after[account_to]]
        # Render page
        template_values['before'] = before
        template_values['after'] = after
    elif 'customer' in request.GET:
        template_values['customers'] = fetch_customers(request.GET['customer'])
    return render(request, 'home.html', template_values)


def fetch_balance(account_number):
    # Call CICS balance program through Dapr
    args = {
        "C007": {
            "C007-ACCOUNTNUMBER": account_number
        }
    }
    req = post(BALANCE_URL, args)
    req.raise_for_status()
    return req.json(parse_float=Decimal)['C007']['C007-AMOUNT']


def balances(accounts):
    return {account: fetch_balance(account) for account in accounts}


def fetch_accounts(request):
    # Call CICS accounts program through Dapr
    args = {
        "COB010A-IN": {
            "COB010A-DIRECTION": "D",
            "COB010A-LINECOUNT": 25,
            "COB010A-ACCOUNTNUMBER-FILTER": "",
            "COB010A-LASTNAME-FILTER": "",
            "COB010A-FIRSTNAME-FILTER": "",
            "COB010A-ZIPCODE-FILTER": "",
            "COB010A-CITY-FILTER": ""
        }
    }
    req = post(ACCOUNTS_URL, args)
    req.raise_for_status()
    accounts = req.json(parse_float=Decimal)[
        'COB010A-OUT']['COB010A-ACCOUNTNUMBER']
    return accounts


def do_payment(request):
    # Call CICS payment program through Dapr
    args = {
        "p001_accountnumber_from": request.POST['account_from'],
        "p001_accountnumber_to": request.POST['account_to'],
        "p001_amount": Decimal(request.POST['amount']),
        "p001_descriptiontext": request.POST['description']
    }
    req = post(PAYMENTS_URL, args)
    req.raise_for_status()


def fetch_customers(prefix):
    # Call CICS customer program through Dapr
    args = {
        "COB004A-IN": {
            "COB004A-DIRECTION": "D",
            "COB004A-SORT": "L",
            "COB004A-LASTNAME-FILTER": prefix,
            "COB004A-FIRSTNAME-FILTER": "",
            "COB004A-ZIPCODE-FILTER": "",
            "COB004A-CITY-FILTER": "",
            "COB004A-BIRTHDATE-FILTER": "",
            "COB004A-LINECOUNT": 25
        }
    }
    req = post(CUSTOMERS_URL, args)
    req.raise_for_status()
    result = req.json(parse_float=Decimal)['COB004A-OUT']
    customers = []
    for i in range(result['COB004A-FETCHED']):
        customer = {}
        customer['id'] = result['COB004A-CUSTOMERID'][i]
        customer['lastname'] = result['COB004A-LASTNAME'][i]
        customer['firstname'] = result['COB004A-FIRSTNAME'][i]
        customer['zipcode'] = result['COB004A-ZIPCODE'][i]
        customer['city'] = result['COB004A-CITY'][i]
        customer['birthdate'] = datetime.strptime(
            result['COB004A-BIRTHDATE'][i], '%Y-%m-%d').date()
        customers.append(customer)
    return customers
