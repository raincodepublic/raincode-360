MICROSOFT SOFTWARE LICENSE TERMS

MICROSOFT POWER BI DESKTOP

These license terms are an agreement between Microsoft Corporation (or
based on where you live, one of its affiliates) and you. Please read
them. They apply to the software named above, which includes the media
on which you received it, if any. The terms also apply to any Microsoft

  * updates,

  * supplements,

  * Internet-based services, and

  * support services

for this software, unless other terms accompany those items. If so,
those terms apply.

BY USING THE SOFTWARE, YOU ACCEPT THESE TERMS. YOU MAY CHOOSE NOT
TO ACCEPT THESE TERMS, IN WHICH CASE YOU MAY NOT USE THE SOFTWARE
(IF YOU HAVE NOT ALREADY INSTALLED IT) OR WITHDRAW YOUR ACCEPTANCE
ANY TIME BY UNINSTALLING THE SOFTWARE.

IF YOU COMPLY WITH THESE LICENSE TERMS, YOU HAVE THE RIGHTS BELOW.

1. INSTALLATION AND USE RIGHTS.

  a. Installation and Use.

    * You may install and use any number of copies of the software
    on your devices.

  b. Third Party Programs.

    i. The software may include third party programs that Microsoft,
    not the third party, licenses to you under this agreement. Notices,
    if any, for the third party program are included for your
    information only.

    ii. The software may also include components licensed
    under open source licenses with source code availability
    obligations. Copies of those licenses, if applicable, are
    included in the ThirdPartyNotices file. You may obtain this
    source code from us, if and as required under the relevant
    open source licenses, by sending a money order or check for
    $5.00 to: Source Code Compliance Team, Microsoft Corporation,
    1 Microsoft Way, Redmond, WA 98052. Please write "third party
    open source code in Power BI Desktop" in the memo line of your
    payment. We may also make a copy of the source code available
    at <http://thirdpartysource.microsoft.com>.

  c. Third Party Connectors. The software lets you connect to various
  third party data sources. Your agreements with any of these third
  party data source providers is solely between you and the applicable
  third party data source providers and may be governed by other terms
  of use or agreements that apply to such third party sources. You
  agree that you are solely responsible for and have the authority to
  import data from these third party data sources. Microsoft does not
  monitor, control or assume any liability associated with any data
  that you import from third party data sources and is not responsible
  for its quality, accuracy, nature and/or ownership. You agree
  that you are solely responsible for any content that you create,
  transmit, distribute or display based on the data that you import,
  while using the software.

  d. Included Microsoft Programs. The software contains other
  Microsoft programs. Any content provided through Bing Maps,
  including geocodes, can only be used within the software
  through which the content is provided. Your use of Bing Maps
  is governed by the Bing Maps End User Terms of Use available at
  go.microsoft.com/?linkid=9710837 and the Bing Maps Privacy Statement
  available at go.microsoft.com/fwlink/?LinkID=248686.

2. UPDATES. The software may periodically check for updates, and
download and install them for you. You may obtain updates only from
Microsoft or authorized sources. Microsoft may need to update your
system to provide you with updates. You agree to receive these
automatic updates without any additional notice. Updates may not
include or support all existing software features, services, or
peripheral devices.

3. DATA COLLECTION. The software may collect information about you and
your use of the software and send that to Microsoft. Microsoft may use
this information to provide services and improve Microsoft’s products
and services. Your opt-out rights, if any, are described in the product
documentation. Some features in the software may enable collection
of data from users of your applications that access or use the
software. If you use these features to enable data collection in your
applications, you must comply with applicable law, including getting
any required user consent, and maintain a prominent privacy policy that
accurately informs users about how you use, collect, and share their
data. You can learn more about Microsoft’s data collection and use
in the product documentation and the Microsoft Privacy Statement at
https://go.microsoft.com/fwlink/?LinkId=512132. You agree to comply
with all applicable provisions of the Microsoft Privacy Statement.

4. PROCESSING OF PERSONAL DATA. To the extent Microsoft is a
processor or subprocessor of personal data in connection with
the software, Microsoft makes the commitments in the European
Union General Data Protection Regulation Terms of the Online
Services Terms to all customers effective May 25, 2018, at
http://go.microsoft.com/?linkid=9840733.

5. TIME-SENSITIVE SOFTWARE. The software will stop running when a
new version of the software is available. You will receive notice in
the Status Bar in the software at least thirty days before it stops
running. You may not be able to access data used with the software
when it stops running.

6. SCOPE OF LICENSE. The software is licensed, not sold. This agreement
only gives you some rights to use the software. Microsoft reserves
all other rights. Unless applicable law gives you more rights despite
this limitation, you may use the software only as expressly permitted
in this agreement. In doing so, you must comply with any technical
limitations in the software that only allow you to use it in certain
ways. You may not

  * allow a third party application to connect and read data from
the software without Microsoft’s prior written approval;

  * download or use the software to conduct competitive research;

  * disclose the results of any benchmark tests of the software to
  any third party without Microsoft’s prior written approval;

  * work around any technical limitations in the software;

  * reverse engineer, decompile or disassemble the software, except
  and only to the extent that applicable law expressly permits,
  despite this limitation;

  * make more copies of the software than specified in this agreement
  or allowed by applicable law, despite this limitation;

  * publish the software for others to copy;

  * rent, lease or lend the software;

  * transfer the software or this agreement to any third party; or

  * use the software for commercial software hosting services.

7. DOCUMENTATION. Any person that has valid access to your computer or
internal network may copy and use the documentation for your internal,
reference purposes.

8. EXPORT RESTRICTIONS. The software is subject to United States
export laws and regulations. You must comply with all domestic
and international export laws and regulations that apply to the
software. These laws include restrictions on destinations,
end users and end use. For additional information, see
www.microsoft.com/exporting.

9. SUPPORT SERVICES. Because this software is "as is," we may not
provide support services for it.

10. ENTIRE AGREEMENT. This agreement, and the terms for supplements,
updates, Internet-based services and support services that you use,
are the entire agreement for the software and support services.

11. APPLICABLE LAW.

  a. United States. If you acquired the software in the United States,
  Washington state law governs the interpretation of this agreement
  and applies to claims for breach of it, regardless of conflict of
  laws principles. The laws of the state where you live govern all
  other claims, including claims under state consumer protection laws,
  unfair competition laws, and in tort.

  b. Outside the United States. If you acquired the software in any
  other country, the laws of that country apply.

12. LEGAL EFFECT. This agreement describes certain legal rights. You
may have other rights under the laws of your country. You may also
have rights with respect to the party from whom you acquired the
software. This agreement does not change your rights under the laws
of your country if the laws of your country do not permit it to do so.

13. DISCLAIMER OF WARRANTY. THE SOFTWARE IS LICENSED "AS-IS."
YOU BEAR THE RISK OF USING IT. MICROSOFT GIVES NO EXPRESS WARRANTIES,
GUARANTEES OR CONDITIONS. YOU MAY HAVE ADDITIONAL CONSUMER RIGHTS OR
STATUTORY GUARANTEES UNDER YOUR LOCAL LAWS WHICH THIS AGREEMENT CANNOT
CHANGE. TO THE EXTENT PERMITTED UNDER YOUR LOCAL LAWS, MICROSOFT
EXCLUDES THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NON-INFRINGEMENT.

FOR AUSTRALIA - YOU HAVE STATUTORY GUARANTEES UNDER THE AUSTRALIAN
CONSUMER LAW AND NOTHING IN THESE TERMS IS INTENDED TO AFFECT THOSE
RIGHTS.

14. LIMITATION ON AND EXCLUSION OF REMEDIES AND DAMAGES. YOU CAN
RECOVER FROM MICROSOFT AND ITS SUPPLIERS ONLY DIRECT DAMAGES UP
TO U.S. $5.00. YOU CANNOT RECOVER ANY OTHER DAMAGES, INCLUDING
CONSEQUENTIAL, LOST PROFITS, SPECIAL, INDIRECT OR INCIDENTAL DAMAGES.

This limitation applies to

  * anything related to the software, services, content (including
  code) on third party Internet sites, or third party programs; and

  * claims for breach of contract, breach of warranty, guarantee
  or condition, strict liability, negligence, or other tort to the
  extent permitted by applicable law.

It also applies even if Microsoft knew or should have known about
the possibility of the damages. The above limitation or exclusion may
not apply to you because your country may not allow the exclusion or
limitation of incidental, consequential or other damages.

Microsoft welcomes your comments. Contact Microsoft at

United States and Canada

  * (800) MICROSOFT;

  * One Microsoft Way, Redmond, WA 98052-6399

For software acquired outside of the United States and Canada,
please contact the Microsoft affiliate serving your country
(<http://www.microsoft.com/worldwide.aspx>).

Microsoft Power BI Desktop incorporates material from
Third Party Code. For more details on this material visit:
(<https://go.microsoft.com/fwlink/?LinkId=856882&clcid=0x409>).
