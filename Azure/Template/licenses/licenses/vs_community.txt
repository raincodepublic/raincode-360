MICROSOFT SOFTWARE LICENSE TERMS

MICROSOFT VISUAL STUDIO COMMUNITY 2019

These license terms are an agreement between you and Microsoft
Corporation (or based on where you live, one of its affiliates). They
apply to the software named above. The terms also apply to any
Microsoft services or updates for the software, except to the extent
those have different terms.

IF YOU COMPLY WITH THESE LICENSE TERMS, YOU HAVE THE RIGHTS BELOW.

1. INSTALLATION AND USE RIGHTS.

  a. Individual License. If you are an individual working on your own
  applications, either to sell or for any other purpose, you may use
  the software to develop and test those applications.

  b. Organizational License. If you are an organization, your users
  may use the software as follows:

    * Any number of your users may use the software to develop and
    test applications released under Open Source Initiative (OSI)
    approved open source software licenses.

    * Any number of your users may use the software to develop and
    test extensions to Visual Studio.

    * Any number of your users may use the software to develop and
    test device drivers for the Windows operating system.

    * Any number of your users may use the software only for Microsoft
    SQL Server development when using the SQL Server Data Tools or
    the extensions "Microsoft Analysis Services Projects", "Microsoft
    Reporting Services Projects", or "SQL Server Integration Services
    Projects" to develop Microsoft SQL Server database projects or
    Analysis Services, Reporting Services, Power BI Report Server,
    or Integration Services projects.

    * Any number of your users may use the software to develop and
    test your applications as part of online or in person classroom
    training and education, or for performing academic research.

    * If none of the above apply, and you are also not an enterprise
    (defined below), then up to 5 of your individual users can use
    the software concurrently to develop and test your applications.

    * If you are an enterprise, your employees and contractors
    may not use the software to develop or test your applications,
    except for: (i) open source; (ii) Visual Studio extensions; (iii)
    device drivers for the Windows operating system; (iv) SQL Server
    development; and, (v) education purposes as permitted above.

        An "enterprise" is any organization and its affiliates who
        collectively have either (a) more than 250 PCs or users or
        (b) one million U.S. dollars (or the equivalent in other
        currencies) in annual revenues, and "affiliates" means those
        entities that control (via majority ownership), are controlled
        by, or are under common control with an organization.

  c. Workloads. These license terms apply to your use of the workloads
  made available to you within the software, except to the extent
  a workload, or a workload component comes with different license
  terms and support policies.

  d. Backup Copy. You may make one backup copy of the software,
  for reinstalling the software.

  e. Online Services in the Software. Some features of the software
  make use of online services to provide you information about updates
  to the software or extensions, or to enable you to retrieve content,
  collaborate with others, or otherwise supplement your development
  experience. As used throughout this agreement, the term "software"
  includes these online service features.

  f. Demo Use. The uses permitted above include use of the software
  in demonstrating your applications.

2. TERMS FOR SPECIFIC COMPONENTS.

  a. Utilities. The software contains items on the Utilities List at
  https://aka.ms/vs/16/utilities. You may copy and install those items
  onto your devices to debug and deploy your applications and databases
  you developed with the software. The Utilities are designed for
  temporary use. Microsoft may not be able to patch or update Utilities
  separately from the rest of the software. Some Utilities by their
  nature may make it possible for others to access the devices on
  which the Utilities are installed. You should delete all Utilities
  you have installed after you finish debugging or deploying your
  applications and databases. Microsoft is not responsible for any
  third party use or access of devices, or of the applications or
  databases on devices, on which Utilities have been installed.

  b. Build Devices and Visual Studio Build Tools. You may copy and
  install files from the software or from Visual Studio Build Tools
  onto your build devices, including physical devices and virtual
  machines or containers on those machines, whether on-premises
  or remote machines that are owned by you, hosted on Microsoft
  Azure for you, or dedicated solely to your use (collectively,
  "Build Devices"). You and others in your organization may use these
  files on your Build Devices solely to compile, build, and verify
  applications developed by using the software, or run quality or
  performance tests of those applications as part of the build process.

  c. Fonts. While the software is running, you may use its fonts to
  display and print content. You may only (i) embed fonts in content
  as permitted by the embedding restrictions in the fonts; and (ii)
  temporarily download them to a printer or other output device to
  print content.

  d. Licenses for Other Components.

    * Microsoft Platforms. The software may include components
    from Microsoft Windows; Microsoft Windows Server; Microsoft
    SQL Server; Microsoft Exchange; Microsoft Office; and Microsoft
    SharePoint. These components are governed by separate agreements
    and their own product support policies, as described in the
    Microsoft "Licenses" folder accompanying the software, except
    that, if license terms for those components are also included in
    the associated installation directory, those license terms control.

    * Third party Components. The software may include third party
    components with separate legal notices or governed by other
    agreements, as may be described in the ThirdPartyNotices file(s)
    accompanying the software.

  e. Package Managers. The software includes package managers, like
  NuGet, that give you the option to download other Microsoft and third
  party software packages to use with your applications. Those packages
  are under their own licenses, and not these license terms. Microsoft
  does not distribute, license or provide any warranties for any of
  the third party packages.

3. DISTRIBUTABLE CODE. The software contains code that you
may distribute in applications you develop as described in this
Section. For purposes of this Section 3, the term "distribution" also
means deployment of your applications for third parties to access
over the Internet.

  a. Right to Use and Distribute. The code and text files listed
  below are "Distributable Code".

    * Distributable List. You may copy and distribute the object
    code form of code listed on the Distributable List located at
    https://aka.ms/vs/16/redistribution.

    * Sample Code, Templates and Styles. You may copy, modify and
    distribute the source and object code form of code marked as
    "sample", "template", "simple styles" and "sketch styles".

    * Third Party Distribution. You may permit distributors of your
    applications to copy and distribute the Distributable Code as
    part of those applications.

  b. Distribution Requirements. For any Distributable Code you
  distribute, you must:

    * add significant primary functionality to it in your applications;
    and

    * require distributors and external end users to agree to terms
    that protect the Distributable Code at least as much as this
    agreement.

  c. Distribution Restrictions. You may not:

    * use Microsoft’s trademarks in your applications’ names or
    in a way that suggests your applications come from or are endorsed
    by Microsoft; or

    * modify or distribute the source code of any Distributable Code
    so that any part of it becomes subject to an Excluded License. An
    "Excluded License" is one that requires, as a condition of use,
    modification or distribution of code, that (i) it be disclosed
    or distributed in source code form; or (ii) others have the right
    to modify it.

4. DEVELOPING EXTENSIONS.

  a. Limits on Extensions. You may not develop or enable others to
  develop extensions for the software (or any other component of the
  Visual Studio family of products) which circumvent the technical
  limitations implemented in the software. If Microsoft technically
  limits or disables extensibility for the software, you may not extend
  the software by, among other things, loading or injecting into the
  software any non-Microsoft add-ins, macros, or packages; modifying
  the software registry settings; or adding features or functionality
  equivalent to that found in the Visual Studio family of products.

  b. No Degrading the Software. If you develop an extension for the
  software (or any other component of the Visual Studio family of
  products), you must test the installation, uninstallation, and
  operation of your extension to ensure that such processes do not
  disable any features or adversely affect the functionality of the
  software (or such component) or of any previous version or edition
  thereof.

5. DATA.

  a. Data Collection. The software may collect information about you
  and your use of the software, and send that to Microsoft. Microsoft
  may use this information to provide services and improve our products
  and services. You may opt-out of many of these scenarios, but not
  all, as described in the software documentation. There are also
  some features in the software that may enable you and Microsoft
  to collect data from users of your applications. If you use these
  features, you must comply with applicable law, including providing
  appropriate notices to users of your applications together with
  Microsoft’s privacy statement. Our privacy statement is located at
  https://go.microsoft.com/fwlink/?LinkID=824704. You can learn more
  about data collection and its use from the software documentation
  and our privacy statement. Your use of the software operates as
  your consent to these practices.

  b. Processing of Personal Data. To the extent Microsoft is a
  processor or subprocessor of personal data in connection with
  the software, Microsoft makes the commitments in the European
  Union General Data Protection Regulation Terms of the Online
  Services Terms to all customers effective May 25, 2018, at
  https://docs.microsoft.com/en-us/legal/gdpr.

6. SCOPE OF LICENSE. The software is licensed, not sold. These license
terms only give you some rights to use the software. Microsoft reserves
all other rights. Unless applicable law gives you more rights despite
this limitation, you may use the software only as expressly permitted
in these license terms. In doing so, you must comply with any technical
limitations in the software that only allow you to use it in certain
ways. In addition, you may not:

  * work around any technical limitations in the software;

  * reverse engineer, decompile or disassemble the software, or
  otherwise attempt to derive the source code for the software,
  except and only to the extent required by third party licensing
  terms governing use of certain open source components that may be
  included with the software;

  * remove, minimize, block or modify any notices of Microsoft or
  its suppliers in the software;

  * use the software in any way that is against the law;

  * share, publish, rent or lease the software; or

  * provide the software as a stand-alone offering or combine it
  with any of your applications for others to use, or transfer the
  software or this agreement to any third party.

7. SUPPORT. Because the software is "as is", we may not provide
support services for it.

8. ENTIRE AGREEMENT. This agreement, and the terms for supplements,
updates, Internet-based services and support services that you use,
are the entire agreement for the software and support services.

9. EXPORT RESTRICTIONS. You must comply with all domestic and
international export laws and regulations that apply to the
software, which include restrictions on destinations, end users
and end use. For further information on export restrictions, visit
www.microsoft.com/exporting.

10. APPLICABLE LAW. If you acquired the software in the United States,
Washington State law applies to interpretation of and claims for breach
of this agreement, and the laws of the state where you live apply to
all other claims. If you acquired the software in any other country,
its laws apply.

11. CONSUMER RIGHTS; REGIONAL VARIATIONS. These license terms
describe certain legal rights. You may have other rights, including
consumer rights, under the laws of your state or country. You may
also have rights with respect to the party from which you acquired
the software. This agreement does not change those other rights if the
laws of your state or country do not permit it to do so. For example,
if you acquired the software in one of the below regions, or mandatory
country law applies, then the following provisions apply to you:

  a. Australia. You have statutory guarantees under the Australian
  Consumer Law and nothing in these license terms is intended to
  affect those rights.

  b. Canada. You may stop receiving updates on your device by turning
  off Internet access. If and when you re-connect to the Internet,
  the software will resume checking for and installing updates.

  c. Germany and Austria.

    (i) Warranty. The properly licensed software will perform
    substantially as described in any Microsoft materials that
    accompany it. However, Microsoft gives no contractual guarantee
    in relation to the software.

    (ii) Limitation of Liability. In case of intentional conduct,
    gross negligence, claims based on the Product Liability Act,
    as well as in case of death or personal or physical injury,
    Microsoft is liable according to the statutory law.

    Subject to the preceding sentence (ii), Microsoft will only
    be liable for slight negligence if Microsoft is in breach of
    such material contractual obligations, the fulfillment of which
    facilitate the due performance of this agreement, the breach
    of which would endanger the purpose of this agreement and the
    compliance with which a party may constantly trust in (so-called
    "cardinal obligations"). In other cases of slight negligence,
    Microsoft will not be liable for slight negligence.

12. DISCLAIMER OF WARRANTY. THE SOFTWARE IS LICENSED "AS-IS". YOU
BEAR THE RISK OF USING IT. MICROSOFT GIVES NO EXPRESS WARRANTIES,
GUARANTEES OR CONDITIONS. TO THE EXTENT PERMITTED UNDER YOUR LOCAL
LAWS, MICROSOFT EXCLUDES THE IMPLIED WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.

13. LIMITATION ON DAMAGES. YOU CAN RECOVER FROM MICROSOFT AND ITS
SUPPLIERS ONLY DIRECT DAMAGES UP TO U.S. $5.00. YOU CANNOT RECOVER
ANY OTHER DAMAGES, INCLUDING CONSEQUENTIAL, LOST PROFITS, SPECIAL,
INDIRECT OR INCIDENTAL DAMAGES.

This limitation applies to (a) anything related to the software,
services, content (including code) on third party Internet sites,
or third party applications; and (b) claims for breach of contract,
breach of warranty, guarantee or condition, strict liability,
negligence, or other tort to the extent permitted by applicable law.

It also applies even if Microsoft knew or should have known about
the possibility of the damages. The above limitation or exclusion
may not apply to you because your state or country may not allow the
exclusion or limitation of incidental, consequential or other damages.
