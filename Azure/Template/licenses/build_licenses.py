#!/usr/bin/env python3

import csv
from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('.'))
template = env.get_template('template.adoc')

with open('licenses.csv') as f:
    licenses = [line for line in csv.DictReader(f)]

# Raincode Studio
with open('RaincodeStudio_ThirdPartyLicenses.adoc', 'w') as f:
    context = {
        'product': 'Raincode Studio',
        'licenses': [license for license in licenses if license['RaincodeStudio'] == 'yes'],
    }
    f.write(template.render(context))

# Raincode 360
with open('Raincode360_ThirdPartyLicenses.adoc', 'w') as f:
    context = {
        'product': 'Raincode 360',
        'licenses': licenses,
    }
    f.write(template.render(context))
