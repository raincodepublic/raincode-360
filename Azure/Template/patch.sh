#!/bin/sh

DIST_FILE="dist/$1"
PATCH_PROFILE="profiles/$2"

mkdir -p dist
cp $1 ${DIST_FILE}
for i in `ls ${PATCH_PROFILE}/*.json-patch`; do
    echo "Applying $i..."
    jsonpatch -i "${DIST_FILE}" "$i"
done

jq --indent 4 . "${DIST_FILE}" >"${DIST_FILE}~"
mv "${DIST_FILE}~" "${DIST_FILE}"
