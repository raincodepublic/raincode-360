<#
.SYNOPSIS
Provisioning script for local based Raincode 360 virtual machines.

.DESCRIPTION
Raincode 360 is deployed as an local VM solution with multiple components,
including a virtual machine that is used as the entrypoint for the
end-user.  This script provisions the virtual machine and configures
the other Azure resources as needed.

.PARAMETER GitBranch
The name of the Git branch or tag that will be initially checked
out when cloning the Raincode 360 repository. This is not used when
deploying on Azure Marketplace.

.PARAMETER Username
The username of the Raincode 360 virtual machine administrator account.

#>

[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingPlainTextForPassword', '')]
[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [string]
    $GitBranch,

    [Parameter(Mandatory)]
    [string]
    $Username
)
$SqlServerIp = "localhost"
$SqlServerLogin = "Raincode"
$SqlServerPassword = "demo"
$ErrorActionPreference = 'Stop'

$rc360Directory = "C:\Raincode360"
$CacheDirectory = "$rc360Directory\Cache"
$MarkerDirectory = "$rc360Directory\Markers"
$CompilationRepositoryDirectory = "$rc360Directory\CompilationRepositories"
$GlobalCompilationRepositoryPath = "$CompilationRepositoryDirectory\repository.db3"
$LogFolder = "$rc360Directory\Logs"
$LogFile = "$LogFolder\provisioning.log"
$SortTranslatorDirectory = "$([Environment]::GetFolderPath('ProgramFiles'))\Raincode\SortTranslator"

$DocsDirectory = "$rc360Directory\Docs"
$GitDirectory = "$rc360Directory\Repositories"
$DemosDirectory = "$GitDirectory\Demos"
$IMSqlDemosDirectory = "$GitDirectory\Demos\IMSql"
$PSModulesDirectory = "$rc360Directory\PSModules"
$InsightDirectory = "$rc360Directory\RaincodeInsight"

$Raincode360ManualPptUri = "https://phidani-my.sharepoint.com/:p:/g/personal/markus_raincode_com/EUrB7lYIkZFJt7kMKGMGFEwBiROheWUfKyouftJtnJa-OA"
$qixps = "$env:RCBIN\ProcessingServerRunner.exe"
$qixts = "$env:RCBIN\TerminalServerRunner.exe"

$rcsubmit = "$env:RCBATCHDIR\Submit.exe"

$DatabaseServerString = "$SqlServerIp\SQLEXPRESS"

# Robustness settings
$maxRetries = 4
$retryWait = 10

function Get-SqlConnectionString {
    [CmdletBinding()]
    Param(
        [Parameter()]
        [string]
        $InitialCatalog,

        [Parameter()]
        [switch]
        $MultipleActiveResultSets,

        [Parameter()]
        [switch]
        $Encrypt,

        [Parameter()]
        [switch]
        $PersistSecurityInfo
    )

    Process {
        $cs = "Server=$DatabaseServerString;"
        $cs += "User ID=$SqlServerLogin;"
        $cs += "Password=$SqlServerPassword;"
        $cs += "TrustServerCertificate=True;"
        if ($PSBoundParameters.ContainsKey('InitialCatalog')) {
            $cs += "Initial Catalog=$InitialCatalog;"
        }
        if ($PSBoundParameters.ContainsKey('MultipleActiveResultSets')) {
            $cs += "MultipleActiveResultSets=$MultipleActiveResultSets;"
        }
        $cs
    }
}


Import-Module -Name "./provision.psm1"

try {
    Install-Directories

    # Allow unrestricted execution of scripts
    Set-ExecutionPolicy -ExecutionPolicy "Unrestricted" -Scope "LocalMachine" -Force

    #$ArtifactsLocation = $ArtifactsLocation.TrimEnd('/')

    Write-Log "Starting provisioning..."

    foreach ($param in $PSBoundParameters.GetEnumerator()) {
        Write-Log ("Received parameter {0}: {1}" -f $param.Key, $param.Value)
    }

    Import-Module -Name "./downloader.psm1"
    Add-PSModulePath -Path $PSModulesDirectory
    Save-Artifacts -TargetDirectory $CacheDirectory
    Install-PowerShellModules -PSModulesDirectory $PSModulesDirectory -TargetDirectory $CacheDirectory

    # Fetch Raincode 360 repository
    Install-GitRepository

    # We do this early so the admin account is likely to be ready on first login.
    Install-Wallpaper
    Set-ShowFileExtensions-AllUsers

    Install-Manual
    Install-EULA
    Install-ThirdPartyLicenses

    # Login
    #Connect-Azure

    # Common environment variable setup
    Initialize-Rc360Environment

    # Tooling installation
    Set-VisualStudioEnvironment
    Initialize-NuGet
    Install-RaincodeQIX
    Install-RaincodeJCL
    Install-VsamSqlDemos
    Install-SortTranslator
    Install-3270
    Install-3270Config
    Install-SqliteOdbcDsn

    # Databases
    Initialize-DemoDatabases

    # Local demos
    Initialize-RaincodeDemos
    Initialize-IMSqlDemos

    # Cloud native demos
    #Install-DaprDemo

    # Logout
    #Disconnect-Azure

    Write-Log "Provisioning completed."
}
catch {
    Write-LogError "Provisioning failed with the following error:"
    Write-LogError "Exception type: $($_.Exception.GetType().FullName)"
    Write-LogError $_
    Write-LogError $_.ScriptStackTrace
    throw
}
