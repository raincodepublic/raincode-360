#!/usr/bin/env pwsh
[CmdletBinding()]
param (
    [Parameter(Mandatory, Position = 0)]
    [string]
    $TargetDirectory
)

if (!(Test-Path -Path $TargetDirectory -PathType "Container")) {
    New-Item -Path $TargetDirectory -ItemType "Directory"
}

Import-Module -Name "./downloader.psm1"
Set-Alias -Name Write-Log -Value Write-Host
Set-Alias -Name Write-LogError -Value Write-Host
Save-Artifacts -TargetDirectory $TargetDirectory

$psModulePath = Join-Path -Path $TargetDirectory -ChildPath "psModules"
if (!(Test-Path -Path $psModulePath -PathType "Container")) {
    New-Item -Path $psModulePath -ItemType "Directory"
}
Save-PowerShellModules -TargetDirectory $psModulePath
