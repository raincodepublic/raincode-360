
function Write-Log {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Message
    )

    Process {
        $Timestamp = (Get-Date).ToString("yyyy-MM-dd HH:mm:ss zzz")
        Add-Content -Path $LogFile -Value "[$Timestamp] [INFO] $Message"
    }
}

function Write-LogError {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Message
    )

    Process {
        $Timestamp = (Get-Date).ToString("yyyy-MM-dd HH:mm:ss zzz")
        Add-Content -Path $LogFile -Value "[$Timestamp] [ERROR] $Message"
    }
}

function Get-ProcessLogNames {
    [CmdletBinding()]
    [OutputType("string")]
    Param(
        [Parameter(Mandatory)]
        [string]
        $ProgramName
    )

    Process {
        $Timestamp = Get-Date -Format "FileDateTimeUniversal"
        $StdOut = "$LogFolder\$ProgramName.$Timestamp.out.log"
        $StdErr = "$LogFolder\$ProgramName.$Timestamp.err.log"
        $Result = New-Object -TypeName "PSObject"
        $Result | Add-Member -MemberType "NoteProperty" -Name "StdOut" -Value $StdOut
        $Result | Add-Member -MemberType "NoteProperty" -Name "StdErr" -Value $StdErr
        $Result
    }
}

function Get-SystemEnvironmentVariable {
    [CmdletBinding()]
    [OutputType("string")]
    Param(
        [Parameter(Mandatory)]
        [string]
        $Name
    )

    Process {
        [Environment]::GetEnvironmentVariable($Name, "Machine")
    }
}

function Set-SystemEnvironmentVariable {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory)]
        [string]
        $Name,

        [Parameter(Mandatory)]
        [string]
        $Value,

        [Parameter()]
        [Switch]
        $NoProcess
    )

    Process {
        [Environment]::SetEnvironmentVariable($Name, $Value, "Machine")
        if (!$NoProcess) {
            [Environment]::SetEnvironmentVariable($Name, $Value, "Process")
        }
    }
}

function Add-PathLike {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory)]
        [string]
        $Key,

        [Parameter(Mandatory)]
        [string]
        $Value
    )

    Process {
        $CurrentSystemValue = Get-SystemEnvironmentVariable -Name $Key
        if (!($CurrentSystemValue -contains $Value)) {
            Write-Log "Adding $Value to $Key (machine)..."
            Set-SystemEnvironmentVariable -Name $Key -Value "$CurrentSystemValue;$Value" -NoProcess
        }
        $CurrentProcessValue = (Get-Item -Path "env:$Key").Value
        if (!($CurrentProcessValue -contains $Value)) {
            Write-Log "Adding $Value to $Key (process)..."
            Set-Item -Path "env:$Key" -Value "$CurrentProcessValue;$Value"
        }
    }
}

function Add-Path {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory)]
        [string]
        $Path
    )

    Process {
        Add-PathLike -Key "PATH" -Value $Path
    }
}

function Add-PSModulePath {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory)]
        [string]
        $Path
    )

    Process {
        Add-PathLike -Key "PSModulePath" -Value $Path
    }
}

function Add-Directory {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory)]
        [string]
        $Directory
    )

    Process {
        if (!(Test-Path $Directory -PathType "Container")) {
            New-Item -Path $Directory -ItemType "Directory"
        }
    }
}

function Initialize-Rc360Environment {
    Set-SystemEnvironmentVariable -Name "RC360_CONNSTRING" -Value (Get-SqlConnectionString)
    Set-SystemEnvironmentVariable -Name "IMSQL_DEMO_ROOT" -Value $IMSqlDemosDirectory
    Set-SystemEnvironmentVariable -Name "DB_SERVER" -Value $DatabaseServerString
    Set-SystemEnvironmentVariable -Name "DB_USER" -Value $SqlServerLogin
    Set-SystemEnvironmentVariable -Name "DB_PASSWORD" -Value $SqlServerPassword
    # Set environment variables for Raincode Console - IMSQL-ConfigDB
    Set-SystemEnvironmentVariable -Name "IMSQL_DEV_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "IMSQL_DealerOnline_Config" )
    Set-SystemEnvironmentVariable -Name "IMSQL_QA_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "IMSQL_DealerOnline_Config" )
    Set-SystemEnvironmentVariable -Name "IMSQL_PROD_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "IMSQL_DealerOnline_Config" )
    # Set environment variables for Raincode Console - IMSQL-ApplicationDB
    Set-SystemEnvironmentVariable -Name "IMSQL_APPL_DEV_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "IMSQL_DealerOnline" )
    Set-SystemEnvironmentVariable -Name "IMSQL_APPL_QA_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "IMSQL_DealerOnline" )
    Set-SystemEnvironmentVariable -Name "IMSQL_APPL_PROD_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "IMSQL_DealerOnline" )
    # Set environment variables for Raincode Console - QIX-ConfigDB
    Set-SystemEnvironmentVariable -Name "QIX_DEV_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "RaincodeQIXMS" )
    Set-SystemEnvironmentVariable -Name "QIX_QA_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "RaincodeQIXMS" )
    Set-SystemEnvironmentVariable -Name "QIX_PROD_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "RaincodeQIXMS" )
    # Set environment variables for Raincode Console - VSAMSQL-ConfigDB
    Set-SystemEnvironmentVariable -Name "VSAMSQL_DEV_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "VsamSql" )
    Set-SystemEnvironmentVariable -Name "VSAMSQL_QA_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "VsamSql" )
    Set-SystemEnvironmentVariable -Name "VSAMSQL_PROD_CONNSTRING" -Value (Get-SqlConnectionString -InitialCatalog "VsamSql" )
}

function Set-VisualStudioEnvironment {
    Install-Module VSSetup -Scope CurrentUser
    $VisualStudioInstance = Get-VSSetupInstance | Select-VSSetupInstance -Latest
    Import-Module "$($VisualStudioInstance.InstallationPath)\Common7\Tools\Microsoft.VisualStudio.DevShell.dll"
    Write-Log "Setting up Visual Studio developer shell environment..."
    Enter-VsDevShell $VisualStudioInstance.InstanceId
}

function Install-RaincodeQIX {
    Initialize-QIXDatabase
    Set-FirewallRule -Name "Raincode QIX PS" -Program $qixps
    Set-FirewallRule -Name "Raincode QIX TS" -Program $qixts
}

function Set-FirewallRule {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        $Name,

        [Parameter(Mandatory)]
        $Program
    )

    Process {
        Write-Log "Creating firewall rule for $Program..."
        try {
            Get-NetFirewallRule -DisplayName $Name
        }
        catch {
            New-NetFirewallRule -DisplayName $Name -Program $Program -Profile "Any" -Action "Allow" -Protocol "TCP"
        }
    }
}

function Install-RaincodeJCL {
    $rcdbDirectory = "$([Environment]::GetFolderPath('CommonApplicationData'))\Raincode\Batch"
    Add-Directory $rcdbDirectory
    Out-File -InputObject "VSAMSQL,SqlServer,$(Get-SqlConnectionString -InitialCatalog "VsamSql")" -FilePath "$rcdbDirectory\RcDbConnections.csv" -Encoding "utf8"
    Out-File -InputObject "IMSDB,SqlServer,$(Get-SqlConnectionString -InitialCatalog "IMSQL_DealerOnline")" -FilePath "$rcdbDirectory\RcDbConnections.csv" -Encoding "utf8"
	Out-File -InputObject ".*,SqlServer,$(Get-SqlConnectionString -InitialCatalog "BankDemo")" -FilePath "$rcdbDirectory\RcDbConnections.csv" -Encoding "utf8" -Append
    Set-FirewallRule -Name "Raincode JCL SUBMIT" -Program $rcsubmit
    Set-DesktopShortcut -Name "Raincode JCL data" -TargetPath $rcdbDirectory
    Add-Path -Path $env:RCBIN
}

function Install-VsamSqlDemos {
    # Initialize RcDbConnections.csv
    $vsamSqlDemoDirectory = Join-Path -Path $DemosDirectory -ChildPath "Batch - VsamSql"
    Out-File -InputObject "VSAMSQL,SqlServer,$(Get-SqlConnectionString -InitialCatalog "VsamSql")" -FilePath "$vsamSqlDemoDirectory\catalog\RcDbConnections.csv" -Encoding "utf8"
    Out-File -InputObject ".*,SqlServer,$(Get-SqlConnectionString -InitialCatalog "BankDemo")" -FilePath "$vsamSqlDemoDirectory\catalog\RcDbConnections.csv" -Encoding "utf8" -Append

    # Initialize catalog
    $jcl = Join-Path -Path $vsamSqlDemoDirectory -ChildPath "JCL/JCL001.jcl"
    & $rcsubmit -ScanOnly "-File=$jcl"

    New-Database -ConnectionString $(Get-SqlConnectionString) -DatabaseName "VsamSql"
    $connectionString = Get-SqlConnectionString -InitialCatalog "VsamSql"
    Invoke-RobustSqlcmd -ConnectionString $connectionString -InputFile (Join-Path -Path $env:RCDIR -ChildPath "bin/sql/Functions.sql")
    Invoke-RobustSqlcmd -ConnectionString $connectionString -InputFile (Join-Path -Path $env:RCDIR -ChildPath "bin/sql/EbcdicFuncs.sql")

    # Setup VsamSql entries in Catalog
    $catalogPath = Join-Path -Path $vsamSqlDemoDirectory -ChildPath "catalog/Raincode.Catalog.xml"

    $copybookDirectory = Join-Path -Path $vsamSqlDemoDirectory -ChildPath "COBOL"

    Push-Location $vsamSqlDemoDirectory

    # Create data tables
    $logNames = Get-ProcessLogNames -ProgramName "VsamSql.DbGenerator"
    $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "VsamSql.DbGenerator" -ArgumentList @("-OutputFile=`"01_create_vsamsql_tables.sql`"", "-CatalogConfiguration=`"$catalogPath`"") -RedirectStandardOutput $logNames.StdOut -RedirectStandardError $logNames.StdErr
    if ($Process.ExitCode -ne 0) {
        throw "VsamSql.DbGenerator failed with code $($Process.ExitCode)."
    }
    Write-Log "VsamSql.DbGenerator succeeded."
    Invoke-RobustSqlcmd -ConnectionString $connectionString -InputFile "01_create_vsamsql_tables.sql"

    # Create views and triggers
    $logNames = Get-ProcessLogNames -ProgramName "CopybookViewGenerator.ACCOUNT"
    $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "CopybookViewGenerator" -ArgumentList @("-conn=`"$connectionString`"", "-db=VsamSql", "-schema=dbo", "-table=REC_ACCOUNT", "-output=`"02_account_view.sql`"", "-cpy=`"$(Join-Path -Path $copybookDirectory -ChildPath "ACCOUNT.cpy")`"") -RedirectStandardOutput $logNames.StdOut -RedirectStandardError $logNames.StdErr
    if ($Process.ExitCode -ne 0) {
        throw "CopybookViewGenerator failed with code $($Process.ExitCode)."
    }
    Write-Log "CopybookViewGenerator succeeded."
    Invoke-RobustSqlcmd -ConnectionString $connectionString -InputFile "02_account_view.sql"

    $logNames = Get-ProcessLogNames -ProgramName "CopybookViewGenerator.CUSTOMER"
    $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "CopybookViewGenerator" -ArgumentList @("-conn=`"$connectionString`"", "-db=VsamSql", "-schema=dbo", "-table=REC_CUSTOMER", "-output=`"03_customer_view.sql`"", "-cpy=`"$(Join-Path -Path $copybookDirectory -ChildPath "CUSTOMER.cpy")`"") -RedirectStandardOutput $logNames.StdOut -RedirectStandardError $logNames.StdErr
    if ($Process.ExitCode -ne 0) {
        throw "CopybookViewGenerator.CUSTOMER failed with code $($Process.ExitCode)."
    }
    Write-Log "CopybookViewGenerator succeeded."
    Invoke-RobustSqlcmd -ConnectionString $connectionString -InputFile "03_customer_view.sql"

    Pop-Location
}

function Install-zBridgeDemos {
    # Initialize RcDbConnections.csv for zBridge/zBridgeFile
    $zBridgeFileDemoDirectory = Join-Path -Path $DemosDirectory -ChildPath "zBridge/zBridgeFile"
    Out-File -InputObject "VSAMSQL,SqlServer,$(Get-SqlConnectionString -InitialCatalog "VsamSql")" -FilePath "$zBridgeFileDemoDirectory\catalog\RcDbConnections.csv" -Encoding "utf8"
    Out-File -InputObject "*,SqlServer,$(Get-SqlConnectionString -InitialCatalog "BankDemo")" -FilePath "$zBridgeFileDemoDirectory\catalog\RcDbConnections.csv" -Encoding "utf8" -Append
}

function New-FileTemplate {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        $TableName,

        [Parameter(Mandatory)]
        $FilePattern,

        [Parameter(Mandatory)]
        [int] $RecordSize,

        [Parameter(Mandatory)]
        [int] $KeyStart,

        [Parameter(Mandatory)]
        [int] $KeyLength
    )

    Process {
        $fileTemplate = $doc.CreateElement("fileTemplate")
        $fileTemplate.SetAttribute("tableName", $TableName)
        $fileTemplate.SetAttribute("connection", "VSAMSQL")

        $pattern = $doc.CreateElement("pattern")
        $dsn = $doc.CreateElement("DSN")
        $dsn.InnerText = $filePattern
        $null = $pattern.AppendChild($dsn)
        $params = $doc.CreateElement("parameters")
        $params.SetAttribute("length", $RecordSize)
        $key = $doc.CreateElement("key")
        $key.SetAttribute("start", $KeyStart)
        $key.SetAttribute("length", $KeyLength)
        $null = $params.AppendChild($key)

        $null = $fileTemplate.AppendChild($pattern)
        $null = $fileTemplate.AppendChild($params)
        $fileTemplate
    }
}

function Install-3270 {
    $Artifact = Get-ArtifactInfo "Wc3270"
    $LocalPath = "$CacheDirectory\$($Artifact["Filename"])"
    Write-Log "Installing $($Artifact["Name"])..."
    # Inno Setup
    $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath $LocalPath -ArgumentList "/VERYSILENT", "/NORESTART", "/NOCANCEL", "/SP-", "/CLOSEAPPLICATIONS", "/RESTARTAPPLICATIONS"
    if ($Process.ExitCode -ne 0) {
        throw "$($Artifact["Name"]) installation failed with code $($Process.ExitCode)."
    }
    Write-Log "$($Artifact["Name"]) installation succeeded."
    Remove-Item "$([Environment]::GetFolderPath('CommonDesktopDirectory'))\wc3270.lnk"
    Remove-Item "$([Environment]::GetFolderPath('CommonDesktopDirectory'))\Session Wizard.lnk"
}

function Install-SqliteOdbcDsn {
    $DsnName = "Raincode 360 Repository"
    Write-Log "Creating system DSN for Raincode 360 Repository..."
    try {
        $null = Get-OdbcDsn -Name $DsnName
        Write-Log "System DSN for Raincode 360 Repository already created. Skipping."
    }
    catch [Microsoft.Management.Infrastructure.CimException] {
        Add-OdbcDsn -Name $DsnName  -DsnType System -Platform "64-bit" -DriverName "SQLite3 ODBC Driver" -SetPropertyValue "Database=$GlobalCompilationRepositoryPath"
        Write-Log "Created system DSN for Raincode 360 Repository."
    }
    Set-SystemEnvironmentVariable -Name "RC_COMPILATION_REPOSITORY" -Value $GlobalCompilationRepositoryPath
    $env:RC_COMPILATION_REPOSITORY = $GlobalCompilationRepositoryPath
}

function Initialize-NuGet {
	Write-Nuget -Location "Before Initialize-NuGet"
    # Reset nuget.config to fix missing package source.
    $NugetConfig = Join-Path -Path $([Environment]::GetFolderPath('ApplicationData')) -ChildPath "NuGet/nuget.config"
    if (Test-Path -Path $NugetConfig -PathType "Leaf") {
        Remove-Item -Path $NugetConfig
    }
	#Creates nuget.config file with public nuget url
    dotnet nuget list source
    Write-Nuget -Location "After Initialize-NuGet"
}

function Write-Nuget {
    # Display Nuget information when nuget.org key is not found
   [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Location
    )

    Process {
		$nugetOrgKeyPresent=$false
    	$NugetConfig = Join-Path -Path $([Environment]::GetFolderPath('ApplicationData')) -ChildPath "NuGet/nuget.config"
    	if (Test-Path -Path $NugetConfig -PathType "Leaf") {
            foreach($line in Get-Content -Path $NugetConfig){
               if($line -match "https://api.nuget.org/v3/index.json"){
				  $nugetOrgKeyPresent=$true
			   }
            }
    	}else{
			Write-Log "Write-Nuget function called from: $Location"
	        Write-Log "NugetConfig file not found at $NugetConfig"
    	}
		if(!($nugetOrgKeyPresent)){
		   Write-Log "Write-Nuget function called from: $Location"
		   Write-Log "NugetConfig file does not have api.nuget.org key"
		}
   }
}

function Initialize-IMSqlDemos {
	Write-Nuget -Location "Before Initialize-IMSqlDemos"
    Push-Location $IMSqlDemosDirectory
    Write-Log "Building IMSql demos..."
    $logNames = Get-ProcessLogNames -ProgramName "make"
    $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "make" -ArgumentList "build_all" -RedirectStandardOutput $logNames.StdOut -RedirectStandardError $logNames.StdErr
    if ($Process.ExitCode -ne 0) {
        throw " IMSql demos build failed with code $($Process.ExitCode)."
    }
    Write-Log "IMSql demos build succeeded."
    Pop-Location
	Write-Nuget -Location "After Initialize-IMSqlDemos"
}

function Install-3270Config {
    $wc3270Directory = "$([Environment]::GetFolderPath('ProgramFiles'))\wc3270"

    Write-Log "Setting up Raincode Bank wc3270 session shortcut..."
    Set-DesktopShortcut -Name "Raincode Bank (3270)" -TargetPath "$wc3270Directory\wc3270.exe" -Arguments "+S -model 4 -title `"Raincode Bank`" 127.0.0.1" -WorkingDirectory $wc3270Directory -Description "Raincode Bank wc3270 session" -IconLocation "$wc3270Directory\wc3270.exe,0"
    Write-Log "Raincode Bank wc3270 session shortcut created on common Desktop."

    Write-Log "Setting up IMSql DealerOnline wc3270 session shortcut..."
    Set-DesktopShortcut -Name "IMSql DealerOnline (3270)" -TargetPath "$wc3270Directory\wc3270.exe" -Arguments "+S -model 4 -title `"IMSql DealerOnline`" 127.0.0.1:32023" -WorkingDirectory $wc3270Directory -Description "IMSql DealerOnline wc3270 session" -IconLocation "$wc3270Directory\wc3270.exe,0"
    Write-Log "IMSql DealerOnline wc3270 session shortcut created on common Desktop."
}

function Set-DesktopShortcut {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        $Name,

        [Parameter(Mandatory)]
        [string]
        $TargetPath,

        [Parameter()]
        [string]
        $WorkingDirectory,

        [Parameter()]
        [string]
        $Arguments,

        [Parameter()]
        [string]
        $Description,

        [Parameter()]
        [string]
        $IconLocation,

        [Parameter()]
        [int]
        $WindowStyle
    )

    Process {
        $DesktopDirectory = [Environment]::GetFolderPath("CommonDesktopDirectory")
        $wsh = New-Object -ComObject "WScript.Shell"
        $shortcut = $wsh.CreateShortcut("$DesktopDirectory\$Name.lnk")

        $shortcut.TargetPath = $TargetPath

        if ($PSBoundParameters.ContainsKey('WorkingDirectory')) {
            $shortcut.WorkingDirectory = $WorkingDirectory
        }
        else {
            $shortcut.WorkingDirectory = Split-Path -Path $TargetPath -Parent
        }
        if ($PSBoundParameters.ContainsKey('Arguments')) {
            $shortcut.Arguments = $Arguments
        }
        if ($PSBoundParameters.ContainsKey('Description')) {
            $shortcut.Description = $Description
        }
        if ($PSBoundParameters.ContainsKey('WindowStyle')) {
            $shortcut.WindowStyle = $WindowStyle
        }
        if ($PSBoundParameters.ContainsKey('IconLocation')) {
            $shortcut.IconLocation = $IconLocation
        }

        $shortcut.Save()
    }
}

function Set-DesktopBatShortcut {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        $Name,

        [Parameter(Mandatory)]
        [string]
        $TargetPath,

        [Parameter(Mandatory)]
        [string]
        $WorkingDirectory
    )

    Process {
        Set-DesktopShortcut -Name $Name -TargetPath $TargetPath -WorkingDirectory $WorkingDirectory
    }
}

function Set-DesktopPs1Shortcut {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        $Name,

        [Parameter(Mandatory)]
        [string]
        $TargetPath,

        [Parameter()]
        [string]
        $WorkingDirectory
    )

    Process {
        if ($PSBoundParameters.ContainsKey('WorkingDirectory')) {
            Set-DesktopShortcut -Name $Name -TargetPath "pwsh.exe" -Argument "-File `"$TargetPath`"" -WorkingDirectory $WorkingDirectory
        }
        else {
            Set-DesktopShortcut -Name $Name -TargetPath "pwsh.exe" -Argument "-File `"$TargetPath`"" -WorkingDirectory (Split-Path -Path $TargetPath -Parent)
        }
    }
}

function Initialize-DemoDatabases {
    Get-ChildItem -Directory "$DemosDirectory\Database" | ForEach-Object {
        $catalog = $_.Name
        $catalogScriptPath = $_.FullName
        Write-Log "Creating $catalog database..."
        $retries = 0
        while ($true) {
            try {
                New-Database -ConnectionString $(Get-SqlConnectionString) -DatabaseName $catalog
                break
            }
            catch {
                if ($retries -lt $maxRetries) {
                    Write-Log "Database creation failed. $($maxRetries - $retries) retries left..."
                    $retries += 1
                    Start-Sleep -Seconds $retryWait
                }
                else {
                    Write-LogError "Database creation failed too many times. Giving up!"
                    throw
                }
            }
        }

        Write-Log "Initializing $catalog database..."
        $connectionString = Get-SqlConnectionString -InitialCatalog $catalog

        Write-Log "Running DB2 stored procedure script on $catalog database..."
        Invoke-RobustSqlcmd -ConnectionString $connectionString -InputFile "$env:RCDIR\sql\StoredProcedures_Raincode.sql"
        Write-Log "DB2 stored procedure script was executed successfully."

        Get-ChildItem -Path $catalogScriptPath -Filter "*.sql" | ForEach-Object {
            Write-Log "Running $($_.FullName) on $catalog database..."
            Invoke-RobustSqlcmd -ConnectionString $connectionString -InputFile $_.FullName
            Write-Log "$($_.FullName) was executed successfully."
        }
    }
}

function Initialize-QIXDatabase {
    $setupMarker = "$MarkerDirectory\qixdb.done"
    if (!(Test-Path -Path $setupMarker)) {
        $catalog = "RaincodeQIXMS"
        Write-Log "Creating database $catalog..."
        New-Database -ConnectionString (Get-SqlConnectionString) -DatabaseName $catalog
        $connectionString = Get-SqlConnectionString -InitialCatalog $catalog -MultipleActiveResultSets
        Write-Log "Initializing $catalog database..."
        $setupScript = "$env:RCDIR\sql\rcqix_db.sql"
        Write-Log "Running $setupScript on $catalog database..."
        Invoke-RobustSqlcmd -ConnectionString $connectionString -InputFile $setupScript
        Write-Log "$setupScript was executed successfully."
        Out-File -FilePath $setupMarker
    }
    else {
        Write-Log "QIX database already setup. Skipping initialization."
    }
}

function Install-GitRepository {
    $item = "Raincode 360 demo sources"
    if (!(Test-Path -Path $DemosDirectory)) {
        Write-Log "Fetching $item..."
        if ($Marketplace) {
            Invoke-RobustWebRequest -Uri "$ArtifactsLocation/installers/git_repo.zip$ArtifactsLocationSasToken" -OutFile "$CacheDirectory/git_repo.zip"
            Expand-Archive -Path "$CacheDirectory/git_repo.zip" -DestinationPath $DemosDirectory -Force
        }
        else {
            $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "git" -ArgumentList "clone", "--branch", $GitBranch, "https://gitlab.phidani.be/raincodepublic/raincode-360.git", "$DemosDirectory"
            if ($Process.ExitCode -ne 0) {
                throw "$item download failed with code $($Process.ExitCode)."
            }
        }
        Write-Log "$item download succeeded."
    }
    else {
        Write-Log "$item already downloaded. Skipping."
    }
}

function Test-BuildOptOut {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Path,

        [Parameter()]
        [Switch]
        $Release
    )

    Process {
        $Mode = if ($Release) { "Release" } else { "Debug" }
        $OptOutName = "$($Mode.ToLower()).optout"
        $OptOutPath = Join-Path -Path (Split-Path -Path $Path -Parent) -ChildPath $OptOutName
        Test-Path -Path $OptOutPath
    }
}

function Initialize-RaincodeDemos {
    # FIXME: ScanOnly fails on JCL procedures, needs to be fixed in Submit.
    # Prepare JCLs for repository
    # Get-ChildItem -Directory -Recurse -Path "$DemosDirectory" | Get-ChildItem -Filter "*.jcl" | ForEach-Object {
    #     Write-Log "Scanning JCL: $($_.FullName)..."
    #     $logNames = Get-ProcessLogNames -ProgramName "submit"
    #     $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "submit.exe" -ArgumentList "-DBDriver=SQLite", "-DBConnectString=`"$GlobalCompilationRepositoryPath`"", "-ScanOnly", "-File=`"$($_.FullName)`"" -RedirectStandardOutput $logNames.StdOut -RedirectStandardError $logNames.StdErr
    #     if ($Process.ExitCode -ne 0) {
    #         throw "Scan of JCL $($_.FullName) failed with code $($Process.ExitCode)."
    #     }
    #     Write-Log "JCL $($_.FullName) scanned successfully."
    # }
    # Compile solutions
    Get-ChildItem -Directory -Recurse -Path "$DemosDirectory" | Get-ChildItem -Filter "*.sln" | ForEach-Object {
        if (!(Test-BuildOptOut -Path $_.FullName)) {
            Initialize-Solution -Path $_.FullName
        }
        else {
            Write-Log "Not building $($_.FullName) in Debug configuration due to opt-out."
        }

        if (!(Test-BuildOptOut -Path $_.FullName -Release)) {
            Initialize-Solution -Path $_.FullName -Release
        }
        else {
            Write-Log "Not building $($_.FullName) in Release configuration due to opt-out."
        }
    }
    Install-RaincodeDemosDesktopShortcuts
}

function Initialize-Solution {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Path,

        [Parameter()]
        [Switch]
        $Release
    )

    Process {
		Write-Nuget -Location $Path
        $Mode = if ($Release) { "Release" } else { "Debug" }
        Write-Log "Building solution $Path in $Mode mode..."
        $logNames = Get-ProcessLogNames -ProgramName "msbuild"
        Write-Log "Writing stdout to $($logNames.StdOut)."
        Write-Log "Writing stderr to $($logNames.StdErr)."
        $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "MSBuild.exe" -ArgumentList "/restore", "/nodeReuse:false", "/p:Configuration=$Mode", "/p:UseSharedCompilation=false", "`"$Path`"" -RedirectStandardOutput $logNames.StdOut -RedirectStandardError $logNames.StdErr
        if ($Process.ExitCode -ne 0) {
            throw "Solution $Path build in $Mode mode failed with code $($Process.ExitCode)."
        }
        Write-Log "Solution $Path built successfully in $Mode mode."
    }
}

function Install-RaincodeDemosDesktopShortcuts {
    $dir = Join-Path -Path $DemosDirectory -ChildPath "Qix - Emulator - Bank Application"
    Set-DesktopPs1Shortcut -Name "Start CICS demo" -TargetPath "$dir\run.ps1"
	Set-DesktopPs1Shortcut -Name "Rehost CICS demo on AKS" -TargetPath "$dir\Rehost on AKS\RehostBankDemoOnAKS.ps1"
    Set-DesktopShortcut -Name "Demos" -TargetPath $DemosDirectory

    $dir = Join-Path -Path $IMSqlDemosDirectory -ChildPath "DealerOnline"
    Set-DesktopShortcut -Name "IMSql Demos" -TargetPath $IMSqlDemosDirectory
    Set-DesktopPs1Shortcut -Name "Start IMSql 3270 demo" -TargetPath "$dir\run360.ps1"
}

function Install-DaprDemo {
    Write-Log "Creating desktop shortcut to Dapr demo..."
    Set-DesktopLink -Uri "https://$IngressFqdn/" -Name "Cloud native demo"
}

function Connect-Azure {
    Write-Log "Logging into Azure using VM identity..."
    Connect-AzAccount -Identity
}

function Disconnect-Azure {
    Write-Log "Logging out of Azure..."
    Logout-AzAccount
}

function New-Database {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $ConnectionString,

        [Parameter(Mandatory)]
        [string]
        $DatabaseName,

        [Parameter()]
        [string]
        $CollationName
    )

    Process {
        #just in case 
        try {
            $Query = "DROP DATABASE [$DatabaseName]"
            Invoke-Sqlcmd -AbortOnError -ConnectionString $ConnectionString -Query $Query            
        }
        catch {
            #ignore errors 
        }
        $Query = "CREATE DATABASE [$DatabaseName]"
        if ($CollationName) {
            $Query += " COLLATE $CollationName"
        }
        Invoke-RobustSqlcmd -ConnectionString $ConnectionString -Query $Query
    }
}

function Invoke-RobustSqlcmd {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory, ParameterSetName = 'File')]
        [Parameter(Mandatory, ParameterSetName = 'Query')]
        [string]
        $ConnectionString,

        [Parameter(Mandatory, ParameterSetName = 'File')]
        [string]
        $InputFile,

        [Parameter(Mandatory, ParameterSetName = 'Query')]
        [string]
        $Query
    )

    Process {
        $retries = 0
        while ($true) {
            try {
                if ($PSBoundParameters.ContainsKey('InputFile')) {
                    Invoke-Sqlcmd -AbortOnError -ConnectionString $ConnectionString -InputFile $InputFile
                }
                else {
                    Invoke-Sqlcmd -AbortOnError -ConnectionString $ConnectionString -Query $Query
                }
                break
            }
            catch [System.Data.SqlClient.SqlException], [System.InvalidOperationException] {
                if ($retries -lt $maxRetries) {
                    Write-Log "SQL command failed. $($maxRetries - $retries) retries left..."
                    $retries += 1
                    Start-Sleep -Seconds $retryWait
                }
                else {
                    Write-LogError "SQL command failed too many times. Giving up!"
                    throw "SQL command failed too many times."
                }
            }
        }
    }
}

function Install-Wallpaper {
    $Artifact = Get-ArtifactInfo "Raincode360Wallpaper"
    $WallpaperPath = "$CacheDirectory\$($Artifact["Filename"])"

    try {
        Write-Log "Creating HKEY_USERS PSDrive..."
        New-PSDrive -PSProvider "registry" -Root "HKEY_USERS" -Name "HKU"
    }
    catch [System.Management.Automation.SessionStateException] {
        Write-Log "HKEY_USERS PSDrive creation already exists. Skipping."
    }

    # Configure default profile
    Set-WallpaperForStore -StorePath "C:\Users\Default\NTUSER.DAT" -WallpaperPath $WallpaperPath

    # Set for admin user if profile already exists
    $adminStore = "C:\Users\$Username\NTUSER.DAT"
    if (Test-Path -Path $adminStore -PathType "Leaf") {
        try {
            Set-WallpaperForStore -StorePath $adminStore -WallpaperPath $WallpaperPath
        }
        catch [System.Management.Automation.RuntimeException] {
            Write-Log "Unable to set wallpaper for admin. Continuing."
        }
    }
}

function Set-WallpaperForStore {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory)]
        [string]
        $StorePath,

        [Parameter(Mandatory)]
        [string]
        $WallpaperPath
    )

    Process {
        Write-Log "Configuring wallpaper on registry store $StorePath..."
        $LoadLocation = "provisioning"
        $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "reg" -ArgumentList "load", "HKU\$LoadLocation", $StorePath
        if ($Process.ExitCode -ne 0) {
            throw "Could not load registry store (code $($Process.ExitCode))."
        }

        try {
            $key = "HKU:\$LoadLocation\Software\Microsoft\Windows\CurrentVersion\Policies\System"
            if (!(Test-Path $key)) {
                Write-Log "Creating registry entry $key..."
                $item = New-Item -Type "Directory" -Path $key -Force
                # Close pending handle, or reg unload will fail.
                # See: https://stackoverflow.com/a/29452727
                $item.Handle.Close()
                [gc]::Collect()
            }
            Write-Log "Setting registry key Wallpaper..."
            Set-ItemProperty -Path $key -Name "Wallpaper" -Value $WallpaperPath
            # Style: fill
            Write-Log "Setting registry key WallpaperStyle..."
            Set-ItemProperty -Path $key -Name "WallpaperStyle" -Value "4"
        }
        finally {
            $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "reg" -ArgumentList "unload", "HKU\$LoadLocation"
            if ($Process.ExitCode -ne 0) {
                throw "Could not unload registry store (code $($Process.ExitCode))."
            }
        }
    }
}

function Set-ShowFileExtensions-AllUsers {
    try {
        Write-Log "Creating HKEY_USERS PSDrive..."
        New-PSDrive -PSProvider "registry" -Root "HKEY_USERS" -Name "HKU"
    }
    catch [System.Management.Automation.SessionStateException] {
        Write-Log "HKEY_USERS PSDrive creation already exists. Skipping."
    }

    # Configure default profile
    Set-ShowFileExtensions -StorePath "C:\Users\Default\NTUSER.DAT"

    # Set for admin user if profile already exists
    $adminStore = "C:\Users\$Username\NTUSER.DAT"
    if (Test-Path -Path $adminStore -PathType "Leaf") {
        try {
            Set-ShowFileExtensions -StorePath $adminStore
        }
        catch [System.Management.Automation.RuntimeException] {
            Write-Log "Unable to set wallpaper for admin. Continuing."
        }
    }
}

function Set-ShowFileExtensions {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory)]
        [string]
        $StorePath
    )

    Process {
        Write-Log "Showing file extensions on registry store $StorePath..."
        $LoadLocation = "provisioning"
        $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "reg" -ArgumentList "load", "HKU\$LoadLocation", $StorePath
        if ($Process.ExitCode -ne 0) {
            throw "Could not load registry store (code $($Process.ExitCode))."
        }

        try {
            $key = "HKU:\$LoadLocation\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
            Write-Log "Setting registry key HideFileExt..."
            Set-ItemProperty -Path $key -Name "HideFileExt" -Value 0
        }
        finally {
            $Process = Start-Process -Wait -NoNewWindow -PassThru -FilePath "reg" -ArgumentList "unload", "HKU\$LoadLocation"
            if ($Process.ExitCode -ne 0) {
                throw "Could not unload registry store (code $($Process.ExitCode))."
            }
        }
    }
}

function Set-DesktopLink {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        $Uri,

        [Parameter(Mandatory)]
        [string]
        $Name
    )

    Process {
        $wsh = New-Object -ComObject "WScript.Shell"
        $link = $wsh.CreateShortcut("$([Environment]::GetFolderPath('CommonDesktopDirectory'))\$Name.url")
        $link.TargetPath = $Uri
        $link.Save()
    }
}

function Install-Manual {
    $Artifact = Get-ArtifactInfo "Raincode360Manual"
    $LocalPath = "$CacheDirectory\$($Artifact["Filename"])"
    $TargetPath = "$DocsDirectory\$($Artifact["Filename"])"
    if (Test-Path -Path $TargetPath -PathType "Leaf") {
        Remove-Item $TargetPath
    }
    Write-Log "Installing $($Artifact["Name"])..."
    Copy-Item -Path $LocalPath -Destination $TargetPath
    Write-Log "$($Artifact["Name"]) installation succeeded."

    Write-Log "Creating desktop shortcut to Raincode 360 PDF manual..."
    Set-DesktopShortcut -TargetPath $LocalPath -Name "Raincode 360 Manual (local)"
#    Write-Log "Creating desktop shortcut to Raincode 360 online manual..."
#    Set-DesktopLink -Name "Raincode 360 Manual (online)" -Uri $Raincode360ManualPptUri
}

function Install-EULA {
    $Artifact = Get-ArtifactInfo "Raincode360EULA"
    $LocalPath = "$CacheDirectory\$($Artifact["Filename"])"
    Set-DesktopShortcut -Name "EULA" -TargetPath $LocalPath
}

function Install-ThirdPartyLicenses {
    $Artifact = Get-ArtifactInfo "Raincode360ThirdPartyLicenses"
    $LocalPath = "$CacheDirectory\$($Artifact["Filename"])"
    Set-DesktopShortcut -Name "Third-Party Licenses" -TargetPath $LocalPath
}

function Install-SortTranslator {
    $Artifact = Get-ArtifactInfo "SortTranslator"
    $LocalPath = "$CacheDirectory\$($Artifact["Filename"])"
    Write-Log "Installing $($Artifact["Name"])..."
    Expand-Archive -Path $LocalPath -DestinationPath $SortTranslatorDirectory -Force
    Write-Log "$($Artifact["Name"]) installation succeeded."

    $Artifact = Get-ArtifactInfo "SortTranslatorBat"
    $LocalPath = "$CacheDirectory\$($Artifact["Filename"])"
    Write-Log "Installing $($Artifact["Name"])..."
    Expand-Archive -Path $LocalPath -DestinationPath $env:RCBIN -Force
    Write-Log "$($Artifact["Name"]) installation succeeded."
}

function Install-Directories {
    Add-Directory $LogFolder
    Add-Directory $CacheDirectory
    Add-Directory $MarkerDirectory
    Add-Directory $GitDirectory
    Add-Directory $DocsDirectory
    Add-Directory $CompilationRepositoryDirectory
    Add-Directory $PSModulesDirectory
    Add-Directory $InsightDirectory
}
