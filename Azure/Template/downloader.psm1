$StorageContainer = "https://raincode360.blob.core.windows.net/provisioning"

# Software versions
$Raincode360ManualVersion = "2.9.0"
$SortTranslatorVersion = "1.2.5.0"

# PowerShell modules
$SqlServerPowerShellVersion = "21.1.18256"

$Artifacts = [ordered]@{
    Raincode360Manual             = @{
        Name     = "Raincode 360 manual"
        Uri      = "$StorageContainer/docs/Raincode360_v$Raincode360ManualVersion.pdf"
        Filename = "Raincode_360_Manual.pdf"
    }
    Raincode360ThirdPartyLicenses = @{
        Name = "Raincode 360 third-party licenses"
        Uri  = "$StorageContainer/Raincode360_ThirdPartyLicenses.html"
    }
    SortTranslator                = @{
        Name     = "Syncsort translator"
        Uri      = "$StorageContainer/SortTranslator/SortTranslator_v$SortTranslatorVersion.zip"
        Filename = "SortTranslator.zip"
    }
    SortTranslatorBat             = @{
        Name     = "Syncsort translator batch"
        Uri      = "$StorageContainer/SortTranslator/SortTranslatorBat_v$SortTranslatorVersion.zip"
        Filename = "SortTranslatorBat.zip"
    }
    Raincode360EULA               = @{
        Name = "Raincode 360 EULA"
        Uri  = "https://www.raincode.com/legal/TLA.pdf"
    }
    Wc3270                        = @{
        Name = "wc3270 terminal emulator"
        Uri  = "http://x3270.bgp.nu/download/04.02/wc3270-4.2ga4-setup.exe"
    }
    Raincode360Wallpaper          = @{
        Name = "Raincode 360 wallpaper"
        Uri  = "$StorageContainer/BG-Raincode360.jpg"
    }
}

function Get-UriLeaf {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory)]
        [string]
        $Uri,

        [Parameter()]
        [Switch]
        $FollowRedirect
    )

    Process {
        if ($FollowRedirect) {
            $request = Invoke-WebRequest -Method Head -Uri $Uri -UseBasicParsing
            if ($PSVersionTable.PSVersion.Major -le 5) {
                $Uri = $request.BaseResponse.ResponseUri
            }
            else {
                $Uri = $request.BaseResponse.RequestMessage.RequestUri
            }
        }
        ([Uri]$Uri).Segments[-1]
    }
}

function Invoke-RobustWebRequest {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory)]
        [string]
        $Uri,

        [Parameter(Mandatory)]
        [string]
        $OutFile
    )

    Process {
        $retries = 0
        while ($true) {
            try {
                Invoke-WebRequest -Uri $Uri -OutFile $OutFile -UseBasicParsing
                break
            }
            catch [System.Net.WebException], [System.IO.IOException] {
                if ($retries -lt $maxRetries) {
                    Write-LogError "Download failed. $($maxRetries - $retries) retries left..."
                    $retries += 1
                    Start-Sleep -Seconds $retryWait
                }
                else {
                    Write-LogError "Download failed too many times. Giving up!"
                    throw
                }
            }
        }
    }
}

function Save-Artifact {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        $ProgramName,

        [Parameter(Mandatory)]
        [string]
        $TargetDirectory,

        [Parameter(Mandatory)]
        [string]
        $Uri,

        [Parameter()]
        [Switch]
        $FollowRedirect,

        [Parameter()]
        [string]
        $FileName
    )

    Process {
        if (!($PSBoundParameters.ContainsKey('FileName'))) {
            $FileName = Get-UriLeaf -Uri $Uri -FollowRedirect:$FollowRedirect
        }
        $LocalPath = Join-Path -Path $TargetDirectory -ChildPath $FileName
        if (!(Test-Path -Path $LocalPath -PathType "Leaf")) {
            Write-Log "Downloading $ProgramName from $Uri as $LocalPath..."
            Invoke-RobustWebRequest -Uri $Uri -OutFile $LocalPath
        }
        else {
            Write-Log "Skipping download of $ProgramName."
        }
    }
}

function Save-Artifacts {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        $TargetDirectory,

        [Parameter()]
        [Switch]
        $Marketplace,

        [Parameter()]
        [string]
        $ArtifactsLocation,

        [Parameter()]
        [string]
        $ArtifactsLocationSasToken
    )

    Process {
        $Artifacts.Keys | ForEach-Object {
            if (!($Artifacts[$_].Contains('Filename'))) {
                $Artifacts[$_].Add("Filename", (Get-UriLeaf -Uri $Artifacts[$_]["Uri"]))
            }
            if ($Marketplace) {
                $Artifacts[$_]["Uri"] = "$ArtifactsLocation/installers/$($Artifacts[$_]["Filename"])$ArtifactsLocationSasToken"
            }
            Save-Artifact -TargetDirectory $TargetDirectory -ProgramName $Artifacts[$_]["Name"] -Uri $Artifacts[$_]["Uri"] -FileName $Artifacts[$_]["Filename"]
        }
    }
}

function Get-ArtifactInfo {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory, Position = 0)]
        [string]
        $Key
    )

    Process {
        $Artifacts.Keys | ForEach-Object {
            if (!($Artifacts[$_].Contains('Filename'))) {
                $Artifacts[$_].Add("Filename", (Get-UriLeaf -Uri $Artifacts[$_]["Uri"]))
            }
        }
        $Artifacts[$Key]
    }
}

function Save-PowerShellModules {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        $TargetDirectory
    )

    Process {
        Write-Log "Trusting PSGallery repository..."
        Set-PSRepository -Name "PSGallery" -InstallationPolicy "Trusted"

        if (!(Test-Path -Path (Join-Path -Path $TargetDirectory -ChildPath "SqlServer") -PathType "Container")) {
            Write-Log "Fetching SqlServer PowerShell module..."
            Save-Module -Name "SqlServer" -Path $TargetDirectory -Repository "PSGallery" -MaximumVersion $SqlServerPowerShellVersion -Verbose
        }
    }
}

function Install-PowerShellModules {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        $PSModulesDirectory,

        [Parameter(Mandatory)]
        [string]
        $TargetDirectory,

        [Parameter()]
        [Switch]
        $Marketplace,

        [Parameter()]
        [string]
        $ArtifactsLocation,

        [Parameter()]
        [string]
        $ArtifactsLocationSasToken
    )

    Process {
        if ($Marketplace) {
            $Filename = "psModules.zip"
            Save-Artifact -TargetDirectory $TargetDirectory -Uri "$ArtifactsLocation/installers/$Filename$ArtifactsLocationSasToken" -FileName $Filename -ProgramName "PowerShell module archive"
            Write-Log "Extracting PowerShell modules..."
            Expand-Archive -Path (Join-Path -Path $TargetDirectory -ChildPath $Filename) -DestinationPath $PSModulesDirectory -Force
            Write-Log "PowerShell modules extracted into $PSModulesDirectory."
        }
        else {
            Save-PowerShellModules -TargetDirectory $PSModulesDirectory
        }
    }
}
