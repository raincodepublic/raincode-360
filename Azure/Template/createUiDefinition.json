{
    "$schema": "https://schema.management.azure.com/schemas/0.1.2-preview/CreateUIDefinition.MultiVm.json#",
    "handler": "Microsoft.Azure.CreateUIDef",
    "version": "0.1.2-preview",
    "parameters": {
        "basics": [],
        "steps": [
            {
                "name": "vmParameters",
                "label": "VM Parameters",
                "elements": [
                    {
                        "name": "vmInfo",
                        "type": "Microsoft.Common.InfoBox",
                        "visible": true,
                        "options": {
                            "icon": "Info",
                            "text": "Raincode 360 provides a virtual machine which houses the Raincode Development Environment and our product showcases."
                        }
                    },
                    {
                        "name": "vmSection",
                        "type": "Microsoft.Common.Section",
                        "label": "Customize your VM",
                        "elements": [
                            {
                                "name": "vmSize",
                                "type": "Microsoft.Compute.SizeSelector",
                                "label": "Size",
                                "toolTip": "Size of the Raincode 360 VM",
                                "recommendedSizes": [
                                    "Standard_B2ms",
                                    "Standard_D2s_v3"
                                ],
                                "options": {
                                    "hideDiskTypeFilter": false
                                },
                                "osPlatform": "Windows",
                                "imageReference": {
                                    "publisher": "MicrosoftWindowsDesktop",
                                    "offer": "Windows-10",
                                    "sku": "20h1-pron"
                                },
                                "count": 1,
                                "visible": true
                            }
                        ],
                        "visible": true
                    },
                    {
                        "name": "adminUsername",
                        "type": "Microsoft.Compute.UserNameTextBox",
                        "label": "Admin Username",
                        "defaultValue": "",
                        "toolTip": "Administrator username for the Raincode 360 VM",
                        "constraints": {
                            "required": true,
                            "regex": "^[a-z0-9A-Z]{1,30}$",
                            "validationMessage": "Only alphanumeric characters are allowed, and the value must be 1-30 characters long."
                        },
                        "osPlatform": "Windows",
                        "visible": true
                    },
                    {
                        "name": "vmCredentials",
                        "type": "Microsoft.Compute.CredentialsCombo",
                        "label": {
                            "password": "Password",
                            "confirmPassword": "Confirm password"
                        },
                        "toolTip": {
                            "password": "Administrator password for the Raincode 360 VM"
                        },
                        "constraints": {
                            "required": true,
                            "customPasswordRegex": "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[\\w~@#$%^&*+=|{}:;!.?\\()\\[\\]-]{8,}$",
                            "customValidationMessage": "The password must contain at least 8 characters, with at least 1 uppercase, 1 lowercase and 1 number."
                        },
                        "options": {
                            "hideConfirmation": false
                        },
                        "osPlatform": "Windows",
                        "visible": true
                    }
                ]
            },
            {
                "name": "networkParameters",
                "label": "Network Configuration",
                "elements": [
                    {
                        "name": "accessInfo",
                        "type": "Microsoft.Common.InfoBox",
                        "visible": true,
                        "options": {
                            "icon": "Info",
                            "text": "Specify whether the Raincode 360 VM should be accessible in the open through RDP, or whether it should be protected by Azure Bastion."
                        }
                    },
                    {
                        "name": "vmAccess",
                        "type": "Microsoft.Common.DropDown",
                        "label": "VM access type",
                        "toolTip": "The Raincode 360 VM can be accessed through RDP on a public IP, or can be secured using [Azure Bastion](https://azure.microsoft.com/en-us/services/azure-bastion/).",
                        "defaultValue": "public",
                        "constraints": {
                            "allowedValues": [
                                {
                                    "label": "RDP through public IP",
                                    "value": "public"
                                },
                                {
                                    "label": "Azure Bastion",
                                    "value": "bastion"
                                }
                            ],
                            "required": true
                        },
                        "visible": true
                    },
                    {
                        "name": "vnetSection",
                        "type": "Microsoft.Common.Section",
                        "label": "Virtual networks",
                        "elements": [
                            {
                                "name": "vnetInfo",
                                "type": "Microsoft.Common.InfoBox",
                                "visible": true,
                                "options": {
                                    "icon": "Warning",
                                    "text": "For advanced users. Leaving default values is safe."
                                }
                            },
                            {
                                "name": "vmVnetAddressPrefix",
                                "type": "Microsoft.Common.TextBox",
                                "label": "Core VNet address space",
                                "toolTip": "Must be valid CIDR notation.",
                                "defaultValue": "172.16.1.0/24",
                                "constraints": {
                                    "required": true,
                                    "validations": [
                                        {
                                            "regex": "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\/[0-9]{1,2}$",
                                            "message": "Must be valid CIDR notation."
                                        }
                                    ]
                                },
                                "visible": true
                            },
                            {
                                "name": "vmVnetBaseSubnetAddressPrefix",
                                "type": "Microsoft.Common.TextBox",
                                "label": "Core VNet subnet",
                                "toolTip": "Must be valid CIDR notation.",
                                "defaultValue": "172.16.1.0/26",
                                "constraints": {
                                    "required": true,
                                    "validations": [
                                        {
                                            "regex": "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\/[0-9]{1,2}$",
                                            "message": "Must be valid CIDR notation."
                                        }
                                    ]
                                },
                                "visible": true
                            },
                            {
                                "name": "vmVnetBastionSubnetAddressPrefix",
                                "type": "Microsoft.Common.TextBox",
                                "label": "Core VNet Bastion subnet",
                                "toolTip": "Must be valid CIDR notation.",
                                "defaultValue": "172.16.1.192/26",
                                "constraints": {
                                    "required": true,
                                    "validations": [
                                        {
                                            "regex": "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\/[0-9]{1,2}$",
                                            "message": "Must be valid CIDR notation."
                                        }
                                    ]
                                },
                                "visible": true
                            },
                            {
                                "name": "containerVnetAddressPrefix",
                                "type": "Microsoft.Common.TextBox",
                                "toolTip": "Must be valid CIDR notation.",
                                "label": "Container Apps VNet address space",
                                "defaultValue": "10.0.0.0/16",
                                "constraints": {
                                    "required": true,
                                    "validations": [
                                        {
                                            "regex": "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\/[0-9]{1,2}$",
                                            "message": "Must be valid CIDR notation."
                                        }
                                    ]
                                },
                                "visible": true
                            },
                            {
                                "name": "containerAppsInfrastructureSubnetAddressPrefix",
                                "type": "Microsoft.Common.TextBox",
                                "toolTip": "Must be valid CIDR notation.",
                                "label": "Container Apps VNet infrastructure subnet",
                                "defaultValue": "10.0.0.0/23",
                                "constraints": {
                                    "required": true,
                                    "validations": [
                                        {
                                            "regex": "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\/[0-9]{1,2}$",
                                            "message": "Must be valid CIDR notation."
                                        }
                                    ]
                                },
                                "visible": true
                            }
                        ]
                    }
                ]
            },
            {
                "name": "dbParameters",
                "label": "Database Parameters",
                "elements": [
                    {
                        "name": "dbInfo",
                        "type": "Microsoft.Common.InfoBox",
                        "visible": true,
                        "options": {
                            "icon": "Info",
                            "text": "Raincode 360 provides a SQL Server Developer virtual machine which houses the databases shared by our various showcases."
                        }
                    },
                    {
                        "name": "adminUsername",
                        "type": "Microsoft.Common.TextBox",
                        "label": "SQL Server Admin Username",
                        "defaultValue": "",
                        "toolTip": "Database administrator username for the Raincode 360 SQL Server",
                        "constraints": {
                            "required": true,
                            "regex": "^[a-z0-9A-Z]{1,30}$",
                            "validationMessage": "Only alphanumeric characters are allowed, and the value must be 1-30 characters long."
                        },
                        "visible": true
                    },
                    {
                        "name": "password",
                        "type": "Microsoft.Common.PasswordBox",
                        "label": {
                            "password": "Password",
                            "confirmPassword": "Confirm password"
                        },
                        "toolTip": "Database administrator password for the Raincode 360 SQL Server",
                        "constraints": {
                            "required": true,
                            "regex": "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[\\w~@#$+{}:-]{12,}$",
                            "validationMessage": "The password must contain at least 12 characters, with at least 1 uppercase, 1 lowercase and 1 number."
                        },
                        "options": {
                            "hideConfirmation": false
                        },
                        "visible": true
                    }
                ]
            },
            {
                "name": "tags",
                "label": "Tags",
                "elements": [
                    {
                        "name": "tagsByResource",
                        "type": "Microsoft.Common.TagsByResource",
                        "toolTip": "Tags to be set by type of resource",
                        "resources": [
                            "Microsoft.Compute/virtualMachines",
                            "Microsoft.ContainerService/managedClusters",
                            "Microsoft.Sql/servers",
                            "Microsoft.Sql/servers/databases",
                            "microsoft.insights/components"
                        ]
                    }
                ]
            }
        ],
        "outputs": {
            "location": "[location()]",
            "vmSize": "[steps('vmParameters').vmSection.vmSize]",
            "vmAdminUsername": "[steps('vmParameters').adminUsername]",
            "vmAdminPassword": "[steps('vmParameters').vmCredentials.password]",
            "vmAccessPolicy": "[steps('networkParameters').vmAccess]",
            "vmVnetAddressPrefix": "[steps('networkParameters').vnetSection.vmVnetAddressPrefix]",
            "vmVnetBaseSubnetAddressPrefix": "[steps('networkParameters').vnetSection.vmVnetBaseSubnetAddressPrefix]",
            "vmVnetBastionSubnetAddressPrefix": "[steps('networkParameters').vnetSection.vmVnetBastionSubnetAddressPrefix]",
            "containerVnetAddressPrefix": "[steps('networkParameters').vnetSection.containerVnetAddressPrefix]",
            "containerAppsInfrastructureSubnetAddressPrefix": "[steps('networkParameters').vnetSection.containerAppsInfrastructureSubnetAddressPrefix]",
            "sqlServerAdministratorLogin": "[steps('dbParameters').adminUsername]",
            "sqlServerAdministratorLoginPassword": "[steps('dbParameters').password]",
            "_marketplace": true,
            "tagsByResource": "[steps('tags').tagsByResource]"
        }
    }
}
