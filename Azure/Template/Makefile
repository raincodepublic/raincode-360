.PHONY: marketplace patch clip dist download distimage

.DEFAULT_GOAL := marketplace

VM_INDEX=$(shell jq '.resources | to_entries | map(select(.value.type == "Microsoft.Compute/virtualMachines")) | map(select(.value.name | test("vmName"))) | .[].key' mainTemplate.json)

mainTemplate.json: mainTemplate.bicep
	az bicep build --file $< --stdout | jq --indent 4 . >$@

marketplace: mainTemplate.json dist download
	cp createUiDefinition.json dist/
	cp mainTemplate.json dist/
	cp provision.ps1 dist/
	cp provision.psm1 dist/
	cp downloader.psm1 dist/
	(cd ../.. && git archive -9 --output Azure/Template/dist/installers/git_repo.zip HEAD)
	(cd dist && zip -9 -r raincode360_marketplace.zip *)

download: dist
	./downloader.ps1 -TargetDirectory dist/installers
	(cd dist/installers/psModules; zip -9 -r ../psModules.zip *)
	rm -rvf dist/installers/psModules

dist:
	rm -rvf dist
	mkdir -p dist

xclip: mainTemplate.json
	xclip -sel clip <$<

distimage: mainTemplate.json
ifeq ($(IMAGE),)
	$(error Image resource ID must be defined through the IMAGE variable)
endif
	rm -rvf dist
	mkdir -p dist
	mkdir -p profiles/distimage.tmp
	sed -e 's@\$$STUDIO_IMAGE_ID@$(IMAGE)@' -e 's@\$$VM_INDEX@$(VM_INDEX)@' profiles/distimage/image.json-patch > profiles/distimage.tmp/image.json-patch
	./patch.sh mainTemplate.json distimage.tmp
