@description('Default location for deployed resources')
param location string = resourceGroup().location

@description('Raincode 360 version to deploy. Ignored on Marketplace deployments.)')
param productVersion string = 'master'

@description('The name of the SQL Server VM used by Raincode 360 demos')
param sqlServerName string = 'sqlserver'

@description('Size of the SQL Server virtual machine used by Raincode 360')
param sqlServerVmSize string = 'Standard_B1ms'

@description('The database administrator username of the SQL Server VM used by Raincode 360 demos')
param sqlServerAdministratorLogin string = 'raincode360'

@description('The database administrator password of the SQL Server VM used by Raincode 360 demos')
@secure()
param sqlServerAdministratorLoginPassword string

@description('Name of the virtual machine hosting Raincode 360')
param vmName string = 'raincode360-vm'

@description('Size of the virtual machine hosting Raincode 360')
param vmSize string = 'Standard_D2s_v3'

@description('Administrator username of the virtual machine hosting Raincode 360')
param vmAdminUsername string = 'raincode360'

@description('Administrator password of the virtual machine hosting Raincode 360')
@secure()
param vmAdminPassword string

@description('Deploy Raincode 360 VM in Azure Bastion. Will otherwise use a public IP and direct RDP access.')
@allowed([
  'public'
  'bastion'
])
param vmAccessPolicy string = 'public'

@description('Name of the Container Apps managed environment to create.')
param containerEnvName string = 'raincode360-containers-env'

@description('Name of the log workspace to create.')
param logWorkspaceName string = 'raincode360-logs'

@description('The name of the Application Insights component used by Raincode 360 demos')
param insightsName string = 'raincode360-insights-${uniqueString(resourceGroup().id)}'

@description('Tags to be set by resource type')
param tagsByResource object = {}

@description('Address space for main virtual network.')
param vmVnetAddressPrefix string = '172.16.1.0/24'

@description('Main subnet on main virtual network.')
param vmVnetBaseSubnetAddressPrefix string = '172.16.1.0/26'

@description('Bastion subnet on main virtual network.')
param vmVnetBastionSubnetAddressPrefix string = '172.16.1.192/26'

@description('Address space for container apps virtual network.')
param containerVnetAddressPrefix string = '10.0.0.0/16'

@description('Infrastructure subnet on container apps virtual network.')
param containerAppsInfrastructureSubnetAddressPrefix string = '10.0.0.0/23'

@description('Reserved for Azure Marketplace deployments. Leave as is.')
param _marketplace bool = false

@description('Reserved for Azure Marketplace deployments. Leave as is.')
param _artifactsLocation string = _marketplace ? deployment().properties.templateLink.uri : ''

@description('Reserved for Azure Marketplace deployments. Leave as is.')
@secure()
param _artifactsLocationSasToken string = ''

var enableAzureBastion = (vmAccessPolicy == 'bastion')
var nsgName = '${vmName}-nsg'
var publicIpName = '${(enableAzureBastion ? bastionName : vmName)}-ip'

var vmVnetBastionSubnetName = 'AzureBastionSubnet'
var vmVnetName = '${resourceGroup().name}-vnet'

var vmVnetBaseSubnetName = 'BaseSubnet'
var nicName = '${resourceGroup().name}-nic'
var bastionNicName = '${resourceGroup().name}-bastion-nic'
var bastionName = '${resourceGroup().name}-bastion'
var vmImage = {
  publisher: 'raincode'
  offer: 'raincode_metal'
  sku: 'raincode_metal'
  version: 'latest'
}
var vmScriptBaseCommand = 'pwsh -ExecutionPolicy Unrestricted -File provision.ps1 -ResourceGroupName ${resourceGroup().name} -IngressFqdn ${pod_pyfrontend.properties.configuration.ingress.fqdn} -SqlServerIp ${sqlServerNic.properties.ipConfigurations[0].properties.privateIPAddress} -SqlServerLogin ${sqlServerAdministratorLogin} -SqlServerPassword ${sqlServerAdministratorLoginPassword} -Username ${vmAdminUsername} -GitBranch ${productVersion}'
var vmScriptMarketplaceCommand = '${vmScriptBaseCommand} -Marketplace -ArtifactsLocation ${uri(_artifactsLocation, '.')} -ArtifactsLocationSasToken "${_artifactsLocationSasToken}"'
var provisioningScriptUriPath = _marketplace ? uri(_artifactsLocation, 'provision.ps1${_artifactsLocationSasToken}') : '${productVersion}/Azure/Template/provision.ps1'
var provisioningModuleUriPath = _marketplace ? uri(_artifactsLocation, 'provision.psm1${_artifactsLocationSasToken}') : '${productVersion}/Azure/Template/provision.psm1'
var downloaderModuleUriPath = _marketplace ? uri(_artifactsLocation, 'downloader.psm1${_artifactsLocationSasToken}') : '${productVersion}/Azure/Template/downloader.psm1'
var provisioningScriptUri = uri('https://gitlab.phidani.be/raincodepublic/raincode-360/-/raw/', provisioningScriptUriPath)
var provisioningModuleUri = uri('https://gitlab.phidani.be/raincodepublic/raincode-360/-/raw/', provisioningModuleUriPath)
var downloaderModuleUri = uri('https://gitlab.phidani.be/raincodepublic/raincode-360/-/raw/', downloaderModuleUriPath)
var sqlServerNicName = '${sqlServerName}-nic'
var sqlDataDiskName = '${sqlServerName}-disk'

var db_secret_name = 'db-connection-string'
var db_secret = {
  name: db_secret_name
  value: 'Server=tcp:${sqlServerNic.properties.ipConfigurations[0].properties.privateIPAddress},1433;User ID=${sqlServerAdministratorLogin};Password=${sqlServerAdministratorLoginPassword};Persist Security Info=False;Encrypt=False;Connection Timeout=30;Initial Catalog=BankDemo;'
}
var container_envvars = [
  {
    name: 'DB_CONNECTION_STRING'
    secretRef: db_secret_name
  }
]

var containersVnetName = 'containers-vnet'

var podResourceLimits = {
  cpu: json('0.25')
  memory: '.5Gi'
}
var podScale = {
  minReplicas: 1
  rules: [
    {
      name: 'http-requests'
      http: {
        metadata: {
          concurrentRequests: '10'
        }
      }
    }
  ]
}

resource vmVnet 'Microsoft.Network/virtualNetworks@2022-01-01' = {
  name: vmVnetName
  location: location
  properties: {
    addressSpace: {
      addressPrefixes: [
        vmVnetAddressPrefix
      ]
    }
  }
}

resource vmVnetBaseSubnet 'Microsoft.Network/virtualNetworks/subnets@2022-01-01' = {
  name: vmVnetBaseSubnetName
  parent: vmVnet
  properties: {
    addressPrefix: vmVnetBaseSubnetAddressPrefix
  }
}

resource vmVnetBastionSubnet 'Microsoft.Network/virtualNetworks/subnets@2022-01-01' = if (enableAzureBastion) {
  name: vmVnetBastionSubnetName
  parent: vmVnet
  properties: {
    addressPrefix: vmVnetBastionSubnetAddressPrefix
  }
  dependsOn: [
    vmVnetBaseSubnet
  ]
}

resource containerAppsVnet 'Microsoft.Network/virtualNetworks@2022-01-01' = {
  name: containersVnetName
  location: location
  properties: {
    addressSpace: {
      addressPrefixes: [
        containerVnetAddressPrefix
      ]
    }
  }
}

resource containerAppsInfrastructureSubnet 'Microsoft.Network/virtualNetworks/subnets@2022-01-01' = {
  name: '${containersVnetName}-infra-subnet'
  parent: containerAppsVnet
  properties: {
    addressPrefix: containerAppsInfrastructureSubnetAddressPrefix
  }
}

resource nic 'Microsoft.Network/networkInterfaces@2022-01-01' = if (!enableAzureBastion) {
  name: nicName
  location: location
  properties: {
    ipConfigurations: [
      {
        name: uniqueString(nicName)
        properties: {
          subnet: {
            id: vmVnetBaseSubnet.id
          }
          privateIPAllocationMethod: 'Dynamic'
          publicIPAddress: {
            id: publicIp.id
          }
        }
      }
    ]
    networkSecurityGroup: {
      id: nsg.id
    }
  }
}

resource bastionNic 'Microsoft.Network/networkInterfaces@2022-01-01' = if (enableAzureBastion) {
  name: bastionNicName
  location: location
  properties: {
    ipConfigurations: [
      {
        name: uniqueString(bastionNicName)
        properties: {
          subnet: {
            id: vmVnetBaseSubnet.id
          }
          privateIPAllocationMethod: 'Dynamic'
        }
      }
    ]
  }
}

resource sqlServerNic 'Microsoft.Network/networkInterfaces@2022-01-01' = {
  name: sqlServerNicName
  location: location
  properties: {
    ipConfigurations: [
      {
        name: uniqueString(sqlServerNicName)
        properties: {
          subnet: {
            id: vmVnetBaseSubnet.id
          }
          privateIPAllocationMethod: 'Dynamic'
        }
      }
    ]
  }
}

resource nsg 'Microsoft.Network/networkSecurityGroups@2022-01-01' = if (!enableAzureBastion) {
  name: nsgName
  location: location
  properties: {
    securityRules: [
      {
        name: 'RDP'
        properties: {
          priority: 300
          protocol: 'TCP'
          access: 'Allow'
          direction: 'Inbound'
          sourceAddressPrefix: '*'
          sourcePortRange: '*'
          destinationAddressPrefix: '*'
          destinationPortRange: '3389'
        }
      }
    ]
  }
}

resource publicIp 'Microsoft.Network/publicIPAddresses@2022-01-01' = {
  name: publicIpName
  location: location
  sku: {
    name: enableAzureBastion ? 'Standard' : 'Basic'
  }
  properties: {
    publicIPAllocationMethod: enableAzureBastion ? 'Static' : 'Dynamic'
  }
}

resource bastionHost 'Microsoft.Network/bastionHosts@2022-01-01' = if (enableAzureBastion) {
  name: bastionName
  location: location
  tags: contains(tagsByResource, 'Microsoft.Network/bastionHosts') ? tagsByResource['Microsoft.Network/bastionHosts'] : {}
  properties: {
    ipConfigurations: [
      {
        name: 'IpConf'
        properties: {
          subnet: {
            id: vmVnetBastionSubnet.id
          }
          publicIPAddress: {
            id: publicIp.id
          }
        }
      }
    ]
  }
}

resource sqlDataDisk 'Microsoft.Compute/disks@2022-03-02' = {
  name: sqlDataDiskName
  location: location
  properties: {
    diskSizeGB: 8
    creationData: {
      createOption: 'empty'
    }
  }
  sku: {
    name: 'Premium_LRS'
  }
}

resource vm 'Microsoft.Compute/virtualMachines@2022-03-01' = {
  name: vmName
  location: location
  identity: {
    type: 'SystemAssigned'
  }
  tags: contains(tagsByResource, 'Microsoft.Compute/virtualMachines') ? tagsByResource['Microsoft.Compute/virtualMachines'] : {}
  properties: {
    hardwareProfile: {
      vmSize: vmSize
    }
    storageProfile: {
      osDisk: {
        createOption: 'fromImage'
        managedDisk: {
          storageAccountType: 'Premium_LRS'
        }
      }
      imageReference: vmImage
    }
    networkProfile: {
      networkInterfaces: [
        {
          id: enableAzureBastion ? bastionNic.id : nic.id
        }
      ]
    }
    osProfile: {
      computerName: vmName
      adminUsername: vmAdminUsername
      adminPassword: vmAdminPassword
      windowsConfiguration: {
        enableAutomaticUpdates: true
        provisionVMAgent: true
      }
    }
  }
  plan: {
    publisher: 'raincode'
    product: 'raincode_metal'
    name: 'raincode_metal'
  }
}

resource sqlServerRootVm 'Microsoft.Compute/virtualMachines@2022-03-01' = {
  name: sqlServerName
  location: location
  tags: contains(tagsByResource, 'Microsoft.Compute/virtualMachines') ? tagsByResource['Microsoft.Compute/virtualMachines'] : {}
  properties: {
    hardwareProfile: {
      vmSize: sqlServerVmSize
    }
    storageProfile: {
      osDisk: {
        createOption: 'fromImage'
        managedDisk: {
          storageAccountType: 'Premium_LRS'
        }
      }
      imageReference: {
        publisher: 'microsoftsqlserver'
        offer: 'sql2019-ws2019'
        sku: 'sqldev'
        version: 'latest'
      }
      dataDisks: [
        {
          lun: 0
          createOption: 'Attach'
          caching: 'ReadOnly'
          managedDisk: {
            id: sqlDataDisk.id
          }
        }
      ]
    }
    networkProfile: {
      networkInterfaces: [
        {
          id: sqlServerNic.id
        }
      ]
    }
    osProfile: {
      computerName: sqlServerName
      adminUsername: vmAdminUsername
      adminPassword: vmAdminPassword
      windowsConfiguration: {
        enableAutomaticUpdates: true
        provisionVMAgent: true
      }
    }
  }
}

resource sqlServerVm 'Microsoft.SqlVirtualMachine/sqlVirtualMachines@2022-02-01' = {
  name: sqlServerName
  location: location
  tags: contains(tagsByResource, 'Microsoft.SqlVirtualMachine/SqlVirtualMachines') ? tagsByResource['Microsoft.SqlVirtualMachine/SqlVirtualMachines'] : {}
  properties: {
    virtualMachineResourceId: sqlServerRootVm.id
    sqlManagement: 'Full'
    sqlServerLicenseType: 'PAYG'
    storageConfigurationSettings: {
      diskConfigurationType: 'NEW'
      storageWorkloadType: 'General'
      sqlDataSettings: {
        luns: [
          0
        ]
        defaultFilePath: 'F:\\data'
      }
      sqlLogSettings: {
        luns: [
          0
        ]
        defaultFilePath: 'F:\\log'
      }
      sqlTempDbSettings: {
        luns: [
          0
        ]
        defaultFilePath: 'F:\\tempDb'
      }
    }
    serverConfigurationsManagementSettings: {
      sqlConnectivityUpdateSettings: {
        connectivityType: 'Private'
        port: 1433
        sqlAuthUpdateUserName: sqlServerAdministratorLogin
        sqlAuthUpdatePassword: sqlServerAdministratorLoginPassword
      }
      additionalFeaturesServerConfigurations: {
        isRServicesEnabled: true
      }
    }
  }
}

resource vmExtension 'Microsoft.Compute/virtualMachines/extensions@2022-03-01' = {
  parent: vm
  name: 'raincode360'
  location: location
  properties: {
    publisher: 'Microsoft.Compute'
    type: 'CustomScriptExtension'
    typeHandlerVersion: '1.10'
    autoUpgradeMinorVersion: true
    settings: {
      fileUris: [
        provisioningScriptUri
        provisioningModuleUri
        downloaderModuleUri
      ]
    }
    protectedSettings: {
      commandToExecute: _marketplace ? vmScriptMarketplaceCommand : vmScriptBaseCommand
    }
  }
  dependsOn: [
    sqlServerVm
  ]
}

resource pod_pyfrontend 'Microsoft.App/containerApps@2022-03-01' = {
  name: 'pod-pyfrontend'
  location: location
  properties: {
    configuration: {
      activeRevisionsMode: 'Single'
      dapr: {
        appId: 'pyfrontend'
        appProtocol: 'http'
        enabled: true
      }
      ingress: {
        targetPort: 8000
        external: true
      }
    }
    template: {
      containers: [
        {
          name: 'pyfrontend'
          image: 'docker.io/raincode360/pyfrontend:2.1.0'
          resources: podResourceLimits
          env: [
            {
              name: 'DEBUG'
              value: '1'
            }
            {
              name: 'STATIC_URL'
              value: 'https://${pod_pystaticfiles.properties.configuration.ingress.fqdn}/'
            }
            {
              name: 'APPINSIGHTS_URL'
              value: 'https://portal.azure.com/#@${tenant().tenantId}/resource${applicationInsights.id}/applicationMap'
            }
          ]
        }
      ]
      scale: podScale
    }
    managedEnvironmentId: containerEnv.id
  }
}

resource pod_pystaticfiles 'Microsoft.App/containerApps@2022-03-01' = {
  name: 'pod-pystaticfiles'
  location: location
  properties: {
    configuration: {
      activeRevisionsMode: 'Single'
      ingress: {
        targetPort: 80
        external: true
      }
    }
    template: {
      containers: [
        {
          name: 'pystaticfiles'
          image: 'docker.io/raincode360/pystaticfiles:2.1.0'
          resources: podResourceLimits
        }
      ]
      scale: podScale
    }
    managedEnvironmentId: containerEnv.id
  }
}

resource pod_accounts 'Microsoft.App/containerApps@2022-03-01' = {
  name: 'pod-accounts'
  location: location
  properties: {
    configuration: {
      activeRevisionsMode: 'Single'
      dapr: {
        appId: 'accounts'
        appPort: 80
        appProtocol: 'http'
        enabled: true
      }
      secrets: [
        db_secret
      ]
    }
    template: {
      containers: [
        {
          name: 'accounts'
          image: 'docker.io/raincode360/bankdemo_accounts:2.1.0'
          resources: podResourceLimits
          env: container_envvars
        }
      ]
      scale: podScale
    }
    managedEnvironmentId: containerEnv.id
  }
  dependsOn: [
    vmExtension
  ]
}

resource pod_customers 'Microsoft.App/containerApps@2022-03-01' = {
  name: 'pod-customers'
  location: location
  properties: {
    configuration: {
      activeRevisionsMode: 'Single'
      dapr: {
        appId: 'customers'
        appPort: 80
        appProtocol: 'http'
        enabled: true
      }
      secrets: [
        db_secret
      ]
    }
    template: {
      containers: [
        {
          name: 'customers'
          image: 'docker.io/raincode360/bankdemo_customers:2.1.0'
          resources: podResourceLimits
          env: container_envvars
        }
      ]
      scale: podScale
    }
    managedEnvironmentId: containerEnv.id
  }
  dependsOn: [
    vmExtension
  ]
}

resource pod_payment 'Microsoft.App/containerApps@2022-03-01' = {
  name: 'pod-payment'
  location: location
  properties: {
    configuration: {
      activeRevisionsMode: 'Single'
      dapr: {
        appId: 'payment'
        appPort: 80
        appProtocol: 'http'
        enabled: true
      }
      secrets: [
        db_secret
      ]
    }
    template: {
      containers: [
        {
          name: 'payment'
          image: 'docker.io/raincode360/bankdemo_payment:2.1.0'
          resources: podResourceLimits
          env: container_envvars
        }
      ]
      scale: podScale
    }
    managedEnvironmentId: containerEnv.id
  }
  dependsOn: [
    vmExtension
  ]
}

resource pod_balance 'Microsoft.App/containerApps@2022-03-01' = {
  name: 'pod-balance'
  location: location
  properties: {
    configuration: {
      activeRevisionsMode: 'Single'
      dapr: {
        appId: 'balance'
        appPort: 80
        appProtocol: 'http'
        enabled: true
      }
      secrets: [
        db_secret
      ]
    }
    template: {
      containers: [
        {
          name: 'balance'
          image: 'docker.io/raincode360/bankdemo_balance:2.1.0'
          resources: podResourceLimits
          env: container_envvars
        }
      ]
      scale: podScale
    }
    managedEnvironmentId: containerEnv.id
  }
  dependsOn: [
    vmExtension
  ]
}

resource applicationInsights 'Microsoft.Insights/components@2020-02-02' = {
  name: insightsName
  location: location
  kind: 'other'
  properties: {
    Application_Type: 'web'
    Flow_Type: 'Bluefield'
    Request_Source: 'CustomDeployment'
    WorkspaceResourceId: logWorkspace.id
  }
}

resource containerEnv 'Microsoft.App/managedEnvironments@2022-03-01' = {
  name: containerEnvName
  location: location
  properties: {
	  daprAIInstrumentationKey:applicationInsights.properties.InstrumentationKey
    appLogsConfiguration: {
      destination: 'log-analytics'
      logAnalyticsConfiguration: {
        customerId: logWorkspace.properties.customerId
        sharedKey: logWorkspace.listKeys().primarySharedKey
      }
    }
    vnetConfiguration: {
      infrastructureSubnetId: containerAppsInfrastructureSubnet.id
      internal: false
    }
    zoneRedundant: false
  }
}

resource logWorkspace 'Microsoft.OperationalInsights/workspaces@2021-06-01' = {
  name: logWorkspaceName
  location: location
  properties: {
    sku: {
      name: 'PerGB2018'
    }
    retentionInDays: 30
  }
}

resource peering_container_apps_to_vm 'Microsoft.Network/virtualNetworks/virtualNetworkPeerings@2022-01-01' = {
  parent: containerAppsVnet
  name: 'container_apps_to_vm'
  properties: {
    allowVirtualNetworkAccess: true
    allowForwardedTraffic: true
    allowGatewayTransit: false
    useRemoteGateways: false
    remoteVirtualNetwork: {
      id: vmVnet.id
    }
    remoteAddressSpace: {
      addressPrefixes: vmVnet.properties.addressSpace.addressPrefixes
    }
  }
  dependsOn: [
    vmVnetBaseSubnet
    vmVnetBastionSubnet
    containerAppsInfrastructureSubnet
  ]
}

resource vmVnet_vm_to_container_apps 'Microsoft.Network/virtualNetworks/virtualNetworkPeerings@2022-01-01' = {
  parent: vmVnet
  name: 'vm_to_container_apps'
  properties: {
    allowVirtualNetworkAccess: true
    allowForwardedTraffic: true
    allowGatewayTransit: false
    useRemoteGateways: false
    remoteVirtualNetwork: {
      id: containerAppsVnet.id
    }
    remoteAddressSpace: {
      addressPrefixes: containerAppsVnet.properties.addressSpace.addressPrefixes
    }
  }
  dependsOn: [
    vmVnetBaseSubnet
    vmVnetBastionSubnet
    containerAppsInfrastructureSubnet
  ]
}
