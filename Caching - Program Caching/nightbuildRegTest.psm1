#Nightbuild Reg test required configuration 
$nightbuidConnString = "`"$SQLSERVERCONNECTIONSTRING$`"" # Gets substituted during nightbuild reg test
if(-Not ($nightbuidConnString -eq "`"$`"")){ #Setting variables nightbuild specific 
   $global:quotedconn= $nightbuidConnString
   $global:ArgCommon =  "-SqlServer=$($quotedconn) $($ProgName).dll "
   $global:ArgProgramCache = "-Plugin=CacheRegistration "
}