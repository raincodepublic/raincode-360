$SqlCached = 400
$SqlTotal  = 500

$SolutionDir=$PSScriptRoot
$rclrun = "$($Env:RCBIN)\rclrun"
$ProgName = "MainProgCache"
$ArgDynamic = "-SQLExecutionMode=DynamicOnly "
$quotedconn= "`"$($Env:RC360_CONNSTRING)Initial Catalog=BankDemo;`""
$Configuration = "Debug"
$ArgProgramCache = "-PluginPath=`"$($Solutiondir)\CacheRegistration\bin\$($Configuration)\net8.0`" -Plugin=CacheRegistration "
$assemblyDirectory="`"$($SolutionDir)\ProgramCaching\bin\$($Configuration)\net8.0`""
$ArgCommon = "-SqlServer=$($quotedconn) -AddAssemblySearchDir=$($assemblyDirectory) $($ProgName).dll"

Import-Module -Name "./nightbuildRegTest.psm1"

function Start-Main    
{
    param([string]$CmdArgs)
    $p = Start-Process -PassThru -Wait -NoNewWindow -FilePath $rclrun -ArgumentList $CmdArgs
    if ($p.ExitCode -ne 0) {
        Set-Location -Path $SolutionDir
        throw "program $($ProgName) failed with code $($p.ExitCode)`n$cmdargs"
    }
}
#####################################################
## Dynamic SQL with no caching                      ##
#####################################################
Write-Host "Test 1: Dynamic SQL with no caching"
$Elapsed = Measure-Command { Start-Main -CmdArgs ($ArgDynamic + $ArgCommon) } 
$TimeDynamic = [int](Get-Content timings.txt)[0] / 1000

#####################################################
## Dynamic SQL with program caching                 ##
#####################################################
Write-Host "Test 2: Dynamic SQL with program caching"
$Elapsed = Measure-Command { Start-Main -CmdArgs ($ArgDynamic + $ArgProgramCache + $ArgCommon) } 
$TimeDynamicProgramCache = [int](Get-Content timings.txt)[0] / 1000

############################
## REPORT                 ##
############################
function Output-Result    
{
    param([String]$TestName, [Double]$Time, [Double]$SqlTotal, [Double]$SqlCached, [string]$Color)
    Write-Host -NoNewLine $TestName.PadRight(8) 
    Write-Host -NoNewLine $SqlTotal.ToString().PadLeft(10)
    Write-Host -NoNewLine $SqlCached.ToString().PadLeft(10) 
    Write-Host -NoNewLine $Time.tostring("0.00").PadLeft(13)
    Write-Host -NoNewLine "    "
    For ($i=0; $i -lt [math]::Round($Time * 4); $i++) { write-host -nonewline -BackgroundColor $Color ' '}
    Write-Host 
}
Write-Host "`nELAPSED TIMES"
Write-Host "-------------------------------------------"
Write-Host "               SQLs      SQLs        Time"
Write-Host "Test           Total     Cached      (s)"
Write-Host "-------------------------------------------"
Output-Result -TestName "Test 1" -Time $TimeDynamic              -SqlTotal $SqlTotal -SqlCached 0          -Color "Green"
Output-Result -TestName "Test 2" -Time $TimeDynamicProgramCache      -SqlTotal $SqlTotal -SqlCached $SqlCached -Color "DarkYellow"
Write-Host
Set-Location $SolutionDir
