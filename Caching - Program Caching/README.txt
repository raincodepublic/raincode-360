********************************************************************
*                                                                  *
*   DEMO : PROGRAM CACHING                                         *
*                                                                  *
********************************************************************

PURPOSE
  . Test the performance of program caching
  . Illustrate the use of dynamic sql

RUNNING THE DEMO
  . Open a developer command prompt
  . Type 'pwsh'
  . Type '.\run'

NOTES
  . 'pwsh' starts version 7.0.2 (or higher) of PowerShell
  . The script must be run from the solution directory
  . When the cache is active, the first 50 SQLs will conect to the database to fill up the cache
  . Both tests run the same compiled code, the only difference being the -plugin argument of rclrun
  . The run script assumes that the solution was built in debug mode