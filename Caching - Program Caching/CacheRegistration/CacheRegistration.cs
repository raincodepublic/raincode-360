﻿using RainCode.Core.Extensions;
using RainCode.Core.Plugin;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Module;
using System.Collections.Generic;

[assembly: PluginProvider(typeof(CacheRegistration.CacheRegistration),
    nameof(CacheRegistration.CacheRegistration.Register))]


namespace CacheRegistration
{
    public class CacheRegistration
    {
        public static void Register()
        {
            ExecutionContext.PreparePlugin.Provide(1000, Initialize);
        }
        private static void Initialize(ExecutionContext ctx, string entry)
        {
            ModuleDictionary.CustomExecutableLoadedEvent += InitInterceptor;
        }
        private static void InitInterceptor(Executable Exec)
        {
            switch (Exec.Name.ToUpper())
            {
                case "ZIPPROGCACHE":
                    Exec.Interceptor = new CacheResultsInterceptor();
                    break;
                default:
                    return;
            }
        }
    }
    class CacheResultsInterceptor : CallIntercept
    {
        private System.Collections.Concurrent.ConcurrentDictionary<byte[], byte[]> cache = new System.Collections.Concurrent.ConcurrentDictionary<byte[], byte[]>(new ByteArrayEqualityComparer());
        private ParameterModeUtilities pm = null;
        private byte[] key;

        public void Intercept(Executable.Intercepted c)
        {
            if (pm == null)
            {
                pm = new ParameterModeUtilities(c.Ec, c.Args, c.GetParameterDescriptors());
                key = new byte[pm.Size(ParameterMode.ModeIn)];
            }
            pm.PackUnpack(ParameterModeUtilities.PackOrUnpack.Pack, ParameterMode.ModeIn, key);
            byte[] cached;
            if (cache.TryGetValue(key, out cached))
            {
                System.Console.WriteLine("found in cache {0}", System.BitConverter.ToString(key));
                pm.PackUnpack(ParameterModeUtilities.PackOrUnpack.Unpack, ParameterMode.ModeOut, cached);
            }
            else
            {
                c.Execute();
                byte[] result = new byte[pm.Size(ParameterMode.ModeOut)];
                pm.PackUnpack(ParameterModeUtilities.PackOrUnpack.Pack, ParameterMode.ModeOut, result);
                cache.GetOrAdd((byte[])key.Clone(), result);
            }
        }
    }
    class ParameterModeUtilities
    {
        private ExecutionContext Ec;
        private CallParameters Args;
        private Dictionary<string, ParameterDescriptor> inCache, outCache, modes;

        public ParameterModeUtilities(ExecutionContext ec, CallParameters a, Dictionary<string, ParameterDescriptor> m)
        {
            Ec = ec;
            Args = a;
            modes = m;
            inCache = null;
            outCache = null;
            inSize = 0;
            outSize = 0;
        }

        public Dictionary<string, ParameterDescriptor> GetParameterMode(ParameterMode input)
        {
            switch (input)
            {
                case ParameterMode.ModeIn:
                    if (inCache == null)
                    {
                        foreach (var v in modes)
                        {
                            if (v.Value.ArgumentNumber >= 0 && (v.Value.Mode == ParameterMode.ModeIn || v.Value.Mode == ParameterMode.ModeInout))
                            {
                                if (inCache == null)
                                {
                                    inCache = new Dictionary<string, ParameterDescriptor>();
                                }

                                inCache.Add(v.Key, v.Value);
                            }
                        }
                    }
                    return inCache;
                case ParameterMode.ModeOut:
                    if (outCache == null)
                    {
                        foreach (var v in modes)
                        {
                            if (v.Value.ArgumentNumber >= 0 && (v.Value.Mode == ParameterMode.ModeOut || v.Value.Mode == ParameterMode.ModeInout))
                            {
                                if (outCache == null)
                                {
                                    outCache = new Dictionary<string, ParameterDescriptor>();
                                }

                                outCache.Add(v.Key, v.Value);
                            }
                        }
                    }
                    return outCache;
                default:
                    return null;
            }
        }

        private int inSize, outSize;
        public int Size(ParameterMode input)
        {
            int sz;
            switch (input)
            {
                case ParameterMode.ModeIn:
                    sz = inSize;
                    break;
                case ParameterMode.ModeOut:
                    sz = outSize;
                    break;
                default:
                    return 0;
            }
            if (sz == 0)
            {
                var mode = GetParameterMode(input);
                if (mode != null)
                {
                    foreach (var m in mode.Values)
                    {
                        if (m.Stride == null)
                        {
                            sz += m.Size;
                        }
                        else
                        {
                            var asz = m.Size;
                            for (int i = 0; i < m.Stride.Length; i++)
                            {
                                asz *= m.Count[i];
                            }
                            sz += asz;
                        }
                    }
                    if (sz != 0)
                    {
                        switch (input)
                        {
                            case ParameterMode.ModeIn:
                                inSize = sz;
                                break;
                            case ParameterMode.ModeOut:
                                outSize = sz;
                                break;
                        }
                    }
                }
            }
            return sz;
        }

        public enum PackOrUnpack { Pack, Unpack }

        private int Copy(PackOrUnpack pack, int argumentNumber, int offset, int size, byte[] res, int ofs)
        {
            var mem = Args.GetParameterAddress(Ec, argumentNumber);
            switch (pack)
            {
                case PackOrUnpack.Pack:
                    System.Buffer.BlockCopy(mem.Asp.Mem, mem.Ofs + offset, res, ofs, size);
                    break;
                case PackOrUnpack.Unpack:
                    System.Buffer.BlockCopy(res, ofs, mem.Asp.Mem, mem.Ofs + offset, size);
                    break;
            }
            return ofs + size;
        }

        public void PackUnpack(PackOrUnpack pack, ParameterMode input, byte[] res)
        {
            int ofs = 0;
            foreach (var m in GetParameterMode(input).Values)
            {
                if (m.Stride == null)
                {
                    ofs = Copy(pack, m.ArgumentNumber, m.Offset, m.Size, res, ofs);
                }
                else
                {
                    bool done = false;
                    int[] current = new int[m.Stride.Length];
                    for (int i = 0; i < m.Stride.Length; i++)
                    {
                        current[i] = 0;
                    }
                    while (!done)
                    {
                        int aofs = m.Offset;
                        for (int i = 0; i < m.Stride.Length; i++)
                        {
                            aofs += m.Stride[i] * current[i];
                        }
                        ofs = Copy(pack, m.ArgumentNumber, aofs, m.Size, res, ofs);
                        for (int i = m.Stride.Length - 1; i >= 0; i--)
                        {
                            current[i]++;
                            if (current[i] < m.Count[i])
                            {
                                break;
                            }
                            else
                            {
                                current[i] = 0;
                                done = i == 0;
                            }
                        }
                    }
                }
            }
        }
    }
    class ByteArrayEqualityComparer : IEqualityComparer<byte[]>
    {
        public bool Equals(byte[] x, byte[] y)
        {
            if (x.Length != y.Length)
            {
                return false;
            }
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] != y[i])
                {
                    return false;
                }
            }
            return true;
        }

        public int GetHashCode(byte[] obj)
        {
            int result = 17;
            for (int i = 0; i < obj.Length; i++)
            {
                result = result * 23 + obj[i];
            }
            return result;
        }
    }


}
