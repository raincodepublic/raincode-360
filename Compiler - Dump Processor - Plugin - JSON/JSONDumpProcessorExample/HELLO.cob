﻿       IDENTIFICATION DIVISION.
       PROGRAM-ID.     HELLO.
       ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
       DATA DIVISION.
        WORKING-STORAGE SECTION.
         01 HELLO-MSG     PIC X(12).
         01 IN-OUT-VAR.
           03 A           PIC 9(09).
           03 B           PIC 9(09).
           03 C           PIC 9(09).
         01 D             PIC 9(15).
         01 E REDEFINES D PIC X(09).
         01 A-NATIONAL    PIC N(50) USAGE NATIONAL. 
         01 CHECK-DAY.
            05 DAY PIC X(3).
               88 MONDAY     VALUE 'MON'.
               88 TUESDAY    VALUE 'TUE'.
               88 WEDNESDAY  VALUE 'WED'.
               88 THURSDAY   VALUE 'THU'.
               88 FRIDAY     VALUE 'FRI'.
               88 SATURDAY   VALUE 'SAT'.
               88 SUNDAY     VALUE 'SUN'.
         01 SOME-TABLES.
          02 TBL OCCURS 2.
           03 TBL-0 PIC X(4).
           03 TBL-1 PIC S9(7) COMP-3.
           03 STTBL PIC X(16) OCCURS 4.
              05 STTBL-0   PIC S9(9) COMP-3.
         77 R0 PIC S9(9) COMP-5.
         77 R1 PIC S9(9) COMP-5.
         77 R2 PIC S9(9) COMP-5.
                
       PROCEDURE DIVISION.
       BEGIN.
           MOVE "Hello World!" TO HELLO-MSG.
           DISPLAY HELLO-MSG.
           MOVE N'在庫レベル'   TO A-NATIONAL.
           DISPLAY A-NATIONAL.
           MOVE 10 TO A.
           MOVE  0 TO B.
           CALL 'VERBS' USING IN-OUT-VAR;
           DISPLAY "C: " C.
           GOBACK.