	        IDENTIFICATION DIVISION.
            PROGRAM-ID. VERBS.
            DATA DIVISION.
              WORKING-STORAGE SECTION.
               01 NUM1     PIC 9(9) VALUE 90.
               01 NUM2     PIC 9(9) VALUE 10.
               01 NUMA     PIC 9(9) VALUE 100.
               01 NUMB     PIC 9(9) VALUE 15.
               01 NUMC     PIC 9(9).
               01 RES-DIV  PIC 9(9).
               01 RES-MULT PIC 9(9).
               01 RES-SUB  PIC 9(9).
               01 RES-ADD  PIC 9(9).
               01 RES-MOV  PIC X(9).
              LINKAGE SECTION. 
               01 IN-OUT-PARMS.
                 03 PARM-A   PIC 9(9).
                 03 PARM-B   PIC 9(9).
                 03 RESULT   PIC 9(9).
            PROCEDURE DIVISION USING IN-OUT-PARMS.
      *      divide numa by numb and store result in res-div
             DIVIDE NUMA BY NUMB GIVING RES-DIV.
      *      multiply numa by numb storing result in res-mult
             MULTIPLY NUMA BY NUMB GIVING RES-MULT.
      *      subtract numa from numb store result in res-sub
             SUBTRACT NUMA FROM NUMB GIVING RES-SUB.
      *      add numa to numb and store result in res-add
             ADD NUMA TO NUMB GIVING RES-ADD.
      *      the pointer from numa to res-mov
             MOVE NUMA TO RES-MOV.
      *      reinitilize num1
             INITIALIZE NUM1.
      *      reinitilize num2 but replace numeric data with 12345
             INITIALIZE NUM2 REPLACING NUMERIC DATA BY 12345.
             DISPLAY "RES-DIV:"  RES-DIV
             DISPLAY "RES-MULT:" RES-MULT
             DISPLAY "RES-SUB:"  RES-SUB
             DISPLAY "RES-ADD:"  RES-ADD
             DISPLAY "RES-MOV:"  RES-MOV
             DISPLAY "REINITIALIZED NUM1: " NUM1
             DISPLAY "REINITIALIZED NUM2: " NUM2
      *      perform compute on received values
             COMPUTE RESULT = PARM-A/PARM-B;
             DISPLAY "RESULT:" RESULT

             GOBACK.