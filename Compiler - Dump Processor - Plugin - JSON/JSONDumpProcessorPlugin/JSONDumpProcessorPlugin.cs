﻿using Newtonsoft.Json;
using RainCode.Core.Plugin;
using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Debug;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

[assembly: PluginProvider(typeof(JSONDumpProcessorPlugin.JSONDumpProcessorPlugin), "RegisterDumper")]
//Registering the custom dump processor with Base dump processor so that we can expect the dump in custom(JSON) format

namespace JSONDumpProcessorPlugin
{
    class JSONDumpProcessorPlugin : BaseDumpProcessor
    {
        const string FILENAME = @"..\..\..\dump.json";
        Dump dump = new Dump();
        List<Entry> entries = new List<Entry>();
        List<Module> moduleList = new List<Module>();

        public static void RegisterDumper()
        {
            ConstructorPlugin.Provide(10, Start);
        }

        public static JSONDumpProcessorPlugin Start(ExecutionContext ctx)
        {
            return new JSONDumpProcessorPlugin();
        }
        public override void End()
        {
            dump.Rununit.ModuleList = moduleList;
            dump.Callstack.Entries = entries;
            Rootobject rootobject = new Rootobject
            {
                Dump = dump
            };

            if (File.Exists(FILENAME))
            {
                File.Delete(FILENAME);
            }
            using (var streamWriter = new StreamWriter(FILENAME, true))
            {
                streamWriter.WriteLine(JsonConvert.SerializeObject(rootobject, Formatting.Indented));
                streamWriter.Close();
            }
        }
        public override void Exception(Exception e)
        {
            dump.Exception = e.Message;
        }
        public override void CurrentStatement(string position)
        {
            dump.Position = position;
        }
        public override void StartCallStack(int stackSize)
        {
            dump.Callstack = new Callstack
            {
                Depth = stackSize
            };
        }
        public override void CallStackEntry(RainCodeLegacyRuntime.Module.Module module, string position)
        {
            Entry entry = new Entry
            {
                Module = module.Name,
                Position = position
            };
            entries.Add(entry);
        }
        public override void StartRunUnit(int moduleCount)
        {
            dump.Rununit = new Rununit
            {
                ModuleCount = moduleCount
            };
        }
        public override void StartModule(RainCodeLegacyRuntime.Module.Module module)
        {
            Module moduleLocal = new Module
            {
                VariableList = new List<Variable>(),
                Assembly = module.GetType().Assembly.FullName,
                File = module.FileName,
                Name = module.Name,
                Lang = module.Language.ToString()
            };
            moduleList.Add(moduleLocal);
        }
        public override void Scalar(ScalarData s)
        {
            Variable variable = new Variable
            {
                HexValue = s.HexValue,
                TextValue = s.TextValue.Trim(),
                Type = s.Type,
                Name = s.Name,
            };
            moduleList.Last().VariableList.Add(variable);
        }
        public override void Level88(ScalarData s)
        {
            Variable variable = new Variable
            {
                HexValue = s.HexValue,
                TextValue = s.TextValue.Trim(),
                Type = s.Type,
                Name = s.Name,
            };
            moduleList.Last().VariableList.Add(variable);
        }

    }

    public class Rootobject
   {
       public Dump Dump { get; set; }
    }

    public class Dump
    {
        public string Exception { get; set; }
        public string Position { get; set; }
        public Callstack Callstack { get; set; }
        public Rununit Rununit { get; set; }
    }

    public class Callstack
    {
        public int Depth { get; set; }
        public List<Entry> Entries { get; set; }
    }

    public class Entry
    {
        public string Module { get; set; }
        public string Position { get; set; }
    }

    public class Rununit
    {
        public int ModuleCount { get; set; }
        public List<Module> ModuleList { get; set; }

    }

    public class Module
    {
        public string Name { get; set; }
        public string Lang { get; set; }
        public string File { get; set; }
        public string Assembly { get; set; }
        public List<Variable> VariableList { get; set; }
    }

    public class Variable
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string TextValue { get; set; }
        public string HexValue { get; set; }

    }

}
