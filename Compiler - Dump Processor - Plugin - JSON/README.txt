
**************************************************************
*                                                            *
*   Compiler Demo : Dump Processor                           *
*                   rclrun with plugin implementation        *
*                                                            *
**************************************************************

PURPOSE
  . Install a dump processor based on the abstract BaseDumpProcessor class.
  . The Dump Processor will produce a formatted report in JSON when a program crashes.

HOW TO RUN
  . Start the Raincode debugger.
  . The execution will hit a breakpoint in hello.cob caused by a DivideByZeroException in the called module verbs.cob.
  . Press continue.
  . Close the application window to exit the debugger.

EXAMINING THE RESULT
  . Open the dump.json solution object.
  . The json contains the working storage of the two COBOL programs involved at the time of the crash.