﻿using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions;
using RainCodeLegacyRuntime.Exceptions.QIX;
using RainCodeLegacyRuntime.Module;
using RainCodeLegacyRuntime.QIX.Interface;
using RainCodeLegacyRuntime.Sql.SqlServer;
using System;

namespace RainCode.Samples
{
    class main
    {
        static void Main(string[] args)
        {
            var ecargs = new ExecutionContextArgs()
            {
                QixMode = true,
            };
            var ec = new ExecutionContext(ecargs);
            ec.SqlRuntime = new SqlServerRuntime(ec, GetConnectionString());
            ec.SqlRuntime.ExecutionStrategy = RainCodeLegacyRuntime.Sql.SqlRuntime.ExecutionModeStrategy.DynamicOnly;
            ec.IOOperation.ConfigureSystemDatasets();
            var link = ec.QixInterface.Factory.CreateLink();
            link.Context = ec;
            link.PROGRAM = "PLI001";
            var CommArea = ec.Allocate(129);
            RainCodeLegacyRuntime.Types.Convert.move_string_to_char_string(ec, String.Format("{0,56}", "BE00-1000-0000-1438 BE00-1000-0000-2643 99999Program1.cs"), ref CommArea);
            var b1 = new Byte[5] { 0x00, 0x00, 0x00, 0x00, 0x1c };
            Array.Copy(b1, 0, CommArea.Asp.Mem, CommArea.Ofs + 40, 5);
            link.COMMAREA = new QixPointer(CommArea);
            try
            {
                ec.QixInterface.Operation.Link(link);
                ec.SqlRuntime.commit();
            }
            catch (AbendException e)
            {
                Console.WriteLine("Abend {0} detected", e.Message);
            }
            Environment.Exit(ec.ReturnCode);
        }
        #region connectionstring
        private static string GetConnectionString()
        {
            string NightbuildConnectionString = "$SQLSERVERCONNECTIONSTRING$"; //Automated test framework substitution
            string Rc360ConnectionString = Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog=BankDemo;";
            return !NightbuildConnectionString.Contains("$SQLSERVERCONNECTIONSTRING") ? NightbuildConnectionString : Rc360ConnectionString;
        }
        #endregion connectionstring
    }
}
