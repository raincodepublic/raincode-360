namespace RainCode.Samples
{
    using RainCodeLegacyRuntime.Core;
    using RainCodeLegacyRuntime.Descriptor;
    using RainCodeLegacyRuntime.Types;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Generated on 02/06/2021 at 16:15:28
    /// by Raincode PL/1 compiler version 4.1.185.0 (Build nb_v4.1.184.0-5-ge7a4565460)
    /// with arguments -HelperClassName=RainCode.Samples.CommArea -SQL=sqlserver -HelperStructure=ca -IncludeSearchPath=.\legacy -HelperClassOutputDir=.\main2 .\legacy\pli001.pli
    ///
    /// Input program path: 'C:\repositories\raincode-360\Qix - No Emulator - Csharp Main\legacy\pli001.pli'
    /// Structure was originally in files:
    ///   pli001.pli
    ///   pli001.plinc
    /// </summary>
    public partial class CommArea : BaseHelper, System.IDisposable
    {
        public const int StructureSize = 129;

        public CommArea(ExecutionContext ctxt, MemoryArea mem)
            : base(ctxt, mem)
        {
        }

        public override int Size { get { return StructureSize; } }

        public CommArea(ExecutionContext ctxt, TypedLValue lv)
            : base(ctxt, lv.BitString.MemoryArea)
        {
        }

        public static CommArea StackAllocate(ExecutionContext ctxt)
        {
            return new CommArea(ctxt, ctxt.StackAllocate(StructureSize));
        }

        private bool disposable = false;
        public static CommArea Allocate(ExecutionContext ctxt)
        {
            CommArea result = new CommArea(ctxt, ctxt.Allocate(StructureSize));
            result.disposable = true;
            return result;
        }

        public void Deallocate()
        {
            if (disposable)
            {
                theContext.Deallocate(theMemoryArea);
                disposable = false;
            }
        }

        public void Dispose()
        {
            Deallocate();
        }

        /// <summary>
        /// name:   P001_ACCOUNTNUMBER_FROM
        /// type:   string
        /// offset: 0
        /// size:   20
        /// </summary> 
        public string P001_ACCOUNTNUMBER_FROM
        {
            get
            {
                return Convert.cnv_char_string_to_string(theContext, theMemoryArea.Substr(0, 20));
            }
            set
            {
                Convert.move_string_to_char_string(theContext, value, theMemoryArea.Substr(0, 20));
            }
        }

        private TypedLValue theCachedP001_ACCOUNTNUMBER_FROM_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_ACCOUNTNUMBER_FROM_TypedLValue
        {
            get
            {
                if (theCachedP001_ACCOUNTNUMBER_FROM_TypedLValue.IsNull)
                {
                    theCachedP001_ACCOUNTNUMBER_FROM_TypedLValue = new TypedLValue(theMemoryArea.Substr(0, 20), CharacterStringType.Get(20,0,false));
                }
                return theCachedP001_ACCOUNTNUMBER_FROM_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_ACCOUNTNUMBER_TO
        /// type:   string
        /// offset: 20
        /// size:   20
        /// </summary> 
        public string P001_ACCOUNTNUMBER_TO
        {
            get
            {
                return Convert.cnv_char_string_to_string(theContext, theMemoryArea.Substr(20, 20));
            }
            set
            {
                Convert.move_string_to_char_string(theContext, value, theMemoryArea.Substr(20, 20));
            }
        }

        private TypedLValue theCachedP001_ACCOUNTNUMBER_TO_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_ACCOUNTNUMBER_TO_TypedLValue
        {
            get
            {
                if (theCachedP001_ACCOUNTNUMBER_TO_TypedLValue.IsNull)
                {
                    theCachedP001_ACCOUNTNUMBER_TO_TypedLValue = new TypedLValue(theMemoryArea.Substr(20, 20), CharacterStringType.Get(20,0,false));
                }
                return theCachedP001_ACCOUNTNUMBER_TO_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_AMOUNT
        /// type:   System.Decimal
        /// offset: 40
        /// size:   5
        /// </summary> 
        public System.Decimal P001_AMOUNT
        {
            get
            {
                return Convert.cnv_fixed_dec_to_decimal(theContext, theMemoryArea.Substr(40, 5), FixedDecimalDescriptor.Get(9,2,FixedDecimalDescriptor.Storage_BCD_PLI()));
            }
            set
            {
                Convert.move_decimal_to_fixed_dec(theContext, value, theMemoryArea.Substr(40, 5), FixedDecimalDescriptor.Get(9,2,FixedDecimalDescriptor.Storage_BCD_PLI()));
            }
        }

        private TypedLValue theCachedP001_AMOUNT_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_AMOUNT_TypedLValue
        {
            get
            {
                if (theCachedP001_AMOUNT_TypedLValue.IsNull)
                {
                    theCachedP001_AMOUNT_TypedLValue = new TypedLValue(theMemoryArea.Substr(40, 5), FixedDecimalDescriptor.Get(9,2,FixedDecimalDescriptor.Storage_BCD_PLI()));
                }
                return theCachedP001_AMOUNT_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_DESCRIPTIONTEXT
        /// type:   string
        /// offset: 45
        /// size:   40
        /// </summary> 
        public string P001_DESCRIPTIONTEXT
        {
            get
            {
                return Convert.cnv_char_string_to_string(theContext, theMemoryArea.Substr(45, 40));
            }
            set
            {
                Convert.move_string_to_char_string(theContext, value, theMemoryArea.Substr(45, 40));
            }
        }

        private TypedLValue theCachedP001_DESCRIPTIONTEXT_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_DESCRIPTIONTEXT_TypedLValue
        {
            get
            {
                if (theCachedP001_DESCRIPTIONTEXT_TypedLValue.IsNull)
                {
                    theCachedP001_DESCRIPTIONTEXT_TypedLValue = new TypedLValue(theMemoryArea.Substr(45, 40), CharacterStringType.Get(40,0,false));
                }
                return theCachedP001_DESCRIPTIONTEXT_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_SQLCODE
        /// type:   int
        /// offset: 85
        /// size:   4
        /// </summary> 
        public int P001_SQLCODE
        {
            get
            {
                return Convert.cnv_fixed_bin_to_int32(theContext, theMemoryArea.Substr(85, 4), FixedBinaryDescriptor.Get(31,0,true));
            }
            set
            {
                Convert.move_int32_to_fixed_bin(theContext, value, theMemoryArea.Substr(85, 4), FixedBinaryDescriptor.Get(31,0,true));
            }
        }

        private TypedLValue theCachedP001_SQLCODE_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_SQLCODE_TypedLValue
        {
            get
            {
                if (theCachedP001_SQLCODE_TypedLValue.IsNull)
                {
                    theCachedP001_SQLCODE_TypedLValue = new TypedLValue(theMemoryArea.Substr(85, 4), FixedBinaryDescriptor.Get(31,0,true));
                }
                return theCachedP001_SQLCODE_TypedLValue;
            }
        }

        /// <summary>
        /// name:   P001_ERRTXT
        /// type:   string
        /// offset: 89
        /// size:   40
        /// </summary> 
        public string P001_ERRTXT
        {
            get
            {
                return Convert.cnv_char_string_to_string(theContext, theMemoryArea.Substr(89, 40));
            }
            set
            {
                Convert.move_string_to_char_string(theContext, value, theMemoryArea.Substr(89, 40));
            }
        }

        private TypedLValue theCachedP001_ERRTXT_TypedLValue = TypedLValue.Null;
        public TypedLValue P001_ERRTXT_TypedLValue
        {
            get
            {
                if (theCachedP001_ERRTXT_TypedLValue.IsNull)
                {
                    theCachedP001_ERRTXT_TypedLValue = new TypedLValue(theMemoryArea.Substr(89, 40), CharacterStringType.Get(40,0,false));
                }
                return theCachedP001_ERRTXT_TypedLValue;
            }
        }


        private static ITypeDescriptor theCachedDesc = null;

        public static ITypeDescriptor GetStaticDescriptor()
        {

            if (theCachedDesc == null)
            {

                theCachedDesc =
                    StructTypeDescriptor.Get(new FieldDescriptor[] {
                            new FieldDescriptor ("p001_accountnumber_from",0,CharacterStringType.Get(20,0,false)),
                            new FieldDescriptor ("p001_accountnumber_to",20,CharacterStringType.Get(20,0,false)),
                            new FieldDescriptor ("p001_amount",40,FixedDecimalDescriptor.Get(9,2,FixedDecimalDescriptor.Storage_BCD_PLI())),
                            new FieldDescriptor ("p001_descriptiontext",45,CharacterStringType.Get(40,0,false)),
                            new FieldDescriptor ("p001_sqlcode",85,FixedBinaryDescriptor.Get(31,0,true)),
                            new FieldDescriptor ("p001_errtxt",89,CharacterStringType.Get(40,0,false))
                    });
            }
            return theCachedDesc;
        }

        public override ITypeDescriptor Descriptor { get { return GetStaticDescriptor(); } }

    }

}
