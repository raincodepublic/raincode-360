﻿using RainCodeLegacyRuntime.Core;
using RainCodeLegacyRuntime.Exceptions;
using RainCodeLegacyRuntime.Exceptions.QIX;
using RainCodeLegacyRuntime.Module;
using RainCodeLegacyRuntime.QIX.Interface;
using RainCodeLegacyRuntime.Sql.SqlServer;
using System;

namespace RainCode.Samples
{
    class main2
    {
        static void Main(string[] args)
        {
            var ecargs = new ExecutionContextArgs()
            {
                QixMode = true,
            };
            var ec = new ExecutionContext(ecargs);
            ec.SqlRuntime = new SqlServerRuntime(ec, GetConnectionString());
            ec.SqlRuntime.ExecutionStrategy = RainCodeLegacyRuntime.Sql.SqlRuntime.ExecutionModeStrategy.DynamicOnly;
            ec.IOOperation.ConfigureSystemDatasets();
            var link = ec.QixInterface.Factory.CreateLink();
            link.Context = ec;
            link.PROGRAM = "PLI001";
            var ca = ec.Allocate(129);

            CommArea commArea = new CommArea(ec, ca);
            commArea.P001_ACCOUNTNUMBER_FROM = "BE00-1000-0000-1438";
            commArea.P001_ACCOUNTNUMBER_TO = "BE00-1000-0000-2643 ";
            commArea.P001_AMOUNT = 0.01M;
            commArea.P001_DESCRIPTIONTEXT = "Program2.cs";

            link.COMMAREA = new QixPointer(ca);

            try
            {
                ec.QixInterface.Operation.Link(link);
                ec.SqlRuntime.commit();
            }
            catch (AbendException e)
            {
                Console.WriteLine("Abend {0} detected", e.Message);
            }
            Environment.Exit(ec.ReturnCode);
        }
        #region connectionstring
        private static string GetConnectionString()
        {
            string NightbuildConnectionString = "$SQLSERVERCONNECTIONSTRING$"; //Automated test framework substitution
            string Rc360ConnectionString = Environment.GetEnvironmentVariable("RC360_CONNSTRING") + "Initial Catalog=BankDemo;";
            return !NightbuildConnectionString.Contains("$SQLSERVERCONNECTIONSTRING") ? NightbuildConnectionString : Rc360ConnectionString;
        }
        #endregion connectionstring
    }
}
